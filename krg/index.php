<?php require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Welcome to KRG Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="/PCTEM/scripts/index/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="/PCTEM/scripts/index/css/style.css" rel='stylesheet' type='text/css' />
<script src="/PCTEM/scripts/index/js/jquery-1.11.1.min.js"></script>
<script src="/PCTEM/scripts/index/js/modernizr.custom.js"></script>
<link href="/PCTEM/scripts/index/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/PCTEM/scripts/index/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<script src="/PCTEM/scripts/index/js/metisMenu.min.js"></script>
<script src="/PCTEM/scripts/index/js/custom.js"></script>
<link href="/PCTEM/scripts/index/css/custom.css" rel="stylesheet">
<script src="/PCTEM/angular-1.6.9/angular.js" type='text/javascript'></script>
<link href="/PCTEM/fonts/material/material-icons.css" rel="stylesheet" type="text/css" media="all">
<link href='/PCTEM/fonts/roboto/roboto.css' rel='stylesheet'>
<link rel="shortcut icon" href="/PCTEM/scripts/images/kr.jpg" type="image/x-icon">
<style>
 .holds-the-iframe {
  background:url(/PCTEM/scripts/images/loading.gif) center center no-repeat;
 }
</style>
</head> 
<body class="cbp-spmenu-push"  ng-app="myApp" ng-controller="userCtrl">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
					<ul class="nav" id="side-menu">
                         <li>
							<a ng-click="loaddash()" class="active"><i class='material-icons'>dashboard</i>Dashboard</a>
						</li> 
					    <?php session_start(); echo $_SESSION["page_list"];  ?>
						
						
					</ul>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class='material-icons fa-bars'>menu</i></button>
				<!--toggle button end-->
				<!--logo -->
				<div class="logo">
					<a href="index.php">
						<h1>KRG</h1>
						<span>Web Portal
						<!-- <php session_start();echo $_SESSION['loggedin_time']; ?> -->
						</span>
					</a>
				</div>
			 <!--   <div class="search-box">
					
				</div>  -->
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
			<div style="color:red;align:left;"> {{login_data.company_name}}</div>
				<!-- <div class="profile_details_left">
					<ul class="nofitications-dropdown">
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class='material-icons'>markunread</i><span class="badge">{{utcount}}</span></a>
						    <ul class="dropdown-menu">
                                <li>
                                    <div class="notification_header">
                                        <h3>You have {{utcount}} new messages</h3>
                                    </div>
                                </li>
                                 <li ng-repeat="task in utask">
                                    <a href="#">
                                        <div class="user_img"><img src="" alt=""></div>
                                        <div class="notification_desc">
                                            <p>{{task.work_name}}</p>
                                            <p>Expected Date : <span>{{task.deadline_date}}</span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <div class="notification_bottom">
                                        <a href="#">See all messages</a>
                                    </div>
                                </li>
                            </ul>
						</li>
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class='material-icons'>list</i><span class="badge blue1">{{ltcount}}</span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="notification_header">
                                        <h3>You have {{ltcount}} pending task</h3>
                                    </div>
                                </li>
                                <li ng-repeat="task in ltask">
                                    <a href="#">
                                        <div class="task-info">
                                            <span class="task-desc">{{task.work_name}}</span><span class="percentage">{{task.finished_percentage}}%</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="progress progress-striped active">
                                            <div ng-class-odd="class='bar yellow'" ng-class-even="class='bar green'" style="width:{{task.finished_percentage}}%;"></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <div class="notification_bottom">
                                        <a href="#">See all pending tasks</a>
                                    </div>
                                </li>
                            </ul>
						</li>	
					</ul>
					<div class="clearfix"> </div>
				</div> -->
			
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                         <span class="prfil-img">
										<!--  <i class='material-icons' style='font-size:40px'>account_circle</i> -->
										 <img ng-src="{{login_data.image}}" alt=""> 
										 </span>
                                        <div class="user-name">
                                            <p> {{ login_data.name }} </p>
                                            <span> {{ login_data.designation }} </span>
                                        </div>
										<i class='material-icons fa fa-angle-down lnr'>arrow_drop_down</i>
										<i class='material-icons fa fa-angle-up lnr'>arrow_drop_up</i>
                                        <div class="clearfix"></div>
                                </div>
							</a>
							<ul class="dropdown-menu drp-mnu">
							<li> <a id="<?php echo $_SESSION["rolesid"];?>" href='forms.html' class='btnCustomers'><i class='material-icons'>assignment</i>Roles and Responsibilities</a> </li> 
								<li> <a id="changepwd" href="#"><i class='material-icons'>open_in_new</i>Change Password</a> </li> 
							<!-- 	<li> <a href="#"><i class='material-icons'>settings</i> Settings</a> </li> 
								<li> <a href="#"><i class='material-icons'>info</i> Profile</a> </li>  -->
								<li id="logoff"> <a href="#"><i class='material-icons'>eject</i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		</div>
        <div id="page-wrapper" height="800px">
			<div class="main-page" height="800px">
			   <!--  <div class="holds-the-iframe">
				   <iframe id="contentFrame" style="width:100%; height:auto;min-height:600px;border:none;"></iframe>
				<div> -->
				<iframe id="contentFrame" style="width:100%; height:700px;border:none;"></iframe>
		    </div>
        </div>
      	<!--footer-->
		<div class="footer">
		   <p>&copy; 2018 KRG Web Portal. All Rights Reserved</p>
		</div>
        <!--//footer-->
	</div>	
	<script src="/PCTEM/scripts/index/js/jquery.nicescroll.js"></script>
<script src="/PCTEM/scripts/index/js/scripts.js"></script>
<script src="/PCTEM/scripts/index/js/jquery.mask.js" type="text/javascript"></script>
<script src="/PCTEM/scripts/index/js/bootstrap.js"> </script>
<script src="/PCTEM/scripts/index/js/classie.js"></script>
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
   <script src="/krg/myjs/index.js"></script>
</body>
</html>