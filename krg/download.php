<?php 
 require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
 session_start();
 $filepath=encrypt_decrypt("decrypt",$_SESSION['download_path']);
 if(file_exists($filepath)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$_SESSION['download_docname'].'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filepath));
    flush(); // Flush system output buffer
    readfile($filepath);
    exit;
}
?>