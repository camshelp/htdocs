<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Poultry Receipt/Voucher Entry</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    <style media="print">
</style>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <div id="printablediv" hidden>
        <table width="80%">
        <tr height="60">
            <td width="8%"></td>
            <td width="65%"></td>
            <td width="15%"></td>
        </tr>
        <tr height="30">
            <td colspan="2">{{prno}}</td>
            <td>{{prdate}}</td>
        </tr>
        <tr height="27">
             <td></td>
            <td colspan="2">{{prhead}}</td>
        </tr>
        <tr height="27">
             <td></td>
            <td colspan="2">{{prname}}</td>
        </tr>
        <tr height="27">
             <td></td>
            <td colspan="2">{{prpurp}}</td>
        </tr>
        <tr height="27">
             <td></td>
            <td colspan="2">{{prword}}</td>
        </tr>
    </table>
    <table width="100%">
        <tr height="12">
            <td width="1%"></td>
            <td></td>
        </tr>
         <tr>
            <td></td>
            <td><h2><label>{{pramount}}</label>/-</h2></td>
        </tr>
    </table>
    </div>
    <md-card ng-show="info_show">
            <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
            <md-card-title-text>
                <span class="md-headline">Receipt & voucher Information</span>
            </md-card-title-text>
            </md-card-title>
            <md-card-actions layout="row" layout-align="start center">
                
            </md-card-actions>
            <md-card-content class="card-content">
            <div class="row">
                         <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:indigo;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">event</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Date</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);"><label>{{rdate}}<br>{{rday}}</label></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">receipt</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Receipts Entered Today</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_receipt}}</span>
                                    <a  ng-click="NewEntry('Receipt')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:#e91e63;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">explicit</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Vouchers Entered Today</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_voucher}}</span>
                                <a  ng-click="NewEntry('Voucher')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:blue;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance_wallet</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Opening Balance</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{open_bal}}</span>
                                </div>
                            </div>
                        </div>      
            </div>
            <div class="row">
            <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">payment</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Today Income</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{today_income}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:#cddc39;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">event</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Income</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{receipt_amount}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:red;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">shop</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Today Expense</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{voch_amount}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:green;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance_wallet</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Closing Balance</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{close_balance}}</span>
                                </div>
                            </div>
                        </div>      
            </div>

    <md-card>
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
	        <span class="md-headline">Receipt List</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
                    <table id="Receipt_list" class="table table-striped table-bordered" width="100%">
                      <thead>
                        <tr>
                             <th>Name</th>
                             <th>Amount</th>
                             <th>Purpose</th>
                             <th>Head Account Name</th>
                             <th class="all"></th>
                        </tr>
                      </thead>
                       <tbody>
                             <tr ng-repeat="inst in rlist" class="noright">
                                <td>{{inst.name}}</td>
                                <td>{{inst.amount}}</td>
                                <td>{{inst.purpose}}</td>
                                <td>{{inst.head_name}}</td>
                               <td>  
                                  <a ng-click="print_data('Receipt',inst.id)" class="waves-effect btn purple" name="action">Print<i class="material-icons">print</i></a>
                                  <md-button ng-click="EditInfo('Receipt',inst.id)" class="waves-effect btn blue material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                                  <md-button ng-click="RemoveInfo('Receipt',inst.id)" class="waves-effect btn pink material-icons"><i class="material-icons">clear</i> Remove</md-button>
                               </td>
                            </tr>
                      </tbody>
                    </table>
        </md-card-content>
    </md-card>
    <md-card>
        <md-card-title   class="card-content white-text" style="background-color:#e91e63;">
          <md-card-title-text>
	        <span class="md-headline">Voucher List</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <table id="Voucher_list" class="table table-striped table-bordered" width="100%">
                      <thead>
                        <tr>
                             <th>Name</th>
                             <th>Amount</th>
                             <th>Purpose</th>
                             <th>Head Account Name</th>
                             <th class="all"></th>
                        </tr>
                      </thead>
                       <tbody>
                             <tr ng-repeat="inst in vlist" class="noright">
                                <td>{{inst.name}}</td>
                                <td>{{inst.amount}}</td>
                                <td>{{inst.purpose}}</td>
                                <td>{{inst.head_name}}</td>
                                 <td>  
                                  <a ng-click="print_data('Voucher',inst.id)" class="waves-effect btn purple" name="action">Print<i class="material-icons">print</i></a>
                                  <md-button ng-click="EditInfo('Voucher',inst.id)" class="waves-effect btn blue material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                                  <md-button ng-click="RemoveInfo('Voucher',inst.id)" class="waves-effect btn pink material-icons"><i class="material-icons">clear</i> Remove</md-button>
                               </td>
                            </tr>
                      </tbody>
                    </table>
        </md-card-content>
    </md-card>

            </md-card-content>
    </md-card>
    <md-card ng-show="entry_show">
                <md-card-title   class="card-content white-text" style="background-color:purple;">
                <md-card-title-text>
                     <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
                    <span class="md-headline">Receipt and voucher Entry</span>
                </md-card-title-text>
                </md-card-title>
                <md-card-actions layout="row" layout-align="start center">
                </md-card-actions>
                <md-card-content class="card-content">
                <form enctype="multipart/form-data" method="post" id="rvregister" name="rvregister" >
                    <div class="row">
                        <div class="col-sm-6">
                                 <md-input-container  class="md-block">
                                        <label>Date</label>
                                        <input name="rdate" id="rdate" ng-model="rdate" readonly required md-maxlength="10" minlength="4">
                                        <div ng-messages="rvregister.rdate.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                        <div class="col-sm-6">
                                     <md-input-container  class="md-block">
                                        <label>Day</label>
                                        <input name="rday" id="rday" ng-model="rday" readonly required md-maxlength="10" minlength="4">
                                        <div ng-messages="rvregister.rday.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                             <div>Type</div>
                             <div>
                                         <md-radio-group ng-model="rtype" ng-required="true" ng-disabled="true" layout="row">
                                            <md-radio-button value="Receipt">Receipt</md-radio-button> 
                                            <md-radio-button value="Voucher">Voucher</md-radio-button> 
                                        </md-radio-group>
                                        <input type="text" ng-model="rtype" name="rtype" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                        <div ng-messages="rvregister.rtype.$error">
                                            <div ng-message="required">Type is required.</div>
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                        <label>Receipt/Voucher No.</label>
                                        <input name="rno" id="rno" ng-model="rno" readonly md-maxlength="20" minlength="1">
                                        <div ng-messages="rvregister.rno.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                    </div>
                    <div class="row">
                    <md-input-container>
                                <label>Account Head Name</label>
                                <md-select ng-model="hname" id="hname" name="hname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Head Name.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.head_id" ng-model="hname" ng-repeat="inst in head_list | filter:{category_name:rtype,searchTerm}">{{inst.head_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                        <label>Name</label>
                                        <input name="rname" id="rname" ng-model="rname" required md-maxlength="180" minlength="4">
                                        <div ng-messages="rvregister.rname.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                        <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                        <label>Purpose</label>
                                        <input name="rpurp" id="rpurp" ng-model="rpurp" required md-maxlength="280" minlength="4">
                                        <div ng-messages="rvregister.rpurp.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                        <label>Amount</label>
                                        <input name="ramount" id="ramount" ng-model="ramount" required md-maxlength="20" minlength="1">
                                        <div ng-messages="rvregister.ramount.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                        <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                        <label>Amount in Words</label>
                                        <input name="rword" id="rword" readonly ng-model="rword" required md-maxlength="480" minlength="0">
                                        <div ng-messages="rvregister.rword.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                        </div>
                    </div>
                    <div class="row">
                        <div style="text-align:left;" class="col-sm-6">  <a ng-click="print_data(rtype,rno)" ng-show="bvisi" class="waves-effect btn purple" name="action">Print<i class="material-icons">print</i></a></div> 
                        <div style="text-align:right;" class="col-sm-6"> <button class="waves-effect btn purple" type="submit" name="action">Submit<i class="material-icons">send</i></button></div>
                    </div>
                </md-card-content>
            </md-card>
            <script src="../myjs/poultry/receipt.js"></script>
</body>
</html>