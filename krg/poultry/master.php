<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Institution Registration</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 
        <md-tabs md-dynamic-height md-border-bottom>
            <md-tab>
                 <md-tab-label>Account Category</md-tab-label>
                 <md-tab-body>
                 <md-card>
                    <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
                    <md-card-title-text>
                        <span class="md-headline">Account Category</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                    </md-card-actions>
                    <md-card-content class="card-content">
                        <form enctype="multipart/form-data" method="post" id="acategory" name="acategory1" >
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <md-input-container  class="md-block">
                                                <label>Account Category</label>
                                                <input name="accate" id="accate" ng-model="accate" required md-maxlength="130" minlength="4">
                                                <div ng-messages="acategory1.accate.$error">
                                                    <div ng-message="required">This is required!</div>
                                                    <div ng-message="md-maxlength">That's too long!</div>
                                                    <div ng-message="minlength">That's too short!</div>
                                                </div>
                                            </md-input-container>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="waves-effect btn pink" type="submit" name="action">Submit<i class="material-icons">send</i></button>
                                        </div>
                                    </div>
                        </form>
                        <table id="acate_list" width="100%" class="collection table table-striped"> 
                                <thead> 
                                    <tr>
                                        <th>Account Category Name</th>
                                       <!--  <td></td> -->
                                    </tr>
                                </thead>
                                    <tbody>
                                        <tr ng-repeat="inst in cate_list" class="collection-item avatar">
                                            <td>{{ inst.category_name }}</td>
                                           <!--  <td> 
                                               <md-button ng-click="editUser(inst.group_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button> 
                                            </td> -->
                                        </tr>
                                </tbody>
                        </table>
                            </md-card-content>
                </md-card>
                 </md-tab-body>
            </md-tab>      
            <md-tab>
                <md-tab-label>Account Head</md-tab-label>
                <md-tab-body>
                    <md-card>
                    <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
                    <md-card-title-text>
                        <span class="md-headline">Account Head</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                    </md-card-actions>
                    <md-card-content class="card-content">
                        <form enctype="multipart/form-data" method="post" id="acregister" name="acregister1" >
                            <div class="row">
                                <div class="col-sm-6">
                                    <md-input-container>
                                    <label>Account Category</label>
                                    <md-select ng-model="cname" id="cname" name="cname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Category Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.category_id" ng-model="cname" ng-repeat="inst in cate_list | filter:searchTerm">{{inst.category_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                    </md-input-container>
                                </div>
                                <div class="col-sm-6">
                                    <md-input-container  class="md-block">
                                        <label>Account Name</label>
                                        <input name="acname" id="acname" ng-model="acname" required md-maxlength="130" minlength="4">
                                        <div ng-messages="acregister1.acname.$error">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                                </div>
                                <div class="col-sm-10" style="text-align:right;">
                                <button class="waves-effect btn pink" type="submit" name="action">Submit<i class="material-icons">send</i></button>
                                </div>
                            </div>
                        </form>
                        <table id="achead_list" width="100%" class="collection table table-striped"> 
                                <thead> 
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Account Name</th>
                                       <!--  <td></td> -->
                                    </tr>
                                </thead>
                                    <tbody>
                                        <tr ng-repeat="inst in head_list" class="collection-item avatar">
                                            <td>{{ inst.category_name }}</td>
                                            <td>{{ inst.head_name }}</td>
                                           <!--  <td> 
                                               <md-button ng-click="editUser(inst.group_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button> 
                                            </td> -->
                                        </tr>
                                </tbody>
                        </table>
                    </md-card-content>
            </md-card>
                </md-tab-body>
            </md-tab>
            <md-tab>
                 <md-tab-label>Material List</md-tab-label>
                 <md-tab-body>
              
                 </md-tab-body>
            </md-tab>       
            <md-tab>
                 <md-tab-label>Vendor List</md-tab-label>
                 <md-tab-body>
              
                 </md-tab-body>
            </md-tab>   
            <md-tab>
                 <md-tab-label>Broker List</md-tab-label>
                 <md-tab-body>
              
                 </md-tab-body>
            </md-tab>            
        </md-tabs>            
<script src="../myjs/poultry/master.js"></script>
</body>
</html>