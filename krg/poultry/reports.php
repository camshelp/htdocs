<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Institution Registration</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

        <md-card>
                    <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
                    <md-card-title-text>
                        <span class="md-headline">Reports</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                    </md-card-actions>
                    <md-card-content class="card-content">
<div class="row">
    <div class="col-sm-3">Starting Date</div>
    <div class="col-sm-3" flex-gt-xs>
        <div class="input-group date form_date"  data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input1" data-link-format="dd-mm-yyyy">
            <input class="form-control" size="16" ng-model="sdate" type="text" value="" readonly>
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
        </div>
        <input type="hidden" ng-model="sdate" name="sdate" id="dtp_input1" value="" />
    </div>
    <div class="col-sm-3">Ending Date</div>
    <div class="col-sm-3">
      <div class="input-group date form_date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input class="form-control" size="16" ng-model="edate" type="text" value="" readonly>
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
        </div>
        <input type="hidden" ng-model="edate" name="edate" id="dtp_input2" value="" />
    </div>
</div>
<div class="row">
        <md-input-container>        
            <label>Account Category</label>
            <md-select ng-model="selectedcategory" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" multiple>
            <md-select-header class="demo-select-header">
                <input ng-model="searchTerm" placeholder="Search for Account Category Name.." class="demo-header-searchbox md-text" type="search">
            </md-select-header>
            <md-optgroup label="vegetables">
                <md-option ng-value="inst.category_id" ng-model="acate" ng-click="option_click(inst.category_id)" ng-repeat="inst in hcatlist | filter:searchTerm">{{inst.category_name}}</md-option>
            </md-optgroup>
            </md-select>
        </md-input-container>
</div>
<div class="row">
        <md-input-container>        
            <label>Account Head Name</label>
            <md-select ng-model="selected" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader"  multiple>
            <md-select-header class="demo-select-header">
                <input ng-model="searchTerm" placeholder="Search for Account Head Name.." class="demo-header-searchbox md-text" type="search">
            </md-select-header>
            <md-optgroup label="vegetables">
                <md-option ng-value="inst.head_id" ng-model="ahead" ng-repeat="inst in hlist1 | filter:searchTerm">{{inst.head_name}}</md-option>
            </md-optgroup>
            </md-select>
        </md-input-container>
</div>                                
<div class="row">
    <div class="col-sm-6">
    <md-card>
                    <md-card-title   class="card-content white-text" style="background-color:purple;">
                    <md-card-title-text>
                        <span class="md-headline">DaySheet</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                    </md-card-actions>
                    <md-card-content class="card-content">
                        <a ng-click="daysheet()"><i class="material-icons" style="font-size:20px;">view_day</i>DaySheet</a>
                    </md-card-content>
            </md-card>
    </div>
    <div class="col-sm-6">
    <md-card>
                    <md-card-title   class="card-content white-text" style="background-color:green;">
                    <md-card-title-text>
                        <span class="md-headline">Income/Voucher</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                    </md-card-actions>
                    <md-card-content class="card-content">
                    <div> <a ng-click="summary('Receipt')"><i class="material-icons" style="font-size:20px;">assessment</i>Income Summary</a>
                    </div>
                    <div> <a ng-click="summary('Voucher')"><i class="material-icons" style="font-size:20px;">explicit</i>Expense Summary</a>
                    </div>
                    </md-card-content>
            </md-card>
    </div>
</div>
<div class="row">
    <div class="col-sm-6"></div>
    <div class="col-sm-6"></div>
</div>
<div class="row">
    <div class="col-sm-6"></div>
    <div class="col-sm-6"></div>
</div>
  
</md-card-content>
</md-card>
<script src="../myjs/poultry/report.js"></script>
</body>
</html>