 <?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Daily Production Information</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
	
	<!-- MyJs 
	<script src="../myjs/krcepl/plf.js"></script> -->
	<script src="../myjs/krcepl/production.js"></script>


</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 


<form action="" method="post"> 
  <div class="container">  
   <div class="table-responsive">  
    <div class="row"><div class="col-sm-6"><h2 align="center">Daily Production Update</h2></div><div class="col-sm-3"></div>
	<div class="col-sm-3"><br><input class="btn btn-lg btn-success" type="submit" name="justsubmit" id="justsubmit" value="Submit"/>
	</div></div><br>
	<div>
	<input type="hidden" id="nrows" name="nrows" value="<?php echo $n; ?>">
	<input type="hidden" id="academic_year" name="academic_year" value="<?php echo $academic_year; ?>">
	<input type="hidden" id="date" name="date" value="<?php echo $date; ?>">
	<input type="hidden" id="unit_id" name="unit_id" value="<?php echo $unit_id; ?>">
	<input type="hidden" id="location_id" name="location_id" value="<?php echo $location_id; ?>">
	<input type="hidden" id="division_id" name="division_id" value="<?php echo $division_id; ?>">
    <table class="table table-bordered">
    <tr>  
   <th>Machine Name</th>
   <th>Machine Hour</th>
   <th>Machine Minute</th>
   <th>Machine Breakdown Hour</th>
   <th>Machine Reason</th>
   <th>Machine Available</th>
   <th>Grid Hour</th>
   <th>Grid Minute</th>
   <th>Grid Breakdown Hour</th>
   <th>Reason</th>
   <th>Grid Available</th>
   <th>Production</th>
   <th>*****Plf*****</th>
   <th>Generation Hour</th>
   <th>Generation Minute</th>
   
    </tr>
     <?php
	 $r=1;
     while($row = mysqli_fetch_assoc($result))  
     {
        ?>  
		<input type="hidden" id="machine_id/<?php echo $r; ?>" name="machine_id/<?php echo $r; ?>" value="<?php echo $row["machine_id"];?>">
       <tr> 
	   
         
		 <td><input type="varchar" name="machine_name/<?php echo $r; ?>" ng-model="machinename" ng-init="machinename ='Allwin'" disabled></td>
	     <td><input type="number" name="machine_hour/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="machine_min/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="machine_breakdown/<?php echo $r; ?>"/></td>
		 <td><input type="varchar" name="machine_reason/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="machine_available/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="grid_hour/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="grid_min/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="grid_breakdown/<?php echo $r; ?>"/></td>
		 <td><input type="varchar" name="reason/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="grid_available/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="production/<?php echo $r; ?>" ng-model="production" ng-change="myFuncPlf()"/></td>
		 <td><input type="number" name="plf/<?php echo $r; ?>" value="{{valplf | number:2}}"/></td>
		 <td><input type="number" name="generation_hour/<?php echo $r; ?>"/></td>
		 <td><input type="number" name="generation_min/<?php echo $r; ?>"/></td>
       </tr>  
        <?php 
		$r=$r+1;
     }?>
 

    </table>
	</div>	
    <br />
   </div>  
  </div> 
</form>  
 </body>  
</html>
<?php
if(isset($_POST['justsubmit'])){
$n=$_POST["nrows"];	
$academic_year=$_POST["academic_year"];	
$date=$_POST["date"];	
$unit_id=$_POST["unit_id"];	
$location_id=$_POST["location_id"];	
$division_id=$_POST["division_id"];	
	$s=1;
while ($s<=$n){
$machine_name=$_POST["machine_name/$s"];
$machine_id=$_POST["machine_id/$s"];
$machine_hour=$_POST["machine_hour/$s"];
$machine_min=$_POST["machine_min/$s"];
$machine_breakdown=$_POST["machine_breakdown/$s"];
$machine_available=$_POST["machine_available/$s"];
$machine_reason=$_POST["machine_reason/$s"];
$grid_hour=$_POST["grid_hour/$s"];
$grid_min=$_POST["grid_min/$s"];
$grid_breakdown=$_POST["grid_breakdown/$s"];
$grid_available=$_POST["grid_available/$s"];
$reason=$_POST["reason/$s"];
$production=$_POST["production/$s"];
$plf=$_POST["plf/$s"];
$generation_hour=$_POST["generation_hour/$s"];
$generation_min=$_POST["generation_min/$s"];
$status="yes";


$updated_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
$updated_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
$updated_time= date("Y-m-d h:i:sa");

		$sql2="INSERT INTO daily_entry VALUES ('$academic_year', '$date', '$unit_id', '$machine_id', '$location_id', '$division_id', '$machine_hour', '$machine_min', '$machine_breakdown', '$machine_reason', '$machine_available', '$grid_hour', '$grid_min', '$grid_breakdown', '$reason', '$grid_available', '$production', '$plf', '$generation_hour', '$generation_min', '$status', '$updated_by', '$updated_session', '$updated_time')";
		$result1 = mysqli_query($connect, $sql2);

		 
		
	$s=$s+1;
	echo"<script> alert('Submitted Successfully');window.location='production.php'; </script>";

}}


     ?>
	 
	 
