<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html>
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Machine Registration</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 
       <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
            <span class="md-headline">Machine List</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
            <div class="row">
                    <div class="col-sm-6">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Machines</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{mc_count}}</span>
                              <a  ng-click="editUser('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">signal_cellular_alt</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Capacity</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{tot_capa}}</span>
                             
                         </div>
                        </div>
                    </div>
            </div>
            <div class="row" style="height:30px;text-align:right;">
            <button class="waves-effect btn pink material-icons" id="export_pdf">
            <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#000000" d="M11.43,10.94C11.2,11.68 10.87,12.47 10.42,13.34C10.22,13.72 10,14.08 9.92,14.38L10.03,14.34V14.34C11.3,13.85 12.5,13.57 13.37,13.41C13.22,13.31 13.08,13.2 12.96,13.09C12.36,12.58 11.84,11.84 11.43,10.94M17.91,14.75C17.74,14.94 17.44,15.05 17,15.05C16.24,15.05 15,14.82 14,14.31C12.28,14.5 11,14.73 9.97,15.06C9.92,15.08 9.86,15.1 9.79,15.13C8.55,17.25 7.63,18.2 6.82,18.2C6.66,18.2 6.5,18.16 6.38,18.09L5.9,17.78L5.87,17.73C5.8,17.55 5.78,17.38 5.82,17.19C5.93,16.66 6.5,15.82 7.7,15.07C7.89,14.93 8.19,14.77 8.59,14.58C8.89,14.06 9.21,13.45 9.55,12.78C10.06,11.75 10.38,10.73 10.63,9.85V9.84C10.26,8.63 10.04,7.9 10.41,6.57C10.5,6.19 10.83,5.8 11.2,5.8H11.44C11.67,5.8 11.89,5.88 12.05,6.04C12.71,6.7 12.4,8.31 12.07,9.64C12.05,9.7 12.04,9.75 12.03,9.78C12.43,10.91 13,11.82 13.63,12.34C13.89,12.54 14.18,12.74 14.5,12.92C14.95,12.87 15.38,12.85 15.79,12.85C17.03,12.85 17.78,13.07 18.07,13.54C18.17,13.7 18.22,13.89 18.19,14.09C18.18,14.34 18.09,14.57 17.91,14.75M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3M17.5,14.04C17.4,13.94 17,13.69 15.6,13.69C15.53,13.69 15.46,13.69 15.37,13.79C16.1,14.11 16.81,14.3 17.27,14.3C17.34,14.3 17.4,14.29 17.46,14.28H17.5C17.55,14.26 17.58,14.25 17.59,14.15C17.57,14.12 17.55,14.08 17.5,14.04M8.33,15.5C8.12,15.62 7.95,15.73 7.85,15.81C7.14,16.46 6.69,17.12 6.64,17.5C7.09,17.35 7.68,16.69 8.33,15.5M11.35,8.59L11.4,8.55C11.47,8.23 11.5,7.95 11.56,7.73L11.59,7.57C11.69,7 11.67,6.71 11.5,6.47L11.35,6.42C11.33,6.45 11.3,6.5 11.28,6.54C11.11,6.96 11.12,7.69 11.35,8.59Z" />
            </svg>
            Export
            </button>
            </div>
			 <form method ="get" action ="insert_loc.php">  
			<div class="row">
				 <div class="col-sm-6">
				 <md-input-container class="md-block">
				 <label>New Location Name</label>
				 <input name="lcname" id="lcname" ng-model="lcname">
				 </md-input-container>
				 </div>
				  <div class="col-sm-6">
				 <md-input-container class="md-block">
				 <label>New Division Name</label>
				 <input name="dcname" id="dcname" ng-model="dcname">
				 </md-input-container>
				 </div>
				 <div class="row" style="text-align:right;padding-right:10px;">
                     <button class="waves-effect btn pink" type="submit" id = "ins" name="ins">Insert<i class="material-icons">insert</i></button> 
                        </div>
				</div> </form>
             <table id="machine_list" width="100%" class="collection table table-striped"> 
                       <thead> 
                        <tr>
                            <td>Machine Name</td>
                            <td>Unit Name</td>
                            <td>Location Name</td>
                            <td>Capacity</td>
                            <td>Insurance Date</td>
                            <td>Insurance Amount</td>
                            <td>Insurance Info</td>
                            <td>O & M Date</td>
                            <td>O & M Amount</td>
                            <td>O & M Info</td>
                            <td class="all">Action</td>
                        </tr>
                       </thead>
                        <tbody>
                            <tr ng-repeat="inst in machines_list" class="collection-item avatar">
                                <td>{{ inst.machine_name }}</td>
                                <td> {{ inst.unit_name }}</td>
                                <td> {{ inst.location_name }}</td>
                                <td>{{ inst.capacity }}</td>
                                <td>{{ inst.insurance_date }}</td>
                                <td>{{ inst.insurance_amount }}</td>
                                <td>Due Date : {{ inst.insurance_date }}<br>Amount : {{inst.insurance_amount}}</td>
                                <td>{{ inst.om_date }}</td>
                                <td>{{ inst.om_amount }}</td>
                                <td>Due Date : {{ inst.om_date }}<br>Amount : {{inst.om_amount}}</td>
                                <td> 
                                     <md-button ng-click="editUser(inst.machine_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                                </td>
                            </tr>
                       </tbody>
            </table>
        </md-card-content>
      </md-card>

    <md-card ng-show="RegisterVisible">
        <md-card-title   class="card-content white-text indigo">
          <md-card-title-text>
            <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Machine Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
             
        </md-card-actions>
        <md-card-content class="card-content">
        <md-content layout-padding>
       <!--  <form name="projectForm">
        <md-input-container class="md-block">
            <label>Description</label>
            <input md-maxlength="30" required md-no-asterisk name="description" ng-model="project.description">
            <div ng-messages="projectForm.description.$error">
            <div ng-message="required">This is required.</div>
            <div ng-message="md-maxlength">The description must be less than 30 characters long.</div>
            </div>
        </md-input-container>
        </form> -->
        <form enctype="multipart/form-data" method="post" id="MachineRegister" name="MForm" >
                <div hidden>
                  <input name="mid" id="mid" ng-model="mid">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                          <md-input-container  class="md-block">
                                    <label>Unit Name</label>
                                    <md-select ng-model="uname" id="uname" name="uname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Unit Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.unit_id" ng-model="uname" ng-repeat="inst in unit_list | filter:searchTerm">{{inst.unit_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                            </md-input-container>
                    </div>
                    <div class="col-sm-6">
                                 <md-input-container class="md-block">
                                    <label>Location Name</label>
                                    <md-select ng-model="lname" id="lname" name="lname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Location Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.location_id" ng-model="lname" ng-repeat="inst in location_list | filter:searchTerm">{{inst.location_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                </div> <div class="row">
                    <div class="col-sm-6">
                                <md-input-container class="md-block">
                                    <label>Division Name</label>
                                    <md-select ng-model="dname" id="dname" name="dname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Division Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.division_id" ng-model="lname" ng-repeat="inst in division_list | filter:searchTerm">{{inst.division_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
				
                <div class="row">
                    <div class="col-sm-6">
                    <md-input-container  class="md-block">
                            <label>Machine Name</label>
                            <input name="mname" id="mname" ng-model="mname" required md-maxlength="50" minlength="1">
                            <div ng-messages="MForm.mname.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                    <md-input-container  class="md-block">
                            <label>HTSC No.</label>
                            <input name="htsc" ng-model="htsc" required md-maxlength="60" minlength="2">
                            <div ng-messages="MForm.htsc.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" flex-gt-xs>
                            <label>Date of Functioning</label>
                            <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input1" data-link-format="dd-mm-yyyy">
                                <input ng-model="dof" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="dof" name="dof" id="dtp_input1" value="" hidden/>
                    </div>
                    <div class="col-sm-6">
                    <md-input-container class="md-block">
                            <label>Capacity in MW</label>
                            <input name="capa" id="capa" ng-model="capa" required md-maxlength="10" minlength="1">
                            <div ng-messages="MForm.capa.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" flex-gt-xs>
                            <label>Insurance Due Date</label>
                            <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                                <input ng-model="idd" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="idd" name="idd" id="dtp_input2" value="" hidden/>
                    </div>
                    <div class="col-sm-6" class="md-block">
                         <md-input-container>
                            <label>Insurance Amount</label>
                            <input name="iamount" id="iamount" ng-model="iamount" md-maxlength="10" required>
                            <div ng-messages="MForm.iamount.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" flex-gt-xs>
                        <label>O & M Due Date</label>
                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input3" data-link-format="dd-mm-yyyy">
                            <input ng-model="omd" class="form-control" size="16" type="text" value="" readonly required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input ng-model="omd" name="omd" id="dtp_input3" value="" hidden/>
                    </div>
                    <div class="col-sm-6">
                    <md-input-container class="md-block">
                            <label>O & M Amount</label>
                            <input name="oamount" id="oamount" ng-model="oamount" md-maxlength="10" required>
                            <div ng-messages="MForm.oamount.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>
                        <div class="row" style="text-align:right;padding-right:10px;">
                        <button class="waves-effect btn pink" type="submit" name="action">Submit<i class="material-icons">send</i></button>
                        </div>
                </form>
        </md-card-content>
      </md-card>
 
    <script src="/krg/myjs/krcepl/machine.js"></script>
    <script src="/krg/myjs/fileupload.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    });
    </script>
</body>
</html>
