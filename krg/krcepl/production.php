<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Daily Production Information</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
	
	<!-- MyJs 
	<script src="../myjs/krcepl/plf.js"></script> -->
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>  
	<script src="../myjs/krcepl/production.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
	


</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

<md-card ng-show="summaryVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
             <span> <a ng-click="view_summary_back()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
	        <span class="md-headline">Production Summary</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
            <div class="row">
                <div class="col-sm-2">Starting Date :</div>
                <div class="col-sm-4">{{edate}}</div>
                <div class="col-sm-2">Ending Date :</div>
                <div class="col-sm-4">{{sdate}}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">Name :</div>
                <div class="col-sm-10">{{disp_name}}</div>
            </div>
            <div>
            <table id="vlist" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all" width="30%">Name</th>
                            <th class="all">Capacity</th>
                            <th>M/c Breakdown</th>
                            <th>M/c Avl %</th>
                            <th>Grid Breakdown</th>
                            <th>Grid Avl %</th>
                            <th class="all">Total Production</th>
                            <th>PLF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in vproduction_list" class="collection-item avatar">
                            <td>{{info.name}}</td>
                            <td>{{info.capacity}}</td>
                            <td>{{info.mc_brk}}</td>
                            <td>{{info.mc_avl}}</td>
                            <td>{{info.gd_brk}}</td>
                            <td>{{info.gd_avl}}</td>
                            <td style="text-align:right;">{{info.production}}</td>
                            <td>{{info.plf}}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr >
                            <th style="text-align:center;">Total</th>
                            <th>{{vftotal.capacity}}</th>
                            <th>{{vftotal.mc_brk}}</th>
                            <th>{{vftotal.mc_avl}}</th>
                            <th>{{vftotal.gd_brk}}</th>
                            <th>{{vftotal.gd_avl}}</th>
                            <th style="text-align:right;">{{vftotal.production}}</th>
                            <th>{{vftotal.plf}}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </md-card-content>
</md-card>
   <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">Daily Production Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">receipt</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Files Uploaded</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{fcount}}</span>
                           <a  ng-click="FileUpload('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">money</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Overall Production</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{tot_prod}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: blue;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Overall Grid Breakdown</label></span>
                           <span style="display: block; font-weight: 500; font-size: 13px; color: rgb(66, 66, 66);">{{grid_break}}</span>
                         </div>
                        </div>
                    </div>
			 </div>
			 <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: indigo;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">money</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Overall Machine Breakdown</label></span>
                           <span style="display: block; font-weight: 500; font-size: 13px; color: rgb(66, 66, 66);">{{machine_break}}</span>
                         </div>
                        </div>
                    </div>
					<div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: seagreen;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">receipt</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Production Data Update</label></span>
                           <!--<span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{tot_prod}}</span>-->
						   <a  ng-click="dailyupdate('new')" style=" margin-top:-25px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>Production Data</a>
                         </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                                    <div class="col-sm-1"></div>
                                     <div class="col-sm-5" flex-gt-xs>
                                        <label>Starting Date</label>
                                        <div class="input-group date form_date" id="edate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2">
                                            <input ng-model="edate" class="form-control" size="16" type="text" value="" readonly required>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input ng-model="edate" name="edate" id="dtp_input2" value="" required hidden/>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-5" flex-gt-xs>
                                        <label>Ending Date</label>
                                        <div class="input-group date form_date" id="sdate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input1">
                                            <input ng-model="sdate" class="form-control" size="16" type="text" value="" readonly required>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input ng-model="sdate" name="sdate" id="dtp_input1" value="" required hidden/>
                                    </div>
            </div>
            <div class="row">
                <div class="col-sm-2" style="text-align:right;"><label>List Type</label></div>
                <div class="col-sm-10" style="text-align:left;">
                    <md-radio-group ng-model="type" id="type" ng-click="date_selected(sdate,edate)" ng-required="true" layout="row">
                        <md-radio-button value="Unit" class="md-primary">Unit</md-radio-button>
                        <md-radio-button value="Division">Division</md-radio-button>
                        <md-radio-button value="Location">Location</md-radio-button>
                        <md-radio-button value="Machine">Machine</md-radio-button>
                    </md-radio-group>
                    <input type="text" ng-model="type" name="type" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                    <div ng-messages="type.$error" role="alert">
                        <div ng-message="required">Type is required.</div>
                    </div>
                </div>
            </div>
            <div>
                <table id="list" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all" width="30%">Name</th>
                            <th class="all">Capacity</th>
                            <th>M/c Breakdown</th>
                            <th>M/c Avl %</th>
                            <th>Grid Breakdown</th>
                            <th>Grid Avl %</th>
                            <th class="all">Total Production</th>
                            <th>PLF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in production_list" class="collection-item avatar">
                            <td>
                                <a ng-click="view_detailed_summary(info.id,info.name)"> {{info.name}}</a>
                            </td>
                            <td>{{info.capacity}}</td>
                            <td>{{info.mc_brk}}</td>
                            <td>{{info.mc_avl}}</td>
                            <td>{{info.gd_brk}}</td>
                            <td>{{info.gd_avl}}</td>
                            <td style="text-align:right;">{{info.production}}</td>
                            <td>{{info.plf}}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr >
                            <th style="text-align:center;">Total</th>
                            <th>{{ftotal.capacity}}</th>
                            <th>{{ftotal.mc_brk}}</th>
                            <th>{{ftotal.mc_avl}}</th>
                            <th>{{ftotal.gd_brk}}</th>
                            <th>{{ftotal.gd_avl}}</th>
                            <th style="text-align:right;">{{ftotal.production}}</th>
                            <th>{{ftotal.plf}}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                         <div id="prod_chart"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="break_chart"></div>
                    </div>
                </div>
                <div class="row">
                 <div class="col-sm-6">  <h5> Date :  {{ldate}}</h5></div> 
                 <div class="col-sm-6">
                 <md-input-container>
                                    <label>Machine Name</label>
                                    <md-select ng-model="mname" id="mname" name="mname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Machine Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-click="machine_production_summary(inst.machine_id)" ng-value="inst.machine_id" ng-model="mname" ng-repeat="inst in m_list | filter:searchTerm">{{inst.machine_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                 </div>
                </div>
                <div class="row">
                    <table  width="100%" class="collection table table-striped">
                        <thead>
                            <tr>
                                <th>Unit Name</th>
                                <th>No.of Machines</th>
                                <th>Total Capacity in MW</th>
                                <th>Daily Generation</th>
                                <th>Monthly Generation</th>
                                <th>Yearly Generation</th>
                            </tr>
                        </thead>
                        </tbody>
                                <tr ng-repeat="info in last_production" class="collection-item avatar">
                                    <td>{{info.unit}}</td>
                                    <td>{{info.total_machine}}</td>
                                    <td>{{info.total_capacity}}</td>
                                    <td>{{info.daily_generation}}</td>
                                    <td>{{info.monthly_generation}}</td>
                                    <td>{{info.yearly_generation}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div ng-show="msummary">
                         <div style="text-align:center;"><h5>Production Summary of {{machine_name}} Wind Turbine</h5></div>
                        <div class="row">
                            <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">event</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Generation as on {{mc_date}}</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{md_production}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: purple;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">info</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Generation as on {{pdate}}</label></span>
                                <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{mpproduction}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: blue;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>MTD as on {{mc_date}}</label></span>
                                <span style="display: block; font-weight: 500; font-size: 13px; color: rgb(66, 66, 66);">{{mproduction}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: indigo;">
                                    <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assessment</i>
                                </span>
                                <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>YTD as on {{mc_date}}</label></span>
                                <span style="display: block; font-weight: 500; font-size: 13px; color: rgb(66, 66, 66);">{{yproduction}}</span>
                                </div>
                                </div>
                            </div>
                       </div>
                       <div class="row">
                            <div class="col-sm-3" style="text-align:center;background:#f3e5f5;padding-top:15px;height:400px;">
                               <h6> Production Month</h6>
                                <div id="pmonth"></div>
                            </div>
                            <div class="col-sm-9" style="text-align:center;background:#e3f2fd;padding-top:15px;height:400px;">
                            Production Year
                              <div id="pyear"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6" style="text-align:center;background:#e0f2f1;padding-top:15px;height:400px;">
                                Machine Availability
                                <div id="mcavl"></div>
                            </div>
                            <div class="col-sm-6" style="text-align:center;background:#fff8e1;padding-top:15px;height:400px;">
                                Grid Availability
                              <div id="gdavl"></div>
                            </div>
                        </div>
               
                </div>
            </div>
            <md-card>
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span class="md-headline">Reports</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
            <div class="col-sm-1"></div>
                                        <div class="col-sm-5" flex-gt-xs>
                                            <label>Starting Date</label>
                                            <div class="input-group date form_date" id="redate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input3">
                                                <input ng-model="redate" class="form-control" size="16" type="text" value="" readonly required>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                            <input ng-model="redate" name="redate" id="dtp_input3" value="" required hidden/>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5" flex-gt-xs>
                                            <label>Ending Date</label>
                                            <div class="input-group date form_date" id="rsdate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input4">
                                                <input ng-model="rsdate" class="form-control" size="16" type="text" value="" readonly required>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                            <input ng-model="rsdate" name="rsdate" id="dtp_input4" value="" required hidden/>
                                        </div>
                </div>
                <div class="row">
                <div class="col-sm-6">
                              <md-input-container>
                                    <label>Unit Name</label>
                                    <md-select ng-model="uname" id="uname" name="uname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required multiple>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Unit Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.unit_id" ng-model="uname" ng-repeat="inst in unit_list | filter:searchTerm">{{inst.unit_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                </div>
                <div class="col-sm-6">
                <md-input-container>        
                                <label>Financial Year</label>
                                <md-select ng-model="selectedcategory" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" multiple>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Financial Year.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.academic_year" ng-model="acate" ng-repeat="inst in fyear | filter:searchTerm">{{inst.academic_year}}</md-option>
                                </md-optgroup>
                                </md-select>
                            </md-input-container>    
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-6">
                      <md-button  type="submit" id="yearsummary" class="waves-effect btn red material-icons">
                            <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#000000" d="M11.43,10.94C11.2,11.68 10.87,12.47 10.42,13.34C10.22,13.72 10,14.08 9.92,14.38L10.03,14.34V14.34C11.3,13.85 12.5,13.57 13.37,13.41C13.22,13.31 13.08,13.2 12.96,13.09C12.36,12.58 11.84,11.84 11.43,10.94M17.91,14.75C17.74,14.94 17.44,15.05 17,15.05C16.24,15.05 15,14.82 14,14.31C12.28,14.5 11,14.73 9.97,15.06C9.92,15.08 9.86,15.1 9.79,15.13C8.55,17.25 7.63,18.2 6.82,18.2C6.66,18.2 6.5,18.16 6.38,18.09L5.9,17.78L5.87,17.73C5.8,17.55 5.78,17.38 5.82,17.19C5.93,16.66 6.5,15.82 7.7,15.07C7.89,14.93 8.19,14.77 8.59,14.58C8.89,14.06 9.21,13.45 9.55,12.78C10.06,11.75 10.38,10.73 10.63,9.85V9.84C10.26,8.63 10.04,7.9 10.41,6.57C10.5,6.19 10.83,5.8 11.2,5.8H11.44C11.67,5.8 11.89,5.88 12.05,6.04C12.71,6.7 12.4,8.31 12.07,9.64C12.05,9.7 12.04,9.75 12.03,9.78C12.43,10.91 13,11.82 13.63,12.34C13.89,12.54 14.18,12.74 14.5,12.92C14.95,12.87 15.38,12.85 15.79,12.85C17.03,12.85 17.78,13.07 18.07,13.54C18.17,13.7 18.22,13.89 18.19,14.09C18.18,14.34 18.09,14.57 17.91,14.75M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3M17.5,14.04C17.4,13.94 17,13.69 15.6,13.69C15.53,13.69 15.46,13.69 15.37,13.79C16.1,14.11 16.81,14.3 17.27,14.3C17.34,14.3 17.4,14.29 17.46,14.28H17.5C17.55,14.26 17.58,14.25 17.59,14.15C17.57,14.12 17.55,14.08 17.5,14.04M8.33,15.5C8.12,15.62 7.95,15.73 7.85,15.81C7.14,16.46 6.69,17.12 6.64,17.5C7.09,17.35 7.68,16.69 8.33,15.5M11.35,8.59L11.4,8.55C11.47,8.23 11.5,7.95 11.56,7.73L11.59,7.57C11.69,7 11.67,6.71 11.5,6.47L11.35,6.42C11.33,6.45 11.3,6.5 11.28,6.54C11.11,6.96 11.12,7.69 11.35,8.59Z" />
                            </svg>
                            Year Summary</md-button>
                        </div>
                        <div class="col-sm-6">
                            <md-button  type="submit" id="dailysummary" class="waves-effect btn red material-icons">
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#000000" d="M11.43,10.94C11.2,11.68 10.87,12.47 10.42,13.34C10.22,13.72 10,14.08 9.92,14.38L10.03,14.34V14.34C11.3,13.85 12.5,13.57 13.37,13.41C13.22,13.31 13.08,13.2 12.96,13.09C12.36,12.58 11.84,11.84 11.43,10.94M17.91,14.75C17.74,14.94 17.44,15.05 17,15.05C16.24,15.05 15,14.82 14,14.31C12.28,14.5 11,14.73 9.97,15.06C9.92,15.08 9.86,15.1 9.79,15.13C8.55,17.25 7.63,18.2 6.82,18.2C6.66,18.2 6.5,18.16 6.38,18.09L5.9,17.78L5.87,17.73C5.8,17.55 5.78,17.38 5.82,17.19C5.93,16.66 6.5,15.82 7.7,15.07C7.89,14.93 8.19,14.77 8.59,14.58C8.89,14.06 9.21,13.45 9.55,12.78C10.06,11.75 10.38,10.73 10.63,9.85V9.84C10.26,8.63 10.04,7.9 10.41,6.57C10.5,6.19 10.83,5.8 11.2,5.8H11.44C11.67,5.8 11.89,5.88 12.05,6.04C12.71,6.7 12.4,8.31 12.07,9.64C12.05,9.7 12.04,9.75 12.03,9.78C12.43,10.91 13,11.82 13.63,12.34C13.89,12.54 14.18,12.74 14.5,12.92C14.95,12.87 15.38,12.85 15.79,12.85C17.03,12.85 17.78,13.07 18.07,13.54C18.17,13.7 18.22,13.89 18.19,14.09C18.18,14.34 18.09,14.57 17.91,14.75M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3M17.5,14.04C17.4,13.94 17,13.69 15.6,13.69C15.53,13.69 15.46,13.69 15.37,13.79C16.1,14.11 16.81,14.3 17.27,14.3C17.34,14.3 17.4,14.29 17.46,14.28H17.5C17.55,14.26 17.58,14.25 17.59,14.15C17.57,14.12 17.55,14.08 17.5,14.04M8.33,15.5C8.12,15.62 7.95,15.73 7.85,15.81C7.14,16.46 6.69,17.12 6.64,17.5C7.09,17.35 7.68,16.69 8.33,15.5M11.35,8.59L11.4,8.55C11.47,8.23 11.5,7.95 11.56,7.73L11.59,7.57C11.69,7 11.67,6.71 11.5,6.47L11.35,6.42C11.33,6.45 11.3,6.5 11.28,6.54C11.11,6.96 11.12,7.69 11.35,8.59Z" />
                                </svg>
                            Daily Generation</md-button>
                        </div>
                </div>
            </md-card-content>
        </md-card>

        </md-card-content>
    </md-card>
    <md-card ng-show="Upload">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
          <span> <a ng-click="UploadSummary()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
	        <span class="md-headline">Production Data Upload</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
            <form enctype="multipart/form-data" method="post" id="UploadExcel">
                         <div class="row">
                            <div class="col-sm-2"><label>Excel Category</label></div>
                            <div class="col-sm-4">
                                <md-radio-group ng-model="etype" layout="row">
                                    <md-radio-button value="SuZlon">SuZlon</md-radio-button>
                                    <md-radio-button value="Vestas">Vestas</md-radio-button>
                                    <md-radio-button value="Gamesa">Gamesa</md-radio-button>
                                </md-radio-group>
                                <input type="text" ng-model="etype" id="etype" name="etype" style="max-height:0; opacity: 0; border: none"  ng-required="true" aria-label="Status">
                                <div ng-messages="etype.$error" role="alert">
                                        <div ng-message="required">SMS Category is required.</div>
                                </div>
                            </div>
                            <div class="col-sm-2"><label>Excel Sheet(.xlsx)</label></div>
                            <div class="col-sm-4">
                            <input id="file_to_upload" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                            </div>
                        </div>
                        <div class="col-sm-12" style="text-align:right;">
                        <md-button  type="submit" id="upload" class="waves-effect btn red material-icons"><i class="material-icons">cloud_upload</i> Upload</md-button>
                        </div>
            </form>
              <table id="data_list" class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th class="all">Excel Category</th>
                                    <th>Sheet Name</th>
                                    <th>Uploaded Time</th>
                                    <th class="all">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr ng-repeat="inst in usheets" class="noright">
                                        <td>{{inst.format_type}}</td>
                                        <td>{{inst.file_name}}</td>
                                        <td>{{inst.upload_time}}</td>
                                        <td> 
                                              <md-button ng-click="view_info(inst.upload_id)" style="text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> View</md-button> 
                                              <md-button ng-click="delete_info(inst.upload_id)" style="text-align:left;" class="waves-effect btn red material-icons"><i class="material-icons">clear</i> Remove</md-button> 
                                        </td>
                                    </tr>
                            </tbody>
                </table>
        </md-card-content>
    </md-card>
	
	<!--Production data update-->
    <md-card ng-show="Updatepro">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
          <span> <a ng-click="UploadSummary1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
	        <span class="md-headline">Production Data Update</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
            <form  method="post">
				<div class="form-group row">
				<div class="col-sm-6">
					<p>Academic Year:</p>
					<select class="form-control" name="acyear" id="acyear" ng-model="acyear" ng-init="acyear = '2019-2020'">
						<option value="2016-2017">2016-2017</option>
						<option value="2017-2018">2017-2018</option>
						<option value="2018-2019">2018-2019</option>
						<option value="2019-2020">2019-2020</option>
						<option value="2020-2021">2020-2021</option>
					</select>
				</div>
				<div class="col-sm-6">
				<p>Date:</p><input type="date" class="form-control" id="date" name="date"  ng-model="date"/>
				</div>
				</div>
						<!--<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Dropdown button
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton" required>
						<a class="dropdown-item" name="2016-2017">2016-2017</a>
						<a class="dropdown-item" name="2017-2018">2017-2018</a>
						<a class="dropdown-item" name="2018-2019">2018-2019</a>
						<a class="dropdown-item" name="2019-2020">2019-2020</a>
						<a class="dropdown-item" name="2020-2021">2020-2021</a>
						</div>
						</div>
						<div class="col-sm-6"></div>
						</div>-->
                <div class="row">
                    <div class="col-sm-6">
                          <md-input-container  class="md-block">
                                    <label>Unit Name</label>
                                    <md-select ng-model="uname" id="uname" name="uname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="unitname" placeholder="Search for Unit Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.unit_id" ng-model="uname" ng-repeat="inst in unit_list | filter:searchTerm">{{inst.unit_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                            </md-input-container>
                    </div>
                    <div class="col-sm-6">
                                 <md-input-container class="md-block">
                                    <label>Location Name</label>
                                    <md-select ng-model="lname" id="lname" name="lname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="locationname" placeholder="Search for Location Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.location_id" ng-model="lname" ng-repeat="inst in location_list | filter:searchTerm">{{inst.location_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                </div> 
				<div class="row">
                    <div class="col-sm-6">
                                <md-input-container class="md-block">
                                    <label>Division Name</label>
                                    <md-select ng-model="dname" id="dname" name="dname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="divisionname" placeholder="Search for Division Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.division_id" ng-model="dname" ng-repeat="inst in division_list | filter:searchTerm">{{inst.division_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                    
                    <div class="col-sm-6">
					<!--<md-input-container class="md-block">
                                    <label>Machine Name</label>
                                    <md-select ng-model="mname" id="mname" name="mname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Machine Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.division_id" ng-model="mname" ng-repeat="inst in division_list | filter:searchTerm">{{inst.machine_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>-->
					<!--<input type="search" name="search" id="search" class="" placeholder="Enter the Machine Name">
					<div id="searchList" style="color: blue;"></div>  -->
					</div>		
					</div>
					<!--<div class="row">
                    <div class="col-sm-6">
                            <p>Machine Hour:</p><input type="int" class="form-control" id="machine_hour" name="machine_hour" required/>
					</div>
					<div class="col-sm-6">
                            <p>Machine Minute:</p><input type="int" class="form-control" id="machine_min" name="machine_min" required/>
					</div></div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Machine Breakdown:</p><input type="double" class="form-control" id="machine_breakdown" name="machine_breakdown" required/>
					</div>
					<div class="col-sm-6">
							<p>Machine Available:</p><input type="double" class="form-control" id="machine_available" name="machine_available" required/>
					</div></div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Machine Reason:</p><input type="varchar" class="form-control" id="machine_reason" name="machine_reason" required/>
					</div>
					<div class="col-sm-6"></div>
					</div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Grid Hour:</p><input type="int" class="form-control" id="grid_hour" name="grid_hour" required/>
					</div>
					<div class="col-sm-6">
                            <p>Grid Minute:</p><input type="int" class="form-control" id="grid_min" name="grid_min" required/>
					</div></div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Grid Breakdown:</p><input type="double" class="form-control" id="grid_breakdown" name="grid_breakdown" required/>
					</div>
					<div class="col-sm-6">
							<p>Grid Available:</p><input type="double" class="form-control" id="grid_available" name="grid_available" required/>
					</div></div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Reason:</p><input type="varchar" class="form-control" id="reason" name="reason" required/> 
					</div>
					<div class="col-sm-6">
					</div>
					</div>
					<div class="row">
					<div class="col-sm-6">
                            <p>Production:</p><input type="double" class="form-control" id="production" name="production" required/> 
					</div>
					<div class="col-sm-6">
                            <p>Plf:</p><input type="double" class="form-control" id="plf" name="plf" required/>
					</div></div>
					<div class="row">
                    <div class="col-sm-6">
                            <p>Generation Hour:</p><input type="int" class="form-control" id="generation_hour" name="generation_hour" required/>
					</div>
					<div class="col-sm-6">
                            <p>Generation Minute:</p><input type="int" class="form-control" id="generation_min" name="generation_min" required/>
					</div></div>-->
					<br /><br />
							<div align="center"><input type="submit" class="btn btn-success btn-lg" ng-click="Updatepro2()" value="Submit" /></div>
              
            </form>
        </md-card-content>
    </md-card>
	<!--/production data update-->
	
	<!--production data update2-->
	<md-card ng-hide="true" ng-show="UpdateproExtended">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
          <span> <a ng-click="UploadSummary2()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
	        <span class="md-headline">Production Data Update</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
		
		<input type="hidden" name="loaded_academic_year" id="loaded_academic_year" ng-model="loaded_academic_year" ng-value="loaded_academic_year"/>
		<input type="hidden" name="loaded_date" id="loaded_date" ng-model="loaded_date" ng-value="loaded_date"/>
		<input type="hidden" name="loaded_unit_id" id="loaded_unit_id" ng-model="loaded_unit_id" ng-value="loaded_unit_id"/>
		<input type="hidden" name="loaded_location_id" id="loaded_location_id" ng-model="loaded_location_id" ng-value="loaded_location_id"/>
		<input type="hidden" name="loaded_division_id" id="loaded_division_id" ng-model="loaded_division_id" ng-value="loaded_division_id"/><br><br><br>
		
		<form method="post"> 
  <div class="container">  
   <div class="table-responsive">  
	<div style="height: 300px; overflow: scroll;">
    <table class="table table-bordered">
    <tr> 
   <th>Machine Name</th>
   <th>Machine Hour</th>
   <th>Machine Minute</th>
   <th>Machine Breakdown Hour</th>
   <th>Machine Reason</th>
   <th>Machine Available</th>
   <th>Grid Hour</th>
   <th>Grid Minute</th>
   <th>Grid Breakdown Hour</th>
   <th>Reason</th>
   <th>Grid Available</th>
   <th>Production</th>
   <th>*****Plf*****</th>
   <th>Generation Hour</th>
   <th>Generation Minute</th>
   
    </tr>
		
       <tr ng-repeat="x in machine_details"> 
	   
	     <td>{{x.machine_name}}</td>
	     <td><input type="number" ng-model="x.loaded_machine_hour"/></td>
		 <td><input type="number" ng-model="x.loaded_machine_minute"/></td>
		 <td><input type="number" ng-model="x.loaded_machine_breakdown_hour"/></td>
		 <td><input type="varchar" ng-model="x.loaded_machine_reason"/></td>
		 <td><input type="number" ng-model="x.loaded_machine_available"/></td>
		 <td><input type="number" ng-model="x.loaded_grid_hour"/></td>
		 <td><input type="number" ng-model="x.loaded_grid_minute"/></td>
		 <td><input type="number" ng-model="x.loaded_grid_breakdown_hour"/></td>
		 <td><input type="varchar" ng-model="x.loaded_reason"/></td>
		 <td><input type="number" ng-model="x.loaded_grid_available"/></td>
		 <td><input type="number" ng-model="x.loaded_production" ng-change="myFuncPlf(x)"/></td>
		 <td><input type="varchar" ng-model="x.loaded_plf_value" ng-value="x.loaded_plf_value | number:2" disabled /></td>
		 <td><input type="number" ng-model="x.loaded_generation_hour"/></td>
		 <td><input type="number" ng-model="x.loaded_generation_minute"/></td>
       </tr>   

	
	
	
	</table>
	</div><br>
	<div align="center"><input type="submit" class="btn btn-success btn-lg" ng-click="production_submit(machine_details)" value="Submit" /></div><br><br>
   </div>  
  </div> 
</form>  
		
        </md-card-content>
    </md-card>
	<!--/production data update2-->
	
	
    <md-card ng-show="ProductionInfo">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
          <span> <a ng-click="InfoUpload()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
	        <span class="md-headline">Production Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
         <form enctype="multipart/form-data" method="post" id="ApproveData">
            <div class="row">
                <div class="col-sm-2"><strong>Excel Type</strong></div>
                <div class="col-sm-4">{{sheet_type}}</div>
                <div class="col-sm-2"><strong>Excel Sheet Name</strong></div>
                <div class="col-sm-4">{{sheet_name}}</div>
            </div>
            <div class="row">
            <table id="pdata" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all" width="15%">S.No.</th>
                            <th class="all" width="15%">Machine Name</th>
                            <th class="all">Date</th>
                            <th class="all">Capacity</th>
                            <th>M/c Breakdown in HH:MM</th>
                            <th>M/c Breakdown %</th>
                            <th>M/c Breakdown Reason</th>
                            <th>M/c Avl %</th>
                            <th>Grid Breakdown in HH:MM</th>
                            <th>Grid Breakdown %</th>
                            <th>Grid Breakdown Reason</th>
                            <th>Grid Avl %</th>
                            <th class="all">Production</th>
                            <th>PLF</th>
                            <th>Generation Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in pinfo" class="collection-item avatar">
                            <td>{{$index+1}}</td>
                            <td>{{info.name}}</td>
                            <td>{{info.date}}</td>
                            <td>{{info.capacity}}</td>
                            <td>{{info.mc_brk}}</td>
                            <td>{{info.mc_brk_per}}</td>
                            <td>{{info.mc_brk_reason}}</td>
                            <td>{{info.mc_avl}}</td>
                            <td>{{info.gd_brk}}</td>
                            <td>{{info.gd_brk_per}}</td>
                            <td>{{info.gd_brk_reason}}</td>
                            <td>{{info.gd_avl}}</td>
                            <td align="right">{{info.production}}</td>
                            <td>{{info.plf}}</td>
                            <td>{{info.generation_hour}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row" style="text-align:right;">
                 <md-button type="submit" class="waves-effect btn pink material-icons"><i class="material-icons">send</i>Approve</md-button>
            </div>
            </form>
        </md-card-content>
    </md-card>
<script>  
 $(document).ready(function(){  
      $('#search').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                $.ajax({  
                     url:"search.php",  
                     method:"POST",  
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#searchList').fadeIn();  
                          $('#searchList').html(data);  
                     }  
                });  
           }
		   else{
			 $("#searchList").hide();  
		   }
	
      });  
      $(document).on('click', 'li', function(){  
           $('#search').val($(this).text());  
           $('#searchList').fadeOut();  
      }); 
$(document).click(function(){
  $("#searchList").hide();
});	  
	  
 });  
 </script>  
</body>
</html>