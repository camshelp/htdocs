<!DOCTYPE html>
<html>
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Invoice Details</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <md-card ng-show="invoiceinfo">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span class="md-headline">Invoice Informaton</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
            <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Client(s)</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_cli}}</span>
                              <a  ng-click="newclient('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Invoice(s)</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_invoice}}</span>
                              <a  ng-click="newinvoice('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:red;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">signal_cellular_alt</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Pending Amount</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_pending}}</span>
                             
                         </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                            <md-input-container  class="md-block">
                                    <label>Unit Name</label>
                                    <md-select ng-model="suname" id="suname" name="suname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Unit Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-click="RefreshInvoice(inst.unit_id)" ng-value="inst.unit_id" ng-model="suname" ng-repeat="inst in unit_list | filter:searchTerm">{{inst.unit_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                            </md-input-container>
                </div>
                <div class="col-sm-6">
                            <label>Invoice Month & Year</label>
                                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_inputs" data-link-format="yyyy-mm">
                                    <input ng-model="imonths" id="monthd" class="form-control" size="16" type="text" value="" readonly required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input ng-model="imonths" name="imonths" id="dtp_inputs" value="" hidden/>
                </div>
            </div>
            <table id="ilist" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all">Client Name</th>
                            <th>Unit Rate</th>
                            <th>Total Unit</th>
                            <th>Total Amount</th>
                            <th>Reimbursements</th>
                            <th>Invoice Amount</th>
                            <th>Received</th>
                            <th class="all">Balance</th>
                            <th class="all">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in invoice_list" class="collection-item avatar">
                            <td>{{info.client_name}}</td>
                            <td>{{info.unit_cost}}</td>
                            <td>{{info.total_unit}}</td>
                            <td>{{info.total_cost}}</td>
                            <td>{{info.less_reimburse}}</td>
                            <td>{{info.total_amount}}</td>
                            <td>{{info.amount_paid}}</td>
                            <td>{{info.amount_pending}}</td>
                            <td style="text-align:right;">
                                <table>
                                    <tr>
                                        <td>
                                              <md-button ng-click="newinvoice(info.invoice_id)" style="width:10px;" class="waves-effect btn blue material-icons"><i class="material-icons">mode_edit</i></md-button>
                                        </td>
                                        <td>
                                               <md-button ng-click="Update_Payment(info.invoice_id)" style="width:10px;"  class="waves-effect btn yellow material-icons"><i class="material-icons">receipt</i></md-button>
                                        </td>
                                        <td>
                                             <md-button ng-click="invoicereport(info.invoice_id)" style="width:30px;"  class="waves-effect btn pink material-icons"><i class="material-icons">print</i></md-button>
                                        </td>
                                        <td>
                                             <md-button ng-click="invoiceremove(info.invoice_id)" style="width:30px;"  class="waves-effect btn red material-icons"><i class="material-icons">clear</i></md-button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </md-card-content>
    </md-card>

    <md-card ng-show="paymentinfo">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span> <a ng-click="payment_back()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Payment History</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
          <form enctype="multipart/form-data" method="post" id="PRegister" name="PForm" >
            <div hidden>
                 <input ng-model="piid" name="piid" id="piid" value=""/>
                 <input ng-model="pcid" name="pcid" id="pcid" value=""/>
                 <input ng-model="puid" name="puid" id="puid" value=""/>
            </div>
            <div class="row">
                <div class="col-sm-6">
                         <md-input-container  class="md-block">
                                <label>Invoice No.</label>
                                <input name="pino" id="pino" ng-model="pino" required readonly >
                                <div ng-messages="PForm.pino.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
                <div class="col-sm-6">
                         <md-input-container  class="md-block">
                                <label>Invoice Date</label>
                                <input name="pidate" id="pidate" ng-model="pidate" required readonly>
                                <div ng-messages="PForm.pidate.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                <md-input-container  class="md-block">
                                <label>Unit Name</label>
                                <input name="puname" id="puname" ng-model="puname" required readonly>
                                <div ng-messages="PForm.puname.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
                <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                <label>Client Name</label>
                                <input name="pcname" id="pcname" ng-model="pcname" required readonly>
                                <div ng-messages="PForm.pcname.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                        </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                <md-input-container  class="md-block">
                                <label>Invoice Month & Year</label>
                                <input name="pmyear" id="pmyear" ng-model="pmyear" required readonly>
                                <div ng-messages="PForm.pmyear.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
                <div class="col-sm-6">
                      <md-input-container  class="md-block">
                                <label>Bill Period</label>
                                <input name="pbper" id="pbper" ng-model="pbper" required readonly>
                                <div ng-messages="PForm.pbper.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                      <md-input-container  class="md-block">
                                <label>Amount to Pay</label>
                                <input name="ppay" id="ppay" ng-model="ppay" required readonly>
                                <div ng-messages="PForm.ppay.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                <div>Payment Type</div>
                                <div>
                                         <md-radio-group ng-model="sal" name="sal" ng-required="true" layout="row">
                                            <md-radio-button ng-model="sal" ng-repeat="d in saluation" ng-value="d.saluation">{{d.saluation}}</md-radio-button> 
                                        </md-radio-group>
                                        <input type="text" ng-model="sal" name="sal" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                        <div ng-messages="sal.$error" role="alert">
                                            <div ng-message="required">Saluation is required.</div>
                                        </div>
                                </div>
                </div>
                <div class="col-sm-6">
                             <label>Paid Date</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_inputp" data-link-format="yyyy-mm-dd">
                                <input ng-model="pmonth" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="pmonth" name="pmonth" id="dtp_inputp" value="" hidden/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                          <md-input-container  class="md-block">
                                <label>Paid Amount</label>
                                <input name="pamount" id="pamount" ng-model="pamount" required md-maxlength="20" minlength="1">
                                <div ng-messages="PForm.pamount.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
                <div class="col-sm-6">
                            <md-input-container  class="md-block">
                                <label>Remarks</label>
                                <input name="premarks" id="premarks" ng-model="premarks" md-maxlength="250">
                                <div ng-messages="PForm.premarks.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6" style="text-align:right;">
                      <button class="waves-effect btn pink" type="submit" name="action">Update Payment<i class="material-icons">send</i></button>
                </div>
            </div>
           </form>
                <table id="plist" width="100%" class="collection table table-striped">
                            <thead>
                                <tr>
                                    <th class="all">Payment Type</th>
                                    <th>Date</th>
                                    <th>Amount Paid</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="info in phistory" class="collection-item avatar">
                                    <td>{{info.payment_type}}</td>
                                    <td>{{info.paid_date}}</td>
                                    <td>{{info.paid_amount}}</td>
                                    <td>{{info.remarks}}</td>
                                </tr>
                            </tbody>
                    </table>
        </md-card-content>
    </md-card>

    <md-card ng-show="invoiceregister">
        <md-card-title   class="card-content white-text" style="background-color:blue;">
          <md-card-title-text>
            <span> <a ng-click="register_back1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Invoice Registration</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">

        <form enctype="multipart/form-data" method="post" id="InvoiceRegister" name="IForm" >
                <div hidden>
                  <input name="iid" id="iid" ng-model="iid">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                          <md-input-container  class="md-block">
                                    <label>Unit Name</label>
                                    <md-select ng-model="uname" id="uname" name="uname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Unit Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.unit_id" ng-model="uname" ng-repeat="inst in unit_list | filter:searchTerm">{{inst.unit_name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                            </md-input-container>
                    </div>
                    <div class="col-sm-6">
                                 <md-input-container class="md-block">
                                    <label>Client Name</label>
                                    <md-select ng-model="clname" id="clname" name="clname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Client Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.id" ng-click="select_client(inst.id)" ng-model="clname" ng-repeat="inst in clientlist | filter:searchTerm">{{inst.name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                            <label>  Contact Person :</label> {{comm_person}}
                    </div>
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                <label>HTSC No.</label>
                                <input name="ihtsc" id="ihtsc" ng-model="ihtsc" required md-maxlength="30" minlength="1">
                                <div ng-messages="IForm.htsc.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                    <label>Invoice Month & Year</label>
                            <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_input1" data-link-format="yyyy-mm">
                                <input ng-model="imonth" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonth" name="imonth" id="dtp_input1" value="" hidden/>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <label>Invoice Date</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_inputid" data-link-format="yyyy-mm-dd">
                                <input ng-model="idate" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="idate" name="idate" id="dtp_inputid" value="" hidden/>
                    </div>
                    <div class="col-sm-6">
                            <label>Pay by Date</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                <input ng-model="ipay" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="ipay" name="ipay" id="dtp_input2" value="" hidden/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" style="background:#fff8e1;">
                    <table style="border: 1px solid black;padding:1px;">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Description</th>
                            <th>Units</th>
                            <th>Unit Rate</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>A</td>
                            <td>Wind Power Units Alloted</td>
                            <td><input name="iallocated" id="iallocated" ng-model="iallocated" required></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>B</td>
                            <td>Line Loss</td>
                            <td><input name="iloss" id="iloss" ng-model="iloss" required></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>C</td>
                            <td>Net Generation</td>
                            <td>{{inet}}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>D</td>
                            <td>Wind Power Units Consumed</td>
                            <td>{{iconsume}}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>E</td>
                            <td>Wind Power Units Banked</td>
                            <td><input name="ibanked" id="ibanked" ng-model="ibanked" required></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>F</td>
                            <td>Taken from Banked Units</td>
                            <td><input name="itaken" id="itaken" ng-model="itaken" required></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>G</th>
                            <th>Charges for Consumption of Wind Power Units</th>
                            <th>{{total_unit}}</th>
                            <th><input name="irate" id="irate" ng-model="irate" required></th>
                            <th>{{con_amount}}</th>
                        </tr>
                        </tbody>
                </table>
                    </div>

                    <div class="col-sm-6" style="background:#e0f2f1;">
                    <table style="border: 1px solid black;">
                        <thead>
                            <tr>
                                    <th>S.No.</th>
                                    <th>Description</th>
                                    <th>Units</th>
                                    <th>Amount</th>
                            </tr>
                        </thead>
                    <tbody>
                          <tr>
                            <th></th>
                            <th>Reimbursement</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>H</td>
                            <td>Meter Rent</td>
                            <td><input name="imrent" id="imrent" ng-model="imrent" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td>RKVAH Penalty</td>
                            <td><input name="ipenalty" id="ipenalty" ng-model="ipenalty" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>J</td>
                            <td>System Operating Charges</td>
                            <td><input name="iop" id="iop" ng-model="iop" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>K</td>
                            <td>Substation O&M Charges</td>
                            <td><input name="iom" id="iom" ng-model="iom" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>L</td>
                            <td>Transmission Charges</td>
                            <td><input name="itr" id="itr" ng-model="itr" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>M</td>
                            <td>Wheeling Charges</td>
                            <td><input name="iwhl" id="iwhl" ng-model="iwhl" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>N</td>
                            <td>Banking Charges</td>
                            <td><input name="ibc" id="ibc" ng-model="ibc" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>O</td>
                            <td>Self Generation Tax</td>
                            <td><input name="isgt" id="isgt" ng-model="isgt" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>P</td>
                            <td>Negative Charges</td>
                            <td><input name="inc" id="inc" ng-model="inc" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Q</td>
                            <td><input name="ocname" id="ocname" ng-model="ocname" required></td>
                            <td><input name="oc" id="oc" ng-model="oc" required></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>R</th>
                            <th>Less Reimbursements</th>
                            <th>{{lr_amount}}</th>
                            <th></th>
                        </tr>
                    </tbody>
                </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4" style="text-align:right;"><h5>Total Amount Payable : <h5>
                    </div>
                    <div class="col-sm-2"><h5>{{pay_amount}}<h5>
                    </div>
                    <div class="col-sm-6" style="text-align:right;">
                        <button class="waves-effect btn pink" type="submit" name="action">Update Invoice<i class="material-icons">send</i></button>
                    </div>
                </div>
                </form>
        </md-card-content>
    </md-card>

    <md-card ng-show="clientregister">
        <md-card-title   class="card-content white-text" style="background-color:indigo;">
          <md-card-title-text>
            <span> <a ng-click="client_back1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Client Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
            <div style="text-align:right;">
            <md-button ng-click="addclient('new')" style="text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">add</i> Add Client</md-button>
            </div>
            <table id="clist" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all">Client Name</th>
                            <th>Unit Rate</th>
                            <th>Mobile No.</th>
                            <th>Contact Person</th>
                            <th class="all">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in clientlist" class="collection-item avatar">
                            <td>{{info.name}}</td>
                            <td>{{info.cost}}</td>
                            <td>{{info.mobile}}</td>
                            <td>{{info.person}}<span>-</span>{{info.designation}}</td>
                            <td style="text-align:right;">
                            <md-button ng-click="addclient(info.id)"  class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </md-card-content>
    </md-card>

    <md-card ng-show="clientadd">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span> <a ng-click="client_back2()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Client Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
            <form enctype="multipart/form-data" method="post" id="cregister" name="CForm" >
                <div hidden>
                  <input name="ccid" id="ccid" ng-model="ccid">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Client Name</label>
                            <input name="cname" id="cname" ng-model="cname" required md-maxlength="200" minlength="1">
                            <div ng-messages="CForm.cname.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                           <md-input-container  class="md-block">
                            <label>Unit Cost</label>
                            <input name="ccost" id="ccost" ng-model="ccost" required md-maxlength="10" minlength="1">
                            <div ng-messages="Cform.ccost.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Address Line1</label>
                            <input name="cline1" id="cline1" ng-model="cline1" required md-maxlength="200" minlength="1">
                            <div ng-messages="Cform.cline1.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Address Line2</label>
                            <input name="cline2" id="cline2" ng-model="cline2" required md-maxlength="200" minlength="1">
                            <div ng-messages="Cform.cline2.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                            <label>Address Line3</label>
                            <input name="cline3" id="cline3" ng-model="cline3" md-maxlength="200">
                            <div ng-messages="Cform.cline3.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Address Line4</label>
                            <input name="cline4" id="cline4" ng-model="cline4" md-maxlength="200">
                            <div ng-messages="Cform.cline4.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Mobile No.</label>
                            <input name="cmobile" id="cmobile" ng-model="cmobile" md-maxlength="15" minlength="1">
                            <div ng-messages="Cform.cmobile.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                       <md-input-container  class="md-block">
                            <label>e-mail Id</label>
                            <input name="cmail" id="cmail" ng-model="cmail" md-maxlength="100">
                            <div ng-messages="Cform.cmail.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>EDC Name</label>
                            <input name="cedc" id="cedc" ng-model="cedc" required md-maxlength="30">
                            <div ng-messages="Cform.cedc.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                       <md-input-container  class="md-block">
                            <label>HTSC No.</label>
                            <input name="chtsc" id="chtsc" ng-model="chtsc" required md-maxlength="30">
                            <div ng-messages="Cform.chtsc.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>GST No.</label>
                            <input name="cgst" id="cgst" ng-model="cgst" md-maxlength="30">
                            <div ng-messages="Cform.cgst.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                            <label>PAN No.</label>
                            <input name="cpan" id="cpan" ng-model="cpan" md-maxlength="30">
                            <div ng-messages="Cform.cpan.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Contact Person Name</label>
                            <input name="ccontact" id="ccontact" ng-model="ccontact" md-maxlength="200">
                            <div ng-messages="Cform.ccontact.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                            <label>Contact Person Designation</label>
                            <input name="cdesig" id="cdesig" ng-model="cdesig" md-maxlength="200">
                            <div ng-messages="Cform.cdesig.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div style="text-align:right;">
                    <button class="waves-effect btn pink" type="submit" name="action">Submit<i class="material-icons">send</i></button>
                </div>
            </form>
        </md-card-content>
    </md-card>

    <md-card ng-show="invoiceinfo">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span class="md-headline">Reports</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
            <div class="col-sm-6">
                           <label>Starting Month & Year</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_input2" data-link-format="yyyy-mm">
                                <input ng-model="imonth1" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonth1" name="imonth1" id="dtp_input2" value="" hidden/>
            </div>
            <div class="col-sm-6">
                            <label>Ending Month & Year</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_inpute" data-link-format="yyyy-mm">
                                <input ng-model="imonthe" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonthe" name="imonthe" id="dtp_inpute" value="" hidden/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <md-button ng-click="consolidated_invoice()"  class="waves-effect btn pink material-icons">Unitwise Consolidation</md-button>
            </div>
            <div class="col-sm-6">
               <md-button ng-click="consolidated_invoice1()"  class="waves-effect btn pink material-icons">Clientwise Consolidation</md-button>
            </div>
        </div>
        </md-card-content>
    </md-card>
    <script src="/krg/myjs/krcepl/invoice.js"></script>
    <script src="/krg/myjs/fileupload.js"></script>
</body>
</html>