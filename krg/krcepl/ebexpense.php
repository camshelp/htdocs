<!DOCTYPE html>
<html>
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Invoice Details</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

<md-card ng-show="Disp_Info">
        <md-card-title   class="card-content white-text" style="background-color:indigo;">
          <md-card-title-text>
            <span class="md-headline">EB Expense Summary</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
            <div class="col-sm-9"></div>
            <div class="col-sm-3">
                           <label>Expense Month & Year</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_input1" data-link-format="yyyy-mm">
                                <input ng-model="imonth" class="form-control" id="myr" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonth" name="imonth" id="dtp_input1" value="" hidden/>
            </div>
        </div>
     
            <div class="row">
                    <div class="col-sm-3">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">import_export</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Import</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_import}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:indigo;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">import_export</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Export</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_export}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:green;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">info</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Net Generation</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_generation}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:red;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">shop</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>Total Expense</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{total_expense}}</span>
                         </div>
                        </div>
                    </div>
        </div>

            <table id="clist" width="100%" class="collection table table-striped">
                    <thead>
                        <tr>
                            <th class="all">Unit Name</th>
                            <th>Total Import</th>
                            <th>Total Export</th>
                            <th>Net Generation</th>
                            <th>Total Charges</th>
                            <th class="all">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="info in unitlist" class="collection-item avatar">
                            <td>{{info.unit_name}}</td>
                            <td>{{info.import}}</td>
                            <td>{{info.export}}</td>
                            <td>{{info.net_generation}}</td>
                            <td>{{info.total_charges}}</td>
                            <td style="text-align:right;">
                                <md-button ng-click="view_info(info.unit_id,info.unit_name)"  class="waves-effect btn pink material-icons"><i class="material-icons">arrow_forward_ios</i> View</md-button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <md-card>
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span class="md-headline">Reports</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
            <div class="col-sm-6">
                           <label>Starting Month & Year</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_input2" data-link-format="yyyy-mm">
                                <input ng-model="imonth1" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonth1" name="imonth1" id="dtp_input2" value="" hidden/>
            </div>
            <div class="col-sm-6">
                            <label>Ending Month & Year</label>
                            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm" data-link-field="dtp_inpute" data-link-format="yyyy-mm">
                                <input ng-model="imonthe" class="form-control" size="16" type="text" value="" readonly required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <input ng-model="imonthe" name="imonthe" id="dtp_inpute" value="" hidden/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <md-button ng-click="expense_summary()"  class="waves-effect btn pink material-icons">Expense Summary</md-button>
            </div>
            <div class="col-sm-6">
               
            </div>
        </div>
        </md-card-content>
    </md-card>

        </md-card-content>
    </md-card>

    <md-card ng-show="Add_Info">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span> <a ng-click="back_info()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">EB Expense Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
            <form enctype="multipart/form-data" method="post" id="cregister" name="CForm" >
                <div class="row">
                    <div class="col-sm-6">
                          <input name="euname" id="euname" ng-model="euname" readonly>
                          <input name="euid" id="euid" ng-model="euid" hidden>
                          <input name="etype" id="etype" ng-model="etype" hidden>
                    </div>
                    <div class="col-sm-6">
                              <md-input-container>
                                    <label>Machine Name</label>
                                    <md-select ng-model="emname" id="emname" name="emname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Machine Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.machine_id" ng-model="emname" ng-repeat="inst in m_list | filter:searchTerm">{{inst.machine_name}}<span>({{inst.htsc}})</span></md-option>
                                    </md-optgroup>
                                    </md-select>
                                </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Total Export</label>
                            <input name="eexport" id="eexport" ng-model="eexport" required md-maxlength="30">
                            <div ng-messages="Cform.eexport.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Total Import</label>
                            <input name="eimport" id="eimport" ng-model="eimport" required md-maxlength="30">
                            <div ng-messages="Cform.eimport.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                            <label>Net Generation</label>
                            <input name="egen" id="egen" ng-model="egen" required md-maxlength="30">
                            <div ng-messages="Cform.egen.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>10% of Net Generation</label>
                            <input name="e10gen" id="e10gen" ng-model="e10gen" required md-maxlength="30">
                            <div ng-messages="Cform.e10gen.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Power Factor</label>
                            <input name="epower" id="epower" ng-model="epower" required md-maxlength="30">
                            <div ng-messages="Cform.epower.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                       <md-input-container  class="md-block">
                            <label>RKVAh Penalty @ 025/050 paise/unit(Worked out in Rupess)</label>
                            <input name="epenalty" id="epenalty" ng-model="epenalty" required md-maxlength="30">
                            <div ng-messages="Cform.epenalty.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Monthly Meter Reading Charges</label>
                            <input name="ereading" id="ereading" ng-model="ereading" required md-maxlength="30">
                            <div ng-messages="Cform.ereading.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                       <md-input-container  class="md-block">
                            <label>Operation and Maintainance Charges</label>
                            <input name="eomcharge" id="eomcharge" ng-model="eomcharge" required md-maxlength="30">
                            <div ng-messages="Cform.eomcharge.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                         <md-input-container  class="md-block">
                            <label>Scheduling Charges</label>
                            <input name="eschedule" id="eschedule" ng-model="eschedule" required md-maxlength="30">
                            <div ng-messages="Cform.eschedule.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                            <label>System Operating Charges</label>
                            <input name="eoper" id="eoper" ng-model="eoper" required md-maxlength="30">
                            <div ng-messages="Cform.eoper.$error">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                            </div>
                        </md-input-container>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <md-input-container  class="md-block">
                                <label>Transmission Charges</label>
                                <input name="etrans" id="etrans" ng-model="etrans" required md-maxlength="30">
                                <div ng-messages="Cform.etrans.$error">
                                <div ng-message="required">This is required!</div>
                                <div ng-message="md-maxlength">That's too long!</div>
                                <div ng-message="minlength">That's too short!</div>
                                </div>
                            </md-input-container>
                    </div>
                    <div class="col-sm-6" style="text-align:right;">
                         <button class="waves-effect btn pink" type="submit" name="action">Submit<i class="material-icons">send</i></button>
                    </div>
                </div>
            </form>
        </md-card-content>
    </md-card>
    <md-card ng-show="View_Info">
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span> <a ng-click="back_info1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">EB Expense Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
                <table id="mdata" width="100%" class="collection table table-striped">
                        <div style="texta-lign:right;">
                             <md-button ng-click="edit_info(euid,euname)"  class="waves-effect btn pink material-icons"><i class="material-icons">add_circle</i> Add</md-button>
                        </div>
                            <thead>
                                <tr>
                                    <th class="all">Machine Name</th>
                                    <th>Total Import</th>
                                    <th>Total Export</th>
                                    <th>Net Generation</th>
                                    <th>Total Charges</th>
                                    <th class="all">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="info in machinelist" class="collection-item avatar">
                                    <td>{{info.machine_name}}</td>
                                    <td>{{info.total_import}}</td>
                                    <td>{{info.total_export}}</td>
                                    <td>{{info.net_generation}}</td>
                                    <td>{{info.total_charges}}</td>
                                    <td style="text-align:right;">
                                      <md-button ng-click="update_info(info.machine_id)"  class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                                    </td> 
                                </tr>
                            </tbody>
                </table>
        </md-card-content>
    </md-card>

<script src="/krg/myjs/krcepl/eb.js"></script>
    <script src="/krg/myjs/fileupload.js"></script>
</body>
</html>