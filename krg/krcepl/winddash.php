<html>
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Wind Mill - Dashboard</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 
<div class="row">
    <div class="col-sm-6">
     <md-card>
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
            <span class="md-headline">Insurance Reminder</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <table width="100%" class="collection table table-striped"> 
                    <thead> 
                        <tr>
                            <td></td>
                            <td class="all">Machine Name</td>
                            <td>Amount</td>
                            <td>Due Date</td>
                        </tr>
                    </thead>
                    <tbody>
                            <tr ng-repeat="inst in insur_list" class="collection-item avatar">
                                <td>{{ $index+1 }}<span>.</span></td>
                                <td>{{ inst.machine_name }}</td>
                                <td>{{ inst.amount }}</td>
                                <td> {{ inst.due_date }}</td>
                            </tr>
                    </tbody>
            </table>
        </md-card-content>
      </md-card>
    </div>
    <div class="col-sm-6">
     <md-card>
        <md-card-title   class="card-content white-text" style="background-color:purple;">
          <md-card-title-text>
            <span class="md-headline">O&M Reminder</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <table width="100%" class="collection table table-striped"> 
                    <thead> 
                        <tr>
                            <td></td>
                            <td class="all">Machine Name</td>
                            <td>Amount</td>
                            <td>Due Date</td>
                        </tr>
                    </thead>
                    <tbody>
                            <tr ng-repeat="inst in om_list" class="collection-item avatar">
                                <td>{{ $index+1 }}<span>.</span></td>
                                <td>{{ inst.machine_name }}</td>
                                <td>{{ inst.amount }}</td>
                                <td> {{ inst.due_date }}</td>
                            </tr>
                    </tbody>
            </table>
        </md-card-content>
      </md-card>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <md-card>
          <md-card-title   class="card-content white-text" style="background-color:purple;">
            <md-card-title-text>
              <span class="md-headline">Invoice Payment Pending</span>
            </md-card-title-text>
          </md-card-title>
          <md-card-actions layout="row" layout-align="start center">
          </md-card-actions>
          <md-card-content class="card-content">
            <table width="100%" class="collection table table-striped"> 
                      <thead> 
                          <tr>
                              <td></td>
                              <td class="all">Client Name</td>
                              <td>Invoice Month</td>
                              <td>Due Date</td>
                              <td>Balance</td>
                          </tr>
                      </thead>
                      <tbody>
                              <tr ng-repeat="inst in pay_list" class="collection-item avatar">
                                  <td>{{ $index+1 }}<span>.</span></td>
                                  <td>{{ inst.client_name }}</td>
                                  <td>{{ inst.myear }}</td>
                                  <td> {{ inst.pay_date }}</td>
                                  <td> {{ inst.amount_pending }}</td>
                              </tr>
                      </tbody>
              </table>
              </md-card-content>
      </md-card>
      </div>
      <div class="col-sm-6">
      </div>
    </div>

</div>
    <script src="/krg/myjs/krcepl/dashboard.js"></script>     
    <script src="/krg/myjs/fileupload.js"></script>
</body>
</html>