<!DOCTYPE html>
<html>
<head>
<title>Welcome to K.Ramakrishnan Clean Energy Pvt. Ltd.</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<script type="text/javascript" src="/PCTEM/scripts/krcepl_website/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/PCTEM/scripts/krcepl_website/bootstrap.min.js"></script>
<script type="text/javascript" src="/PCTEM/scripts/krcepl_website/counterup.min.js"></script>
<link href="/PCTEM/scripts/krcepl_website/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/PCTEM/scripts/krcepl_website/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel='stylesheet' type='text/css' href='/PCTEM/scripts/krcepl_website/jquery.easy-gallery.css' />
<script type="text/javascript" src="/PCTEM/scripts/krcepl_website/move-top.js"></script>
<script type="text/javascript" src="/PCTEM/scripts/krcepl_website/easing.js"></script>
<script src="/PCTEM/scripts/gitter/bootstrap-notify.js" type="text/javascript"></script>

<!-- start-smoth-scrolling -->

</head>
	
<body>
<!-- banner -->
	<div class="banner">
		<div class="container">
			<div class="header">
              	<div class="logo">
                      <img src="/PCTEM/scripts/images/kr.jpg" width="50" height="50" />
                    <h1><a href="/krg/krcepl"><br />K.Ramakrishnan <span>Clean Energy Pvt. Ltd.</span></a></h1>
				</div>
				<div class="mail-phone">
					<ul>
						<li><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>+91 9842498377<br>&nbsp;&nbsp;&nbsp;+91 9842881377</li>
						<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:kramakrishnancleanenegry@gmail.com">kramakrishnancleanenegry@gmail.com<br />&nbsp;&nbsp;&nbsp;&nbsp;krvgreen@gmail.com</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="banner-info">
				<ul>
					<li class="active"><a href="index.html">Home</a></li>
                    <li><a href="/krg/login.php" class="hvr-radial-out">Login</a></li>
					<li><a href="#services" class="scroll hvr-radial-out">Our Clients</a></li>
					<li><a href="#gallery" class="scroll hvr-radial-out">Gallery</a></li>
					<li><a href="#team" class="scroll hvr-radial-out">About MD</a></li>
                	<li><a href="#mail" class="scroll hvr-radial-out">Mail Us</a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- about -->

	<div class="about">
		<div class="col-md-6 about-left">
			<h3>About Us</h3>
            <p class="qui"><span></span></p>
			<i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i>
			<p>The flow of wind and wind speeds has an important bearing on generation capacity. K.Ramakrishnan Clean Energy (P) Ltd has years of experience in determining the best location and putting up wind turbines for maximum efficiency. Even the design of our wind turbines is optimized to get the best out of low wind speeds at varying points during the year.</p>
            <p> With a total of nearly 50 MW wind, our wind farms are located in the states of Tamil Nadu and Karnataka, the annual generation from these wind mills are 6.0 Crores units per annum.</p>
		</div>
		<div class="col-md-6 about-right">
			<div class="content-item">
  
				<div class="overlay"></div>
			  
				<div class="corner-overlay-content">Info</div>
			  
				<div class="overlay-content">
					<h2>Wind Turbine</h2>
					<p>A wind turbine is a device that converts the wind's kinetic energy into electrical energy.Wind turbines are manufactured in a wide range of vertical and horizontal axis. The smallest turbines are used for applications such as battery charging for auxiliary power for boats or caravans or to power traffic warning signs. Slightly larger turbines can be used for making contributions to a domestic power supply while selling unused power back to the utility supplier via the electrical grid</p>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- //about -->
<!-- about-bottom -->
	<div class="about-bottom">
		<div class="container">
			<div class="video-grids-left1">
				<a class="play-icon popup-with-zoom-anim" href="#small-dialog">
						<span> </span>
				</a>
				<div class="video-grid-pos">
					<h3>watch our video</h3>
					<ul>
						<li>9:32 AM<label>|</label></li>
						<li><a href="#">KRCEPL</a> <label>|</label></li>
						<li><span>Wind Turbine</span></li>
					</ul>
				</div>
					<!-- pop-up-box -->    
						<link href="/PCTEM/scripts/krcepl_website/popuo-box.css" rel="stylesheet" type="text/css" property="" media="all" />
						<script src="/PCTEM/scripts/krcepl_website/jquery.magnific-popup.js" type="text/javascript"></script>
					<!--//pop-up-box -->
					<div id="small-dialog" class="mfp-hide">
						<iframe src="https://player.vimeo.com/video/33654241"></iframe>
					</div>

					<script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
					</script>
			</div>
		</div>
	</div>
<!-- //about-bottom -->
<!-- services -->
	<div class="services" id="services">
		<div class="container">
			<h3>Our Clients</h3>
            <p class="qui"><span></span></p>
            <br />
            <div class="list-group">
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  SKM Animal Feeds & Foods (India) Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  IP Rings Limited</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Thanga Prataph Spinning Mills (P) Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Stanadyne Amalgamations (P) Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Brooke Fields Estate (P) Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Plaza Maintenance & Service Limited</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  SP Superfine Cotton Mills (P) Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Addison & Co Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  K.A.Ramaiah Reddy</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  Bhourkha Park Ltd</a>
                <a href="#" class="list-group-item"><span class="glyphicon glyphicon-ok-circle" style="color:green;font-size:20px"></span>  M/S FORGEPRO INDIA (P) Ltd</a>
            </div> 
		</div>
	</div>
<!-- //services -->
<!-- team -->
	<div class="team" id="team">
		<div class="container">
			<h3>About Managing Director</h3>
            <p class="qui"><span></span></p>
			<div class="team-grids" style="text-align:center">
				<div class="col-md-12 team-grid">
					<div class="pic">
						<div class="stack twisted">	
							<img src="/PCTEM/scripts/krcepl_website/images/secretary.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="pic-caption top-to-bottom">
							<h5 class="pic-title">Dr.K.Ramakrishnan</h5>
							<p>Managing Director</p>
						</div>
					</div>
					<h4>Dr.K.Ramakrishnan B.E.,</h4>
					<p>Managing Director</p>
				</div>
				
				<div class="clearfix"> </div>
			</div>
            <p><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr.K.Ramakrishnan was born on 2oth December 1966 in Thalavapalayam, Karur District. He finished his schooling at Kandasamy Kander Higher Secondary School, Paramathi Velur. He did his under graduation in Production Engineering at Annamalai University in 1984-88.He is the part of management in the following Trust/Company.</p>
            <br />
            <ul class="list-group">
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  <a href="http://www.mkce.ac.in">M.Kumarasamy College of Engineering</a></li>
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  <a href="http://www.krce.ac.in">K.Ramakrishnan College of Enginnering</a></li>
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  <a href="http://www.krct.ac.in">K. Ramakrishnan College of Technology</a></li>
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  Ramkrishna Poultry Farm</li>
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  Ramakrishna Poultry Farm (P) Ltd</li>
              <!--   <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  K.Ramakrishnan Clean Energy Pvt. Ltd.</li>
                <li class="list-group-item"><span class="glyphicon glyphicon-record" style="font-size:15px;color:blue;"></span>  KRV Green Pvt. Ltd.</li> -->
            </ul> 

		</div>
	</div>
<!-- //team -->
<!-- gallery -->
	<div class="gallery" id="gallery">
		<div class="container">
			<h3>Gallery</h3>
			<p class="qui"><span></span></p>
			<div class="gallery-grids">
				<div class="col-md-6 gallery-grids-left">
					<div class="gallery-grid">
						<img src='/PCTEM/scripts/krcepl_website/images/8.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/8.jpg' alt=" " />
						<p  style='opacity:0;display:none'></p>
					
					</div>
					<div class="gallery-grids-left-sub">
						<div class="col-md-6 gallery-grids-left-subl">
							<div class="gallery-grid">
								<img src='/PCTEM/scripts/krcepl_website/images/14.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/14-.jpg' alt=" " />
								<p  style='opacity:0;display:none'></p>
							</div>
							<div class="gallery-grid gallery-grid-sub">
								<img src='/PCTEM/scripts/krcepl_website/images/11.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/11-.jpg' alt=" " />
								<p  style='opacity:0;display:none'></p>
							</div>
						</div>
						<div class="col-md-6 gallery-grids-left-subr">
							<div class="gallery-grid">
								<img src='/PCTEM/scripts/krcepl_website/images/12.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/12-.jpg' alt=" " />
								<p  style='opacity:0;display:none'></p>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6 gallery-grids-left">
					<div class="col-md-6 gallery-grids-right">
						<div class="gallery-grid">
							<img src='/PCTEM/scripts/krcepl_website/images/10.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/10-.jpg' alt=" " />
							<p  style='opacity:0;display:none'></p>
						</div>
					</div>
					<div class="col-md-6 gallery-grids-right">
						<div class="gallery-grid">
							<img src='/PCTEM/scripts/krcepl_website/images/9.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/9-.jpg' alt=" " />
							<p  style='opacity:0;display:none'></p>
						</div>
					</div>
					<div class="clearfix"> </div>
					<div class="gallery-grids-right1">
						<div class="gallery-grid">
							<img src='/PCTEM/scripts/krcepl_website/images/13.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/13.jpg' alt=" " />
							<p  style='opacity:0;display:none'></p>
						</div>
					</div>
					<div class="col-md-6 gallery-grids-right">
						<div class="gallery-grid">
							<img src='/PCTEM/scripts/krcepl_website/images/15.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/15-.jpg' alt=" " />
							<p  style='opacity:0;display:none'></p>
						</div>
					</div>
					<div class="col-md-6 gallery-grids-right">
						<div class="gallery-grid">
							<img src='/PCTEM/scripts/krcepl_website/images/16.jpg' data-big-src='/PCTEM/scripts/krcepl_website/images/16-.jpg' alt=" " />
							<p  style='opacity:0;display:none'></p>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			   <div class="clearfix"> </div>
			    <script type='text/javascript' src='/PCTEM/scripts/krcepl_website/jquery.easy-gallery.js' ></script>
				<script type='text/javascript'>
				  //init Gallery
				  $('.gallery').easyGallery();
				</script>
			</div>
		</div>
	</div>
<!-- //gallery -->
<!-- counting -->
	<div class="counting">
		<div class="container">
			<!--<div class="counting-grids">
				<div class="col-md-3 counting-grid">
					<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
					<h3>Users</h3>
					<p class="counter">89,147</p> 
				</div>
				<div class="col-md-3 counting-grid">
					<i class="glyphicon glyphicon-arrow-up" aria-hidden="true"></i>
					<h3>Support</h3>
					<p class="counter">45,436</p> 
				</div>
				<div class="col-md-3 counting-grid">
					<i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>
					<h3>Creative Design</h3>
					<p class="counter">66,546</p> 
				</div>
				<div class="col-md-3 counting-grid">
					<i class="glyphicon glyphicon-usd" aria-hidden="true"></i>
					<h3>Profit</h3>
					<p class="counter">43,865</p> 
				</div>
				<div class="clearfix"> </div>
				<script src="js/waypoints.min.js"></script> 
				<script src="js/counterup.min.js"></script> 
				<script>
					jQuery(document).ready(function( $ ) {
						$('.counter').counterUp({
							delay:30,
							time:1000
						});
					});
				</script>
			</div>-->
		</div>
	</div>
<!-- //counting -->
<!-- mail -->
	<div class="mail" id="mail">
		<div class="container">
			<h3>Mail Us</h3>
			<p class="qui"><span></span></p>
			<div class="mail-grids">
				<div class="col-md-4 mail-grid-left">
					<h4>Contact Info</h4>
					<ul>
						<li><i class="glyphicon glyphicon-home" aria-hidden="true"></i>Visit Us<span>127-128,Main Road,Thalavapalyam,<br />Karur - 639 113,Tamil Nadu, India.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Mail Us<a href="mailto:kramakrishnancleanenergy@gmail.com">kramakrishnancleanenergy@gmail.com, krvgreen@gmail.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Call Us<span>+91 9842498377,+91 9842881377</span></li>
						<li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>Working Time<span> 24 X 7</span></li>
					</ul>
				</div>
				<div class="col-md-8 mail-grid-right">
					<form method="post" id="Message">
						<h5>Your Name*</h5>
						<input type="text" placeholder="Name" name="Name" required>
                        <h5>Your Contact No.*</h5>
                        <input type="text" placeholder="Contact No." name="contact" required>
                        <h6>Your Email*</h6>
						<input type="email" placeholder="E-mail Id" name="Email" required>
						<h5>Subject*</h5>
						<input type="text" placeholder="Subject" name="Subject" required>
						<h6>Your Message*</h6>
						<textarea name="Message" placeholder="Message..." required=""></textarea>
						<input type="submit" value="Submit Form" >
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="map">
        <iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&height=400&hl=en&coord=11.054935,78.04807600000004&q=K.Kumarasamy%20College%20of%20Engineering+(MKCE%20COE)&ie=UTF8&t=h&z=14&iwloc=B&output=embed"></iframe>
	</div>
<!-- //mail -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<ul class="footer-nav">
				<li><a href="#services" class="scroll">Our Clients</a></li>
				<li><a href="#gallery" class="scroll">Gallery</a></li>
				<li><a href="#team" class="scroll">About MD</a></li>
				<li><a href="#mail" class="scroll">Mail Us</a></li>
			</ul>
			<h3><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>127-128,Main Road,Thalavapalyam,Karur - 639 113<span><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+91 9842498377,+91 9842881377</span></h3>
			<div class="mail-foot">
				<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:kramakrishnancleanenergy@gmail.com">kramakrishnancleanenergy@gmail.com, krvgreen@gmail.com</a>
			</div>
		</div>
		<div class="copy-right">
			<p>© 2018 KRCEPL. All rights reserved | Designed by KRG Portal Team</p>
		</div>
	</div>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="/PCTEM/scripts/krcepl_website/bootstrap.js"></script>
	<script src="/krg/myjs/krcepl/index.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>