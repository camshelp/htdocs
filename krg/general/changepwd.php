<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>KRG Portal - Update Password</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl" style="background:#33c9dc;"> 
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
            <md-card>
                <md-card-title   class="card-content white-text" style="background-color:purple;">
                <md-card-title-text>
                    <span> <a ng-click="krg_home()"><i class="material-icons" style="font-size:30px;">home</i></a><span>
                    <span class="md-headline">KRG Portal - Password Change</span>
                </md-card-title-text>
                </md-card-title>
                <md-card-actions layout="row" layout-align="start center">

                </md-card-actions>
                <md-card-content class="card-content">
                 <form enctype="multipart/form-data" method="post" id="updatepwd" >
                 <div class="row">
                    <md-input-container>
                        <label>Current Password</label>
                        <input name="cpass" type="password" id="cpass" ng-model="cpass" required  md-maxlength="10" minlength="6">
                        <div ng-messages="cpass.$error" ng-show="cpass.$dirty">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                    </div>
                    </md-input-container>
                </div>
                <div class="row">
                    <md-input-container>
                        <label>New Password</label>
                        <input name="npass" type="password" id="npass" ng-model="npass" required  md-maxlength="10" minlength="6">
                        <div ng-messages="npass.$error" ng-show="npass.$dirty">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                        </div>
                    </md-input-container>
                </div>
                <div class="row">
                    <md-input-container>
                        <label>Confirm New Password</label>
                        <input name="ncpass" type="password" id="ncpass" ng-model="ncpass" required  md-maxlength="10" minlength="6">
                        <div ng-messages="ncpass.$error" ng-show="ncpass.$dirty">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                    </div>
                    </md-input-container>
                    <span style="color:red;">{{comresult}}</span>
                </div>
                <div class="row" style="text-align:right;">
                    <button class="waves-effect btn purple" type="submit" name="action">Change Password<i class="material-icons">send</i></button>
                </div>
                 </form>
             </md-card-content>
    </md-card>
            </div>
            <div class="col-sm-3"></div>
        </div>
  <script src="../myjs/general/pwdchange.js"></script>
</body>
</html>