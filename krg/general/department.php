<?php

require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");

$conn = database_open();
$sql1 = "select count(*) from general.department_list";
                    $stmt = $conn->prepare($sql1); 
                    $stmt->execute();
                    $row = $stmt->fetchColumn(0);
                    $add_value="1";
                    $total=(integer)$row+(integer)$add_value;
                    $dept_id="DEPT".SixDigitIdGenerator("$total");
$group_id=$_POST["gname"];
$graduation_type=$_POST["graduation"];
$degree_name=$_POST["degree"];
$course_name=$_POST["course"];
$acronym=$_POST["acronym"];
$sem_type=$_POST["sem"];
$min_duration=$_POST["min"];
$max_duration=$_POST["max"];
$no_semester=$_POST["nosem"];
$board_name=$_POST["board"];
$register_code=$_POST["register"];
$intake=$_POST["intake"];
$starting_year=$_POST["start"];
$is_academic_type=$_POST["type"];
$display_order=$_POST["display"];
$faculty_code=$_POST["facultycode"];
if(isset($_POST['sub_fid'])){  $head_id = $_POST['sub_fid']; }else{echo alert("Responsible Person Not Assigned was not posted");}
if(isset($_POST['type'])){  $is_academic_type=$_POST["type"]; }else{$is_academic_type="no";}

session_start();
$updated_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
$updated_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);

	 //echo "Entered...";
	 $sql = "INSERT INTO general.department_list (group_id,dept_id,graduation_type,degree_name,course_name,acronym,sem_type,min_duration,max_duration,no_semester,
	 board_name,register_code,intake,starting_year,is_academic_type,display_order,faculty_code,head_id,updated_by,updated_session,updated_time) VALUES 
	 (:gi,:di,:gt,:dn,:cn,:ac,:st,:min,:max,:ns,:bn,:rc,:it,:sy,:ist,:do,:fc,:hi,:ub,:us,:ut)";
	 
	$stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':gi',$group_id);
                        $stmt->bindParam(':di',$dept_id);
                        $stmt->bindParam(':gt',$graduation_type);
                        $stmt->bindParam(':dn',$degree_name);
                        $stmt->bindParam(':cn',$course_name);
                        $stmt->bindParam(':ac',$acronym);
                        $stmt->bindParam(':st',$sem_type);
                        $stmt->bindParam(':min',$min_duration);
                        $stmt->bindParam(':max',$max_duration);
                        $stmt->bindParam(':ns',$no_semester);
                        $stmt->bindParam(':bn',$board_name);
                        $stmt->bindParam(':rc',$register_code);
                        $stmt->bindParam(':it',$intake);
                        $stmt->bindParam(':sy',$starting_year);
                        $stmt->bindParam(':ist',$is_academic_type);
                        $stmt->bindParam(':do',$display_order);
                        $stmt->bindParam(':fc',$faculty_code);
                        $stmt->bindParam(':hi',$head_id);
                        $stmt->bindParam(':ub', $updated_by);
                        $stmt->bindParam(':us', $updated_session);
                        $stmt->bindParam(':ut', date("Y-m-d h:i:sa"));
									
						if($stmt-> execute()==TRUE)
                        {
                           //echo "The Division Information Submitted Successfully";
							echo "<script>alert('The Department register Updated Successfully');window.location='institution.php';</script>";
                        }
						else{ 
							//echo "The Division Name was not Submitted";
							echo "<script>alert('The Department register was not Updateded');window.location='institution.php';</script>";

						}
			
		database_close($conn);
			
    ?>