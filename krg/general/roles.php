<?php  
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Roles & Responsibilities</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    <link href="/PCTEM/scripts/timeline.css" rel="stylesheet" />
    <style>
        md-slider[md-discrete] .md-sign
        {  
            opacity: 1;
          -webkit-transform: translate3d(0, 0, 0) scale(1);
           transform: translate3d(100, 100, 100) scale(1);
        }
        md-slider[md-discrete] .md-sign:after {
        opacity: 1;
        -webkit-transform: translate3d(0, 0, 0) scale(1);
        transform: translate3d(0, 0, 0) scale(1);
        }
    </style>
    <style type="text/css">
    body {
        color: #566787;
        background: #f5f5f5;
		font-family: 'Roboto', sans-serif;
	}
	.table-wrapper {
        width: 850px;
        background: #fff;
        padding: 20px 30px 5px;
        margin: 30px auto;
        box-shadow: 0 0 1px 0 rgba(0,0,0,.25);
    }
	.table-title .btn-group {
		float: right;
	}
	.table-title .btn {
		min-width: 50px;
		border-radius: 2px;
		border: none;
		padding: 6px 12px;
		font-size: 95%;
		outline: none !important;
		height: 30px;
	}
    .table-title {
		border-bottom: 1px solid #e9e9e9;
		padding-bottom: 15px;
		margin-bottom: 5px;
		background: rgb(0, 50, 74);
		margin: -20px -31px 10px;
		padding: 15px 30px;
		color: #fff;
    }
    .table-title h2 {
		margin: 2px 0 0;
		font-size: 24px;
	}
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
		padding: 12px 15px;
		vertical-align: middle;
    }
	table.table tr th:first-child {
		width: 40px;
	}
	table.table tr th:last-child {
		width: 100px;
	}
    table.table-striped tbody tr:nth-of-type(odd) {
    	background-color: #fcfcfc;
	}
	table.table-striped.table-hover tbody tr:hover {
		background: #f5f5f5;
	}
    table.table td a {
        color: #2196f3;
    }
	table.table td .btn.manage {
		padding: 2px 10px;
		background: #37BC9B;
		color: #fff;
		border-radius: 2px;
	}
	table.table td .btn.manage:hover {
		background: #2e9c81;		
	}
</style>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">Roles and Responsibilities</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
            <div class="row">
			<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  $conn = database_open();
  $user=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
		$sql="SELECT general.staff_basic_info.roles FROM general.staff_basic_info WHERE general.staff_basic_info.staff_id='$user'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row)
        {
			while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
       
  ?>
                    <p> <?php echo nl2br($row['roles']); ?></p>
		
		<?php
			}
		}
		?>
		
                    </div>
        </md-card-content>
    </md-card>
        
  <script src="../myjs/general/work.js"></script>
</body>
</html>