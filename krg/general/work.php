<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Task Progress Updation</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    <link href="/PCTEM/scripts/timeline.css" rel="stylesheet" />
    <style>
        md-slider[md-discrete] .md-sign
        {  
            opacity: 1;
          -webkit-transform: translate3d(0, 0, 0) scale(1);
           transform: translate3d(100, 100, 100) scale(1);
        }
        md-slider[md-discrete] .md-sign:after {
        opacity: 1;
        -webkit-transform: translate3d(0, 0, 0) scale(1);
        transform: translate3d(0, 0, 0) scale(1);
        }
    </style>
    <style type="text/css">
    body {
        color: #566787;
        background: #f5f5f5;
		font-family: 'Roboto', sans-serif;
	}
	.table-wrapper {
        width: 850px;
        background: #fff;
        padding: 20px 30px 5px;
        margin: 30px auto;
        box-shadow: 0 0 1px 0 rgba(0,0,0,.25);
    }
	.table-title .btn-group {
		float: right;
	}
	.table-title .btn {
		min-width: 50px;
		border-radius: 2px;
		border: none;
		padding: 6px 12px;
		font-size: 95%;
		outline: none !important;
		height: 30px;
	}
    .table-title {
		border-bottom: 1px solid #e9e9e9;
		padding-bottom: 15px;
		margin-bottom: 5px;
		background: rgb(0, 50, 74);
		margin: -20px -31px 10px;
		padding: 15px 30px;
		color: #fff;
    }
    .table-title h2 {
		margin: 2px 0 0;
		font-size: 24px;
	}
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
		padding: 12px 15px;
		vertical-align: middle;
    }
	table.table tr th:first-child {
		width: 40px;
	}
	table.table tr th:last-child {
		width: 100px;
	}
    table.table-striped tbody tr:nth-of-type(odd) {
    	background-color: #fcfcfc;
	}
	table.table-striped.table-hover tbody tr:hover {
		background: #f5f5f5;
	}
    table.table td a {
        color: #2196f3;
    }
	table.table td .btn.manage {
		padding: 2px 10px;
		background: #37BC9B;
		color: #fff;
		border-radius: 2px;
	}
	table.table td .btn.manage:hover {
		background: #2e9c81;		
	}
</style>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">Task Information</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
            <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:indigo;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Assigned to Others</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{assigned_others}}</span>
                              <a  ng-click="NewTask('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: red;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment_late</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Pending by Others</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{pending_others}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:green;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment_turned_in</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Completed by Others</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{completed_others}}</span>
                         </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: #5393ff;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">assignment_ind</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Assigned to You</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{assigned_own}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:#f73378;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">inbox</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Pending by You</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{pending_own}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: #00e676;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">check_circle</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Task Completed by You</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{completed_own}}</span>
                         </div>
                        </div>
                    </div>
            </div>
                    <div class="row" style="text-align:left;">
                        <md-radio-group layout="row" layout-sm="column" layout-align="center center" layout-wrap ng-model="tstat" ng-required="true">
                            <md-radio-button ng-model="tstat" ng-value="All">All</md-radio-button> 
                             <md-radio-button ng-model="tstat" ng-repeat="d in work_status" style="height:30px;width:120px;vertical-align:middle;" ng-style="set_color(d.name)" ng-value="d.name">{{d.name}}</md-radio-button> 
                             <input type="text" ng-model="tstat" hidden> 
                        </md-radio-group>  
                      </div>
                   
                    <table id="task_list" class="collection table table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Work Name</th>
                            <th>Descripton</th>
                            <th>Responsible Person</th>
                            <th>Deadline Date</th>
                            <th>Task Status</th>
                            <th class="all">Status</th>
                        </tr>  
                        </thead>
                        <tbody>
                        <tr ng-repeat="user in task | filter:{ task_status: tstat}" style="border:1px;" ng-style="set_color(user.task_status);" class="collection-item avatar">
                             <td>{{ user.work_name }}</td>
                             <td>{{ user.description }}</td> 
                             <td>{{ user.responsibile}}</td>
                             <td>{{ user.deadline_date }}</td>
                             <td>{{ user.task_status }}</td>
                              <td>
                                <span ng-if="user.astatus!='Approve'">{{user.astatus}}</span>
                                <button ng-if="IsApproved(user.task_id)" class="w3-btn w3-ripple" ng-click="Approve(user.task_id)"><span class="glyphicon glyphicon-ok-circle"></span>Approve</button>
                                <md-button ng-if="user.estatus!='Yes'" ng-click="EditTask(user.task_id)" ng-click="EditDept(user.dept_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</md-button>
                                <md-button ng-click="ViewTimeline(user.task_id);" style="width:100%;text-align:left;" class="waves-effect btn blue material-icons"><i class="material-icons">insert_invitation</i> Timeline</md-button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
        </md-card-content>
    </md-card>

    <md-card ng-show="update_task">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span> <a ng-click="registration_list1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Task Status Updation</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
                            <div hidden>
                                <input name="Tid" id="Tid" ng-model="Tid" readonly>
                                <input name="sub_fid" id="sub_fid" ng-model="sub_fid" readonly>
                            </div>

                        <md-card>
                            <md-card-title   class="card-content white-text" style="background-color:#33bfff;">
                            <md-card-title-text>
                                <span class="md-headline">Sub-Task Management</span>
                            </md-card-title-text>
                            </md-card-title>
                            <md-card-actions layout="row" layout-align="start center">
                            </md-card-actions>
                            <md-card-content class="card-content">
                            <form enctype="multipart/form-data" method="post" id="subtask" >
                                         <md-input-container>
                                                        <label>Sub-Task Name</label>
                                                        <input name="subname" id="subname" ng-model="subname" required md-maxlength="300" minlength="4">
                                                        <div ng-messages="subname.$error" ng-show="subname.$dirty">
                                                        <div ng-message="required">This is required!</div>
                                                        <div ng-message="md-maxlength">That's too long!</div>
                                                        <div ng-message="minlength">That's too short!</div>
                                                        </div>
                                            </md-input-container>
                                            <div>
                                                <md-input-container>
                                                            <label>Sub-Task Description</label>
                                                            <input name="subdesc" id="subdesc" ng-model="subdesc" required md-maxlength="300" minlength="4">
                                                            <div ng-messages="subdesc.$error" ng-show="subdesc.$dirty">
                                                                <div ng-message="required">This is required!</div>
                                                                <div ng-message="md-maxlength">That's too long!</div>
                                                                <div ng-message="minlength">That's too short!</div>
                                                            </div>
                                                </md-input-container>
                                            </div>
                                             <div class="row">
                                                <div class="col-sm-7">
                                                    <md-input-container>
                                                                <label>Responsible Person</label>
                                                                <input name="subresp" id="subresp" ng-model="subresp" required md-maxlength="300" minlength="4"  ng-disabled="true" required>
                                                                <div ng-messages="subresp.$error" ng-show="subresp.$dirty">
                                                                    <div ng-message="required">This is required!</div>
                                                                    <div ng-message="md-maxlength">That's too long!</div>
                                                                    <div ng-message="minlength">That's too short!</div>
                                                                </div>
                                                    </md-input-container>
                                                    </div>
                                                    <div class="col-sm-5">
                                                    <md-button ng-click="head_view('subtask')" class="md-secondary"><span class="glyphicon glyphicon-edit"></span>Choose</md-button>
                                                    </div>
                                            </div>
                                            <div>
                                                   <md-input-container>
                                                    <label>Work Nature</label>
                                                    <md-select ng-model="subtype" id="subtype" name="subtype" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                                    <md-select-header class="demo-select-header">
                                                        <input ng-model="searchTerm" placeholder="Search for Work Nature.." class="demo-header-searchbox md-text" type="search">
                                                    </md-select-header>
                                                    <md-optgroup label="vegetables">
                                                        <md-option ng-value="inst.name" ng-model="subtype"  ng-repeat="inst in subnature | filter:searchTerm">{{inst.name}}</md-option>
                                                    </md-optgroup>
                                                    </md-select>
                                                    </md-input-container>
                                            </div>
                                            <div>
                                                <label>Expected Date of Completion</label>
                                                <div class="input-group date form_date col-md-5" id="subdate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dead_sub" data-link-format="dd-mm-yyyy">
                                                      <input ng-model="sub_dead" class="form-control" size="16" type="text" value="" readonly required>
                                                      <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                 </div>
                                                 <input ng-model="sub_dead" name="sub_dead" id="dead_sub" value="" required hidden/>
                                            </div>
                                            <div style="text-align:right;padding-right:50px;">
                                                <button class="waves-effect btn pink" type="submit" name="action" id="register_sub">Create Sub-Task<i class="material-icons">send</i></button>
                                            </div>
                                        </form>
                                        <div>
                                        <table id="subtask_list" class="table table-striped table-bordered" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Descripton</th>
                                                    <th>Responsible Person</th>
                                                    <th>Deadline Date</th>
                                                    <th>% Completed</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="user in subtask">
                                                    <td>{{ user.work_name }}</td>
                                                    <td>{{ user.description }}</td>
                                                    <td>{{ user.responsibile}}</td>
                                                    <td>{{ user.deadline_date }}</td>
                                                    <td>{{ user.finished_percentage }}</td>
                                                    <td>
                                                        <span ng-if="user.astatus!='Approve'">{{user.astatus}}</span>
                                                       <!--  <button ng-if="IsApproved(user.task_id)" class="w3-btn w3-ripple" ng-click="Approve(user.task_id)"><span class="glyphicon glyphicon-ok-circle"></span>Approve</button>
                                                        <button ng-if="user.estatus=='Yes'" class="w3-btn w3-ripple" ng-click="EditTask(user.task_id)"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                                        <button ng-if="user.estatus=='Yes'" class="w3-btn w3-ripple" ng-click="EditDate(user.task_id)"><span class="glyphicon glyphicon-edit"></span>Revise Date</button> -->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                            </md-card-content>
                        </md-card>
                        <md-card>
                            <md-card-title   class="card-content white-text" style="background-color:#33eb91;">
                            <md-card-title-text>
                                <span class="md-headline">Task Current Status Updation</span>
                            </md-card-title-text>
                            </md-card-title>
                            <md-card-actions layout="row" layout-align="start center">
                            </md-card-actions>
                            <md-card-content class="card-content">
                            <form enctype="multipart/form-data" method="post" id="taskupdate" >
                                            <div>
                                                 <md-input-container>
                                                        <label>Description about Current Status</label>
                                                        <input name="CDesc" id="CDesc" ng-model="CDesc" required md-maxlength="300" minlength="4">
                                                        <div ng-messages="CDesc.$error" ng-show="CDesc.$dirty">
                                                            <div ng-message="required">This is required!</div>
                                                            <div ng-message="md-maxlength">That's too long!</div>
                                                            <div ng-message="minlength">That's too short!</div>
                                                        </div>
                                                    </md-input-container>
                                            </div>
                                            <div layout="">
                                                <div flex="1" layout="" layout-align="center center">
                                                    <span class="md-body-1">Completed Percentage</span>
                                                </div>
                                                <md-slider flex="" class="md-primary" md-discrete="" ng-model="WPer" step="1" min="0" max="100" aria-label="rating">
                                                </md-slider><span>{{WPer}}%</span>
                                                <input name="WPer" ng-model="WPer" hidden readonly/>
                                            </div>
                                            <div>
                                                <md-radio-group ng-model="stat" name="stat" ng-required="true" layout="row">
                                                    <md-radio-button ng-model="stat" ng-repeat="d in work_status" ng-value="d.name">{{d.name}}</md-radio-button> 
                                                </md-radio-group>
                                                <input type="text" ng-model="stat" name="stat" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                                <div ng-messages="stat.$error" role="alert">
                                                    <div ng-message="required">Status is required.</div>
                                                </div>
                                                <div  style="text-align:right;padding-right:10px;">
                                                      <button class="waves-effect btn pink" type="submit" name="action" id="register_inst">Update<i class="material-icons">send</i></button>
                                                </div>
                                             </div>
                                            <div><label>Attachment</label></div>
                                            <div>
                                            <input id="file_to_upload" type="file" placeholder="Do you have a .pdf?" accept=".pdf" name="file_to_upload">
                                            </div>
                                </form>    
                            </md-card-content>
                        </md-card>
                        <md-card>
                            <md-card-title   class="card-content white-text" style="background-color:#ffa733;">
                            <md-card-title-text>
                                <span class="md-headline">Task Revision</span>
                            </md-card-title-text>
                            </md-card-title>
                            <md-card-actions layout="row" layout-align="start center">
                            </md-card-actions>
                            <md-card-content class="card-content">
                            <form enctype="multipart/form-data" method="post" id="revisetask" >
                                                 <div>
                                                     <md-input-container>
                                                            <label>Reason for Extend the Deadline</label>
                                                            <input name="reason" id="reason" ng-model="reason" required md-maxlength="200" minlength="4">
                                                            <div ng-messages="reason.$error" ng-show="reason.$dirty">
                                                            <div ng-message="required">This is required!</div>
                                                            <div ng-message="md-maxlength">That's too long!</div>
                                                            <div ng-message="minlength">That's too short!</div>
                                                            </div>
                                                     </md-input-container>
                                                </div>
											<div>
                                    <md-input-container>
                                        <label>Work Name</label>
                                        <input name="WName" id="WName" ng-model="WName" required md-maxlength="200" minlength="4">
                                        <div ng-messages="WName.$error" ng-show="WName.$dirty">
                                        <div ng-message="required">This is required!</div>
                                        <div ng-message="md-maxlength">That's too long!</div>
                                        <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container>
                                        <label>Work Description</label>
                                        <input name="WDesc" id="WDesc" ng-model="WDesc" required  md-maxlength="300" minlength="4">
                                        <div ng-messages="WDesc.$error" ng-show="WDesc.$dirty">
                                        <div ng-message="required">This is required!</div>
                                        <div ng-message="md-maxlength">That's too long!</div>
                                        <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container>
                                    <label>Work Nature</label>
                                    <md-select ng-model="wtype" id="wtype" name="wtype" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Work Nature.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.name" ng-model="wtype"  ng-repeat="inst in wnature | filter:searchTerm">{{inst.name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                    </md-input-container>
                            </div>
                            <div class="row">
                                             <div class="col-sm-7">
                                                <md-input-container>
                                                            <label>Responsible Person</label>
                                                            <input name="taskresp" id="taskresp" ng-model="taskresp" required md-maxlength="300" minlength="4"  ng-disabled="true" required>
                                                            <div ng-messages="taskresp.$error" ng-show="taskresp.$dirty">
                                                                <div ng-message="required">This is required!</div>
                                                                <div ng-message="md-maxlength">That's too long!</div>
                                                                <div ng-message="minlength">That's too short!</div>
                                                            </div>
                                                </md-input-container>
                                                </div>
                                                <div class="col-sm-5">
                                                <md-button ng-click="head_view('task')" ng-show="user_type=='yes'" class="md-secondary"><span class="glyphicon glyphicon-edit"></span>Choose</md-button>
                                                </div>
                                            </div>
                                                <div class="row">
                                                <div class="col-lg-6" flex-gt-xs>
                                                    <div class="form-group">
                                                        <label>Expected Date of Completion</label>
                                                        <div class="input-group date form_date col-md-5" id="revdate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_inputre" data-link-format="dd-mm-yyyy">
                                                            <input ng-model="rdead" id="rdead" class="form-control" size="16" type="text" value="" readonly required>
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        </div>
                                                        <input ng-model="rdead" name="rdead" id="dtp_inputre" value="" required hidden/>
                                                    </div>
                                                    <div class="col-lg-6"  style="text-align:right;padding-right:50px;">
                                                         <button class="waves-effect btn pink" type="submit" name="action" id="revise_task1">Revise<i class="material-icons">send</i></button>
                                                    </div>
                                                    </div>
                                                </div>
                                     </form>
                            </md-card-content>
                        </md-card>
        </md-card-content>
    </md-card>
    <md-card ng-show="registervisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Task Registration</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <form enctype="multipart/form-data" method="post" id="taskRegister" >
                            <div hidden>
                                <input name="task_fid" id="task_fid" ng-model="task_fid" readonly>
                            </div>
                            <div>
                                    <md-input-container>
                                        <label>Work Name</label>
                                        <input name="WName" id="WName" ng-model="WName" required md-maxlength="200" minlength="4">
                                        <div ng-messages="WName.$error" ng-show="WName.$dirty">
                                        <div ng-message="required">This is required!</div>
                                        <div ng-message="md-maxlength">That's too long!</div>
                                        <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container>
                                        <label>Work Description</label>
                                        <input name="WDesc" id="WDesc" ng-model="WDesc" required  md-maxlength="300" minlength="4">
                                        <div ng-messages="WDesc.$error" ng-show="WDesc.$dirty">
                                        <div ng-message="required">This is required!</div>
                                        <div ng-message="md-maxlength">That's too long!</div>
                                        <div ng-message="minlength">That's too short!</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container>
                                    <label>Work Nature</label>
                                    <md-select ng-model="wtype" id="wtype" name="wtype" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Work Nature.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.name" ng-model="wtype"  ng-repeat="inst in wnature | filter:searchTerm">{{inst.name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                    </md-input-container>
                            </div>
                            <div class="row">
                                             <div class="col-sm-7">
                                                <md-input-container>
                                                            <label>Responsible Person</label>
                                                            <input name="taskresp" id="taskresp" ng-model="taskresp" required md-maxlength="300" minlength="4"  ng-disabled="true" required>
                                                            <div ng-messages="taskresp.$error" ng-show="taskresp.$dirty">
                                                                <div ng-message="required">This is required!</div>
                                                                <div ng-message="md-maxlength">That's too long!</div>
                                                                <div ng-message="minlength">That's too short!</div>
                                                            </div>
                                                </md-input-container>
                                                </div>
                                                <div class="col-sm-5">
                                                <md-button ng-click="head_view('task')" ng-show="user_type=='yes'" class="md-secondary"><span class="glyphicon glyphicon-edit"></span>Choose</md-button>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col-lg-6" flex-gt-xs style="position:relative">
                                        <label>Expected Date of Completion</label>
                                        <div class="input-group date form_date col-md-5" id="expdate" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                                            <input ng-model="dead" class="form-control" size="16" type="text" value="" readonly required>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input ng-model="dead" name="dead" id="dtp_input2" value="" required hidden/>
                                    </div>
                                    <div  style="text-align:right;padding-right:10px;">
                                         <button class="waves-effect btn pink" type="submit" name="action" id="register_inst">Submit<i class="material-icons">send</i></button>
                                    </div>
                            </div>
                        </form>
        </md-card-content>
    </md-card>

 <md-card ng-show="timeline_task">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="timeline_back()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Task Timeline</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <!--  <header>
                            <nav>
                                <ul id="main_nav">
                                            <li class="active all" ng-click="toggleAllActivities()">All</li>
                                            <li ng-repeat="activity in activities" ng-class="[activity.class, activity.active]" ng-click="selectActivity(activity)">{{activity.text}}</li>
                                </ul>
                                <ul id="nav_ctrl">
                                        <li class="icon-up-open" data-title="Next"></li>
                                        <li class="icon-down-open" data-title="Previous"></li>
                                </ul>
                            </nav>
                        </header> -->
                        <div>
                            <div class="container" id="timeline_container">
                                <div class="page-header">
                                    <h1 id="timeline">{{disp_task_name}}</h1>
                                    <h4 id="timeline">Assigned To :{{responsibile}}</h4>
                                </div>
                                <ul class="timeline">
                               <!--  ng-class-even="'timeline-inverted'" -->
                                    <li ng-repeat="event in active_events | filter:isTypeActive(event)">
                                        <div class="timeline-badge" ng-class="event.type">
                                            <i class="glyphicon" ng-class="event.glyphicon"></i>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title">{{event.title}}</h4>
                                            </div>
                                            <div class="timeline-body">
                                                <p>{{event.text}}</p>
                                                <p>By : {{event.updated_by}}</p>
                                            </div>
                                                <p ng-if="event.download!=''"><small class="text-muted"><a ng-click="download_document(event.download,event.document_name)">{{event.download}}</a></small></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
        </md-card-content>
    </md-card>
    <md-card ng-show="head_update">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="back_to_home()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Responsible Person Update</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div hidden>
                            <input name="select_type" id="select_type" ng-model="select_type" readonly>
                        </div>
                        <div>
                        <table id="staff_list" class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Staff Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Mobile No.</th>
                                    <th class="all"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in faculty">
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ user.name }}</td>
                                    <td>{{ user.designation }}</td>
                                    <td>{{ user.department }}</td>
                                    <td>{{ user.mobile }}</td>
                                    <td>
                                        <md-button  ng-click="Fixhead(user.faculty_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">assignment_turned_in</i>Assign</md-button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
        </md-card-content>
    </md-card>
  <script src="../myjs/general/work.js"></script>
</body>
</html>