<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Faculty Management</title>
    <?php require_once ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">Staff Management</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Company</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_company}}</span>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4" ng-show="dept_button">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">group</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Dept in Choosed Company</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_dept}}</span>
                             
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4" ng-show="add_button">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:indigo;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">group_add</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Staff in Choosed Dept</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_dept}}</span>
                              <a  ng-click="EditFaculty('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>Department</a>
                         </div>
                        </div>
                    </div>
            </div>
        
        <div class="row">
                        <div class="col-lg-5">
                                <md-input-container>
                                <label>Company Name</label>
                                <md-select ng-model="gname" id="gname" name="gname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Company Name.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.group_id" ng-model="gname" ng-click="option_click(inst.group_id)" ng-repeat="inst in institution | filter:searchTerm">{{inst.group_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                        </div>
                        <div class="col-lg-4">
                                <md-input-container>
                                <label>Department Name</label>
                                <md-select ng-model="dname" id="dname" name="dname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Department Name.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.dept_id" ng-model="dname" ng-click="option_click1(inst.dept_id)" ng-repeat="inst in dept | filter:searchTerm">{{inst.course_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                        </div>
                    </div>
                    <div>
                    <table id="data_list" class="table table-striped table-bordered" width="100%">
                      <thead>
                        <tr>
                            <!--  <th></th> -->
                             <th>Staff Name</th>
                             <th>Designation</th>
                             <th>Date of Joining</th>
                             <th>Date of Birth</th>
                             <th class="all">Action</th>
                        </tr>
                      </thead>
                       <tbody>
                             <tr ng-repeat="inst in staff_data" class="noright">
                                <!-- <td><img alt=""  ng-src="{{inst.image }}" class="md-avatar" /></td> -->
                                <td>{{inst.saluation}}{{inst.first_name }}  {{inst.last_name }}</td>
                                <td>{{inst.designation}}</td>
                                <td>{{inst.doj}}</td>
                                <td>{{inst.dob}}</td>
                                <td>  
                                <md-button><a ng-click="editFaculty(inst.faculty_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> Edit</a></md-button>
                               </td>
                            </tr>
                      </tbody>
                    </table>
                    </div>
        </md-card-content>
    </md-card>
    <md-card ng-show="RegisterVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Staff Details</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <form enctype="multipart/form-data" method="post" id="FacultyRegister" >
                                <div hidden>
                                    <input name="Iid" id="Iid" ng-model="Iid">
                                    <input name="did" id="did" ng-model="did">
									<input name="Faculty_id" id="Faculty_id" ng-model="Faculty_id">
                                </div>
                                <div>
                                    <md-input-container>
                                            <label>Company Name</label>
                                            <input name="IName" id="IName" ng-model="IName" ng-value="IName" required ng-disabled="true" md-maxlength="300" minlength="4">
                                            <div ng-messages="IName.$error" ng-show="IName.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>
                                    <md-input-container>
                                            <label>Department Name</label>
                                            <input name="DeptName" id="DeptName" ng-model="DeptName" ng-value="DeptName" required ng-disabled="true" md-maxlength="300" minlength="4">
                                            <div ng-messages="DeptName.$error" ng-show="DeptName.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>Saluation</div>
                                <div>
                                         <md-radio-group ng-model="sal" name="sal" ng-required="true" layout="row">
                                            <md-radio-button ng-model="sal" ng-repeat="d in saluation" ng-value="d.saluation">{{d.saluation}}</md-radio-button> 
                                        </md-radio-group>
                                        <input type="text" ng-model="sal" name="sal" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                        <div ng-messages="sal.$error" role="alert">
                                            <div ng-message="required">Saluation is required.</div>
                                        </div>
                                </div>
                                <div>
                                    <md-input-container>
                                            <label>First Name</label>
                                            <input name="FName" id="FName" ng-model="searchText" ng-value="searchText" required md-maxlength="100">
                                            <div ng-messages="FName.$error" ng-show="FName.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>
                                    <md-input-container>
                                            <label>Last Name</label>
                                            <input name="LName" id="LName" ng-model="LName" ng-value="LName" required md-maxlength="100">
                                            <div ng-messages="LName.$error" ng-show="LName.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>
                                    <md-input-container>
                                        <label>Designation</label>
                                        <md-select ng-model="desig" id="desig" name="desig" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                        <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Designation Name.." class="demo-header-searchbox md-text" type="search">
                                        </md-select-header>
                                        <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.designation" ng-model="desig" ng-repeat="inst in designation | filter:searchTerm">{{inst.designation}}</md-option>
                                        </md-optgroup>
                                        </md-select>
                                     </md-input-container>
                                </div>
                                <!-- <div>
                                            <div class="input-group date form_datetime col-md-5" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                                <input class="form-control" size="16" type="text" value="" readonly>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                            </div>
                                            <input type="hidden" id="dtp_input1" value="" /><br/>
                                </div> -->
                                <div class="row">
                                    <div class="col-lg-6" flex-gt-xs hidden>
                                        <h5>Date of Joining</h5>
                                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                                            <input ng-model="doj" class="form-control" size="16" type="text" value="" readonly required>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input ng-model="doj" name="doj" id="dtp_input2" value="" hidden/>
                                    </div>
                                    <div class="col-lg-6" flex-gt-xs hidden>
                                        <h5>Date of Birth</h5>
                                        <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input1" data-link-format="dd-mm-yyyy">
                                            <input ng-model="dob" class="form-control" size="16" type="text" value="" readonly required>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input ng-model="dob" name="dob" id="dtp_input1" value="" hidden/>
                                    </div>
                                </div>
                                <div>Gender</div>
                                <div>
                                         <md-radio-group ng-model="gender" name="gender" ng-required="true" layout="row">
                                            <md-radio-button ng-model="gender" ng-repeat="d in gender_type" ng-value="d.gender_type">{{d.gender_type}}</md-radio-button> 
                                        </md-radio-group>
                                        <input type="text" ng-model="gender" name="gender" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                        <div ng-messages="gender.$error" role="alert">
                                            <div ng-message="required">Gender is required.</div>
                                        </div>
                                </div>
                                <div hidden>
                                        <md-input-container>
                                            <label>Aadhar No.</label>
                                            <input name="aadhar" id="aadhar" ng-model="aadhar" md-maxlength="12" minlength="4">
                                            <div ng-messages="aadhar.$error" ng-show="aadhar.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>
                                         <md-input-container>
                                            <label>Mobile No.</label>
                                            <input name="mobile" id="mobile" ng-model="mobile" ng-value="mobile" required md-maxlength="12" minlength="4">
                                            <div ng-messages="mobile.$error" ng-show="mobile.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
                                <div>
                                    <md-input-container>
                                                <label>Official Mail Id</label>
                                                <input name="mail" id="mail" ng-model="mail" ng-value="mail" required md-maxlength="100" minlength="4">
                                                <div ng-messages="mail.$error" ng-show="mail.$dirty">
                                                <div ng-message="required">This is required!</div>
                                                <div ng-message="md-maxlength">That's too long!</div>
                                                <div ng-message="minlength">That's too short!</div>
                                                </div>
                                    </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                                <label>Roles and Responsibilities</label>
												<!--<textarea name="faculty_roles" id="faculty_roles" md-maxlength="100000" minlength="20" rows="4" cols="100" required ></textarea>-->
												<textarea name="faculty_roles" id="faculty_roles" ng-model="faculty_roles" md-maxlength="100000" minlength="20" rows="4" cols="100" required >{{faculty_roles}}</textarea>
                                                <div ng-messages="faculty_roles.$error" ng-show="faculty_roles.$dirty">
                                                <div ng-message="required">This is required!</div>
                                                <div ng-message="md-maxlength">That's too long!</div>
                                                <div ng-message="minlength">That's too short!</div>
                                                </div>
                                    </md-input-container>
                                </div>
                                <div class="row" style="text-align:right;padding-right:10px;">
                                  <md-button ng-show="registerbtn" type="submit" style="text-align:left;" class="waves-effect btn pink material-icons">Register<i class="material-icons">send</i></md-button>
								  <md-button ng-show="editbtn" ng-click="faculty_update()" style="text-align:left;" class="waves-effect btn pink material-icons">Edit<i class="material-icons">send</i></md-button>
                               </div>
                        </form>
        </md-card-content
    </md-card>
<script src="../myjs/general/faculty.js"></script>
</body>
</html>