<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>   
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /> 
     <meta charset="UTF-8">
    <title>SMS Sender</title>
    <?php require_once ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
    <script src="../myjs/general/sms.js"></script>
   
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

 <md-card ng-show="register_list">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">SMS Gateway</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">

                        <div class="row">
                            <div class="col-sm-3">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">cloud_done</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Files Uploaded</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_files}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: purple;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">sms</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of SMS Sent</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_sent}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: red;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">sms_failed</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of SMS Failed</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_fail}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: indigo;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance_wallet</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>SMS Balance</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{utcount}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

            <md-card ng-show="register_list">
                    <md-card-title   class="card-content white-text red">
                    <md-card-title-text>
                        <span class="md-headline">Excel File Upload</span>
                    </md-card-title-text>
                    </md-card-title>
                    <md-card-actions layout="row" layout-align="start center">
                        </md-card-actions>
                    <md-card-content class="card-content">
                    <form enctype="multipart/form-data" method="post" id="excelupload" >
                       <div class="row">
                            <div class="col-sm-2"><label>SMS Category</label></div>
                            <div class="col-sm-4">
                                <md-radio-group ng-model="type" layout="row">
                                    <md-radio-button value="Fee Pending">Fee Pending</md-radio-button>
                                    <md-radio-button value="Mess Bill">Mess Bill</md-radio-button>
                                </md-radio-group>
                                <input type="text" ng-model="type" id="type" name="type" style="max-height:0; opacity: 0; border: none" ng-required="true" aria-label="Status">
                                <div ng-messages="type.$error" role="alert">
                                        <div ng-message="required">SMS Category is required.</div>
                                </div>
                            </div>
                            <div class="col-sm-2"><label>Excel Sheet</label></div>
                            <div class="col-sm-4">
                            <input id="file_to_upload" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                            </div>
                        </div>
                        <div class="row">
                            <md-input-container>
                                            <label>SMS Description</label>
                                            <input name="desc" id="desc" ng-model="desc" required md-maxlength="200" minlength="4">
                                            <div ng-messages="desc.$error" ng-show="desc.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                        </div>
                        <div class="row" style="text-align:right;padding-right:10px;">
                        <md-button  type="submit" id="upload" class="waves-effect btn red material-icons"><i class="material-icons">cloud_upload</i> Upload</md-button>
                        </div>
                    </form>
                    </md-card-content>
             </md-card>
                    <div class="row">
                        <table id="data_list" class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Sheet Name</th>
                                    <th>Uploaded Time</th>
                                    <th class="all">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr ng-repeat="inst in list" class="noright">
                                        <td></td>
                                        <td>{{inst.type}}</td>
                                        <td>{{inst.description}}</td>
                                        <td>{{inst.sheet_name}}</td>
                                        <td>{{inst.updated_time}}</td>
                                        <td> 
                                             <md-button ng-click="view_info(inst.id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">mode_edit</i> View</md-button>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>

        </md-card-content>
    </md-card>

        <md-card  ng-show="upload_list">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Uploaded Sheet Info</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
                            <div class="col-sm-4">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: purple;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">sms</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of SMS Sent</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_sentf}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: red;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">sms_failed</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of SMS Failed</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_failf}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                                    <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: indigo;">
                                        <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance_wallet</i>
                                    </span>
                                    <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                                        <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>SMS Balance</label></span>
                                        <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{utcount}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                               <form enctype="multipart/form-data" method="post" id="smssendform" >

                                <md-card  ng-show="upload_list">
                                <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
                                <md-card-title-text>
                                </md-card-title-text>
                                </md-card-title>
                                <md-card-actions layout="row" layout-align="start center">
                                    
                                </md-card-actions>
                                <md-card-content class="card-content">
                               <div hidden>
                                    <input name="uid" id="uid" ng-model="uid" readonly>
                                </div>
                                <div class="row">
                                     <div class="col-sm-2"><label>Category</label></div>
                                     <div class="col-sm-4"><input ng-model="cate" type="text" class="form-control" readonly></div>
                                     <div class="col-sm-2"><label>Description</label></div>
                                     <div class="col-sm-4"><input ng-model="sdesc" type="text" class="form-control" readonly></div>
                                </div>
                                <div class="row">
                                     <div class="col-sm-2"><label>File Name</label></div>
                                     <div class="col-sm-4"><input ng-model="sheet" type="text" class="form-control" readonly></div>
                                     <div class="col-sm-2"><label>Uploaded Time</label></div>
                                     <div class="col-sm-4"><input ng-model="update" type="text" class="form-control" readonly></div>
                                </div>
                                <div class="row">
                                    <md-input-container>
                                    <label>Sheet Name(s)</label>
                                    <md-select ng-model="sname" id="sname" name="sname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                    <md-select-header class="demo-select-header">
                                        <input ng-model="searchTerm" placeholder="Search for Sheet Name Name.." class="demo-header-searchbox md-text" type="search">
                                    </md-select-header>
                                    <md-optgroup label="vegetables">
                                        <md-option ng-value="inst.value" ng-model="sname" ng-click="option_click(inst.value)" ng-repeat="inst in sheets | filter:searchTerm">{{inst.name}}</md-option>
                                    </md-optgroup>
                                    </md-select>
                                    </md-input-container>
                                </div>
                                </md-card-content>
                            </md-card>
                                <div class="row">
                                    <table id="stud_list" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th >Action</th>
                                                <th class="all">Student Name</th>
                                                <th>Current Month</th>
                                                <th>Misc</th>
                                                <th>Misc Description</th>
                                                <th>Pending</th>
                                                <th>Total</th>
                                                <th>Mobile</th>
                                                <th>Status</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <tr ng-repeat="inst in students" class="noright">
                                                <td>{{$index +1}}</td>
                                                <td>{{inst.name}}</td>
                                                    <td>{{inst.current}}</td>
                                                    <td>{{inst.misc}}</td>
                                                    <td>{{inst.misc_desc}}</td>
                                                    <td>{{inst.pending}}</td>
                                                    <td>{{inst.total}}</td>
                                                    <td>{{inst.mobile}}</td>
                                                    <td>{{inst.status}}</td>
                                                    
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row" style="text-align:right;padding-right:10px;">
                                    <button class="waves-effect btn pink" type="submit" name="action">Send SMS<i class="material-icons">send</i></button>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div id="chart"></div>
                                </div>
                                <div class="col-sm-7"  style="padding-right:30px;">
                                <div class="row">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>SMS Sent Time</th>
                                                        <th>From & To SMS Id</th>
                                                        <th>From and To SMS Balance</th>
                                                        <th>Difference</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr ng-repeat="inst in sms_hist" class="noright">
                                                            <td>{{$index +1}}</td>
                                                            <td>{{inst.time}}</td>
                                                            <td>{{inst.sms_id}}</td>
                                                            <td>{{inst.sms_bal}}</td>
                                                            <td>{{inst.diff}}</td>
                                                        </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            <div>
                            <div class="row">
                            <table id="student_sms_list" class="table table-striped table-bordered" ng-if="st_count!=0">
                                                <thead>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th class="all">Register No. & Name</th>
                                                        <th>Mobile No.</th>
                                                        <th>SMS Id</th>
                                                        <th>Status</th>
                                                        <th>Date & Time</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr ng-repeat="inst in sms_list" class="noright">
                                                            <td>{{$index +1}}</td>
                                                            <td>{{inst.name}}</td>
                                                            <td>{{inst.mobile}}</td>
                                                            <td>{{inst.smsid}}</td>
                                                            <td>{{inst.reason}}</td>
                                                            <td>{{inst.dtime}}</td>
                                                        </tr>
                                                </tbody>
                                            </table>
                            </div>      
        </md-card-content>
    </md-card>
</body>
</html>