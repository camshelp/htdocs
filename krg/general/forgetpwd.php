<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>KRG Portal - Forget Password Option</title>
    <?php include ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl" style="background:#33c9dc;"> 
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
            <md-card>
                <md-card-title   class="card-content white-text" style="background-color:purple;">
                <md-card-title-text>
                    <span> <a ng-click="krg_home()"><i class="material-icons" style="font-size:30px;">home</i></a><span>
                    <span class="md-headline">KRG Portal - Forget Password</span>
                </md-card-title-text>
                </md-card-title>
                <md-card-actions layout="row" layout-align="start center">

                </md-card-actions>
                <md-card-content class="card-content">
                 <form enctype="multipart/form-data" method="post" id="updatepwd" >
                 <div class="row">
                    <md-input-container class="md-block">
                        <label>Enter your Mail Id</label>
                        <md-icon md-svg-src="img/icons/ic_email_24px.svg" class="email"></md-icon>
                        <input name="mail" type="mail" id="mail" ng-model="mail" required  md-maxlength="10" minlength="6">
                        <div ng-messages="updatepwd.mail.$error" ng-show="updatepwd.mail.$dirty">
                            <div ng-message="required">This is required!</div>
                            <div ng-message="md-maxlength">That's too long!</div>
                            <div ng-message="minlength">That's too short!</div>
                         </div>
                    </md-input-container>
                </div>
                <div class="row" style="text-align:right;">
                    <button class="waves-effect btn purple" type="submit" name="action">Send Mail<i class="material-icons">send</i></button>
                </div>
                 </form>
             </md-card-content>
    </md-card>
            </div>
            <div class="col-sm-3"></div>
        </div>
  <script src="../myjs/general/pwdforget.js"></script>
</body>
</html>