<?php  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");?>
<!DOCTYPE html>
<html lang="en-US">
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <meta charset="UTF-8">
    <title>Institution Management</title>
    <?php require_once ($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/krg_master.php"); ?>
</head>
<body  ng-app="myApp" ng-controller="userCtrl"> 

    <md-card ng-show="listVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	        <span class="md-headline">Company Register</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div class="row">
                    <div class="col-sm-4">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color: rgb(216, 27, 96);">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">account_balance</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Company</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_company}}</span>
						        <a  ng-click="EditFaculty('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>Company</a>
                         </div>
                        </div>
                    </div>
                    <div class="col-sm-4" ng-show="dept_button">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:purple;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">group</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Dept in Choosed Company</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_dept}}</span>
								<a  ng-click="EditDepartment('new')" style=" margin-top:-50px;margin-left:80%;" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i>Company</a> 
                         </div>
                        </div>
                    </div>
                   <!-- <div class="col-sm-4" ng-show="add_button">
                      <div style="color: rgba(0, 0, 0, 0.87); background-color: rgb(255, 255, 255); transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Roboto, sans-serif; box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px; border-radius: 2px;">
                         <span style="float: left; height: 80px; width: 90px; text-align:left; background-color:indigo;">
                            <i class="material-icons" style="padding-left:20px;padding-top:20px;color:white;font-size:40px;">group_add</i>
                         </span>
                         <div style="padding: 5px 10px; margin-left: 90px; height: 80px;">
                           <span style="font-size: 20px; font-weight: 300; color: rgb(66, 66, 66);"><label>No.of Staff in Choosed Dept</label></span>
                           <span style="display: block; font-weight: 500; font-size: 18px; color: rgb(66, 66, 66);">{{no_dept}}</span>
                             
                         </div>
                        </div>
                    </div>-->
            </div>
        
        <div class="row">
                        <div class="col-lg-5">
                                <md-input-container>
                                <label>Company Name</label>
                                <md-select ng-model="gname" id="gname" name="gname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Company Name.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.group_id" ng-model="gname" ng-click="option_click(inst.group_id)" ng-repeat="inst in institution | filter:searchTerm">{{inst.group_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                        </div>
                        <div class="col-lg-4">
                                <md-input-container>
                                <label>Department Name</label>
                                <md-select ng-model="dname" id="dname" name="dname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-select-header class="demo-select-header">
                                    <input ng-model="searchTerm" placeholder="Search for Department Name.." class="demo-header-searchbox md-text" type="search">
                                </md-select-header>
                                <md-optgroup label="vegetables">
                                    <md-option ng-value="inst.dept_id" ng-model="dname" ng-click="option_click1(inst.dept_id)" ng-repeat="inst in dept | filter:searchTerm">{{inst.course_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                        </div>
                    </div>

        </md-card-content>
    </md-card>
	<!--company update-->
    <md-card ng-show="RegisterVisible">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="registration_list()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Company Details</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <form enctype="multipart/form-data" method="post" action="company.php">
                                <div hidden>
                                    <input name="Iid" id="Iid" ng-model="Iid">
                                    <input name="did" id="did" ng-model="did">
                                </div>
                                <div>
                                    <md-input-container>
                                            <label>Company Name</label>
                                            <input name="company" id="company" ng-model="company" required md-maxlength="200" minlength="4">
                                            <div ng-messages="company.$error" ng-show="company.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Address</label>
                                            <input name="address" id="address" ng-model="address" required md-maxlength="200" minlength="4">
                                            <div ng-messages="address.$error" ng-show="address.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>City and pincode</label>
                                            <input name="city" id="city" ng-model="city" required md-maxlength="200" minlength="4">
                                            <div ng-messages="city.$error" ng-show="city.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>State and country</label>
                                            <input name="state" id="state" ng-model="state" required md-maxlength="200" minlength="4">
                                            <div ng-messages="state.$error" ng-show="state.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Landmark</label>
                                            <input name="landmark" id="landmark" ng-model="landmark" required md-maxlength="200" minlength="4">
                                            <div ng-messages="landmark.$error" ng-show="landmark.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div class="row">
                                            <div class="col-sm-7">
                                                <md-input-container>
                                                            <label>Head Person</label>
                                                            <input name="taskresp" id="taskresp" ng-model="taskresp" required md-maxlength="300" minlength="4"  ng-disabled="true" required>
															<div ng-messages="taskresp.$error" ng-show="taskresp.$dirty">
                                                                <div ng-message="required">This is required!</div>
                                                                <div ng-message="md-maxlength">That's too long!</div>
                                                                <div ng-message="minlength">That's too short!</div>
                                                            </div>
                                                </md-input-container>
                                            </div>
                                            <div class="col-sm-5">
                                                <md-button ng-click="company_head_view()" class="md-secondary pink"><span class="glyphicon glyphicon-edit"></span>Choose</md-button>
                                            </div>
                                </div>
								<div hidden>
                                <input name="task_fid" id="task_fid" ng-model="task_fid" readonly>
								</div>
                                <div class="row" style="text-align:right;padding-right:10px;">
                                  <md-button type="submit" style="text-align:left;" class="waves-effect btn pink material-icons">Submit<i class="material-icons">send</i></md-button>
                               </div>
                        </form>
        </md-card-content>
    </md-card>
	<!--company update-->
	
	
	
	<!--Department update-->
    <md-card ng-show="update_task">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="registration_list1()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Department Details</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
              
        </md-card-actions>
        <md-card-content class="card-content">
        <form enctype="multipart/form-data" method="post" action="department.php" >
                                <div hidden>
                                    <input name="Iid" id="Iid" ng-model="Iid">
                                    <input name="did" id="did" ng-model="did">
                                </div>                              
								<div>
                                    <md-input-container>
                                            <label>Graduation Type</label>
                                            <input type="varchar" name="graduation" id="graduation" ng-model="graduation" md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Degree or Department Name</label>
                                            <input type="varchar" name="degree" id="degree" ng-model="degree" required md-maxlength="200" minlength="4">
                                            <div ng-messages="degree.$error" ng-show="degree.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Course Name</label>
                                            <input type="varchar" name="course" id="course" ng-model="course" required md-maxlength="200" minlength="4">
                                            <div ng-messages="course.$error" ng-show="course.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Acronym</label>
                                            <input type="varchar" name="acronym" id="acronym" ng-model="acronym" required md-maxlength="200" minlength="4">
                                            <div ng-messages="acronym.$error" ng-show="acronym.$dirty">
                                            <div ng-message="required">This is required!</div>
                                            <div ng-message="md-maxlength">That's too long!</div>
                                            <div ng-message="minlength">That's too short!</div>
                                            </div>
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Sem Type</label>
                                            <input type="varchar" name="sem" id="sem" ng-model="sem"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Minimum Duration</label>
                                            <input type="number" name="min" id="min" ng-model="min"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Maximum Duration</label>
                                            <input type="number" name="max" id="max" ng-model="max"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Number of Semester</label>
                                            <input type="number" name="nosem" id="nosem" ng-model="nosem"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Board Nmae</label>
                                            <input type="varchar" name="board" id="board" ng-model="board"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Register Code</label>
                                            <input type="varchar" name="register" id="register" ng-model="register"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Intake</label>
                                            <input type="number" name="intake" id="intake" ng-model="intake"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Starting Year</label>
                                            <input type="number" name="start" id="start" ng-model="start"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Academic Type</label>
                                            <input type="varchar" name="type" id="type" ng-model="type"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Display Order</label>
                                            <input type="varchar" name="display" id="display" ng-model="display"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div>
                                    <md-input-container>
                                            <label>Faculty code</label>
                                            <input type="varchar" name="facultycode" id="facultycode" ng-model="facultycode"  md-maxlength="200" minlength="4">
                                        </md-input-container>
                                </div>
								<div class="row">
                                            <div class="col-sm-7">
                                                <md-input-container>
                                                            <label>Head Person</label>
                                                            <input name="subresp" id="subresp" ng-model="subresp"  md-maxlength="300" minlength="4"  ng-disabled="true" required>
															<div ng-messages="subresp.$error" ng-show="subresp.$dirty">
                                                                <div ng-message="required">This is required!</div>
                                                            </div>
                                                </md-input-container>
                                            </div>
                                            <div class="col-sm-5">
                                                <md-button ng-click ="dept_head_view()" class="md-secondary pink"><span class="glyphicon glyphicon-edit"></span>Choose</md-button>
                                            </div>
                                </div>
								<div hidden>
                                <input name="sub_fid" id="sub_fid" ng-model="sub_fid" readonly>
                                <input name="gname" id="gname" ng-model="gname" readonly>
								</div>
                                <div class="row" style="text-align:right;padding-right:10px;">
                                  <md-button type="submit" style="text-align:left;" class="waves-effect btn pink material-icons">Submit<i class="material-icons">send</i></md-button>
                               </div>
                        </form>
        </md-card-content>
    </md-card>
	<md-card ng-show="head_update">
        <md-card-title   class="card-content white-text" style="background-color:#3f51b5;">
          <md-card-title-text>
	    <span> <a ng-click="back_to_home()"><i class="material-icons" style="font-size:30px;">keyboard_backspace</i></a><span>
            <span class="md-headline">Responsible Person Update</span>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="start center">
        </md-card-actions>
        <md-card-content class="card-content">
        <div hidden>
                            <input name="select_type" id="select_type" ng-model="select_type" readonly>
                        </div>
                        <div>
                        <table id="staff_list" class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Staff Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Mobile No.</th>
                                    <th class="all"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in faculty">
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ user.name }}</td>
                                    <td>{{ user.designation }}</td>
                                    <td>{{ user.department }}</td>
                                    <td>{{ user.mobile }}</td>
                                    <td>
                                        <md-button  ng-click="Fixhead(user.faculty_id)" style="width:100%;text-align:left;" class="waves-effect btn pink material-icons"><i class="material-icons">assignment_turned_in</i>Assign</md-button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
        </md-card-content>
    </md-card>
<script src="../myjs/general/faculty.js"></script>
</body>
</html>