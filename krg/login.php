<!DOCTYPE html>
<html>
<head>
    <title>KRG Portal - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <script src="/PCTEM/scripts/login/jquery-2.2.3.min.js"></script>
    <script src="/PCTEM/bootstrap/bootstrap-3.3.7/dist/js/bootstrap.js" type='text/javascript'></script>
    <link href="/PCTEM/bootstrap/bootstrap-3.3.7/dist/css/bootstrap.css" rel="stylesheet"/>
    <script src="/PCTEM/angular-1.6.9/angular.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-material.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-animate.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-aria.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-messages.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/svg-assets-cache.js" type='text/javascript'></script>
    <link href="/PCTEM/angular-1.6.9/angular-material.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/login/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="/PCTEM/scripts/login/animate.min.css"> 
    <link rel="stylesheet" href="/PCTEM/scripts/login/style.css"> 
    <link rel="stylesheet" href="/PCTEM/scripts/login/bootstrap.min.css"> 
    <script src="/PCTEM/scripts/gitter/bootstrap-notify.js" type="text/javascript"></script>
    <script src='/PCTEM/scripts/login/jquery.validate.min.js'></script>
    <script src='/PCTEM/scripts/login/formAnimation.js'></script>
    <script src='/PCTEM/scripts/login/shake.js'></script> 
    <script src="/PCTEM/scripts/pace.min.js"></script>
    <link href="/PCTEM/scripts/pace loader.css" rel="stylesheet" />
    <link rel="shortcut icon" href="/PCTEM/scripts/images/kr.jpg" type="image/x-icon">
    <link href='/PCTEM/fonts/roboto/roboto.css' rel='stylesheet'>
</head>
<body  ng-app="myApp" ng-controller="userCtrl">
        <!-- main-agileits -->
        <div class="agileits">
            <div class="w3-agileits-info">
                <form class='form animate-form' id='form1' action="#" method="post">
                    <h3>KRG Portal - Login Form</h3>
                    <div class='form-group has-feedback wthree'>
                        <label class='control-label sr-only' for='email'>Email address</label> 
                        <input class='form-control' id='email' ng-model="email" name='email' placeholder='Email address' type='text'><span class='glyphicon glyphicon-ok form-control-feedback'></span>
                    </div>
                    <div class='form-group has-feedback agile'>
                        <label class='control-label sr-only' for='password'>Password</label> 
                        <input class='form-control w3l' id='password' name='password' placeholder='Password' type='password'><span class='glyphicon glyphicon-ok form-control-feedback'></span>
                    </div>
                    <div ng-show="glist" class="row" style="align:left;">
                              <md-input-container>
                                <label>Company Name</label>
                                <md-select ng-model="gname" id="gname" name="gname" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" required>
                                <md-optgroup label="Choose Company Name">
                                    <md-option ng-value="inst.group_id" ng-model="gname" ng-repeat="inst in clist">{{inst.group_name}}</md-option>
                                </md-optgroup>
                                </md-select>
                                </md-input-container>
                    </div>
                    <div class='submit w3-agile'>
                        <input class='btn btn-lg' type='submit' value='Login'>
                    </div>
                    <a href="/krg/general/forgetpwd.php">Forget Password?</a>
                </form>
            </div>	
        </div>	
   <!-- //agileits -->
	<!-- copyright -->
	<div class="w3-agile-copyright">
		<p class="agileinfo"> © 2018 KR Group of Companies . All rights reserved | Design by KRG Portal</p>
	</div>
    <script src='/krg/myjs/login.js'></script> 
</body>
</html>