<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "accounthead_register":register_accounthead();
                    break;
            case "accounthead_list":list_accounthead();
                    break;
            case "accountcategory_register":register_accountcategory();
                    break;
            case "accountcategory_list":list_accountcategory();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function register_accounthead()
        {
            $type="Error";$msg="";
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
            else
            {
                if(isset($_POST['acname'])){  $name = $_POST['acname']; }else{$msg="Account Name was not posted";}
                if(isset($_POST['cname'])){  $cname = $_POST['cname']; }else{$msg="Account Category Name was not posted";}
                if($msg=="")
                {
                session_start();
                $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();

                try
                {
                    $sql="select head_id from poultry.account_head where head_name=:gname";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':gname', $name);
                    $stmt->execute();
                    $rowcount =$stmt->rowCount();
                    if($rowcount==0)
                    {
                        $sql = "select count(*) from poultry.account_head";
                        $stmt = $conn->prepare($sql); 
                        $stmt->execute();
                        $row = $stmt->fetchColumn(0);
                        $add_value="1";
                        $total=$row+$add_value;
                        $gen_id="ACHD".SixDigitIdGenerator("$total");
                        $astatus="yes";
                        $sql="insert into poultry.account_head values(:cid,:id,:name,:cname,:by,:session,:time,:astatus)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':cid',$company_id);
                        $stmt->bindParam(':id',$gen_id);
                        $stmt->bindParam(':name',$name);
                        $stmt->bindParam(':cname',$cname);
                        $stmt->bindParam(':astatus',$astatus);
                        $stmt->bindParam(':by', $update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if ($stmt->execute() == TRUE) 
                        {
                            $type="Success";$msg="The Information Submitted Successfully";
                        } 

                    }else{$msg="The Account Head Name was already found";}
                }catch(Exception $e){$msg=$e->getMessage();}
                }
              database_close($conn);
            } 
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
        function list_accounthead()
        {
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT poultry.account_head.head_id,poultry.account_head.head_name,poultry.account_category.category_id,poultry.account_category.category_name from poultry.account_head left join poultry.account_category on poultry.account_head.account_category=poultry.account_category.category_id where poultry.account_head.active_status='yes' order by poultry.account_category.category_name asc,poultry.account_head.head_name asc";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row)
            {
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                   
                    $json[$sno] = array(
                     'head_id' => $row['head_id'],
                     'head_name' => strtoupper($row['head_name']),
                     'category_id' => $row['category_id'],
                     'category_name' =>strtoupper($row['category_name']));
                    $sno++;
                }
            }
            database_close($conn);
            echo json_encode($json);   
        }
        function register_accountcategory()
        {
            $type="Error";$msg="";
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
            else
            {
                if(isset($_POST['accate'])){  $name = $_POST['accate']; }else{$msg="Account Category Name was not posted";}
                if($msg=="")
                {
                session_start();
                $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();

                try
                {
                    $sql="select category_id from poultry.account_category where category_name=:gname";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':gname', $name);
                    $stmt->execute();
                    $rowcount =$stmt->rowCount();
                    if($rowcount==0)
                    {
                        $sql = "select count(*) from poultry.account_category";
                        $stmt = $conn->prepare($sql); 
                        $stmt->execute();
                        $row = $stmt->fetchColumn(0);
                        $add_value="1";
                        $total=$row+$add_value;
                        $gen_id="ACAT".SixDigitIdGenerator("$total");
                        $astatus="yes";
                        $sql="insert into poultry.account_category values(:cid,:id,:name,:by,:session,:time,:astatus)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':cid',$company_id);
                        $stmt->bindParam(':id',$gen_id);
                        $stmt->bindParam(':name',$name);
                        $stmt->bindParam(':astatus',$astatus);
                        $stmt->bindParam(':by', $update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if ($stmt->execute() == TRUE) 
                        {
                            $type="Success";$msg="The Information Submitted Successfully";
                        } 

                    }else{$msg="The Account Category Name was already found";}
                }catch(Exception $e){$msg=$e->getMessage();}
                }
              database_close($conn);
            } 
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
        function list_accountcategory()
        {
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT category_id,category_name from poultry.account_category where active_status='yes' order by category_name asc";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row)
            {
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                   
                    $json[$sno] = array(
                     'category_id' => $row['category_id'],
                     'category_name' => strtoupper($row['category_name']));
                    $sno++;
                }
            }
            database_close($conn);
            echo json_encode($json);   
        }