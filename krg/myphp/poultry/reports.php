<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/pdfheader.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
  $callparameter="";
   if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "daysheet":daysheet();
                    break;
            case "summaryrv":summaryrv();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    } 
    function daysheet()
    {
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");
        try
        {
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);$title="DaySheet";

            $current_time= get_datetime();
            $disp_date=$_POST['date'];
            $dval=explode('-',$disp_date);
            $date=$dval[2]."-".$dval[1]."-".$dval[0];
            $pdate = date('Y-m-d', strtotime($date .' -1 day'));
            $conn = database_open();
            $type="Receipt";
            $sql="SELECT poultry.account_head.head_id,poultry.account_head.head_name,poultry.receipt_vochure_entry.id,poultry.receipt_vochure_entry.date,poultry.receipt_vochure_entry.day,poultry.receipt_vochure_entry.name,poultry.receipt_vochure_entry.purpose,poultry.receipt_vochure_entry.amount,poultry.receipt_vochure_entry.amount_words FROM poultry.receipt_vochure_entry JOIN poultry.account_head ON poultry.receipt_vochure_entry.head_account=poultry.account_head.head_id WHERE poultry.receipt_vochure_entry.active_status='yes' and poultry.receipt_vochure_entry.type=:typevalue and poultry.receipt_vochure_entry.date=:date and poultry.receipt_vochure_entry.company_id=:company_id order by poultry.receipt_vochure_entry.id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':typevalue',$type);
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $rcount =$stmt->rowCount();
            if($rcount>0)
            {
                $receipt_data = $stmt->fetchAll();
            }
            $type="Voucher";
            $sql="SELECT poultry.account_head.head_id,poultry.account_head.head_name,poultry.receipt_vochure_entry.id,poultry.receipt_vochure_entry.date,poultry.receipt_vochure_entry.day,poultry.receipt_vochure_entry.name,poultry.receipt_vochure_entry.purpose,poultry.receipt_vochure_entry.amount,poultry.receipt_vochure_entry.amount_words FROM poultry.receipt_vochure_entry JOIN poultry.account_head ON poultry.receipt_vochure_entry.head_account=poultry.account_head.head_id WHERE poultry.receipt_vochure_entry.active_status='yes' and poultry.receipt_vochure_entry.type=:typevalue and poultry.receipt_vochure_entry.date=:date and poultry.receipt_vochure_entry.company_id=:company_id order by poultry.receipt_vochure_entry.id asc";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':typevalue',$type);
            $stmt1->bindParam(':date',$date);
            $stmt1->bindParam(':company_id',$company_id);
            $stmt1->execute();
           // $stmt1->debugDumpParams();
            $vcount =$stmt1->rowCount();
            if($vcount>0)
            {
                $vouch_data = $stmt1->fetchAll();
            }
            $max_count=$rcount;
            if($vcount>$max_count){$max_count=$vcount;}
            //echo $rcount."  ".$vcount." ".$max_count;
             //if($max_count>0)
             {
                   
                    $income=0;$expense=0;$current_income=0;
                    $w1=8;$w2=34;$w3=8;
                    $row_height=25;
                    $stmt = $conn->prepare("select sum(closing_balance) from poultry.day_sheet where date=:date and company_id=:company_id");
                    $stmt->bindParam(':date',$pdate);
                    $stmt->bindParam(':company_id',$company_id);
                    $stmt->execute();
                    $open_bal = $stmt->fetchColumn(0);
                    $income+=$open_bal;
                    $tbl_rows='<tr>
                            <td vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"></td>
                            <td height="'.$row_height.'" width="'.$w2.'%">Opening Balance'.$prev_date.'</td>
                            <td height="'.$row_height.'" width="'.$w3.'%" align="right">'.$open_bal.'</td>
                            <td height="'.$row_height.'" width="'.$w1.'%" align="center"></td>
                            <td height="'.$row_height.'" width="'.$w2.'%"></td>
                            <td height="'.$row_height.'" width="'.$w3.'%" align="right"></td>
                            </tr>';
                            for($i=0;$i<$max_count;$i++)
                            {
                                $rno=$receipt_data[$i]["id"];
                                $rpart=ltrim(strtoupper($receipt_data[$i]["name"]." - ".$receipt_data[$i]["purpose"])," ");
                                $ramount=$receipt_data[$i]['amount'];

                                $vno=$vouch_data[$i]["id"];
                                $vpart=ltrim(strtoupper($vouch_data[$i]["name"]." - ".$vouch_data[$i]["purpose"])," ");
                                $vamount=$vouch_data[$i]["amount"];

                                $income+=$ramount; $expense+=$vamount;$current_income+=$ramount;

                                $current='<tr>
                                            <td height="'.$row_height.'" align="center" width="'.$w1.'%">'.$rno.'</td>
                                            <td width="'.$w2.'%">'.ltrim($rpart,"-").'</td>
                                            <td align="right" width="'.$w3.'%">'.$ramount.'</td>
                                            <td align="center" width="'.$w1.'%">'.$vno.'</td>
                                            <td width="'.$w2.'%">'.ltrim($vpart,"-").'</td>
                                            <td align="right" width="'.$w3.'%">'.$vamount.'</td>
                                            </tr>';
                                            $tbl_rows.=$current;
                            }
                            $close_bal=$income-$expense;
                            if (strpos($current_time, $date) !== false) 
                            {

                                $sql="delete from poultry.day_sheet where date=:date and company_id=:company_id";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':company_id',$company_id);
                                $stmt->bindParam(':date',$date);     
                                $stmt->execute();

                                $sql="insert into poultry.day_sheet values(:company_id,:date,:open_bal,:nrep,:nvou,:tincome,:totincome,:texpense,:close_bal,:by,:session,:time)";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':company_id',$company_id);
                                $stmt->bindParam(':date',$date);
                                $stmt->bindParam(':open_bal',$open_bal);
                                $stmt->bindParam(':nrep',$rcount);
                                $stmt->bindParam(':nvou',$vcount);
                                $stmt->bindParam(':tincome',$current_income);
                                $stmt->bindParam(':totincome',$current_income);
                                $stmt->bindParam(':texpense',$expense);
                                $stmt->bindParam(':close_bal',$close_bal);
                                $stmt->bindParam(':by',$update_by);
                                $stmt->bindParam(':session',$update_session);
                                $stmt->bindParam(':time',$current_time);
                                if ($stmt->execute() == TRUE) 
                                {

                                }
                            }
                            database_close($conn);
                            $tfooter='<tfooter><tr><td vertical-align="middle" height="'.$row_height.'" colspan="2" align="center"><strong>Total</strong></td><td vertical-align="middle" height="'.$row_height.'" align="right"><strong>'.$income.'</strong></td><td vertical-align="middle" height="'.$row_height.'" colspan="3" align="right"><strong>'.$expense.'</strong></td></tr><tr><td vertical-align="middle" height="'.$row_height.'" colspan="3" align="right"><strong>Closing Balance : </strong></td><td vertical-align="middle" height="'.$row_height.'" colspan="3"><strong>'.$close_bal.'</strong></td></tr></tfooter>';

                            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                            $pdf->AddPage('L', 'A4');
                            $lastPage = $pdf->getPage();
                            $pdf->deletePage($lastPage);

                            $header_data=document_header($company_id,$title);
                            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
                            $pdf->SetCreator(PDF_CREATOR);
                        
                            $pdf->SetMargins(10, 30, 10, true);
                            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                                require_once(dirname(__FILE__).'/lang/eng.php');
                                $pdf->setLanguageArray($l);
                            }
                        $pdf->setFontSubsetting(true);
                        $pdf->SetFont('dejavusans', '', 10, '', true);
                        $pdf->AddPage();
                        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
                        
                        $tbl='<table border="1" cellpadding="2">
                                            <thead>
                                                <tr>
                                                    <th border="0" align="right" vertical-align="middle" height="'.$row_height.'"><b>Date</b></th>
                                                    <th border="0" vertical-align="middle" height="'.$row_height.'">'.$disp_date.'</th>
                                                    <th border="0" vertical-align="middle" height="'.$row_height.'" colspan="4"></th>
                                                </tr>
                                                <tr style=" background-color:#FFFF00;color:#0000FF;">
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"><b>R.No.</b></th>
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w2.'%" align="center"><b>Particulars</b></th>
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"><b>Income in Rs.</b></th>
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"> <b>V.No.</b></th>
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w2.'%" align="center"><b>Particulrs</b></th>
                                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"><b>Expenses in Rs.</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>'.$tbl_rows.'</tbody>'.$tfooter.'</table>';
                            $pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
                            $doc_location=document_namecreate("daysheet.pdf");;
                            $pdf->Output($doc_location, 'F');
                            $_SESSION["download_docname"]="Poultry DaySheet.pdf";
                            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location);
                }
                //else{echo "No Data";}
        }
        catch(Exception $e) {echo $e->getMessage();} 
    }
    function summaryrv()
    {
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");
        try
        {
            //echo var_dump($_POST);
            $conn = database_open();
           
            $disp_date=$_POST['sdate'];
            $dval=explode('-',$disp_date);
            $start_date=$dval[2]."-".$dval[1]."-".$dval[0];

            $disp_date=$_POST['edate'];
            $dval=explode('-',$disp_date);
            $end_date=$dval[2]."-".$dval[1]."-".$dval[0];

            $type=$_POST['type'];$idno="V.No.";
            if($type=="Receipt"){$idno="R.No.";}
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);$title=$type." - Summary";
           
            $hpassed=$_POST['heads'];
            $heads = explode(',',$hpassed);
            $sort_str="";
            $count=count($hpassed);
            if($count>0)
            {
                $head_depts=" and (";
                for($x=0;$x<$count;$x++)
                {
                    $head_depts.=" poultry.receipt_vochure_entry.head_account='".$hpassed[$x]."' or";
                }
                $sort_str=rtrim($head_depts,"r");
                $sort_str=rtrim($sort_str,"o").")";
            }else{$sort_str=" and  poultry.receipt_vochure_entry.head_account='nil'";}

            $sql="SELECT poultry.account_head.head_id,poultry.account_head.head_name,poultry.receipt_vochure_entry.id,poultry.receipt_vochure_entry.date,poultry.receipt_vochure_entry.day,poultry.receipt_vochure_entry.name,poultry.receipt_vochure_entry.purpose,poultry.receipt_vochure_entry.amount,poultry.receipt_vochure_entry.amount_words FROM poultry.receipt_vochure_entry JOIN poultry.account_head ON poultry.receipt_vochure_entry.head_account=poultry.account_head.head_id WHERE poultry.receipt_vochure_entry.active_status='yes' and poultry.receipt_vochure_entry.type=:typevalue and poultry.receipt_vochure_entry.date>=:sdate and poultry.receipt_vochure_entry.date<=:edate and poultry.receipt_vochure_entry.company_id<=:company_id ".$sort_str." order by poultry.receipt_vochure_entry.updated_time asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':typevalue',$type);
            $stmt->bindParam(':sdate',$start_date);
            $stmt->bindParam(':edate',$end_date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            //$stmt->debugDumpParams();
            $rcount =$stmt->rowCount();
            $w1=6;$w2=10;$w3=13;$w4=10;$w5=22;$w6=30;$w7=10;
            $row_height=20;
            if($rcount>0)
            {
                $tbl_rows="";
                $sno=1;$total=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $row_date=$row['date'];
                    $rdate=explode('-', $row_date);
                    $date_format=$rdate[2]."-".$rdate[1]."-".$rdate[0];
                    $hname=$row['head_name'];
                    $rno=$row['id'];
                    $name=$row['name'];
                    $purpose=$row['purpose'];
                    $amount=$row['amount'];
                    $current='<tr>
                    <td height="'.$row_height.'" align="center" width="'.$w1.'%">'.$sno.'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w2.'%">'.$date_format.'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w3.'%">'.$hname.'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w4.'%">'.$rno.'</td>
                    <td height="'.$row_height.'" align="left" width="'.$w5.'%">'.$name.'</td>
                    <td height="'.$row_height.'" align="left" width="'.$w6.'%">'.$purpose.'</td>
                    <td height="'.$row_height.'" align="right" width="'.$w7.'%">'.$amount.'</td></tr>';
                    $tbl_rows.=$current;
                    $sno++;$total+=$amount;
                }
                $tfooter='<tfooter><tr><td vertical-align="middle" height="'.$row_height.'" colspan="6" align="center"><strong>Total</strong></td><td vertical-align="middle" height="'.$row_height.'" align="right"><strong>'.$total.'</strong></td></tr></tfooter>';

               
       
                $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->AddPage('P', 'A4');
                $lastPage = $pdf->getPage();
                $pdf->deletePage($lastPage);
        
                $header_data=document_header($company_id,$title);
                $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
                $pdf->SetCreator(PDF_CREATOR);
               
                $pdf->SetMargins(10, 30, 10, true);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
              $pdf->setFontSubsetting(true);
               $pdf->SetFont('dejavusans', '', 8, '', true);
               $pdf->AddPage();
               $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
              
               $tbl='<table border="1" cellpadding="2" width="100%">
                                <thead>
                                    <tr>
                                        <th border="0" colspan="2" align="right" vertical-align="middle" height="'.$row_height.'"><b>Starting & Ending Date</b></th>
                                        <th border="0" colspan="2" vertical-align="middle" height="'.$row_height.'">'.$start_date." to ".$end_date.'</th>
                                        <th border="0" vertical-align="middle" height="'.$row_height.'" colspan="3"></th>
                                    </tr>
                                    <tr style=" background-color:#FFFF00;color:#0000FF;">
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"><b>S.No.</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w2.'%" align="center"><b>Date</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"><b>Account Name</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w4.'%" align="center"> <b>'.$idno.'</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w5.'%" align="center"><b>Name</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w6.'%" align="center"><b>Purpose</b></th>
                                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w7.'%" align="center"><b>Amount in Rs.</b></th>
                                    </tr>
                                </thead>
                                <tbody>'.$tbl_rows.'</tbody>'.$tfooter.'</table>';
                               
               
                $pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
                $doc_location=document_namecreate("daysheet.pdf");
                $pdf->Output($doc_location, 'F');
                $_SESSION["download_docname"]="Poultry DaySheet.pdf";
                $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location);
               

            }else{echo "No Data";}
            database_close($conn);
        }
        catch(Exception $e) {echo $e->getMessage();} 
    }
?>