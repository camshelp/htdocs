<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "receipt_register":register_accounthead();
                    break;
            case "receipt_list":list_accounthead();
                    break;
            case "numtoword": numbertowordfn();
                    break;
            case "receipt_summary": receipt_summary();
                    break;
            case "receipt_remove":remove_entry();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function register_accounthead()
        {
            $type="Error";$msg="";$recei_id="";
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
            else
            {
              
                if(isset($_POST['rdate'])){  $date = $_POST['rdate']; }else{$msg="Date was not posted";}
                if(isset($_POST['rday'])){  $day = $_POST['rday']; }else{$msg="Day was not posted";}
                if(isset($_POST['rtype'])){  $type = $_POST['rtype']; }else{$msg="Type was not posted";}
                if(isset($_POST['rno'])){  $rno = $_POST['rno']; }else{$msg="Receipt No. was not posted";}
                if(isset($_POST['rname'])){  $name = $_POST['rname']; }else{$msg="Name was not posted";}
                if(isset($_POST['rpurp'])){  $purpose = $_POST['rpurp']; }else{$msg="Purpose was not posted";}
                if(isset($_POST['ramount'])){  $amount = $_POST['ramount']; }else{$msg="Amount was not posted";}
                if(isset($_POST['rword'])){  $word = $_POST['rword']; }else{$msg="Amount in Words was not posted";}
                if(isset($_POST['hname'])){  $hname = $_POST['hname']; }else{$msg="Account Head Name was not posted";}

                if($msg=="")
                {
                session_start();
                $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();

                try
                {
                    if($rno=="")
                    {
                        $sql="select id from poultry.receipt_vochure_entry where date=:date and head_account=:hacc and name=:name and amount=:amount";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':date', $date);
                        $stmt->bindParam(':hacc', $hname);
                        $stmt->bindParam(':name',$name);
                        $stmt->bindParam(':amount', $amount);
                        $stmt->execute();
                        $rowcount =$stmt->rowCount();
                        if($rowcount==0)
                        {
                            $sql = "select count(*) from poultry.receipt_vochure_entry where type=:type and company_id=:cid";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':type', $type);
                            $stmt->bindParam(':cid', $company_id);
                            $stmt->execute();
                            $row = $stmt->fetchColumn(0);
                            $add_value="1";$sub_id="";
                            $total=$row+$add_value;
                            if($company_id=="INST000004"){$sub_id="F"; }else{$sub_id="P";}
                        
                            if($type=="Receipt"){ $gen_id="R".$sub_id.SixDigitIdGenerator("$total");}else{ $gen_id="V".$sub_id.SixDigitIdGenerator("$total");}
                       
                            $astatus="yes";$appr="Approved";
                            $sql="insert into poultry.receipt_vochure_entry values(:cid,:id,:type,:date,:day,:hacc,:name,:purpose,:amount,:words,:by,:session,:time,:appr,:aby,:asession,:atime,:astatus)";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':cid',$company_id);
                            $stmt->bindParam(':id',$gen_id);
                            $stmt->bindParam(':type',$type);
                            $stmt->bindParam(':date',$date);
                            $stmt->bindParam(':day',$day);
                            $stmt->bindParam(':hacc',$hname);
                            $stmt->bindParam(':name',$name);
                            $stmt->bindParam(':purpose',$purpose);
                            $stmt->bindParam(':amount',$amount);
                            $stmt->bindParam(':words',$word);
                            $stmt->bindParam(':by', $update_by);
                            $stmt->bindParam(':session',$update_session);
                            $stmt->bindParam(':time',$update_time);
                            $stmt->bindParam(':appr',$appr);
                            $stmt->bindParam(':aby',$update_by);
                            $stmt->bindParam(':asession',$update_session);
                            $stmt->bindParam(':atime',$update_time);
                            $stmt->bindParam(':astatus',$astatus);
                            if ($stmt->execute() == TRUE) 
                            {
                                $type="Success";$msg="The Information Submitted Successfully";
                                $recei_id=$gen_id;
                            } 

                        }else{$msg="The Account Head Name was already found";}
                    }
                    else
                    {
                        $sql="update poultry.receipt_vochure_entry set head_account=:hacc,name=:name,purpose=:purpose,amount=:amount,amount_words=:words,updated_by=:by,updated_session=:session,updated_time=:time where id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id',$rno);
                        $stmt->bindParam(':hacc',$hname);
                        $stmt->bindParam(':name',$name);
                        $stmt->bindParam(':purpose',$purpose);
                        $stmt->bindParam(':amount',$amount);
                        $stmt->bindParam(':words',$word);
                        $stmt->bindParam(':by', $update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if ($stmt->execute() == TRUE) 
                        {
                            $type="Success";$msg="The Information Submitted Successfully";
                            $recei_id=$rno;
                        }    
                    }
                }catch(Exception $e){$msg=$e->getMessage();}
                }
              database_close($conn);
            } 
            $arr = ["result" => $type, "Message" => $msg,"id"=>$recei_id];
            echo json_encode($arr);
        }
        function remove_entry()
        {
            $type="Error";$msg="";
            $rno=$_POST['rno'];
            session_start();
            $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $update_time=get_datetime();

            $conn = database_open();
            $sql="update poultry.receipt_vochure_entry set updated_by=:by,updated_session=:session,updated_time=:time,active_status='no' where id=:id";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id',$rno);
            $stmt->bindParam(':by', $update_by);
            $stmt->bindParam(':session',$update_session);
            $stmt->bindParam(':time',$update_time);
            if ($stmt->execute() == TRUE) 
            {
                $type="Success";$msg="The Information Removed Successfully";
            }   
            database_close($conn);

            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
        function receipt_summary()
        { 
            session_start();
            $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
            $date=get_date();
            $day=get_day();
            $date_time=get_datetime();
            $pdate = date('Y-m-d', strtotime($date .' -1 day'));
            $conn = database_open();

            $stmt = $conn->prepare("select sum(closing_balance) from poultry.day_sheet where date=:date and company_id=:company_id");
            $stmt->bindParam(':date',$pdate);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $open_bal = $stmt->fetchColumn(0);
            
            $stmt = $conn->prepare("select count(*) from poultry.receipt_vochure_entry where date=:date and type='Receipt' and company_id=:company_id and active_status='yes'");
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $rcount = $stmt->fetchColumn(0);

            $stmt = $conn->prepare("select sum(amount) from poultry.receipt_vochure_entry where date=:date and type='Receipt' and company_id=:company_id and active_status='yes'");
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $ramount = $stmt->fetchColumn(0);

            $total_income=$open_bal+$ramount;

            $stmt = $conn->prepare("select count(*) from poultry.receipt_vochure_entry where date=:date and type='Voucher' and company_id=:company_id and active_status='yes'");
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $vcount = $stmt->fetchColumn(0);

            $stmt = $conn->prepare("select sum(amount) from poultry.receipt_vochure_entry where date=:date and type='Voucher' and company_id=:company_id and active_status='yes'");
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $vamount = $stmt->fetchColumn(0);

            $close_bal=$total_income-$vamount;

            database_close($conn);
            $arr = ["datetime" => $date_time, "date" => $date,"day"=>$day,"no_receipt"=>"","no_voucher"=>"", "opening_bal"=>$open_bal,"no_receipt"=>$rcount,"today_receipt"=>$ramount,"total_receipt"=>$total_income,"no_voucher"=>$vcount,"today_voucher"=>$vamount,"closing_balance"=>$close_bal];
            echo json_encode($arr);
        }
        function numbertowordfn()
        {
            $amount=$_POST['amount'];
            $word=numberTowords($amount);
            $arr = ["word" =>rtrim($word," ")];
            echo json_encode($arr);
        }
        function list_accounthead()
        {
            session_start();
            $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
            $type=$_POST['type'];
            $date=get_date();
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT poultry.account_head.head_id,poultry.account_head.head_name,poultry.receipt_vochure_entry.id,poultry.receipt_vochure_entry.date,poultry.receipt_vochure_entry.day,poultry.receipt_vochure_entry.name,poultry.receipt_vochure_entry.purpose,poultry.receipt_vochure_entry.amount,poultry.receipt_vochure_entry.amount_words FROM poultry.receipt_vochure_entry JOIN poultry.account_head ON poultry.receipt_vochure_entry.head_account=poultry.account_head.head_id WHERE poultry.receipt_vochure_entry.active_status='yes' and poultry.receipt_vochure_entry.type=:typevalue and poultry.receipt_vochure_entry.date=:date and poultry.receipt_vochure_entry.company_id=:company_id order by poultry.receipt_vochure_entry.updated_time asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':typevalue',$type);
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':company_id',$company_id);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row>0)
            {
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $json[$sno] = array(
                     'head_id' => $row['head_id'],
                     'head_name' => $row['head_name'],
                     'id' => $row['id'],
                     'date' => $row['date'],
                     'day' => $row['day'],
                     'name' => $row['name'],
                     'purpose' => $row['purpose'],
                     'amount' => $row['amount'],'amount_word'=>$row['amount_words']);
                    $sno++;
                }
            }
            database_close($conn);
            echo json_encode($json);   
        }