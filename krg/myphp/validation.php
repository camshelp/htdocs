<?php
class Validation {
    function ValidationCheck($form,$array)
    {
        $count=count($array);
        for($x=0;$x<$count;$x++)
        {
            $fname=$array[$x]["name"];
            $fdisp=$array[$x]["disp"];
            $max_count=$array[$x]["max"];
            $param=$array[$x]["param"];
            if(isset($form[$fname]))
            {
                $value=$form[$fname];
                $length=strlen($value);
                if($length<=$max_count)
                {
                    if($param=="1")
                    {
                        if($value==""){$x=$count;return "Please Enter value of ".$fdisp;}
                    }
                }else{$x=$count;return "Max length must be less than $max_count for ".$fdisp;}

            }else{$x=$count; return "Could Not find value of ".$fdisp;}
        }
    }
    function FileUploadCheck($file,$field_name,$max_size,$format)
    {
        $fileSize       = $file['uploadedFile']['size'];
        if($fileSize<=$max_size)
        {
            $fileName       = $file['uploadedFile']['name'];
            $fileType       = $file['uploadedFile']['type'];
            $fileNameCmps   = explode(".", $fileName);
            $fileExtension  = strtolower(end($fileNameCmps));
            $count=count($format);
            $is_checked=0;
            for($x=0;$x<$count;$x++)
            {
                if($fileExtension==$format[$x]){$is_checked=1;}
            }
            if($is_checked==0){return "Upload Valid File Format in $field_name";}

        }else{return "The File Size of $field_name Must be less than ".$max_size." KB".$fileSize;}
    }
    function FileUploadMove($from,$destination,$filename)
    {
        move_uploaded_file($from,$destination.$filename);
    }
}
?>