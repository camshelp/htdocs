<html>
    <head>
    <title>Welcome to KRG Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <script src="/PCTEM/scripts/jquery-3.3.1.js" type="text/javascript"></script>
   
    
    <script src="/PCTEM/scripts/Datatables/DataTables-1.10.18/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/DataTables-1.10.18/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/DataTables-1.10.18/js/dataTables.material.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/DataTables/Responsive-2.2.2/js/dataTables.responsive.min.js"  type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/buttons-1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/buttons-1.5.2/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/buttons-1.5.2/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/buttons-1.5.2/js/buttons.html5.min.js" type="text/javascript"></script> 
    <script src="/PCTEM/scripts/Datatables/jszip.min.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/pdfmake.min.js" type="text/javascript"></script> 
    <script src="/PCTEM/scripts/Datatables/vfs_fonts.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/Datatables/dataTables.checkboxes.min.js" type="text/javascript"></script>
    
    <link href="/PCTEM/scripts/material.min.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/Datatables/DataTables-1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/Datatables/DataTables-1.10.18/css/dataTables.material.min.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/DataTables/Responsive-2.2.2/css/responsive.dataTables.min.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/DataTables/buttons-1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="/PCTEM/scripts/Datatables/dataTables.checkboxes.css" rel="stylesheet" />

    <script src="/PCTEM/angular-1.6.9/angular.min.js" type='text/javascript'></script> 
    <script src="/PCTEM/angular-1.6.9/angular-route.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-resource.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-animate.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-aria.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-messages.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/svg-assets-cache.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-material.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-sanitize.js" type='text/javascript'></script>
    <script src="/PCTEM/angular-1.6.9/angular-touch.js" type='text/javascript'></script>
    
    <script src="/PCTEM/scripts/jquery.mask.js" type="text/javascript"></script>
    <script src="/PCTEM/scripts/gitter/bootstrap-notify.js" type="text/javascript"></script>
    <script src="/PCTEM/bootstrap/bootstrap-3.3.7/dist/js/bootstrap.js" type='text/javascript'></script>
    <link href="/PCTEM/bootstrap/bootstrap-3.3.7/dist/css/bootstrap.css" rel="stylesheet"/>
    
    <link href='/PCTEM/calander/fullcalendar.min.css' rel='stylesheet' />
    <link href='/PCTEM/calander/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='/PCTEM/calander/moment.min.js'></script>
    <script src='/PCTEM/calander/fullcalendar.min.js'></script>

    <script src="/PCTEM/scripts/pace.min.js"></script>
    <link href="/PCTEM/scripts/pace loader.css" rel="stylesheet" />

    <script src="/PCTEM/scripts/c3chart/d3-5.4.0.min.js"></script>
    <script src="/PCTEM/scripts/c3chart/c3.min.js"></script>
    <link href="/PCTEM/scripts/c3chart/c3.css" rel="stylesheet">

    <script src="/PCTEM/scripts/datetime/bootstrap-datetimepicker.min.js" type='text/javascript'></script>
    <link href="/PCTEM/scripts/datetime/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="/PCTEM/scripts/datetime/moment-with-locales.js" type='text/javascript'></script>
    <link href="/PCTEM/fonts/material/material-icons.css" rel="stylesheet" type="text/css" media="all">

    <script src="/PCTEM/materialize-1-dev/dist/js/materialize.js"></script>
    <link href="/PCTEM/materialize-1-dev/dist/css/materialize.css" type="text/css" rel="stylesheet"/>
    <link href="/PCTEM/angular-1.6.9/angular-material.css" rel="stylesheet" />
    
    <link href='/PCTEM/fonts/roboto/roboto.css' rel='stylesheet'>

    </head>
    <body></body>
</html>