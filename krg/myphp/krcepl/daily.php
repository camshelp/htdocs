<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/validation.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/pdfheader.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] ."/PCTEM/SVGGraph/SVGGraph.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/tcpdf-charts.php');
  include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');

  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "summary":overall_summary();
                    break;
            case "dateproduction":date_production();
                    break;
            case "fileupload":upload_excel();
                    break;
            case "unapprovedsheets":unapproved_sheets();
                    break;
            case "excelinfo":load_excel_data();
                    break;
            case "approvedata":Approve_Excel();
                    break;
            case "lastweekgraph":Last_Week_production();
                    break;
            case "year_summary":YearSummary();
                    break;
            case "fyear_data":entered_academic_year();
                    break;
            case "deletedata":RemoveFile();
                    break;
            case "lastdaysummary":overall_summary_last_date();
                    break;
            case "machineproductionsummary":Machine_summary();
                    break;
            case "dateproductionview": dateproductionview();
                    break;
            case "daily_summary":Daily_Summary();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function RemoveFile()
    {
        $conn = database_open();
        $fileid=$_POST['file_id'];
        $sql="update wind_mill.upload_log set approve_status='RM' where upload_id=:uid";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':uid',$fileid);
        $stmt->execute();
        $arr = ["result" => "Success", "Message" => "The Excel File Removed Successfully"];
        echo json_encode($arr);  
    }
    function overall_summary()
    {

        $conn = database_open();
        $sdate="-".$_POST['date'];

        $sql="select distinct date from wind_mill.daily_entry where status='yes' order by date desc limit 1";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $date = $stmt->fetchColumn(0);

        $sql="select count(*) from wind_mill.upload_log where approve_status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $no_file = $stmt->fetchColumn(0);

        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row = $stmt -> fetch();
        $production =$row[0];
        
        $machine_hour=$row[1];
        $mm= $row[2];
        $mh_mm=$mm/60; $machine_hour+=floor($mh_mm);
        $machine_min=$mm%60;

        $grid_hour= $row[3];
        $mm= $row[4];
        $mh_mm=$mm/60; $grid_hour+=floor($mh_mm);
        $grid_min=$mm%60;

        database_close($conn);
        $dsplt = explode('-',$date);
        $pdate = date('Y-m-d', strtotime($date .' -7 day'));
        $dsplt1 = explode('-',$pdate);
        $sdate=$dsplt[2]."-".$dsplt[1]."-".$dsplt[0];
        $edate=$dsplt1[2]."-".$dsplt1[1]."-".$dsplt1[0];
     
        $arr = ["sdate" =>$sdate,"edate"=>$edate,"no_file"=>$no_file,"production"=>$production,"mhour"=>$machine_hour,"mmin"=>$machine_min,"ghour"=>$grid_hour,"gmin"=>$grid_min];
        echo json_encode($arr);  
    }
    function overall_summary_last_date()
    {
        $conn = database_open();
        $sql="select distinct date from wind_mill.daily_entry where status='yes' order by date desc limit 1";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $last_date = $stmt->fetchColumn(0);
        $ayear=AYEARINKRCEPLBYDATE($last_date);
        $dsplt = explode('-',$last_date);
        $myear=$dsplt[0].'-'.$dsplt[1];
        
        $sql="select distinct wind_mill.daily_entry.unit_id,wind_mill.unit.unit_name from wind_mill.daily_entry join wind_mill.unit on wind_mill.unit.unit_id=wind_mill.daily_entry.unit_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date=:date order by wind_mill.daily_entry.unit_id asc";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':date',$last_date);
        $stmt->execute();
        //$stmt->debugDumpParams();
        $sno=0;
        $udata = array();
        while($data = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $sql="select count(*),sum(capacity) from wind_mill.machine where unit=:uid";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':uid',$data['unit_id']);
            $stmt1->execute();
            $row = $stmt1 -> fetch();
            $capacity = $row[1];
            $num_machine=$row[0];

            $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and unit_id=:uid and date=:ldate";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':ldate',$last_date);
            $stmt1->bindParam(':uid',$data['unit_id']);
            $stmt1->execute();
            $row = $stmt1 -> fetch();
            $production =$row[0];

            $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and unit_id=:uid and date<=:ldate and academic_year=:ayear";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':ldate',$last_date);
            $stmt1->bindParam(':uid',$data['unit_id']);
            $stmt1->bindParam(':ayear',$ayear);
            $stmt1->execute();
            $row = $stmt1 -> fetch();
            $year_production =$row[0];

           $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and unit_id=:uid and date like CONCAT('%',:date, '%') and academic_year=:ayear and date<=:ldate";
           $stmt1 = $conn->prepare($sql); 
           $stmt1->bindParam(':date',$myear);
           $stmt1->bindParam(':ldate',$last_date);
           $stmt1->bindParam(':uid',$data['unit_id']);
           $stmt1->bindParam(':ayear',$ayear);
           $stmt1->execute();
           $row = $stmt1 -> fetch();
           $month_production =$row[0];

            $udata[$sno] = array('id' => $data['unit_id'],'unit' => $data['unit_name'],'total_machine'=>$num_machine,'total_capacity'=>number_format($capacity, 2, '.', ''),'daily_generation'=>$production,'monthly_generation'=>$month_production,'yearly_generation'=>$year_production);
            $sno++;
        }
       

        database_close($conn);
        $arr = ["last_date" =>$last_date,'unit_data'=>$udata];
        echo json_encode($arr);  
    }
    function Machine_summary()
    {
        $last_date=$_POST['date'];
        $ayear=AYEARINKRCEPLBYDATE($last_date);
        $dsplt = explode('-',$last_date);
        $myear=$dsplt[0].'-'.$dsplt[1];
        $pyear= ($dsplt[0]-1).'-'.$dsplt[1].'-'.$dsplt[2];
        $pmyear= ($dsplt[0]-1).'-'.$dsplt[1];
        $conn = database_open();
        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and machine_id=:mid and date=:date";
        $stmt1 = $conn->prepare($sql); 
        $stmt1->bindParam(':date',$last_date);
        $stmt1->bindParam(':mid',$_POST['machine_id']);
        $stmt1->execute();
        $row = $stmt1 -> fetch();
        $current_production =$row[0];
        if($current_production==null){$current_production=0;}
        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and machine_id=:mid and date=:date";
        $stmt1 = $conn->prepare($sql); 
        $stmt1->bindParam(':date',$pyear);
        $stmt1->bindParam(':mid',$_POST['machine_id']);
        $stmt1->execute();
        $row = $stmt1 -> fetch();
        $previous_production =$row[0];if($previous_production==null){$previous_production=0;}
        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and machine_id=:mid and date like CONCAT('%',:date, '%') and date<=:ldate";
        $stmt1 = $conn->prepare($sql); 
        $stmt1->bindParam(':date',$myear);
        $stmt1->bindParam(':ldate',$last_date);
        $stmt1->bindParam(':mid',$_POST['machine_id']);
        $stmt1->execute();
        $row = $stmt1 -> fetch();
        $monthly_production =$row[0];if($monthly_production==null){$monthly_production=0;}

        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and machine_id=:mid and date like CONCAT('%',:date, '%') and date<=:ldate";
        $stmt1 = $conn->prepare($sql); 
        $stmt1->bindParam(':date',$pmyear);
        $stmt1->bindParam(':ldate',$last_date);
        $stmt1->bindParam(':mid',$_POST['machine_id']);
        $stmt1->execute();
        $row = $stmt1 -> fetch();
        $pmonthly_production =$row[0];if($pmonthly_production==null){$pmonthly_production=0;}

        $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and machine_id=:mid and academic_year=:ayear and date<=:ldate";
        $stmt1 = $conn->prepare($sql); 
        $stmt1->bindParam(':ayear',$ayear);
        $stmt1->bindParam(':ldate',$last_date);
        $stmt1->bindParam(':mid',$_POST['machine_id']);
        $stmt1->execute();
        $row = $stmt1 -> fetch();
        $yearly_production =$row[0];if($yearly_production==null){$yearly_production=0;}
        
      
        $month_data=array();
        $month_data[0]=array(date('M, Y', strtotime($last_date .' -0 day')));array_push($month_data[0],$monthly_production);
        $month_data[1]=array(date('M, Y', strtotime($pyear .' -0 day')));array_push($month_data[1],$pmonthly_production);
        
        $cmonth= date('m', strtotime($last_date .' -0 day'));
        $cyear= date('Y', strtotime($last_date .' -0 day'));
        $yearp= date('Y', strtotime($pyear .' -0 day'));

        $months=array("04","05","06","07","08","09","10","11","12","01","02","03");
        $months_name=array("April","May","June","July","August","September","October","November","December","January","Febrary","March");
        $yr_production=array();
        $mc_avl=array();
        $gd_avl=array();
        $sql="SELECT DISTINCT academic_year FROM wind_mill.daily_entry WHERE STATUS='yes' ORDER BY DATE DESC LIMIT 2";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $sno=0;
        while($data = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $yr_production[$sno]=array($data["academic_year"]);
            $mc_avl[$sno]=array($data["academic_year"]);
            $gd_avl[$sno]=array($data["academic_year"]);
            $acsplt = explode('-',$data["academic_year"]);
            for($x=0;$x<count($months);$x++)
            {
                if($cmonth==$months[$x]){$x=count($months);}
                {
                   $month_year=$acsplt[0].'-'.$months[$x];
                   if($months[$x]<"04"){ $month_year=$acsplt[1].'-'.$months[$x];}
                   $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min),count(machine_id),count(distinct date) from wind_mill.daily_entry where status='yes' and machine_id=:mid and date like CONCAT('%',:date, '%')";
                   $stmt1 = $conn->prepare($sql); 
                   $stmt1->bindParam(':date',$month_year);
                   $stmt1->bindParam(':mid',$_POST['machine_id']);
                   $stmt1->execute();
                   $row = $stmt1 -> fetch();

                   $production =$row[0];if($production==null){$production=0;}
                   array_push($yr_production[$sno],$production);

                   $max_time=$row[5]*$row[6]* 24*60;
                   $mc_break_mins=($row[1]*60)+$row[2];
                   $gd_break_mins=($row[3]*60)+$row[4];
   
                   $mc_avl1=($max_time-$mc_break_mins)*100/$max_time;
                   $gd_avl1=($max_time-$gd_break_mins)*100/$max_time;
   
                   array_push($mc_avl[$sno],number_format($mc_avl1, 2, '.', ''));
                   array_push($gd_avl[$sno],number_format($gd_avl1, 2, '.', ''));
                }
            }
            $sno++;

        }
        

        database_close($conn);
        $arr = ['today_date'=>$last_date,'today_production'=>$current_production,'previous_date'=>$pyear,'previous_production'=>$previous_production,'monthly_production'=>$monthly_production,'yearly_production'=>$yearly_production,'mdata'=>$month_data,'months'=>$months_name,'yr_data'=>$yr_production,'mc_avl'=>$mc_avl,'gd_avl'=>$gd_avl];
        echo json_encode($arr);  
    }
    function date_production()
    {
        //var_dump($_POST);
        $json = array();
        $conn = database_open();
        $type=$_POST['type'];
        $dsplt = explode('-',$_POST['sdate']);
        $dsplt1 = explode('-',$_POST['edate']);
        $sdate=$dsplt[2]."-".$dsplt[1]."-".$dsplt[0];
        $edate=$dsplt1[2]."-".$dsplt1[1]."-".$dsplt1[0];

        $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->bindParam(':edate',$edate);
        $stmt->execute();
        $total_date = $stmt->fetchColumn(0);

        $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->bindParam(':edate',$edate);
        $stmt->execute();
        $total_machine = $stmt->fetchColumn(0);

        $total_capacity=0; $total_production=0;
        $tmc_hour=0;$tmc_min=0;$tgd_hour=0;$tgd_min=0;$tmax_time=0;$tmc_brktime=0;$tgd_brktime=0;
        $sno=0;
        if($type=="Unit")
        {
            $sql="select distinct wind_mill.daily_entry.unit_id,wind_mill.unit.unit_name from wind_mill.daily_entry join wind_mill.unit on wind_mill.unit.unit_id=wind_mill.daily_entry.unit_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date<=:sdate and wind_mill.daily_entry.date>=:edate order by wind_mill.daily_entry.unit_id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sdate);
            $stmt->bindParam(':edate',$edate);
            $stmt->execute();
            //$stmt->debugDumpParams();
            while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and unit_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['unit_id']);
                $stmt1->execute();
                $no_machine = $stmt1->fetchColumn(0);

                $sql="SELECT SUM(capacity) FROM wind_mill.machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM wind_mill.daily_entry WHERE date<=:sdate and date>=:edate AND unit_id=:uid)";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid',$data['unit_id']);
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->execute();
                $capacity = $stmt1->fetchColumn(0);
                $total_capacity+=$capacity;

                $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and unit_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['unit_id']);
                $stmt1->execute();
                $no_date = $stmt1->fetchColumn(0);
              
                $max_time=$no_machine*$no_date*24*60;
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where date<=:sdate and date>=:edate and unit_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['unit_id']);
                $stmt1->execute();
                $row = $stmt1 -> fetch();
                $production = $row[0]; $total_production+=$production;

                $mc_break_mins=($row[1]*60)+$row[2];
                $gd_break_mins=($row[3]*60)+$row[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                $machine_hour=floor($mc_break_mins/60); $machine_min=$mc_break_mins%60;
                $grid_hour=floor($gd_break_mins/60); $grid_min=$gd_break_mins%60;

               
                $tmc_brktime+=$mc_break_mins; $tgd_brktime+=$gd_break_mins;
                $plf=plf_calculation($production,$capacity,$no_date);

                $json[$sno] = array('id' => $data['unit_id'],'name' => $data['unit_name'],'capacity'=>number_format($capacity, 2, '.', ''),"mc_brk"=>$machine_hour." Hrs ".$machine_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$grid_hour." Hrs ".$grid_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$production,"plf"=>$plf);
                $sno++;
            }

           
        }
        else if($type=="Location")
        {
            $sql="select distinct wind_mill.daily_entry.location_id,wind_mill.location.location_name from wind_mill.daily_entry join wind_mill.location on wind_mill.location.location_id=wind_mill.daily_entry.location_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date<=:sdate and wind_mill.daily_entry.date>=:edate order by wind_mill.daily_entry.location_id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sdate);
            $stmt->bindParam(':edate',$edate);
            $stmt->execute();
            //$stmt->debugDumpParams();
            while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and location_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['location_id']);
                $stmt1->execute();
                $no_machine = $stmt1->fetchColumn(0);

                $sql="SELECT SUM(capacity) FROM wind_mill.machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM wind_mill.daily_entry WHERE date<=:sdate and date>=:edate AND location_id=:uid)";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid',$data['location_id']);
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->execute();
                $capacity = $stmt1->fetchColumn(0); $total_capacity+=$capacity;

                $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and location_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['location_id']);
                $stmt1->execute();
                $no_date = $stmt1->fetchColumn(0);
                $max_time=$no_machine*$no_date*24*60;
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where date<=:sdate and date>=:edate and location_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['location_id']);
                $stmt1->execute();
                $row = $stmt1 -> fetch();
                $production = $row[0]; $total_production+=$production;
                $mc_break_mins=($row[1]*60)+$row[2];
                $gd_break_mins=($row[3]*60)+$row[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                $machine_hour=floor($mc_break_mins/60); $machine_min=$mc_break_mins%60;
                $grid_hour=floor($gd_break_mins/60); $grid_min=$gd_break_mins%60;
                
                $plf=plf_calculation($production,$capacity,$no_date);
                $tmc_brktime+=$mc_break_mins; $tgd_brktime+=$gd_break_mins;
                $json[$sno] = array('id' => $data['location_id'],'name' => $data['location_name'],'capacity'=>number_format($capacity, 2, '.', ''),"mc_brk"=>$machine_hour." Hrs ".$machine_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$grid_hour." Hrs ".$grid_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$production,"plf"=>$plf);
                $sno++;
            }
        }
        else if($type=="Machine")
        {
            $sql="select distinct wind_mill.daily_entry.machine_id,wind_mill.machine.machine_name from wind_mill.daily_entry join wind_mill.machine on wind_mill.machine.machine_id=wind_mill.daily_entry.machine_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date<=:sdate and wind_mill.daily_entry.date>=:edate order by wind_mill.daily_entry.machine_id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sdate);
            $stmt->bindParam(':edate',$edate);
            $stmt->execute();
            //$stmt->debugDumpParams();
            while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $no_machine = $stmt1->fetchColumn(0);

               
                $sql="SELECT SUM(capacity) FROM wind_mill.machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM wind_mill.daily_entry WHERE date<=:sdate and date>=:edate AND machine_id=:uid)";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->execute();
                $capacity = $stmt1->fetchColumn(0); $total_capacity+=$capacity;

                $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $no_date = $stmt1->fetchColumn(0);
                $max_time=$no_machine*$no_date*24*60;
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $row = $stmt1 -> fetch();
                $production = $row[0]; $total_production+=$production;
                $mc_break_mins=($row[1]*60)+$row[2];
                $gd_break_mins=($row[3]*60)+$row[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                $machine_hour=floor($mc_break_mins/60); $machine_min=$mc_break_mins%60;
                $grid_hour=floor($gd_break_mins/60); $grid_min=$gd_break_mins%60;
                
               
                $plf=plf_calculation($production,$capacity,$no_date);
                $tmc_brktime+=$mc_break_mins; $tgd_brktime+=$gd_break_mins;
                $json[$sno] = array('id' => $data['machine_id'],'name' => $data['machine_name'],'capacity'=>number_format($capacity, 2, '.', ''),"mc_brk"=>$machine_hour." Hrs ".$machine_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$grid_hour." Hrs ".$grid_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$production,"plf"=>$plf);
                $sno++;
            }
        }
        else if($type=="Division")
        {
            $sql="select distinct wind_mill.division_list.division_id,wind_mill.division_list.division_name from wind_mill.daily_entry join wind_mill.division_list on wind_mill.division_list.division_id=wind_mill.daily_entry.division_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date<=:sdate and wind_mill.daily_entry.date>=:edate order by wind_mill.division_list.division_name asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sdate);
            $stmt->bindParam(':edate',$edate);
            $stmt->execute();
            //$stmt->debugDumpParams();
            while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and division_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['division_id']);
                $stmt1->execute();
                $no_machine = $stmt1->fetchColumn(0);

                $sql="SELECT SUM(capacity) FROM wind_mill.machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM wind_mill.daily_entry WHERE date<=:sdate and date>=:edate AND division_id=:uid)";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['division_id']);
                $stmt1->execute();
                $capacity = $stmt1->fetchColumn(0); $total_capacity+=$capacity;

                $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and division_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['division_id']);
                $stmt1->execute();
                $no_date = $stmt1->fetchColumn(0);
                $max_time=$no_machine*$no_date*24*60;
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where date<=:sdate and date>=:edate and division_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['division_id']);
                $stmt1->execute();
                $row = $stmt1 -> fetch();
                $production = $row[0]; $total_production+=$production;
                $mc_break_mins=($row[1]*60)+$row[2];
                $gd_break_mins=($row[3]*60)+$row[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                $machine_hour=floor($mc_break_mins/60); $machine_min=$mc_break_mins%60;
                $grid_hour=floor($gd_break_mins/60); $grid_min=$gd_break_mins%60;
                
               
                $plf=plf_calculation($production,$capacity,$no_date);
                $tmc_brktime+=$mc_break_mins; $tgd_brktime+=$gd_break_mins;
                $json[$sno] = array('id' => $data['division_id'],'name' => $data['division_name'],'capacity'=>number_format($capacity, 2, '.', ''),"mc_brk"=>$machine_hour." Hrs ".$machine_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$grid_hour." Hrs ".$grid_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$production,"plf"=>$plf);
                $sno++;
            }
        }

        $plf=plf_calculation($total_production,$total_capacity,$total_date);
        $tmax_time=$total_machine*$total_date*24*60;
        $tmc_hour=floor($tmc_brktime/60); $tmc_min=$tmc_brktime%60;
        $tgd_hour=floor($tgd_brktime/60); $tgd_min=$tgd_brktime%60;

        $mc_avl=($tmax_time-$tmc_brktime)*100/$tmax_time;
        $gd_avl=($tmax_time-$tgd_brktime)*100/$tmax_time;

        $totalary= ['id' =>'','name' => 'Total','capacity'=>number_format($total_capacity, 2, '.', ''),"mc_brk"=>$tmc_hour." Hrs ".$tmc_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$tgd_hour." Hrs ".$tgd_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$total_production,"plf"=>$plf];
        database_close($conn);
        $arr = ["data" => $json, "total" => $totalary];
        echo json_encode($arr);
    }
    function dateproductionview()
    {
        $json = array();
        $conn = database_open();
        $type=$_POST['type'];
        $dsplt = explode('-',$_POST['sdate']);
        $dsplt1 = explode('-',$_POST['edate']);
        $para_id=$_POST['id'];
        $sdate=$dsplt[2]."-".$dsplt[1]."-".$dsplt[0];
        $edate=$dsplt1[2]."-".$dsplt1[1]."-".$dsplt1[0];

        $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->bindParam(':edate',$edate);
        $stmt->execute();
        $total_date = $stmt->fetchColumn(0);

        $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->bindParam(':edate',$edate);
        $stmt->execute();
        $total_machine = $stmt->fetchColumn(0);

        $total_capacity=0; $total_production=0;
        $tmc_hour=0;$tmc_min=0;$tgd_hour=0;$tgd_min=0;$tmax_time=0;$tmc_brktime=0;$tgd_brktime=0;
        $sno=0;

           $sql="select distinct wind_mill.daily_entry.machine_id,wind_mill.machine.machine_name from wind_mill.daily_entry join wind_mill.machine on wind_mill.machine.machine_id=wind_mill.daily_entry.machine_id where wind_mill.daily_entry.status='yes' and wind_mill.daily_entry.date<=:sdate and wind_mill.daily_entry.date>=:edate and (wind_mill.daily_entry.unit_id=:id or wind_mill.daily_entry.location_id=:id or wind_mill.daily_entry.division_id=:id) order by wind_mill.daily_entry.machine_id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sdate);
            $stmt->bindParam(':edate',$edate);
            $stmt->bindParam(':id',$para_id);
            $stmt->execute();
            //$stmt->debugDumpParams();
            while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select count(distinct machine_id) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $no_machine = $stmt1->fetchColumn(0);

               
                $sql="SELECT SUM(capacity) FROM wind_mill.machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM wind_mill.daily_entry WHERE date<=:sdate and date>=:edate AND machine_id=:uid)";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->execute();
                $capacity = $stmt1->fetchColumn(0); $total_capacity+=$capacity;

                $sql="select count(distinct date) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $no_date = $stmt1->fetchColumn(0);
                $max_time=$no_machine*$no_date*24*60;
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where date<=:sdate and date>=:edate and machine_id=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':sdate',$sdate);
                $stmt1->bindParam(':edate',$edate);
                $stmt1->bindParam(':uid',$data['machine_id']);
                $stmt1->execute();
                $row = $stmt1 -> fetch();
                $production = $row[0]; $total_production+=$production;
                $mc_break_mins=($row[1]*60)+$row[2];
                $gd_break_mins=($row[3]*60)+$row[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                $machine_hour=floor($mc_break_mins/60); $machine_min=$mc_break_mins%60;
                $grid_hour=floor($gd_break_mins/60); $grid_min=$gd_break_mins%60;
                
               
                $plf=plf_calculation($production,$capacity,$no_date);
                $tmc_brktime+=$mc_break_mins; $tgd_brktime+=$gd_break_mins;
                $json[$sno] = array('id' => $data['machine_id'],'name' => $data['machine_name'],'capacity'=>number_format($capacity, 2, '.', ''),"mc_brk"=>$machine_hour." Hrs ".$machine_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$grid_hour." Hrs ".$grid_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$production,"plf"=>$plf);
                $sno++;
            }
        
        

        $plf=plf_calculation($total_production,$total_capacity,$total_date);
        $tmax_time=$total_machine*$total_date*24*60;
        $tmc_hour=floor($tmc_brktime/60); $tmc_min=$tmc_brktime%60;
        $tgd_hour=floor($tgd_brktime/60); $tgd_min=$tgd_brktime%60;

        $mc_avl=($tmax_time-$tmc_brktime)*100/$tmax_time;
        $gd_avl=($tmax_time-$tgd_brktime)*100/$tmax_time; 

        $totalary= ['id' =>'','name' => 'Total','capacity'=>number_format($total_capacity, 2, '.', ''),"mc_brk"=>$tmc_hour." Hrs ".$tmc_min." Mins","mc_avl"=>number_format($mc_avl, 2, '.', ''),"gd_brk"=>$tgd_hour." Hrs ".$tgd_min." Mins","gd_avl"=>number_format($gd_avl, 2, '.', ''),"production"=>$total_production,"plf"=>$plf];
        database_close($conn);
        $arr = ["data" => $json, "total" => $totalary];
        echo json_encode($arr);
    }
    
    function plf_calculation($production,$capacity,$no_day)
    {
        $plf=($production*100)/($capacity*1000*$no_day*24);
        if(is_nan($plf)){$plf=0;}
        return number_format($plf, 2, '.', '');
    }
    function upload_excel()
    {
        $type="Error";$msg="";
         $conn = database_open();
        if (strpos($conn,"Failed") === 0) {$msg=$conn;}
        else
        {
            $varry=array(array('name'=>"etype",'disp'=>'Excel Category','param'=>"1","max"=>20));
            $fformat=array("xlsx");
            $valid=new Validation();
            $msg = $valid->ValidationCheck($_POST,$varry);
            if($msg=="") { $msg = $valid->FileUploadCheck($_FILES,"Excel Sheet",15*1048576,$fformat);  }
            if($msg=="")
            {
                session_start();
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);;
                try
                {
                    $type=$_POST['etype'];
                    $fileTmpPath    = $_FILES['uploadedFile']['tmp_name'];
                    $fileName       = $_FILES['uploadedFile']['name'];
                    $fileNameCmps   = explode(".", $fileName);
                    $fileExtension  = strtolower(end($fileNameCmps));

                    $sql = "select count(*) from wind_mill.upload_log";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $row = $stmt->fetchColumn(0);
                    $add_value="1";
                    $total=(integer)$row+(integer)$add_value;
                    $upd_id="UP".SixDigitIdGenerator("$total");
                    $para="Generation";$astatus="no";$dummy="";
                    $sql="insert into wind_mill.upload_log values(:gid,:type,:id,:para,:name,:ext,:by,:session,:time,:stat,:aby,:asession,:atime)";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':gid',$company_id);
                    $stmt->bindParam(':type',$type);
                    $stmt->bindParam(':id',$upd_id);
                    $stmt->bindParam(':para',$para);
                    $stmt->bindParam(':name',$fileName);
                    $stmt->bindParam(':ext',$fileExtension);
                    $stmt->bindParam(':by', $update_by);
                    $stmt->bindParam(':session', $update_session);
                    $stmt->bindParam(':time', $update_time);
                    $stmt->bindParam(':stat',$astatus);
                    $stmt->bindParam(':aby', $dummy);
                    $stmt->bindParam(':asession', $dummy);
                    $stmt->bindParam(':atime', $dummy);
                    if ($stmt->execute() == TRUE) 
                    {
                        $type="Success";$msg="The Information Submitted Successfully";
                        $valid->FileUploadMove($fileTmpPath, path_location()."krcepl\\uploads\\",$upd_id.".".$fileExtension);
                    }

                }catch(PDOException $e){$msg=$e->getMessage();}

            }
            database_close($conn);
        } 
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);  
    }
    function unapproved_sheets()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $conn = database_open();
        $sql="SELECT upload_id,format_type,file_name,extension,upload_time from wind_mill.upload_log where approve_status='no'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        database_close($conn);
        echo json_encode($result);
    }
    function entered_academic_year()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $conn = database_open();
        $sql="SELECT distinct academic_year from wind_mill.daily_entry where status='yes' order by academic_year asc";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        database_close($conn);
        echo json_encode($result);
    }
    function load_excel_data()
    {
        $path=path_location()."krcepl\\uploads\\";
        $type=$_POST['type'];
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $sno=0;
        $conn = database_open();
        if($type=="SuZlon")
        {
           $production= ReadFromExcelFunctionCommon($_POST['id'],$_POST['sheet_name'],"0",$path);
           $breakdown= ReadFromExcelFunctionCommon($_POST['id'],$_POST['sheet_name'],"1",$path);
           $exceldata=$production[1];
           $row=count($exceldata);

           $bdata=$breakdown[1];
           $brow=count($bdata);

            for($i=2;$i<=$row;$i++)
            {
                $rowdata=$exceldata[$i];
                $machine=$rowdata["G"];

                $sql="select machine_id,unit,location,capacity from wind_mill.machine where machine_name=:id";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':id', $machine);
                $stmt->execute();
                $row1 =$stmt->rowCount();
                if($row1>0)
                {
                    $data= $stmt -> fetch();
                    $date=$rowdata["A"];$capacity=$rowdata["F"];
                    $mc_avl=$rowdata["N"];$gd_avl=((24 - $rowdata["O"])* 100)/24;
                    $mc_break_per=100-$mc_avl; $gd_brk_per=100-$gd_avl;
                    $max_min=24*60;
                    $mc_brk_mins= ((100 - $mc_avl) * $max_min) / 100;
                    $gd_brk_mins=((100-$gd_avl)*$max_min)/100;
                    $mc_hr=floor($mc_brk_mins/60);$mc_min=$mc_brk_mins%60;
                    $gd_hr=floor($gd_brk_mins/60);$gd_min=$gd_brk_mins%60;
                   
                     $plf=plf_calculation($rowdata["H"],$data["capacity"],1);
                     $gen_data=explode('.',$rowdata["T"]);
                     $tgen_mins=($gen_data[0]*60)+$gen_data[1];
                     $gen_hour=ceil($tgen_mins/60);
                     $gen_min=$tgen_mins%60;
                        $mc_reason="";$gd_reason="";
                        for($j=2;$j<=$brow;$j++)
                        {
                            $rowdata1=$bdata[$j];
                            $mc=$rowdata1["G"];$capa=$rowdata1["F"];$da=$rowdata1["A"];
                            $factor=$rowdata1['I'];
                            if($machine==$mc && $capa==$capacity && $da==$date)
                            {
                                if($factor=="GF")
                                {
                                    $gd_reason=$rowdata1['H'];
                                }
                                else
                                {
                                    $mc_reason=$rowdata1['H'];
                                }
                            }
                        }   
                        $json[$sno] = array(
                            'machine_id'=>$data["machine_id"],
                            'unit_id'=>$data["unit"],
                            'location_id'=>$data["location"],
                            'name' => $machine,
                            'date' => date('Y-m-d', strtotime($date .' 0 day')),
                            'capacity' => $capacity,
                            'mc_brk' => $mc_hr.":".$mc_min,
                            'mc_brk_per' =>number_format($mc_break_per, 2, '.', ''),
                            'mc_brk_reason' => $mc_reason,
                            'mc_avl' =>number_format($mc_avl, 2, '.', ''),
                            'gd_brk' => $gd_hr.":".$gd_min,
                            'gd_brk_per' =>number_format($gd_brk_per, 2, '.', ''),
                            'gd_brk_reason' => $gd_reason,
                            'gd_avl' =>number_format($gd_avl, 2, '.', ''),
                            'production'=>$rowdata["H"],
                            'plf'=>$plf,'generation_hour'=>$gen_hour.':'.$gen_min);
                        $sno++;
                }
            }
        }
        else if($type=="Vestas")
        {
            $production= ReadFromExcelFunctionCommon($_POST['id'],$_POST['sheet_name'],"0",$path);
            $breakdown= ReadFromExcelFunctionCommon($_POST['id'],$_POST['sheet_name'],"1",$path);
            $exceldata=$production[1];
            $row=count($exceldata);
 
            $bdata=$breakdown[1];
            $brow=count($bdata);
 
             for($i=2;$i<=$row;$i++)
             {
                 $rowdata=$exceldata[$i];
                 $htsc=$rowdata["E"];
                 $turbine=$rowdata["F"];

                 $sql="select machine_name,machine_id,unit,location,capacity from wind_mill.machine where htsc=:id";
                 $stmt = $conn->prepare($sql); 
                 $stmt->bindParam(':id',$htsc);
                 $stmt->execute();
                 $row1 = $stmt->rowCount();
                 if($row1>0)
                 {
                     $data= $stmt -> fetch();
                     $machine=$data["machine_name"];
                     $date=$rowdata["H"]; $capacity=$data["capacity"];
                     $mc_avl=$rowdata["M"];$gd_avl=$rowdata["N"];
                     $mc_break_per=100-$mc_avl; $gd_brk_per=100-$gd_avl;
                     $max_min=24*60;
                     $mc_brk_mins= ((100 - $mc_avl) * $max_min) / 100;
                     $gd_brk_mins=((100-$gd_avl)*$max_min)/100;
                     $mc_hr=floor($mc_brk_mins/60);$mc_min=$mc_brk_mins%60;
                     $gd_hr=floor($gd_brk_mins/60);$gd_min=$gd_brk_mins%60;
                    
                    $plf=plf_calculation($rowdata["I"],$data["capacity"],1);
                         $mc_reason="";$gd_reason="";
                         for($j=2;$j<=$brow;$j++)
                         {
                             $rowdata1=$bdata[$j];
                             $turb=$rowdata1["D"];$da=$rowdata1["A"];
                             if( $turb==$turbine && $da==$date)
                             {
                                 if($factor=="GF")
                                 {
                                     $gd_reason=$rowdata1['E'];
                                 }
                                 else
                                 {
                                     $mc_reason=$rowdata1['E'];
                                 }
                             }
                         }   
                         $json[$sno] = array(
                             'machine_id'=>$data["machine_id"],
                             'unit_id'=>$data["unit"],
                             'location_id'=>$data["location"],
                             'name' => $machine,
                             'date' => date('Y-m-d', strtotime($date .' 0 day')),
                             'capacity' => $capacity,
                             'mc_brk' => $mc_hr.":".$mc_min,
                             'mc_brk_per' =>number_format($mc_break_per, 2, '.', ''),
                             'mc_brk_reason' => $mc_reason,
                             'mc_avl' =>number_format($mc_avl, 2, '.', ''),
                             'gd_brk' => $gd_hr.":".$gd_min,
                             'gd_brk_per' =>number_format($gd_brk_per, 2, '.', ''),
                             'gd_brk_reason' => $gd_reason,
                             'gd_avl' =>number_format($gd_avl, 2, '.', ''),
                             'production'=>$rowdata["I"],
                             'plf'=>$plf,'generation_hour'=>$rowdata["J"].':0');
                         $sno++;
                 }
             }
        }
        else if($type=="Gamesa")
        {
            $production= ReadFromExcelFunctionCommon($_POST['id'],$_POST['sheet_name'],"0",$path);
            $exceldata=$production[1];
            $row=count($exceldata);
 
             for($i=2;$i<=$row;$i++)
             {
                 $rowdata=$exceldata[$i];
                 $htsc=$rowdata["B"];
                 
                 $sql="select machine_name,machine_id,unit,location,capacity from wind_mill.machine where htsc=:id";
                 $stmt = $conn->prepare($sql); 
                 $stmt->bindParam(':id',$htsc);
                 $stmt->execute();
                 $row1 = $stmt->rowCount();
                 if($row1>0)
                 {
                     $data= $stmt -> fetch();
                     $machine=$data["machine_name"];
                     $date=$rowdata["A"]; $capacity=$data["capacity"];
                     $mc_avl=$rowdata["R"];$gd_avl=$rowdata["S"];
                     $mc_break_per=100-$mc_avl; $gd_brk_per=100-$gd_avl;
                     $max_min=24*60;
                     $mc_brk_mins= ((100 - $mc_avl) * $max_min) / 100;
                     $gd_brk_mins=((100-$gd_avl)*$max_min)/100;
                     $mc_hr=floor($mc_brk_mins/60);$mc_min=$mc_brk_mins%60;
                     $gd_hr=floor($gd_brk_mins/60);$gd_min=$gd_brk_mins%60;
                    
                    $plf=plf_calculation($rowdata["C"],$data["capacity"],1);

                    $gen_data=$rowdata["I"];
                    if($gen_data==":00:00"){$gen_data="24:0";}
                         $mc_reason="";$gd_reason="";
                        
                         $json[$sno] = array(
                             'machine_id'=>$data["machine_id"],
                             'unit_id'=>$data["unit"],
                             'location_id'=>$data["location"],
                             'name' => $machine,
                             'date' => date('Y-m-d', strtotime($date .' 0 day')),
                             'capacity' => $capacity,
                             'mc_brk' => $mc_hr.":".$mc_min,
                             'mc_brk_per' =>number_format($mc_break_per, 2, '.', ''),
                             'mc_brk_reason' => $mc_reason,
                             'mc_avl' =>number_format($mc_avl, 2, '.', ''),
                             'gd_brk' => $gd_hr.":".$gd_min,
                             'gd_brk_per' =>number_format($gd_brk_per, 2, '.', ''),
                             'gd_brk_reason' => $gd_reason,
                             'gd_avl' =>number_format($gd_avl, 2, '.', ''),
                             'production'=>$rowdata["C"],
                             'plf'=>$plf,'generation_hour'=>$gen_data);
                         $sno++;
                 }
             }   
        }
        database_close($conn);
        echo json_encode($json);
    }
    function Approve_Excel()
    {
        $type="Error";$msg="";
        $upload_id=$_POST['file_id'];
        $someArray = json_decode($_POST['data'], true);
        $row=count($someArray);
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        $update_time=get_datetime();
        $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
        $conn = database_open();
        $ok=0;$already_found=0;
        for($x=0;$x<$row;$x++)
        {
            $date=$someArray[$x]["date"];
            $capacity=$someArray[$x]["capacity"];
          
            $mc_brk=explode(":",$someArray[$x]["mc_brk"]);
            $mc_brk_per=$someArray[$x]["mc_brk_per"];
            $mc_brk_reason=$someArray[$x]["mc_brk_reason"];
            $mc_avl=$someArray[$x]["mc_avl"];

            $gd_brk=explode(":",$someArray[$x]["gd_brk"]);
            $gd_brk_per=$someArray[$x]["gd_brk_per"];
            $gd_brk_reason=$someArray[$x]["gd_brk_reason"];
            $gd_avl=$someArray[$x]["gd_avl"];

            $production=$someArray[$x]["production"];
            $plf=$someArray[$x]["plf"];
            $genhour=explode(':',$someArray[$x]["generation"]);

            $ids=explode("_",$someArray[$x]["ids"]);

            $sql="select machine_id,unit,location,division_id,capacity from wind_mill.machine where machine_name=:id";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $ids[0]);
            $stmt->execute();
            $row1 =$stmt->rowCount();
            if($row1>0)
            {
                $data= $stmt -> fetch();
                $sql="select machine_id from wind_mill.daily_entry where date=:date and machine_id=:mid";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':date',$date);
                $stmt->bindParam(':mid',$data['machine_id']);
                $stmt->execute();
                $check_row =$stmt->rowCount();
                if($check_row==0)
                {
                    $ayear=AYEARINKRCEPLBYDATE($date);
                    $disp_date="";
                    $status="yes";
                    $sql="insert into wind_mill.daily_entry values(:ayear,:date,:unit,:machine,:location,:division,:mhour,:mmin,:mbreak,:mreason,:mavl,:ghour,:gmin,:gbreak,:greason,:gavl,:production,:plf,:ghr,:gmin,:status,:by,:session,:time)";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':ayear',$ayear);
                    $stmt->bindParam(':date',$date);
                    $stmt->bindParam(':unit',$data['unit']);
                    $stmt->bindParam(':machine',$data['machine_id']);
                    $stmt->bindParam(':location',$data['location']);
                    $stmt->bindParam(':division',$data['division_id']);
                    $stmt->bindParam(':mhour',$mc_brk[0]);
                    $stmt->bindParam(':mmin',$mc_brk[1]);
                    $stmt->bindParam(':mbreak',$mc_brk_per);
                    $stmt->bindParam(':mreason',$mc_brk_reason);
                    $stmt->bindParam(':mavl',$gd_avl);
                    $stmt->bindParam(':ghour',$gd_brk[0]);
                    $stmt->bindParam(':gmin',$gd_brk[1]);
                    $stmt->bindParam(':gbreak',$gd_brk_per);
                    $stmt->bindParam(':greason',$gd_brk_reason);
                    $stmt->bindParam(':gavl',$gd_avl);
                    $stmt->bindParam(':production',$production);
                    $stmt->bindParam(':plf',$plf);
                    $stmt->bindParam(':ghr',$genhour[0]);
                    $stmt->bindParam(':gmin',$genhour[1]);
                    $stmt->bindParam(':status',$status);
                    $stmt->bindParam(':by',$update_by);
                    $stmt->bindParam(':session',$update_session);
                    $stmt->bindParam(':time',$update_time);
                    $stmt->execute();
                }else{$already_found++;}
            }
            else{$ok=1;}

        }
        if($ok==0)
        {
            if($already_found==0)
            {
                $astatus="yes";
                $sql="update wind_mill.upload_log set approve_status=:astat,approved_by=:by,approved_session=:session,approved_time=:time where upload_id=:id";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':id',$upload_id);
                $stmt->bindParam(':astat',$astatus);
                $stmt->bindParam(':by',$update_by);
                $stmt->bindParam(':session',$update_session);
                $stmt->bindParam(':time',$update_time);
                $stmt->execute();
                $type="Success";$msg="The Information has been approved successfully";
            }
        }
        else{$msg="Some Records are Not Approved";}
        database_close($conn);
        if($already_found>0){$msg=$already_found." No.of Records are already found";$type="danger";}
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);  
    }
    function Last_Week_production()
    {
        $conn = database_open();
       

        $sql="select distinct date from wind_mill.daily_entry where status='yes' order by date desc limit 1";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $date = $stmt->fetchColumn(0);

        $sql="select distinct unit_id,unit_name from wind_mill.unit where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row = $stmt->rowCount();
        $datearry=array();  $json=array();
        $breakdown=array();
        $breakdown[0]=array("M/C Availability");
        $breakdown[1]=array("Grid Availability");
        if($row>0)
        {
            $sno=0;
         /*    while($data = $stmt->fetch(PDO::FETCH_BOTH))
            {
              
                $json[$sno]=array($data['unit_name']);
                for($x=0;$x<7;$x++)
                {
                    $cdate = date('Y-m-d', strtotime($date .' -'.$x.' day'));
                    if($sno==0)
                    {
                        $dsplt1 = explode('-',$cdate);
                        $disp_date=$dsplt1[2]."-".$dsplt1[1]."-".$dsplt1[0];
                        array_push($datearry,$disp_date);
                    }
                    $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min) from wind_mill.daily_entry where status='yes' and unit_id=:uid and date=:date";
                    $stmt1 = $conn->prepare($sql);
                    $stmt1->bindParam(':uid',$data['unit_id']); 
                    $stmt1->bindParam(':date',$cdate); 
                    $stmt1->execute();
                    $pdata = $stmt1 -> fetch();

                    $production =$pdata[0];
                    array_push($json[$sno],$production);
                }
                $sno++;
            } */
            $json[$sno]=array('Production');
           
            for($x=0;$x<7;$x++)
            {
                $cdate = date('Y-m-d', strtotime($date .' -'.$x.' day'));
                $dsplt1 = explode('-',$cdate);
                $disp_date=$dsplt1[2]."-".$dsplt1[1]."-".$dsplt1[0];
                array_push($datearry,$disp_date);
                
                $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min),count(machine_id) from wind_mill.daily_entry where status='yes' and date=:date";
                $stmt1 = $conn->prepare($sql);
                $stmt1->bindParam(':date',$cdate); 
                $stmt1->execute();
                $pdata = $stmt1 -> fetch();
                $production =$pdata[0];
                array_push($json[$sno],$production);
                
                $max_time=$pdata[5]* 24*60;
                $mc_break_mins=($pdata[1]*60)+$pdata[2];
                $gd_break_mins=($pdata[3]*60)+$pdata[4];

                $mc_avl=($max_time-$mc_break_mins)*100/$max_time;
                $gd_avl=($max_time-$gd_break_mins)*100/$max_time;

                array_push($breakdown[0],number_format($mc_avl, 2, '.', ''));
                array_push($breakdown[1],number_format($gd_avl, 2, '.', ''));
            }
        }

        database_close($conn);
        

        $arr = ["x_label" => $datearry, "production" =>$json,"breakdown"=>$breakdown];
        echo json_encode($arr);
    }
    function YearSummary()
    {
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");

        $fyears=$_POST['fyears'];
        $date1=date_create($_POST['edate']);
        $date2=date_create($_POST['sdate']);
        $diff=date_diff($date1,$date2);
        $diff_month= $diff->format("%m Months");
        $wi1=23;$wi2=22;$wi3=10;$wi4=8;$wi5=18;$wi6=15;
        $dsplt = explode('-',$_POST['edate']);
        $ymd_value=$dsplt[2].'-'.$dsplt[1].'-'.$dsplt[0];
        if($diff_month>0)
        {
            $fyear_count=count($fyears);
            if($fyear_count>0)
            {
            $company_id="INST00000511";$title="Machine Production and Availability Details";
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->SetMargins(10, 14, 10, true);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                $pdf->AddPage('L', 'A4');
                $lastPage = $pdf->getPage();
                $pdf->deletePage($lastPage);
    
                $header_data=document_header($company_id,$title);
                $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
                $pdf->SetCreator(PDF_CREATOR);
    
                
                $pdf->SetAutoPageBreak(TRUE, $margin=1);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('dejavusans', '', 8, '', true);
            //$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
            $settings = array(
                'back_colour' => '#eee',  
                'axis_colour' => '#333',  'axis_overlap' => 2,
                'axis_font' => 'Georgia', 'axis_font_size' => 10,
                'grid_colour' => '#666',  'label_colour' => 'black',
                'pad_right' => 20,        'pad_left' => 20,
                'link_base' => '/',       'link_target' => '_top',
                'minimum_grid_spacing' => 20,
                'show_data_labels' => true,
                'data_label_type' => array('plain', 'box', 'plain', 'box','plain', 'box', 'plain'),
                'data_label_space' => 5,
                 'data_label_back_colour' => array('#ccc','yellow', '#ccc', '#ccc'),
                //'data_label_padding' => 3,
                'data_label_round' => 2,
                'data_label_tail_length' => "auto",
                'data_label_tail_width' => 5,
                'data_label_font_size' => 10,
                /* 'data_label_color'=>array('black','yellow','black','red','black'), */
                /* 'data_label_fill' => array(array('#ccc','#fff','#ccc','h')), */
                /* 'data_label_outline_thickness' => 2, */
                'data_label_position' => array('above', 'middle', 'above','middle','above','middle'),
                'data_label_tail_end' => 'filled',
                'data_label_tail_end_width' => 16,
            );
            $colours = array(array('blue'), array('green'), array('yellow'),array('red'), array('orange'), array('pink'));
            $legend=array();
            $uids="";
            $uary=$_POST['unit'];
            for($x=0;$x<count($uary);$x++)
            {
                $uids.=" unit_id='".$uary[$x]."' or";
            }
            if($uids!="")
            {
                $uids="and ".rtrim($uids,"or");
            }
           // echo $uids;
            $conn = database_open();
            $sql="select distinct unit_id,unit_name from wind_mill.unit where status='yes' ".$uids;
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $row =$stmt->rowCount();
            $usno=0;
            if($row>0)
            {
               //$plf[0]=array();
              for($x=0;$x<$fyear_count;$x++)
                {
                    $plf[$x]=array();
                    $machine_production[$x]=array();
                    $total_yr[$x]=array();
                    $tot_machine[$x]=array();
                } 
                //echo $fyear_count;
                 while($udata = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id where wind_mill.machine.status='yes' and wind_mill.machine.unit=:uid";
                    $stmt1 = $conn->prepare($sql); 
                    $stmt1->bindParam(':uid',$udata['unit_id']);
                    $stmt1->execute();
                    $machines =$stmt1->rowCount();
                    if($machines>0)
                    {
                        
                        while($mdata = $stmt1->fetch(PDO::FETCH_BOTH))
                        {
                            $pdf->AddPage();
                            $values=array();
                            $avl_values=array(); $mc_avl=array();$gd_avl=array();
                            $total_max_time=0;
                            $mplf=array();$yr_data="";
                            for($x=0;$x<$fyear_count;$x++)
                            {
                                $fyear=$fyears[$x];
                                array_push($legend,$fyear);
                                $ary1=array();
                                $total_production=0;
                                $total_max_time=0; $total_mc_time=0;$total_gd_time=0;
                                $total_days=0;
                                for($y=0;$y<=$diff_month;$y++)
                                {
                                    $cdate = date('-m-', strtotime($ymd_value.' +'.$y.' month'));
                                    $sql="select sum(production),sum(machine_hour),sum(machine_min),sum(grid_hour),sum(grid_min),count(machine_id),count(date) from wind_mill.daily_entry where status='yes' and date like CONCAT('%',:date, '%') and academic_year=:fyear and machine_id=:mid";
                                    $stmt2 = $conn->prepare($sql);
                                    $stmt2->bindParam(':date',$cdate); 
                                    $stmt2->bindParam(':fyear',$fyear); 
                                    $stmt2->bindParam(':mid',$mdata['machine_id']); 
                                    $stmt2->execute();
                                    $pdata = $stmt2 -> fetch();
                                    $production=$pdata[0];
                                    if($production==""){$production=0;}
                                    
                                    $month_name=date('F', strtotime($ymd_value.' +'.$y.' month'));
                                    $total_days+=$pdata[6];

                                    $total_production+=$production;
                                    $ary2=array($month_name=>$production);
                                    $ary1=array_merge($ary1,$ary2);

                                    if($x==($fyear_count-1))
                                    {
                                        $max_time=$pdata[5]* $pdata[6] * 24 * 60;
                                        $mc_break_mins=($pdata[1]*60)+$pdata[2];
                                        $gd_break_mins=($pdata[3]*60)+$pdata[4];
                        
                                        $mc_avly=($max_time-$mc_break_mins)*100/$max_time;
                                        $gd_avly=($max_time-$gd_break_mins)*100/$max_time;
                                        if(is_nan($mc_avly)){$mc_avly="0";}
                                        if(is_nan($gd_avly)){$gd_avly="0";}

                                        $ary2=array($month_name=>number_format($mc_avly,2));
                                        $mc_avl=array_merge($mc_avl,$ary2);

                                        $ary2=array($month_name=>number_format($gd_avly,2));
                                        $gd_avl=array_merge($gd_avl,$ary2);

                                        $total_max_time+=$max_time;
                                        $total_mc_time+=$mc_break_mins; $total_gd_time+=$gd_break_mins;
                                    }

                                }
                             
                                $plfdata=plf_calculation($total_production,$mdata['capacity'],$total_days);

                               // $plfdata=($total_production*100)/($mdata['capacity']*1000*$total_days*24);

                                if(is_nan($plfdata)){$plfdata="0";}
                                $ary2=array($mdata['machine_name'] => $plfdata);
                                $plf[$x]=array_merge($plf[$x],$ary2);

                                $new_total=(int)$total_yr[$x]+$total_production;
                                $total_yr[$x] = $new_total;
                                if($total_production>0)
                                {
                                    $new_total=(int)$tot_machine[$x]+1;
                                    $tot_machine[$x]=$new_total;
                                }

                                $ary2=array($mdata['machine_name'] => $total_production);
                                $machine_production[$x]=array_merge($machine_production[$x],$ary2);
                                
                                if($x==($fyear_count-1))
                                {
                                    $mc_avly=($total_max_time-$total_mc_time)*100/$total_max_time;
                                    $gd_avly=($total_max_time-$total_gd_time)*100/$total_max_time;
                                    if(is_nan($mc_avly)){$mc_avly="0";}
                                    if(is_nan($gd_avly)){$gd_avly="0";}
                                    
                                    $ary2=array("Avg"=>number_format($mc_avly,2));
                                    $mc_avl=array_merge($mc_avl,$ary2);

                                    $ary2=array("Avg"=>number_format($gd_avly,2));
                                    $gd_avl=array_merge($gd_avl,$ary2);

                                }

                                $avg=$total_production/$diff_month;
                             $yr_data.='<tr>
                                <td align="center">'. $fyear.'</td>
                                <td align="right">'.$total_production.'</td>
                                <td align="right">'.number_format($avg,2).'</td>
                            </tr>';

                               /*  $ary2=array("Total"=>$total_production);
                                $ary1=array_merge($ary1,$ary2);

                                $avg=$total_production/$diff_month;
                                $ary2=array("Avg"=>$avg);
                                $ary1=array_merge($ary1,$ary2); */

                                array_push($values,$ary1);
                            }
                            array_push($avl_values,$mc_avl);
                            array_push($avl_values,$gd_avl);
                            
                            $settings['legend_entries'] = $legend;
                            $settings['legend_position'] ="outer right -20 40";
                            $graph = new SVGGraph(850, 260, $settings);
                            $graph->colours = $colours;
                            $graph->Values($values);
                            $monthly_production = $graph->fetch('GroupedBarGraph');

                            $arr=array('axis_max_v'=>110);
                            $settings1=array_merge($settings,$arr);
                            $settings1['legend_entries']=array("Machine Availbility","Grid Availability");
                            $graph1 = new SVGGraph(850, 250, $settings1);
                            $graph1->colours = $colours;
                            $graph1->Values($avl_values);
                            $avl = $graph1->fetch('GroupedBarGraph');
                    
                            //echo json_encode($values);

                            $html = '<table width="100%" cellpadding="2" cellspacing="2">
                                <tr>
                                        <td width="'.$wi1.'%"><strong>Unit Name</strong></td>
                                        <td width="'.$wi2.'%"><strong>Location Name</strong></td>
                                        <td width="'.$wi3.'%"><strong>Machine Name</strong></td>
                                        <td width="'.$wi4.'%"><strong>Capacity</strong></td>
                                        <td width="'.$wi5.'%"><strong>Insurance Due Date</strong></td>
                                        <td width="'.$wi6.'%"><strong>O & M Due Date</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="'.$wi1.'%">'.$udata["unit_name"].'</td>
                                        <td width="'.$wi2.'%">'.$mdata["location_name"].'</td>
                                        <td width="'.$wi3.'%">'.$mdata["machine_name"].'</td>
                                        <td width="'.$wi4.'%">'.$mdata["capacity"].'</td>
                                        <td width="'.$wi5.'%">'.$mdata["insurance_date"].'</td>
                                        <td width="'.$wi6.'%">'.$mdata["om_date"].'</td>
                                    </tr>
                                        <tr>
                                            <td  width="'.($wi1+$wi2+$wi3+$wi4+$wi5).'%" colspan="5">'.$pdf->ImageSVG( '@'. $monthly_production, $x='0', $y='25', $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false ).'</td>
                                            <td  width="'.$wi6.'%" colspan="1">
                                                <table  width="100%" border="1px" cellpadding="1" cellspacing="1">
                                                        <tr>
                                                            <td align="center" width="58px"><strong>Year</strong></td>
                                                            <td align="center" width="65px"><strong>Total</strong></td>
                                                            <td align="center" width="60px"><strong>Avg</strong></td>
                                                        </tr>
                                                        '.$yr_data.'
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" colspan="6">'.$pdf->ImageSVG( '@'. $avl, $x='0', $y='110', $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false ).'</td>
                                        </tr>
                                    </table>';
                            $pdf->writeHTML($html, true, false, true, false, '');
                        }
                    }
                }
               // $plf=array(array("M1"=>100,"M2"=>200),array("M1"=>150,"M2"=>250));
                //echo json_encode($plf);
                //plf graph
                $pdf->AddPage();
                $yr_data="";
                for($x=0;$x<$fyear_count;$x++)
                {
                    $avg=$total_yr[$x]/$tot_machine[$x];
                    $yr_data.='<tr>
                                <td align="center">'. $fyear[$x].'</td>
                                <td align="right">'. $total_yr[$x].'</td>
                                <td align="right">'.number_format($avg,2).'</td>
                            </tr>';
                }
                $arr=array('axis_text_angle_h' => -90);
                $settings1=array_merge($settings,$arr);
                $settings1['data_label_angle'] = -90;
                $settings1['legend_entries']=$legend;
                $graph1 = new SVGGraph(1050, 290, $settings1);
                $graph1->colours = $colours;
                $graph1->Values($plf);
                $plf_graph = $graph1->fetch('GroupedBarGraph');

                $arr=array('axis_text_angle_h' => -90, 'data_label_position' => array('above', 'middle', 'above','middle','above','middle'), 'data_label_type' => array('plain', 'box', 'plain', 'box','plain', 'box', 'plain'));
                $settings1=array_merge($settings,$arr);
                $settings1['data_label_angle'] = -90;
                $settings1['legend_entries']=$legend;
                $graph1 = new SVGGraph(870, 250, $settings1);
                $graph1->colours = $colours;
                $graph1->Values($machine_production);
                $production_graph = $graph1->fetch('GroupedBarGraph');
                
                $html = '<table width="100%" cellpadding="2" cellspacing="2">
                        <tr>
                            <td align="center" width="100%" colspan="2"><h3>Machinewise Production Information</h3></td>
                        </tr>
                        <tr height="260px">
                            <td height="260px" width="82%">'.$pdf->ImageSVG( '@'. $production_graph, $x='0', $y='20', $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false ).'</td>
                            <td width="18%">
                                <table  width="100%" border="1px" cellpadding="1" cellspacing="1">
                                        <tr>
                                            <td align="center" width="58px"><strong>Year</strong></td>
                                            <td align="center" width="65px"><strong>Total</strong></td>
                                            <td align="center" width="70px"><strong>Avg</strong></td>
                                        </tr>
                                        '.$yr_data.'
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="100%" colspan="2"><h3>Machinewise PLF Information</h3></td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="2">'.$pdf->ImageSVG( '@'. $plf_graph, $x='0', $y='100', $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false ).'</td>
                        </tr>
                    </table>';
                $pdf->writeHTML($html, true, false, true, false, '');
            }
            database_close($conn);

            
           
                $doc_name="Year Summary.pdf";
                $doc_location=document_namecreate($doc_name);
                $pdf->Output($doc_location, 'F');
                $_SESSION["download_docname"]=$doc_name;
                $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
            }
            else{echo "Please Choose Atleast one Financial Year to download report";}
        }
        else
        {
            echo "Month Difference must be grater than or equal to 1";
        }
    }
    function Daily_Summary()
    {
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");

        $date=$_POST['edate'];
        $dspl=explode('-',$date);
        $cdate=$dspl[2].'-'.$dspl[1].'-'.$dspl[0];
        $pdate=($dspl[2]-1).'-'.$dspl[1].'-'.$dspl[0];
        $month='-'.$dspl[1].'-';
        $academic_year=AYEARINKRCEPLBYDATE($cdate);
        $asplt=explode('-',$academic_year);
        $pacademic_year=($asplt[0]-1).'-'.($asplt[1]-1);
      
            $company_id="INST00000511";$title="Wind Generation - ".$date;
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(10, 14, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->AddPage('L', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

           
            $pdf->SetAutoPageBreak(TRUE, $margin=2.5);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('dejavusans', '', 5.7, '', true);
            $pdf->AddPage();
           // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        $row_height=20;$line_height=15;
        $w1=3;$w2=8;$w3=3;$w4=5;$w5=5;$w6=3;$w7=3;$w8=4;$w9=4;$w10=4;$w11=5;$w12=5;$w13=3;$w14=5;$w15=4;$w16=4;$w17=4;$w18=4;$w19=4;$w20=4;
        $w21=5;$w22=11;
        $header_row='<tr>
                        <th rowspan="2" align="center" colspan="1"><b>S.No.</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>Division Name</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>Capacity</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>M/c Avail %</b></th>
                        <th rowspan="2" align="center" colspan="1" ><b>Grid Avail %</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>Gen Hours</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>PLF</b></th>
                        <th rowspan="1" align="center" colspan="3"><b>MRD('.$academic_year.')</b></th>
                        <th rowspan="2" align="center" colspan="1" ><b>M/c Avail %</b></th>
                        <th rowspan="2" align="center" colspan="1" ><b>Grid Avail %</b></th>
                        <th rowspan="2" align="center" colspan="1"><b>Gen Hours</b></th>
                        <th rowspan="2" align="center" colspan="1" ><b>PLF</b></th>
                        <th rowspan="1" align="center" colspan="3"><b>MRD('.$pacademic_year.')</b></th>
                        <th rowspan="1" align="center" colspan="3" ><b>Difference</b></th>
                    </tr>
                    <tr>
                        <th rowspan="1" align="center" colspan="1" ><b>Today</b></th>
                        <th rowspan="1" align="center" colspan="1" ><b>Monthly</b></th>
                        <th rowspan="1" align="center" colspan="1" ><b>Yearly</b></th>
                        <th rowspan="1" align="center" colspan="1" ><b>Today</b></th>
                        <th rowspan="1" align="center" colspan="1" ><b>Monthly</b></th>
                        <th rowspan="1" align="center"  colspan="1" ><b>Yearly</b></th>
                        <th rowspan="1" align="center" colspan="1" ><b>Today</b></th>
                        <th rowspan="1" align="center" colspan="1"><b>Monthly</b></th>
                        <th rowspan="1" align="center" colspan="1"><b>Yearly</b></th>
                    </tr>';

                    $header_row1='<tr>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w1.'%"><b>S.No.</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w2.'%"><b>Name</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w3.'%"><b>Capacity</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w21.'%"><b>Breakdown Hours</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w22.'%"><b>Reason</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w4.'%"><b>M/c Avail %</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w5.'%"><b>Grid Avail %</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w6.'%"><b>Gen Hours</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w7.'%"><b>PLF</b></th>
                                    <th rowspan="1" align="center" colspan="3" width="'.($w8+$w9+$w10).'%"><b>MRD('.$academic_year.')</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w11.'%"><b>M/c Avail %</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w12.'%"><b>Grid Avail %</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w13.'%"><b>Gen Hours</b></th>
                                    <th rowspan="2" align="center" colspan="1" width="'.$w14.'%"><b>PLF</b></th>
                                    <th rowspan="1" align="center" colspan="3" width="'.($w15+$w16+$w17).'%"><b>MRD('.$pacademic_year.')</b></th>
                                    <th rowspan="1" align="center" colspan="3" width="'.($w18+$w19+$w20).'%"><b>Difference</b></th>
                                </tr>
                                <tr>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w8.'%"><b>Today</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w9.'%"><b>Monthly</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w10.'%"><b>Yearly</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w15.'%"><b>Today</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w16.'%"><b>Monthly</b></th>
                                    <th rowspan="1" align="center"  colspan="1" width="'.$w17.'%"><b>Yearly</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w18.'%"><b>Today</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w19.'%"><b>Monthly</b></th>
                                    <th rowspan="1" align="center" colspan="1" width="'.$w20.'%"><b>Yearly</b></th>
                                </tr>';
                    $conn = database_open();
                    $sql="SELECT unit_id,unit_name from wind_mill.unit order by unit_name asc";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $max_time=24*60;
                    $html_data=""; $html_data1="";

                    $tcapacity=0;$tcmtime=0;$tcgtime=0;$tpmtime=0;$tpgtime=0;$tgctime=0;$tgptime=0;$tmachines=0;
                    $tcday=0;$tcmonth=0;$tcyear=0;
                    $tpday=0;$tpmonth=0;$tpyear=0;
                    $tdday=0;$tdmonth=0;$tdyear=0;

                    while($udata = $stmt->fetch(PDO::FETCH_BOTH))
                    {
                        $html_data.='<h2>'.$udata['unit_name'].'</h2>';
                        $html_data1.='<h2>'.$udata['unit_name'].'</h2>';
                        $sql="SELECT distinct wind_mill.division_list.division_id,wind_mill.division_list.division_name from wind_mill.division_list join wind_mill.machine on wind_mill.machine.division_id=wind_mill.division_list.division_id where wind_mill.machine.unit=:uid order by division_name asc";
                        $stmt1 = $conn->prepare($sql); 
                        $stmt1->bindParam(':uid',$udata['unit_id']);
                        $stmt1->execute();
                        $rows1='';$dno=1;
                        $ucapacity=0;$ucmtime=0;$ucgtime=0;$upmtime=0;$upgtime=0;$ugctime=0;$ugptime=0;$umachines=0;
                        $ucday=0;$ucmonth=0;$ucyear=0;
                        $upday=0;$upmonth=0;$upyear=0;
                        $udday=0;$udmonth=0;$udyear=0;
                        while($ddata = $stmt1->fetch(PDO::FETCH_BOTH))
                        {
                            $rows=''; 
                            $sql="SELECT distinct machine_id,machine_name,capacity from wind_mill.machine where wind_mill.machine.unit=:uid and wind_mill.machine.division_id=:did order by machine_name asc";
                            $stmt2 = $conn->prepare($sql); 
                            $stmt2->bindParam(':uid',$udata['unit_id']);
                            $stmt2->bindParam(':did',$ddata['division_id']);
                            $stmt2->execute();
                            $sno=1;
                            $total_capacity=0;$break_mins=0;$cgen_mins=0;$pgen_mins=0;$ctoday=0;$cmon=0;$cyr=0;$pday=0;$pmon=0;$pyr=0;
                            $diffd=0;$diffm=0;$diffyr=0;
                            $machine_timec=0;$grid_timec=0;
                            $machine_timep=0;$grid_timep=0;
                            while($mdata = $stmt2->fetch(PDO::FETCH_BOTH))
                            {
                                $sql="select * from wind_mill.daily_entry where date=:date and machine_id=:mid and status='yes'";
                                $stmt3 = $conn->prepare($sql); 
                                $stmt3->bindParam(':date',$cdate);
                                $stmt3->bindParam(':mid',$mdata['machine_id']);
                                $stmt3->execute();
                                //$stmt3->debugDumpParams();
                                $cdata = $stmt3 -> fetch();

                                $sql="select machine_hour,grid_hour,machine_min,grid_min,machine_available,grid_available,production,plf,generation_hour,generation_min from wind_mill.daily_entry where date=:date and machine_id=:mid and status='yes'";
                                $stmt4 = $conn->prepare($sql); 
                                $stmt4->bindParam(':date',$pdate);
                                $stmt4->bindParam(':mid',$mdata['machine_id']);
                                $stmt4->execute();
                                $pdata = $stmt4 -> fetch();

                                $thour=$cdata['machine_hour']+$cdata['grid_hour'];
                                $tmin=$cdata['machine_min']+$cdata['grid_min'];
                                $minutes=($thour*60)+$tmin;
                              
                                $hr=ceil($minutes/60);
                                $min=$minutes%60;
                                $break_mins+=($hr*60)+$min; 
                                
                                $cmtime=($cdata['machine_hour']*60)+$cdata['machine_min'];
                                $gmtime=($cdata['grid_hour']*60)+$cdata['grid_min'];

                                $pmtime=($pdata['machine_hour']*60)+$pdata['machine_min'];
                                $pgtime=($pdata['grid_hour']*60)+$pdata['grid_min'];

                                $machine_timec+=$cmtime;
                                $grid_timec+=$gmtime;

                                $machine_timep+=$pmtime;
                                $grid_timep+=$pgtime;
                                
                                $reason=$cdata['machine_reason'].' '.$cdata['reason'];

                                $sql="select sum(production) from wind_mill.daily_entry where machine_id=:mid and date like CONCAT('%',:month, '%') and status='yes' and date<=:date and academic_year=:year";
                                $stmt5 = $conn->prepare($sql); 
                                $stmt5->bindParam(':month',$month);
                                $stmt5->bindParam(':year',$academic_year);
                                $stmt5->bindParam(':date',$cdate);
                                $stmt5->bindParam(':mid',$mdata['machine_id']);
                                $stmt5->execute();
                                $cmonthp = $stmt5 -> fetch();

                                $sql="select sum(production) from wind_mill.daily_entry where machine_id=:mid and date like CONCAT('%',:month, '%') and status='yes' and date<=:date and academic_year=:year";
                                $stmt6 = $conn->prepare($sql); 
                                $stmt6->bindParam(':month',$month);
                                $stmt6->bindParam(':year',$pacademic_year);
                                $stmt6->bindParam(':date',$pdate);
                                $stmt6->bindParam(':mid',$mdata['machine_id']);
                                $stmt6->execute();
                                $pmonthp = $stmt6 -> fetch();

                                $sql="select sum(production) from wind_mill.daily_entry where machine_id=:mid and academic_year=:year and status='yes' and date<=:date";
                                $stmt7 = $conn->prepare($sql); 
                                $stmt7->bindParam(':year',$academic_year);
                                $stmt7->bindParam(':date',$cdate);
                                $stmt7->bindParam(':mid',$mdata['machine_id']);
                                $stmt7->execute();
                                $cyearp = $stmt7 -> fetch();

                                $sql="select sum(production) from wind_mill.daily_entry where machine_id=:mid and academic_year=:year and status='yes' and date<=:date";
                                $stmt8 = $conn->prepare($sql); 
                                $stmt8->bindParam(':year',$pacademic_year);
                                $stmt8->bindParam(':date',$pdate);
                                $stmt8->bindParam(':mid',$mdata['machine_id']);
                                $stmt8->execute();
                                $pyearp = $stmt8 -> fetch();

                                $day_diff=$cdata['production']-$pdata['production'];
                                $month_diff=$cmonthp[0]-$pmonthp[0];
                                $year_diff=$cyearp[0]-$pyearp[0];

                                $total_capacity+=$mdata['capacity'];
                                
                                $cgen_mins+= ($cdata['generation_hour']*60)+$cdata['generation_min'];
                                $pgen_mins+= ($pdata['generation_hour']*60)+$pdata['generation_min'];
                                
                               

                                $cmper=($max_time-$cmtime)*100/$max_time;if(is_nan($cmper)){$cmper=0;}
                                $cgper=($max_time-$pgtime)*100/$max_time;if(is_nan($cgper)){$cgper=0;}
    
                                $pmper=($max_time-$pmtime)*100/$max_time;if(is_nan($pmper)){$pmper=0;}
                                $pgper=($max_time-$pgtime)*100/$max_time;if(is_nan($pgper)){$pgper=0;}

                                $ctoday+=$cdata['production'];$cmon+=$cmonthp[0];$cyr+=$cyearp[0];
                                $pday+=$pdata['production'];$pmon+=$pmonthp[0];$pyr+=$pyearp[0];

                                $diffd+=$day_diff;$diffm+=$month_diff;$diffy+=$year_diff;

                                if($day_diff>0){$diff_color1="#00c853";}else{$diff_color1="#d50000";}
                                if($month_diff>0){$diff_color2="#00c853";}else{$diff_color2="#d50000";}
                                if($year_diff>0){$diff_color3="#00c853";}else{$diff_color3="#d50000";}
                                $rows.='<tr>
                                            <th rowspan="1" align="center" colspan="1" width="'.$w1.'%">'.$sno.'</th>
                                            <th rowspan="1" align="left" colspan="1" width="'.$w2.'%">'.$mdata['machine_name'].'</th>
                                            <th rowspan="1" align="center" colspan="1" width="'.$w3.'%">'.number_format($mdata['capacity'],2).'</th>
                                            <th rowspan="1" align="center" colspan="1" width="'.$w21.'%">'.$hr.'.'.$min.'</th>
                                            <th rowspan="1" align="center" colspan="1" width="'.$w22.'%"></th>
                                            <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w4.'%">'.number_format($cmper,2).'</th>
                                            <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w5.'%">'.number_format($cgper,2).'</th>
                                            <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w6.'%">'.$cdata['generation_hour'].'.'.$cdata['generation_min'].'</th>
                                            <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w7.'%">'.$cdata['plf'].'</th>
                                            <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w8.'%">'.$cdata['production'].'</th>
                                            <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w9.'%">'.$cmonthp[0].'</th>
                                            <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w10.'%">'.$cyearp[0].'</th>
                                            <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w11.'%">'.number_format($pmper,2).'</th>
                                            <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w12.'%">'.number_format($pgper,2).'</th>
                                            <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w13.'%">'.$pdata['generation_hour'].'.'.$pdata['generation_min'].'</th>
                                            <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w14.'%">'.$pdata['plf'].'</th>
                                            <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w15.'%">'.$pdata['production'].'</th>
                                            <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w16.'%">'.$pmonthp[0].'</th>
                                            <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w17.'%">'.$pyearp[0].'</th>
                                            <th rowspan="1" style="color:'.$diff_color1.';" align="center" colspan="1" width="'.$w18.'%">'.$day_diff.'</th>
                                            <th rowspan="1" style="color:'.$diff_color2.';" align="center" colspan="1" width="'.$w19.'%">'.$month_diff.'</th>
                                            <th rowspan="1" style="color:'.$diff_color3.';" align="center" colspan="1" width="'.$w20.'%">'.$year_diff.'</th>
                                        </tr>';
                                        $sno++;
                            }
                            $umachines+=$sno;
                            $mtime=$max_time*$sno;
                            $ugctime+=$cgen_mins;$ugptime+=$pgen_mins;
                           
                            $ucday+=$ctoday;$ucmonth+=$cmon;$ucyear+=$cyr;
                            $upday+=$pday;$upmonth+=$pmon;$upyear+=$pyr;
                            $udday+=$diffd;$udmonth+=$diffm;$udyear+=$diffy;

                            $bhr=ceil($break_mins/60);$bmin=$break_mins%60;
                            $chr=ceil($cgen_mins/60);$cmin=$cgen_mins%60;
                            $phr=ceil($pgen_mins/60);$pmin=$pgen_mins%60;
                            
                            if($diffd>0){$diff_color1="#00c853";}else{$diff_color1="#d50000";}
                            if($diffm>0){$diff_color2="#00c853";}else{$diff_color2="#d50000";}
                            if($diffy>0){$diff_color3="#00c853";}else{$diff_color3="#d50000";}

                            $cmper=($mtime-$machine_timec)*100/$mtime;if(is_nan($cmper)){$cmper=0;}
                            $cgper=($mtime-$grid_timec)*100/$mtime;if(is_nan($cgper)){$cgper=0;}

                            $pmper=($mtime-$machine_timep)*100/$mtime;if(is_nan($pmper)){$pmper=0;}
                            $pgper=($mtime-$grid_timep)*100/$mtime;if(is_nan($pgper)){$pgper=0;}
                            
                            $cplf=plf_calculation($ctoday,$total_capacity,1);
                            $pplf=plf_calculation($pday,$total_capacity,1);

                            $ucmtime+=$machine_timec;$ucgtime+=$grid_timec;$upmtime+=$machine_timep;$upgtime+=$grid_timep;

                            $ucapacity+=$total_capacity;

                            $rows.='<tr>
                                        <th rowspan="1" align="center" colspan="2" width="'.($w1+$w2).'%"><b>Total</b></th>
                                        <th rowspan="1" align="center" colspan="1" width="'.$w3.'%"><b>'.number_format($total_capacity,2).'</b></th>
                                        <th rowspan="1" align="center" colspan="1" width="'.$w21.'%"><b>'.$bhr.'.'.$bmin.'</b></th>
                                        <th rowspan="1" align="center" colspan="1" width="'.$w22.'%"></th>
                                        <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w4.'%"><b>'.number_format($cmper,2).'</b></th>
                                        <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w5.'%"><b>'.number_format($cgper,2).'</b></th>
                                        <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w6.'%"><b>'.$chr.':'.$cmin.'</b></th>
                                        <th rowspan="1" style="color:#e91e63;" align="center" colspan="1" width="'.$w7.'%"><b>'.number_format($cplf,2).'</b></th>
                                        <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w8.'%"><b>'.$ctoday.'</b></th>
                                        <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w9.'%"><b>'.$cmon.'</b></th>
                                        <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1" width="'.$w10.'%"><b>'.$cyr.'</b></th>
                                        <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w11.'%"><b>'.number_format($pmper,2).'</b></th>
                                        <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w12.'%"><b>'.number_format($pgper,2).'</b></th>
                                        <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w13.'%"><b>'.$phr.':'.$pmin.'</b></th>
                                        <th rowspan="1" style="color:#c51162;" align="center" colspan="1" width="'.$w14.'%"><b>'.number_format($pplf,2).'</b></th>
                                        <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w15.'%"><b>'.$pday.'</b></th>
                                        <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w16.'%"><b></b>'.$pmon.'</th>
                                        <th rowspan="1" style="color:#2962ff;" align="center" colspan="1" width="'.$w17.'%"><b>'.$pyr.'</b></th>
                                        <th rowspan="1" style="color:'.$diff_color1.';" align="center" colspan="1" width="'.$w18.'%"><b>'.$diffd.'</b></th>
                                        <th rowspan="1" style="color:'.$diff_color2.';" align="center" colspan="1" width="'.$w19.'%"><b>'.$diffm.'</b></th>
                                        <th rowspan="1" style="color:'.$diff_color3.';" align="center" colspan="1" width="'.$w20.'%"><b>'.$diffy.'</b></th>
                                    </tr>';

                        $rows1.='<tr>
                                    <td rowspan="1" align="center" colspan="1">'.$dno.'</td>
                                    <td rowspan="1" align="center" colspan="1">'.$ddata['division_name'].'</td>
                                    <td rowspan="1" align="center" colspan="1">'.number_format($total_capacity,2).'</td>
                                    <td rowspan="1" style="color:#e91e63;" align="center" colspan="1">'.number_format($cmper,2).'</td>
                                    <td rowspan="1" style="color:#e91e63;" align="center" colspan="1">'.number_format($cgper,2).'</td>
                                    <td rowspan="1" style="color:#e91e63;" align="center" colspan="1">'.$chr.':'.$cmin.'</td>
                                    <td rowspan="1" style="color:#e91e63;" align="center" colspan="1">'.number_format($cplf,2).'</td>
                                    <td rowspan="1" style="color:#3f51b5;" align="center" colspan="1">'.$ctoday.'</td>
                                    <td rowspan="1" style="color:#3f51b5;" align="center" colspan="1">'.$cmon.'</td>
                                    <td rowspan="1" style="color:#3f51b5;" align="center" colspan="1">'.$cyr.'</td>
                                    <td rowspan="1" style="color:#c51162;" align="center" colspan="1">'.number_format($pmper,2).'</td>
                                    <td rowspan="1" style="color:#c51162;" align="center" colspan="1">'.number_format($pgper,2).'</td>
                                    <td rowspan="1" style="color:#c51162;" align="center" colspan="1">'.$phr.':'.$pmin.'</td>
                                    <td rowspan="1" style="color:#c51162;" align="center" colspan="1">'.number_format($pplf,2).'</td>
                                    <td rowspan="1" style="color:#2962ff;" align="center" colspan="1">'.$pday.'</td>
                                    <td rowspan="1" style="color:#2962ff;" align="center" colspan="1">'.$pmon.'</td>
                                    <td rowspan="1" style="color:#2962ff;" align="center" colspan="1">'.$pyr.'</td>
                                    <td rowspan="1" style="color:'.$diff_color1.';" align="center" colspan="1">'.$diffd.'</td>
                                    <td rowspan="1" style="color:'.$diff_color2.';" align="center" colspan="1">'.$diffm.'</td>
                                    <td rowspan="1" style="color:'.$diff_color3.';" align="center" colspan="1">'.$diffy.'</td>
                                </tr>';

                                $dno++;

                            $tbl='<table cellpadding="1" border="1" width="100%">
                                    <thead><tr><td colspan="22" align="center" width="100%"><b>'.$ddata['division_name'].'</b></td></tr>'.$header_row1.'</thead>
                                    <tbody>'.$rows.'</tbody>
                                </table>';
                            $html_data.=$tbl;
                        }
                        $mtime=$max_time*$umachines;
                        $chr=ceil($ugctime/60);$cmin=$ugctime%60;
                        $phr=ceil($ugptime/60);$pmin=$ugptime%60;

                        $cmper=($mtime-$ucmtime)*100/$mtime;if(is_nan($cmper)){$cmper=0;}
                        $cgper=($mtime-$ucgtime)*100/$mtime;if(is_nan($cgper)){$cgper=0;}

                        $pmper=($mtime-$upmtime)*100/$mtime;if(is_nan($pmper)){$pmper=0;}
                        $pgper=($mtime-$upgtime)*100/$mtime;if(is_nan($pgper)){$pgper=0;}

                            if($udday>0){$diff_color1="#00c853";}else{$diff_color1="#d50000";}
                            if($udmonth>0){$diff_color2="#00c853";}else{$diff_color2="#d50000";}
                            if($udyear>0){$diff_color3="#00c853";}else{$diff_color3="#d50000";}

                        $cplf=plf_calculation($ucday,$ucapacity,1);
                        $pplf=plf_calculation($upday,$ucapacity,1);

                        $tcapacity+=$ucapacity;
                        $tcmtime+=$ucmtime;$tcgtime+=$ucgtime;$tpmtime+=$upmtime;$tpgtime+=$upgtime;
                        $tgctime+=$ugctime;$tgptime+=$ugptime;
                        $tmachines+=$umachines;
                        $tcday+=$ucday;$tcmonth+=$ucmonth;$tcyear+=$ucyear;
                        $tpday+=$upday;$tpmonth+=$upmonth;$tpyear+=$upyear;
                        $tdday+=$udday;$tdmonth+=$udmonth;$tdyear+=$udyear;

                      $rows1.='<tr>
                                    <th rowspan="1" align="center" colspan="2"><b>Total</b></th>
                                    <th rowspan="1" align="center" colspan="1"><b>'.number_format($ucapacity,2).'</b></th>
                                    <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cmper,2).'</b></th>
                                    <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cgper,2).'</b></th>
                                    <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.$chr.':'.$cmin.'</b></th>
                                    <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cplf,2).'</b></th>
                                    <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$ucday.'</b></th>
                                    <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$ucmonth.'</b></th>
                                    <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$ucyear.'</b></th>
                                    <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pmper,2).'</b></th>
                                    <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pgper,2).'</b></th>
                                    <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.$phr.':'.$pmin.'</b></th>
                                    <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pplf,2).'</b></th>
                                    <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b>'.$upday.'</b></th>
                                    <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b></b>'.$upmonth.'</th>
                                    <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b>'.$upmonth.'</b></th>
                                    <th rowspan="1" style="color:'.$diff_color1.';" align="center" colspan="1"><b>'.$udday.'</b></th>
                                    <th rowspan="1" style="color:'.$diff_color2.';" align="center" colspan="1"><b>'.$udmonth.'</b></th>
                                    <th rowspan="1" style="color:'.$diff_color3.';" align="center" colspan="1"><b>'.$udyear.'</b></th>
                                </tr>'; 


                        $tbl='<table cellpadding="1" border="1" width="100%">
                                    <thead>'.$header_row.'</thead>
                                    <tbody>'.$rows1.'</tbody>
                              </table>';
                            $html_data1.=$tbl;
                    }
                    $mtime=$max_time*$tmachines;
                    $chr=ceil($tgctime/60);$cmin=$tgctime%60;
                    $phr=ceil($tgptime/60);$pmin=$tgptime%60;

                    $cmper=($mtime-$tcmtime)*100/$mtime;if(is_nan($cmper)){$cmper=0;}
                    $cgper=($mtime-$tcgtime)*100/$mtime;if(is_nan($cgper)){$cgper=0;}

                    $pmper=($mtime-$tpmtime)*100/$mtime;if(is_nan($pmper)){$pmper=0;}
                    $pgper=($mtime-$tpgtime)*100/$mtime;if(is_nan($pgper)){$pgper=0;}


                    if($tdday>0){$diff_color1="#00c853";}else{$diff_color1="#d50000";}
                    if($tdmonth>0){$diff_color2="#00c853";}else{$diff_color2="#d50000";}
                    if($tdyear>0){$diff_color3="#00c853";}else{$diff_color3="#d50000";}
                    $cplf=plf_calculation($tcday,$tcapacity,1);
                    $pplf=plf_calculation($tpday,$tcapacity,1);

                    $rows1='<tr>
                                <th rowspan="1" align="center" colspan="2"><b>Grand Total</b></th>
                                <th rowspan="1" align="center" colspan="1"><b>'.number_format($tcapacity,2).'</b></th>
                                <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cmper,2).'</b></th>
                                <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cgper,2).'</b></th>
                                <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.$chr.':'.$cmin.'</b></th>
                                <th rowspan="1" style="color:#e91e63;" align="center" colspan="1"><b>'.number_format($cplf,2).'</b></th>
                                <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$tcday.'</b></th>
                                <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$tcmonth.'</b></th>
                                <th rowspan="1" style="color:#3f51b5;" align="center" colspan="1"><b>'.$tcyear.'</b></th>
                                <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pmper,2).'</b></th>
                                <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pgper,2).'</b></th>
                                <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.$phr.':'.$pmin.'</b></th>
                                <th rowspan="1" style="color:#c51162;" align="center" colspan="1"><b>'.number_format($pplf,2).'</b></th>
                                <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b>'.$tpday.'</b></th>
                                <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b></b>'.$tpmonth.'</th>
                                <th rowspan="1" style="color:#2962ff;" align="center" colspan="1"><b>'.$tpyear.'</b></th>
                                <th rowspan="1" style="color:'.$diff_color1.';" align="center" colspan="1"><b>'.$tdday.'</b></th>
                                <th rowspan="1" style="color:'.$diff_color2.';" align="center" colspan="1"><b>'.$tdmonth.'</b></th>
                                <th rowspan="1" style="color:'.$diff_color3.';" align="center" colspan="1"><b>'.$tdyear.'</b></th>
                            </tr>'; 

                    $pdf->writeHTML($html_data1, true, 0, true, 0);

                    $tbl='<table cellpadding="1" border="1" width="100%">
                                <tbody>'.$rows1.'</tbody>
                        </table>';
                    $pdf->writeHTML($tbl, true, 0, true, 0);

                    $pdf->AddPage();
                    $pdf->writeHTML($html_data, true, 0, true, 0);
                    database_close($conn);
        $doc_name="Daily Summary.pdf";
        $doc_location=document_namecreate($doc_name);
        $pdf->Output($doc_location, 'F');
        $_SESSION["download_docname"]=$doc_name;
        $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location);    
    }
?>