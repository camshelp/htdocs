<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/pdfheader.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
  include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "machine_list":machine_list();
                    break;
            case "machine_summary":machine_summary();
                    break;
            case "machine_register":machine_register();
                    break;
            case "unit_list":unit_list();
                    break;
            case "location_list":location_list();
                    break;
            case "due_reminder":machine_list_reminder();
                    break;
            case "reminder_summary":summary_reminder();
                    break;
            case "update_reminder":update_iomreminder();
                    break;
            case "approve_reminder":reminder_approve();
                    break;
            case "current_month":current_month_year();
                    break;
            case "export_pdf":Export_PDF();
                    break;
            case "export_xls":Export_XLS();
                    break;
            case "machine_pdf":Machine_Summary_PDF();
                    break;
            case "division_list":division_list();
                    break;
            case "reminder_list":reminder_list();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function current_month_year()
    {
        $date=get_date();
        $dsplt = explode('-',$date);
        $arr = ["month" =>$dsplt[1]."-".$dsplt[0]];
        echo json_encode($arr);
    }
   function machine_list()
   {
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.division_list.division_id,wind_mill.division_list.division_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id left join wind_mill.division_list on wind_mill.machine.division_id=wind_mill.division_list.division_id where wind_mill.machine.status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $json[$sno] = array(
                'unit_id' => $row['unit'],
                'unit_name' => $row['unit_name'],
                'division_id' => $row['division_id'],
                'division_name' => $row['division_name'],
                'machine_id' => $row['machine_id'],
                'machine_name' => $row['machine_name'],
                'htsc' => $row['htsc'],
                'function_date' => $row['function_date'],
                'capacity' => $row['capacity'],
                'insurance_date' => $row['insurance_date'],
                'insurance_amount' => $row['insurance_amount'],
                'om_date' => $row['om_date'],
                'om_amount' => $row['om_amount'],
                'location_id' => $row['location'],
                'location_name' => $row['location_name']);
                $sno++;
            }
        }
        database_close($conn);
        echo json_encode($json);
   }
   function division_list()
   {
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT division_id,division_name from wind_mill.division_list where status='yes' order by division_name asc";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $json[$sno] = array(
                'division_id' => $row['division_id'],
                'division_name' => $row['division_name']);
                $sno++;
            }
        }
        database_close($conn);
        echo json_encode($json);
   }
   function machine_list_reminder()
   {
        header("Content-Type: application/json; charset=UTF-8");
        $sdate="-".$_POST['date'];
        $json = array();
        $conn = database_open();
        $sno=0;
        $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name,wind_mill.insurance_om_reminder.new_amount,wind_mill.insurance_om_reminder.dd_bank,wind_mill.insurance_om_reminder.dd_branch,wind_mill.insurance_om_reminder.dd_date,wind_mill.insurance_om_reminder.dd_no,wind_mill.insurance_om_reminder.dd_status FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id LEFT JOIN wind_mill.insurance_om_reminder ON wind_mill.insurance_om_reminder.machine_id=wind_mill.machine.machine_id AND wind_mill.insurance_om_reminder.date=wind_mill.machine.insurance_date WHERE wind_mill.machine.status='yes' AND wind_mill.machine.insurance_date LIKE CONCAT('%', :sdate, '')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row>0)
        {
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $disp_info="";
                if($row['dd_bank']!="")
                {
                     $disp_info=$row['dd_bank']."<br>".$row['dd_branch']."<br>".$row['dd_date']." & No. ".$row['dd_no'];
                }
                $rtype="Insurance"; $description="Insurance Due : ".$row['insurance_date'];  $ramount=$row['insurance_amount'];
                $date=$row['insurance_date'];
                $json[$sno] = array(
                'unit_id' => $row['unit'],
                'unit_name' => $row['unit_name'],
                'machine_id' => $row['machine_id'],
                'machine_name' => $row['machine_name'],
                'location_id' => $row['location'],
                'location_name' => $row['location_name'],
                'capacity' => $row['capacity'],
                'due_date'=>$date,
                'type' => $rtype,
                'description' => $description,
                'amount' => $ramount,'new_amount'=>$row['new_amount'],'dd_disp'=>$disp_info,
                'bank'=>$row['dd_bank'],'branch'=>$row['dd_branch'],'dd_date'=>$row['dd_date'],'dd_no'=>$row['dd_no'],'status'=>$row['dd_status']);
                $sno++;
            }
        }

         $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name,wind_mill.insurance_om_reminder.new_amount,wind_mill.insurance_om_reminder.dd_bank,wind_mill.insurance_om_reminder.dd_branch,wind_mill.insurance_om_reminder.dd_date,wind_mill.insurance_om_reminder.dd_no,wind_mill.insurance_om_reminder.dd_status FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id LEFT JOIN wind_mill.insurance_om_reminder ON wind_mill.insurance_om_reminder.machine_id=wind_mill.machine.machine_id AND wind_mill.insurance_om_reminder.date=wind_mill.machine.om_date WHERE wind_mill.machine.status='yes' AND wind_mill.machine.om_date like CONCAT('%', :sdate, '%')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row>0)
        {
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $disp_info="";
                if($row['dd_bank']!="")
                {
                     $disp_info=$row['dd_bank']."<br>".$row['dd_branch']."<br>".$row['dd_date']." & No. ".$row['dd_no'];
                }
                $ramount=$row['om_amount'];
                $rtype="O and M";
                $description="O & M Due : ".$row['om_date'];
                $date=$row['om_date'];

                $json[$sno] = array(
                'unit_id' => $row['unit'],
                'unit_name' => $row['unit_name'],
                'machine_id' => $row['machine_id'],
                'machine_name' => $row['machine_name'],
                'location_id' => $row['location'],
                'location_name' => $row['location_name'],
                'capacity' => $row['capacity'],
                'due_date'=>$date,
                'type' => $rtype,
                'description' => $description,
                'amount' => $ramount,'new_amount'=>$row['new_amount'],'dd_disp'=>$disp_info,
                'bank'=>$row['dd_bank'],'branch'=>$row['dd_branch'],'dd_date'=>$row['dd_date'],'dd_no'=>$row['dd_no'],'status'=>$row['dd_status']);
                $sno++;
            }
        } 
        database_close($conn);
        echo json_encode($json);
   }
   function reminder_list()
   {
       // header("Content-Type: application/json; charset=UTF-8");
        $sdate=get_date();
        $cdate=$sdate;
        $insurance = array();$om = array();
        $conn = database_open();
        $sno=0;
        for($x=0;$x<6;$x++)
        {
            $cdate = date('d-m-Y', strtotime($cdate .' -'.$x.' month'));
            $dsplt= explode('-',$cdate);
            $sort_date='-'.$dsplt[1].'-'.$dsplt[2];
            $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name,wind_mill.insurance_om_reminder.new_amount,wind_mill.insurance_om_reminder.dd_bank,wind_mill.insurance_om_reminder.dd_branch,wind_mill.insurance_om_reminder.dd_date,wind_mill.insurance_om_reminder.dd_no,wind_mill.insurance_om_reminder.dd_status FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id LEFT JOIN wind_mill.insurance_om_reminder ON wind_mill.insurance_om_reminder.machine_id=wind_mill.machine.machine_id AND wind_mill.insurance_om_reminder.date=wind_mill.machine.insurance_date WHERE wind_mill.machine.status='yes' AND wind_mill.machine.insurance_date LIKE CONCAT('%', :sdate, '%')";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sort_date);
            $stmt->execute();
            //$stmt->debugDumpParams();
            $row =$stmt->rowCount();
            if($row>0)
            {
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $disp_info="";
                    if($row['dd_bank']!="")
                    {
                        $disp_info=$row['dd_bank']."<br>".$row['dd_branch']."<br>".$row['dd_date']." & No. ".$row['dd_no'];
                    }
                    $rtype="Insurance"; $description="Insurance Due : ".$row['insurance_date'];  $ramount=$row['insurance_amount'];
                    $date=$row['insurance_date'];
                    $insurance[$sno] = array(
                    'unit_id' => $row['unit'],
                    'unit_name' => $row['unit_name'],
                    'machine_id' => $row['machine_id'],
                    'machine_name' => $row['machine_name'],
                    'location_id' => $row['location'],
                    'location_name' => $row['location_name'],
                    'capacity' => $row['capacity'],
                    'due_date'=>$date,
                    'type' => $rtype,
                    'description' => $description,
                    'amount' => $ramount,'new_amount'=>$row['new_amount'],'dd_disp'=>$disp_info,
                    'bank'=>$row['dd_bank'],'branch'=>$row['dd_branch'],'dd_date'=>$row['dd_date'],'dd_no'=>$row['dd_no'],'status'=>$row['dd_status']);
                    $sno++;
                }
            }
        }
        $sno=0;$cdate=$sdate;
        for($x=0;$x<6;$x++)
        {
            $cdate = date('d-m-Y', strtotime($cdate .' -'.$x.' month'));
            $dsplt= explode('-',$cdate);
            $sort_date='-'.$dsplt[1].'-'.$dsplt[2];
            $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name,wind_mill.insurance_om_reminder.new_amount,wind_mill.insurance_om_reminder.dd_bank,wind_mill.insurance_om_reminder.dd_branch,wind_mill.insurance_om_reminder.dd_date,wind_mill.insurance_om_reminder.dd_no,wind_mill.insurance_om_reminder.dd_status FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id LEFT JOIN wind_mill.insurance_om_reminder ON wind_mill.insurance_om_reminder.machine_id=wind_mill.machine.machine_id AND wind_mill.insurance_om_reminder.date=wind_mill.machine.om_date WHERE wind_mill.machine.status='yes' AND wind_mill.machine.om_date like CONCAT('%', :sdate, '%')";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':sdate',$sort_date);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row>0)
            {
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $disp_info="";
                    if($row['dd_bank']!="")
                    {
                        $disp_info=$row['dd_bank']."<br>".$row['dd_branch']."<br>".$row['dd_date']." & No. ".$row['dd_no'];
                    }
                    $ramount=$row['om_amount'];
                    $rtype="O and M";
                    $description="O & M Due : ".$row['om_date'];
                    $date=$row['om_date'];

                    $om[$sno] = array(
                    'unit_id' => $row['unit'],
                    'unit_name' => $row['unit_name'],
                    'machine_id' => $row['machine_id'],
                    'machine_name' => $row['machine_name'],
                    'location_id' => $row['location'],
                    'location_name' => $row['location_name'],
                    'capacity' => $row['capacity'],
                    'due_date'=>$date,
                    'type' => $rtype,
                    'description' => $description,
                    'amount' => $ramount,'new_amount'=>$row['new_amount'],'dd_disp'=>$disp_info,
                    'bank'=>$row['dd_bank'],'branch'=>$row['dd_branch'],'dd_date'=>$row['dd_date'],'dd_no'=>$row['dd_no'],'status'=>$row['dd_status']);
                    $sno++;
                }
            } 
        }
        database_close($conn);
        $arr = ["insurance" => $insurance, "om" => $om];
        echo json_encode($arr);  
   }
   function summary_reminder()
   {
        $icount=0;$iamount=0;$ocount=0;$oamount=0;
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sdate="-".$_POST['date'];

        $sql="select count(distinct machine_id) from wind_mill.machine where status='yes' and insurance_date like CONCAT('%', :sdate, '%')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $icount = $stmt->fetchColumn(0);

        $sql="select distinct sum(insurance_amount) from wind_mill.machine where status='yes' and insurance_date like CONCAT('%', :sdate, '%')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $iamount = $stmt->fetchColumn(0);

        $sql="select count(distinct machine_id) from wind_mill.machine where status='yes' and om_date like CONCAT('%', :sdate, '%')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $ocount = $stmt->fetchColumn(0);

        $sql="select distinct sum(om_amount) from wind_mill.machine where status='yes' and om_date like CONCAT('%', :sdate, '%')";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':sdate',$sdate);
        $stmt->execute();
        $oamount = $stmt->fetchColumn(0);

        database_close($conn);
        $arr = ["icount" => $icount, "iamount" => $iamount,"omcount"=>$ocount,"omamount"=>$oamount];
        echo json_encode($arr);  
   }
   function machine_summary()
   {
        $count=0;$capacity=0;
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
       
        $sql = "select count(*) from wind_mill.machine where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $count = $stmt->fetchColumn(0);

        $sql = "select sum(capacity) from wind_mill.machine where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $capacity = $stmt->fetchColumn(0);

        database_close($conn);
        $arr = ["total_machine" => $count, "machine_capacity" => number_format((float)$capacity, 2, '.', '')];
        echo json_encode($arr);  
   }
   function machine_register()
   {
        $msg="";$type="";
        if(isset($_POST['mid'])){  $mid = $_POST['mid']; }else{$msg="Machine Id was not posted";}
        if(isset($_POST['uname'])){  $uname = $_POST['uname']; }else{$msg="Unit Name was not posted";}
        if(isset($_POST['dname'])){  $dname = $_POST['dname']; }else{$msg="Division Name was not posted";}
        if(isset($_POST['lname'])){  $lname = $_POST['lname']; }else{$msg="Location was not posted";}
        if(isset($_POST['mname'])){  $mname = $_POST['mname']; }else{$msg="Machine Name was not posted";}
        if(isset($_POST['htsc'])){  $htsc = $_POST['htsc']; }else{$msg="HTSC No. was not posted";}
        if(isset($_POST['dof'])){  $dof = $_POST['dof']; }else{$msg="Date of Functioning was not posted";}
        if(isset($_POST['capa'])){  $capacity = $_POST['capa']; }else{$msg="Capacity was not posted";}
        if(isset($_POST['idd'])){  $idue = $_POST['idd']; }else{$msg="Insurance Due Date was not posted";}
        if(isset($_POST['iamount'])){  $iamount = $_POST['iamount']; }else{$msg="Insurance Due Amount was not posted";}
        if(isset($_POST['omd'])){  $odue = $_POST['omd']; }else{$msg="O & M Due Date was not posted";}
        if(isset($_POST['oamount'])){  $oamount = $_POST['oamount']; }else{$msg="O & M Due Amount was not posted";}
        if($msg=="")
        {
            $myArray = array(array("Insurance", $idue, $iamount ),array( "OM", $odue, $oamount ));
            $conn = database_open();
            try
            {
                session_start();
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                if($mid!="")
                {
                    $sql="update wind_mill.machine set unit=:uname,division_id=:did,machine_name=:mname,htsc=:htsc,location=:location,function_date=:fdate,capacity=:capa,insurance_date=:idate,insurance_amount=:iamount,om_date=:odue,om_amount=:oamount,updated_by=:by,updated_session=:session,updated_time=:time where machine_id=:mid";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':mid',$mid);
                    $stmt->bindParam(':did',$dname);
                    $stmt->bindParam(':uname', $uname);
                    $stmt->bindParam(':mname', $mname);
                    $stmt->bindParam(':htsc', $htsc);
                    $stmt->bindParam(':location', $lname);
                    $stmt->bindParam(':fdate',$dof);
                    $stmt->bindParam(':capa', $capacity);
                    $stmt->bindParam(':idate', $idue);
                    $stmt->bindParam(':iamount',$iamount);
                    $stmt->bindParam(':odue', $odue);
                    $stmt->bindParam(':oamount', $oamount);
                    $stmt->bindParam(':by',$update_by);
                    $stmt->bindParam(':session',$update_session);
                    $stmt->bindParam(':time',$update_time);
                    if($stmt->execute()==TRUE)
                    {
                        $msg="The Machine Information Submitted Successfully";
                        $type="Success";
                    }

                }
                else
                {
                    $sql="select machine_id from wind_mill.machine where machine_name=:mname";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':mname', $mname);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row==0)
                    {
                        $sql="select count(*) from wind_mill.machine";
                        $stmt = $conn->prepare($sql); 
                        $stmt->execute();
                        $result = $stmt->fetchColumn(0);
                        $row=$result;
                        $add_value="1";
                        $total=(integer)$row+(integer)$add_value;
                        $machine_id="M".SixDigitIdGenerator("$total");$mid=$machine_id;
                        $mstatus="yes";
                        $sql="insert into wind_mill.machine values(:mid,:uname,:mname,:htsc,:location,:did,:fdate,:capa,:idate,:iamount,:odue,:oamount,:mstat,:by,:session,:time)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':mid',$machine_id);
                        $stmt->bindParam(':uname', $uname);
                        $stmt->bindParam(':mname', $mname);
                        $stmt->bindParam(':htsc', $htsc);
                        $stmt->bindParam(':location', $lname);
                        $stmt->bindParam(':did',$dname);
                        $stmt->bindParam(':fdate',$dof);
                        $stmt->bindParam(':capa', $capacity);
                        $stmt->bindParam(':idate', $idue);
                        $stmt->bindParam(':iamount',$iamount);
                        $stmt->bindParam(':odue', $odue);
                        $stmt->bindParam(':oamount', $oamount);
                        $stmt->bindParam(':mstat', $mstatus);
                        $stmt->bindParam(':by',$update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if($stmt->execute()==TRUE)
                        {
                            $msg="The Machine Information Submitted Successfully";
                            $type="Success";
                        }
                    }
                    else{$msg="The Machine Name was already found";}
                }
                if($mid!="")
                    {
                        $item_count=count($myArray);
                        for ($x = 0; $x<$item_count; $x++) 
                        {
                            $type1= $myArray[$x][0];
                            $due_date= $myArray[$x][1];
                            $due_amount= $myArray[$x][2];
                            $sql="select machine_id from wind_mill.insurance_om where machine_id=:mid and type=:type and date=:rdate and amount=:ramount";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':mid',$mid);
                            $stmt->bindParam(':type',$type1);
                            $stmt->bindParam(':rdate',$due_date);
                            $stmt->bindParam(':ramount',$due_amount);
                            $stmt->execute();
                            $row =$stmt->rowCount();
                            if($row==0)
                            {
                                $astatus="yes";
                                $sql="insert into wind_mill.insurance_om values(:mid,:type,:rdate,:ramount,:stat,:by,:session,:time)";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':mid',$mid);
                                $stmt->bindParam(':type',$type1);
                                $stmt->bindParam(':rdate',$due_date);
                                $stmt->bindParam(':ramount',$due_amount);
                                $stmt->bindParam(':stat',$astatus);
                                $stmt->bindParam(':by',$update_by);
                                $stmt->bindParam(':session',$update_session);
                                $stmt->bindParam(':time',$update_time);
                                $stmt->execute();
                            }
                        }
                    }
            }catch(Exception $e){$msg=$e->getMessage(); }
            database_close($conn);
        }
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
        
   }
   function unit_list()
   {
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT unit_id,unit_name from wind_mill.unit where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $json[$sno] = array(
                'unit_id' => $row['unit_id'],
                'unit_name' => $row['unit_name']);
                $sno++;
            }
        }
        database_close($conn);
        echo json_encode($json);
   }
   function location_list()
   {
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT location_id,location_name,district_name,state_name,pincode from wind_mill.location where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $json[$sno] = array(
                'location_id' => $row['location_id'],
                'location_name' => $row['location_name'],
                'district_name' => $row['district_name'],
                'state_name' => $row['state_name'], 'pin' => $row['pincode']);
                $sno++;
            }
        }
        database_close($conn);
        echo json_encode($json);
   }
   function update_iomreminder()
   {
        $msg="";$type="";
        if(isset($_POST['month_year'])){  $myear = $_POST['month_year']; }else{$msg="Month & Year was not posted";}
        if(isset($_POST['bank'])){  $bank = $_POST['bank']; }else{$msg="Bank was not posted";}
        if(isset($_POST['branch'])){  $branch = $_POST['branch']; }else{$msg="Branch was not posted";}
        if(isset($_POST['cdate'])){  $date = $_POST['cdate']; }else{$msg="Date was not posted";}
        if(isset($_POST['cno'])){  $no = $_POST['cno']; }else{$msg="Cheque No. was not posted";}
        if(isset($_POST['data'])){  $data = $_POST['data']; }else{$msg="Machine Data was not posted";}
        if($msg=="")
        {
            $conn = database_open();
            try
            {
                session_start();
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                
                $someArray = json_decode($data, true);
                $item_count=count($someArray);
                for ($x = 0; $x<$item_count; $x++) 
                {
                    $mid=$someArray[$x]["mid"];
                    $uid=$someArray[$x]["uid"];
                    $lid=$someArray[$x]["lid"];
                    $type=$someArray[$x]["type"];
                    $due=$someArray[$x]["due"];
                    $amount=$someArray[$x]["amount"];
                    $oamount=$someArray[$x]["oamount"];

                    $sql="select machine_id from wind_mill.insurance_om_reminder where unit_id=:uid and machine_id=:mid and location_id=:lid and facility_type=:type and date=:date";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':uid',$uid);
                    $stmt->bindParam(':mid',$mid);
                    $stmt->bindParam(':lid',$lid);
                    $stmt->bindParam(':type',$type);
                    $stmt->bindParam(':date',$due);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row==0)
                    {
                        $dstatus="no";
                        $sql="insert into wind_mill.insurance_om_reminder values(:uid,:mid,:lid,:myear,:type,:ddate,:oamt,:namt,:by,:session,:time,:bank,:branch,:date,:no,:by,:session,:time,:dstatus)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':uid',$uid);
                        $stmt->bindParam(':mid',$mid);
                        $stmt->bindParam(':lid',$lid);
                        $stmt->bindParam(':myear',$myear);
                        $stmt->bindParam(':type',$type);
                        $stmt->bindParam(':ddate',$due);
                        $stmt->bindParam(':oamt',$oamount);
                        $stmt->bindParam(':namt',$amount);
                        $stmt->bindParam(':by',$update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        $stmt->bindParam(':bank',$bank);
                        $stmt->bindParam(':branch',$branch);
                        $stmt->bindParam(':date',$date);
                        $stmt->bindParam(':no',$no);
                        $stmt->bindParam(':dstatus',$dstatus);
                        $stmt->execute();
                        $type="Success";$msg="The Information Submitted Successfully";
                    }
                    else
                    {
                        $sql="update wind_mill.insurance_om_reminder set new_amount=:namt,dd_bank=:bank,dd_branch=:branch,dd_date=:date,dd_no=:no,dd_by=:by,dd_session=:session,dd_time=:time where unit_id=:uid and machine_id=:mid and location_id=:lid and facility_type=:type and date=:ddate";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':uid',$uid);
                        $stmt->bindParam(':mid',$mid);
                        $stmt->bindParam(':lid',$lid);
                        $stmt->bindParam(':type',$type);
                        $stmt->bindParam(':ddate',$due);
                        $stmt->bindParam(':namt',$amount);
                        $stmt->bindParam(':by',$update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        $stmt->bindParam(':bank',$bank);
                        $stmt->bindParam(':branch',$branch);
                        $stmt->bindParam(':date',$date);
                        $stmt->bindParam(':no',$no);
                        $stmt->execute();

                        $type="Success";$msg="The Information Updated Successfully";
                    }

                }

            }catch(Exception $e){$msg=$e->getMessage(); }
            database_close($conn);

        }
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);

   }
   function reminder_approve()
   {
        $msg="";$type="";
        if(isset($_POST['mid'])){  $mid = $_POST['mid']; }else{$msg="Machine Id was not posted";}
        if(isset($_POST['uid'])){  $uid = $_POST['uid']; }else{$msg="Unit Id was not posted";}
        if(isset($_POST['lid'])){  $lid = $_POST['lid']; }else{$msg="Location Id was not posted";}
        if(isset($_POST['type'])){  $type = $_POST['type']; }else{$msg="Type was not posted";}
        if(isset($_POST['due'])){  $due = $_POST['due']; }else{$msg="Due was not posted";}
        if(isset($_POST['duration'])){  $duration = $_POST['duration']; }else{$msg="Duration was not posted";}
        if($msg=="")
        {
            $conn = database_open();
            try
            {
                session_start();
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();

                
                $sql="select machine_id,facility_type,date,new_amount from wind_mill.insurance_om_reminder where unit_id=:uid and machine_id=:mid and location_id=:lid and facility_type=:type and date=:date";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':uid',$uid);
                $stmt->bindParam(':mid',$mid);
                $stmt->bindParam(':lid',$lid);
                $stmt->bindParam(':type',$type);
                $stmt->bindParam(':date',$due);
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row>0)
                {
                    $result = $stmt -> fetch();
                    $ftype = $result["facility_type"];
                    $fdate = $result["date"];
                    $famount = $result["new_amount"];
                    $new_data=$date;
                    if($duration="3M"){$new_data = date('d-m-Y', strtotime($fdate .' 92 day'));}
                    else if($duration="6M"){$new_data = date('d-m-Y', strtotime($fdate .' 183 day'));}
                    else if($duration="9M"){$new_data = date('d-m-Y', strtotime($date .' 274 day'));}
                    else if($duration="1Y"){$new_data = date('d-m-Y', strtotime($date .' 365 day'));}
                    
                    $utype="OM";
                    if($ftype=="Insurance"){$utype="Insurance";}
                    $sql="update wind_mill.machine set ".$utype."_date=:rdate,".$utype."_amount=:ramount,updated_by=:by,updated_session=:session,updated_time=:time where machine_id=:mid";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':mid',$mid);
                    $stmt->bindParam(':rdate',$new_data);
                    $stmt->bindParam(':ramount',$famount);
                    $stmt->bindParam(':by',$update_by);
                    $stmt->bindParam(':session',$update_session);
                    $stmt->bindParam(':time',$update_time);
                    $stmt->execute();

                    $astatus="yes";
                    $sql="insert into wind_mill.insurance_om values(:mid,:type,:rdate,:ramount,:stat,:by,:session,:time)";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':mid',$mid);
                    $stmt->bindParam(':type',$utype);
                    $stmt->bindParam(':rdate',$new_data);
                    $stmt->bindParam(':ramount',$famount);
                    $stmt->bindParam(':stat',$astatus);
                    $stmt->bindParam(':by',$update_by);
                    $stmt->bindParam(':session',$update_session);
                    $stmt->bindParam(':time',$update_time);
                    $stmt->execute();

                    $sql="update wind_mill.insurance_om_reminder set dd_status=:stat,dd_by=:by,dd_session=:session,dd_time=:time where unit_id=:uid and machine_id=:mid and location_id=:lid and facility_type=:type and date=:date";
                    $stmt = $conn->prepare($sql); 
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':uid',$uid);
                    $stmt->bindParam(':mid',$mid);
                    $stmt->bindParam(':lid',$lid);
                    $stmt->bindParam(':type',$type);
                    $stmt->bindParam(':date',$due);
                    $stmt->bindParam(':stat',$astatus);
                    $stmt->bindParam(':by',$update_by);
                    $stmt->bindParam(':session',$update_session);
                    $stmt->bindParam(':time',$update_time);
                    $stmt->execute();

                    $type="Success";$msg="The Information Updated Successfully.";


                }else{$msg="The Record Not Found";}


            }catch(Exception $e){$msg=$e->getMessage(); }
            database_close($conn);
        }
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
   }
   function Export_PDF()
   {
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");
            try
            {
                $dval=explode('-',$_POST['month_year']);
                $date=$dval[0]."-".$dval[1]."-".$dval[2];
                $date=$dval[2]."-".$dval[1]."-".$dval[0];
                $company_id="INST000005";$title="Insurance, O & M Due Summary - ".date('F,Y', strtotime($date .' 0 day'));

                $someJSON = $_POST['data'];
                $someArray = json_decode($someJSON, true);
                $item_count=count($someArray);
 
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->AddPage('L', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

            $pdf->SetMargins(10, 30, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 8, '', true);
        $pdf->AddPage();
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        $row_height=22;
        $w1=5;$w2=23;$w3=17;$w4=10;$w5=6;$w6=6;$w7=7;$w8=6;$w9=6;$w10=20;
        $tbl_rows="";
        $conn = database_open();
        $myear=$dval[1]."-".$dval[2];
        $ptotal=0;$ntotal=0;
        for($x=0;$x<$item_count;$x++)
        {
            $ramount=0;$check_info="";
 
           /*  $sql="select new_amount,dd_bank,dd_branch,dd_date,dd_no from wind_mill.insurance_om_reminder where machine_id=:mid and month_year=:myear and facility_type=:type";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':mid',$someArray[$x]["machine_id"]);
            $stmt->bindParam(':type',$someArray[$x]["type"]);
            $stmt->bindParam(':myear',$myear);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row>0)
            {
                $result = $stmt -> fetch();
                $ramount=$result["new_amount"];
                $check_info=$result["dd_bank"].",".$result["dd_branch"]."<br>".$result["dd_date"]." & ".$result["dd_no"];
            } */

            $current='<tr>
            <td height="'.$row_height.'" align="center" width="'.$w1.'%">'.($x+1).'</td>
            <td height="'.$row_height.'" align="center" width="'.$w2.'%">'.$someArray[$x]["unit_name"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w3.'%">'.$someArray[$x]["location_name"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w4.'%">'.$someArray[$x]["machine_name"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w5.'%">'.$someArray[$x]["capacity"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w6.'%">'.$someArray[$x]["type"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w7.'%">'.$someArray[$x]["due_date"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w8.'%">'.$someArray[$x]["pamount"].'</td>
            <td height="'.$row_height.'" align="center" width="'.$w9.'%"></td>
            <td height="'.$row_height.'" align="center" width="'.$w10.'%"></td></tr>';
            $tbl_rows.=$current;

            $ptotal+=$someArray[$x]["pamount"]; $ntotal+=$ramount;
        }
        database_close($conn);
        $tfooter='<tfooter><tr><td vertical-align="middle" height="'.$row_height.'" colspan="7" align="center"><strong>Total</strong></td><td vertical-align="middle" height="'.$row_height.'" align="right"><strong>'.$ptotal.'</strong></td><td vertical-align="middle" height="'.$row_height.'" align="right"><strong></strong></td><td vertical-align="middle" height="'.$row_height.'" align="right"><strong></strong></td></tr></tfooter>';
        $tbl='<table border="1" cellpadding="2">
                            <thead>
                                <tr>
                                    <th border="0" colspan="8" align="right" vertical-align="middle" height="'.$row_height.'"><b>Printed On</b></th>
                                    <th border="0" colspan="2" align="left" vertical-align="middle" height="'.$row_height.'">'.get_datetime().'</th>
                                </tr>
                                <tr style=" background-color:#FFFF00;color:#0000FF;">
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"><b>S.No.</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w2.'%" align="center"><b>Unit Name</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"><b>Location Name</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w4.'%" align="center"> <b>Machine Name</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w5.'%" align="center"><b>Capacity</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w6.'%" align="center"><b>Due Type</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w7.'%" align="center"><b>Due Date</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w8.'%" align="center"><b>Previous Amount</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w9.'%" align="center"><b>Renewal Amount</b></th>
                                    <th vertical-align="middle" height="'.$row_height.'" width="'.$w10.'%" align="center"><b>Cheque Info</b></th>
                                </tr>
                            </thead>
                            <tbody>'.$tbl_rows.'</tbody>'.$tfooter.'</table>';
            $pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
            $doc_location=document_namecreate("insomremind.pdf");;
            $pdf->Output($doc_location, 'F');
            $_SESSION["download_docname"]="InsuranceOMReminder.pdf";
            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
          
        }
        catch(Exception $e) {echo $e->getMessage();} 
   }
   function Export_XLS()
   {
        $someJSON = $_POST['data'];
        $someArray = json_decode($someJSON, true);
        $item_count=count($someArray);

        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->setTitle("Reminder");
        $table_columns = array("S.No.", "Unit Name", "Location Name", "Machine Name", "Capacity","Due Type","Due Date","Previous Amount","Renewal Amount","Cheque Info");
        $column = 0;
        foreach($table_columns as $field)
        {
                        $doc->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                        $column++;
        }  
        $style = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000')));
        $doc->getActiveSheet()->getStyle('A1:J1')->applyFromArray($style);
        $sno=2;
        //$conn = database_open();
        for($x=0;$x<$item_count;$x++)
        {
            $ramount=0;$check_info="";
 
           /*  $sql="select new_amount,dd_bank,dd_branch,dd_date,dd_no from wind_mill.insurance_om_reminder where machine_id=:mid and month_year=:myear and facility_type=:type";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':mid',$someArray[$x]["machine_id"]);
            $stmt->bindParam(':type',$someArray[$x]["type"]);
            $stmt->bindParam(':myear',$myear);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row>0)
            {
                $result = $stmt -> fetch();
                $ramount=$result["new_amount"];
                $check_info=$result["dd_bank"].",".$result["dd_branch"]."<br>".$result["dd_date"]." & ".$result["dd_no"];
            } */

            $column=0;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno, $sno-1); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["unit_name"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["location_name"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["machine_name"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["capacity"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["type"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["due_date"]); $column++;
            $doc->getActiveSheet()->setCellValueByColumnAndRow($column, $sno,$someArray[$x]["pamount"]); $column++;
           
            $ptotal+=$someArray[$x]["pamount"]; $ntotal+=$ramount;
            $sno++;
        }
        //database_close($conn);
        $filename = 'Insurance and OM Reminder.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
        $doc_location=document_namecreate($filename);
        $objWriter->save($doc_location);
        $_SESSION["download_docname"]=$filename;
        $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location);
   }
   function Machine_Summary_PDF()
   {
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");
            try
            {
                $company_id="INST000005";$title="Machine Summary";

                $someJSON = $_POST['data'];
                $someArray = json_decode($someJSON, true);
                $item_count=count($someArray);
 
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->AddPage('L', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

            $pdf->SetMargins(10, 34, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 8, '', true);
        $pdf->AddPage();
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        $row_height=28;
        $w1=5;$w2=23;$w3=20;$w4=10;$w5=6;$w6=10;$w7=10;$w8=10;$w9=8;$w10=8;$w11=8;$w12=8;
        $tbl_rows="";
        $conn = database_open();
        $total_value=array();
        $sql="select distinct unit_id,unit_name from wind_mill.unit where status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $row =$stmt->rowCount();
        $usno=0;
        if($row>0)
        {
            while($udata = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="SELECT wind_mill.machine.unit,wind_mill.unit.unit_name,wind_mill.division_list.division_id,wind_mill.division_list.division_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name FROM wind_mill.machine JOIN wind_mill.unit ON wind_mill.machine.unit=wind_mill.unit.unit_id JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id left join wind_mill.division_list on wind_mill.machine.division_id=wind_mill.division_list.division_id where wind_mill.machine.status='yes' and wind_mill.machine.unit=:uid";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid',$udata['unit_id']);
                $stmt1->execute();
                $row =$stmt1->rowCount();
                if($row>0)
                {
                    
                    $total_capacity=0;$total_insurance=0;$total_om=0;
                    $tfooter="";
                     $tbl_rows=""; $sno=1;
                    $border_less='style="border: 1px solid white;cellpadding=0;"';
                    while($mdata = $stmt1->fetch(PDO::FETCH_BOTH))
                    {
                        $current='<tr>
                        <td height="'.$row_height.'" align="center" width="'.$w1.'%">'.$sno.'</td>
                        <td height="'.$row_height.'" align="left" width="'.$w3.'%">'.$mdata["location_name"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w4.'%">'.$mdata["machine_name"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w12.'%">'.$mdata["division_name"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w5.'%">'.$mdata["capacity"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w6.'%">'.$mdata["insurance_date"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w7.'%">'.$mdata["insurance_amount"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w8.'%">'.$mdata["om_date"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w9.'%">'.$mdata["om_amount"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w10.'%">'.$mdata["htsc"].'</td>
                        <td height="'.$row_height.'" align="center" width="'.$w11.'%">'.$mdata["function_date"].'</td>
                        </tr>';
                        $tbl_rows.=$current;
            
                        $total_capacity+=$mdata["capacity"];
                        $total_insurance+=$mdata["insurance_amount"];
                        $total_om+=$mdata["om_amount"];
                        $sno++;
                    }
                    $tfooter='<tfooter>
                    <tr>
                    <td vertical-align="middle" height="'.$row_height.'" colspan="4" align="center"><strong>Total</strong></td>
                    <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="1" align="center"><strong>'.$total_capacity.'</strong></td>
                    <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="2" align="center"><strong>'.$total_insurance.'</strong></td>
                    <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="2" align="center"><strong>'.$total_om.'</strong></td>
                    <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="2" align="center"><strong></strong></td>
                    </tr></tfooter>';
                    $tbl='<table border="1" cellpadding="2" width="100%">
                                        <thead>
                                            <tr '.$border_less.'>
                                                <th '.$border_less.' colspan="2" align="right" vertical-align="middle" height="'.$row_height.'"><b>Unit Name : </b></th>
                                                <th '.$border_less.' colspan="3" align="left" vertical-align="middle" height="'.$row_height.'">'.$udata['unit_name'].'</th>
                                                <th '.$border_less.' colspan="2" align="right" vertical-align="middle" height="'.$row_height.'"><b>Printed On : </b></th>
                                                <th '.$border_less.' colspan="4" align="left" vertical-align="middle" height="'.$row_height.'">'.get_datetime().'</th>
                                            </tr>
                                            <tr style=" background-color:#FFFF00;color:#0000FF;">
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"><b>S.No.</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"><b>Location Name</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w4.'%" align="center"> <b>Machine Name</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w12.'%" align="center"> <b>Division Name</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w5.'%" align="center"><b>Capacity</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w6.'%" align="center"><b>Insurance Due Date</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w7.'%" align="center"><b>Insurance Due</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w8.'%" align="center"><b>O & M Due Date</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w9.'%" align="center"><b>O & M Due Amount</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w10.'%" align="center"><b>HTSC No.</b></th>
                                                <th vertical-align="middle" height="'.$row_height.'" width="'.$w11.'%" align="center"><b>Date of Functioning</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>'.$tbl_rows.'</tbody>'.$tfooter.'</table>';
                        $pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
                   
                        $total_value[$usno]=array('name'=>$udata['unit_name'],'no_machine'=>($sno-1),'capacity'=>$total_capacity,'insurance_sum'=>$total_insurance,'om_sum'=>$total_om);
                        $usno++;
                }
            }
            if($usno>0)
            {
                $w1=10;$w2=30;$w3=10;$w4=10;$w5=10;$w6=10;
                $tfooter="";
                $tbl_rows=""; $sno=1;
               $border_less='style="border: 1px solid white;cellpadding=0;"';
               $item_count=count($total_value);
                $total_capacity=0;$total_insurance=0;$total_om=0;$total_machine=0;
               for($x=0;$x<$item_count;$x++)
                {
                    $current='<tr>
                    <td height="'.$row_height.'" align="center" width="'.$w1.'%">'.($x+1).'</td>
                    <td height="'.$row_height.'" align="left" width="'.$w2.'%">'.$total_value[$x]["name"].'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w3.'%">'.$total_value[$x]["no_machine"].'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w4.'%">'.$total_value[$x]["capacity"].'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w5.'%">'.$total_value[$x]["insurance_sum"].'</td>
                    <td height="'.$row_height.'" align="center" width="'.$w6.'%">'.$total_value[$x]["om_sum"].'</td>
                    </tr>';
                    $tbl_rows.=$current;

                    $total_machine+=$total_value[$x]["no_machine"];
                    $total_capacity+=$total_value[$x]["capacity"];
                    $total_insurance+=$total_value[$x]["insurance_sum"];
                    $total_om+=$total_value[$x]["om_sum"];
                }
                $tfooter='<tfooter>
                <tr>
                <td vertical-align="middle" height="'.$row_height.'" colspan="2" align="center"><strong>Total</strong></td>
                <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="1" align="center"><strong>'.$total_machine.'</strong></td>
                <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="1" align="center"><strong>'.$total_capacity.'</strong></td>
                <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="1" align="center"><strong>'.$total_insurance.'</strong></td>
                <td vertical-align="middle" align="right" height="'.$row_height.'" colspan="1" align="center"><strong>'.$total_om.'</strong></td>
                </tr></tfooter>';
                $tbl='<table border="1" cellpadding="2" width="100%">
                <thead>
                    <tr style=" background-color:#FFFF00;color:#0000FF;">
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w1.'%" align="center"><b>S.No.</b></th>
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w2.'%" align="center"><b>Unit Name</b></th>
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w3.'%" align="center"> <b>No.of Machines</b></th>
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w4.'%" align="center"><b>Total Capacity</b></th>
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w5.'%" align="center"><b>Total Insurance Due amount</b></th>
                        <th vertical-align="middle" height="'.$row_height.'" width="'.$w6.'%" align="center"><b>Total O&M Due Amount</b></th>
                       </tr>
                </thead>
                <tbody>'.$tbl_rows.'</tbody>'.$tfooter.'</table>';
                $pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
            }
        }

        
        database_close($conn);
          $doc_location=document_namecreate("insomremind.pdf");
            $pdf->Output($doc_location, 'F');
            $_SESSION["download_docname"]="Machine Summary.pdf";
            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
          
        }
        catch(Exception $e) {echo $e->getMessage();} 
   }
?>


