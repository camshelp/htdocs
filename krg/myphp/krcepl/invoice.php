<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/pdfheader.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
  include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "client_register":client_register();
                    break;
            case "client_list":client_list();
                    break;
            case "invoice_register":Register_Invoice();
                    break;
            case "list_invoice":invoice_list();
                    break;
            case "invoice_report":report_invoice();
                    break;
            case "invoice_consolidated":invoice_consolidated();
                    break;
            case "payment_update":Update_Payment();
                    break;
            case "invoice_payments":payment_list();
                    break;
            case "pending_summary":Pending_summary();
                    break;
            case "clientinvoice_consolidated":invoice_consolidated_client();
                    break;
            case "invoice_remove":invoice_remove();
                    break;
        }
  }
  function invoice_remove()
  {
    $msg="";$type="";
    $id=$_POST['invoice_id'];
    $conn = database_open();
    try
    {
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        $update_time=get_datetime();

        $sql="update wind_mill.invoice_list set status='no',updated_by=:by,updated_session=:session,updated_time=:time where invoice_id=:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$id);
        $stmt->bindParam(':by',$update_by);
        $stmt->bindParam(':session',$update_session);
        $stmt->bindParam(':time',$update_time);
        if($stmt->execute()==TRUE)
        {
            $msg="The Invoice Removed";
            $type="Success";
        }

    }catch(Exception $e){$msg=$e->getMessage(); }
    database_close($conn);
    $arr = ["result" => $type, "Message" => $msg];
    echo json_encode($arr);
  }
  function client_register()
  {
    $msg="";$type="";
    if(isset($_POST['ccid'])){  $cid = $_POST['ccid']; }else{$msg="Client Id was not posted";}
    if(isset($_POST['cname'])){  $cname = $_POST['cname']; }else{$msg="Client Name was not posted";}
    if(isset($_POST['ccost'])){  $cost = $_POST['ccost']; }else{$msg="Unit Cost was not posted";}
    if(isset($_POST['cline1'])){  $line1 = $_POST['cline1']; }else{$msg="Address Line1 was not posted";}
    if(isset($_POST['cline2'])){  $line2 = $_POST['cline2']; }else{$msg="Address Line2 was not posted";}
    if(isset($_POST['cline3'])){  $line3 = $_POST['cline3']; }else{$msg="Address Line3 was not posted";}
    if(isset($_POST['cline4'])){  $line4 = $_POST['cline4']; }else{$msg="Address Line4 was not posted";}
    if(isset($_POST['cmobile'])){  $mobile = $_POST['cmobile']; }else{$msg="Mobile No. was not posted";}
    if(isset($_POST['cmail'])){  $mail = $_POST['cmail']; }else{$msg="Mail Id was not posted";}
    if(isset($_POST['cedc'])){  $edc = $_POST['cedc']; }else{$msg="EDC Name was not posted";}
    if(isset($_POST['chtsc'])){  $htsc = $_POST['chtsc']; }else{$msg="HTSC No. was not posted";}
    if(isset($_POST['cgst'])){  $gst = $_POST['cgst']; }else{$msg="GST No. was not posted";}
    if(isset($_POST['cpan'])){  $pan = $_POST['cpan']; }else{$msg="PAN No. was not posted";}
    if(isset($_POST['ccontact'])){  $person = $_POST['ccontact']; }else{$msg="Contact Person Name was not posted";}
    if(isset($_POST['cdesig'])){  $designation = $_POST['cdesig']; }else{$msg="Contact Person Designation was not posted";}
    if($msg=="")
    {
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        $update_time=get_datetime();
        $conn = database_open();
            try
            {

                if($cid!="")
                {
                        $sql="update wind_mill.client set client_name=:name,address1=:line1,address2=:line2,address3=:line3,address4=:line4,mobile_no=:mobile,mail_id=:mail,edc_name=:edc,htsc_no=:htsc,gstin_no=:gst,pan_no=:pan,finalized_unit_cost=:cost,contact_person=:person,person_designation=:desig,updated_by=:by,updated_session=:session,updated_time=:time where client_id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id',$cid);
                        $stmt->bindParam(':name',$cname);
                        $stmt->bindParam(':line1',$line1);
                        $stmt->bindParam(':line2',$line2);
                        $stmt->bindParam(':line3',$line3);
                        $stmt->bindParam(':line4',$line4);
                        $stmt->bindParam(':mobile',$mobile);
                        $stmt->bindParam(':mail',$mail);
                        $stmt->bindParam(':edc',$edc);
                        $stmt->bindParam(':htsc',$htsc);
                        $stmt->bindParam(':gst',$gst);
                        $stmt->bindParam(':pan',$pan);
                        $stmt->bindParam(':cost',$cost);
                        $stmt->bindParam(':person',$person);
                        $stmt->bindParam(':desig',$designation);
                        $stmt->bindParam(':by',$update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if($stmt->execute()==TRUE)
                        {
                            $msg="The Client Information Submitted Successfully";
                            $type="Success";
                        }
                }
                else
                {
                    $sql="select client_id from wind_mill.client where client_name=:mname";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':mname', $cname);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row==0)
                    {
                        $sql="select count(*) from wind_mill.client";
                        $stmt = $conn->prepare($sql); 
                        $stmt->execute();
                        $result = $stmt->fetchColumn(0);
                        $row=$result;
                        $add_value="1";
                        $total=(integer)$row+(integer)$add_value;
                        $machine_id="CL".SixDigitIdGenerator("$total");

                        $sql="insert into wind_mill.client values(:id,:name,:line1,:line2,:line3,:line4,:mobile,:mail,:edc,:htsc,:gst,:pan,:cost,:person,:desig,:by,:session,:time)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id',$machine_id);
                        $stmt->bindParam(':name',$cname);
                        $stmt->bindParam(':line1',$line1);
                        $stmt->bindParam(':line2',$line2);
                        $stmt->bindParam(':line3',$line3);
                        $stmt->bindParam(':line4',$line4);
                        $stmt->bindParam(':mobile',$mobile);
                        $stmt->bindParam(':mail',$mail);
                        $stmt->bindParam(':edc',$edc);
                        $stmt->bindParam(':htsc',$htsc);
                        $stmt->bindParam(':gst',$gst);
                        $stmt->bindParam(':pan',$pan);
                        $stmt->bindParam(':cost',$cost);
                        $stmt->bindParam(':person',$person);
                        $stmt->bindParam(':desig',$designation);
                        $stmt->bindParam(':by',$update_by);
                        $stmt->bindParam(':session',$update_session);
                        $stmt->bindParam(':time',$update_time);
                        if($stmt->execute()==TRUE)
                        {
                            $msg="The Client Information Submitted Successfully";
                            $type="Success";
                        }
                    } else{$msg="The Client Name was already found";}

                }
            }catch(Exception $e){$msg=$e->getMessage(); }
            database_close($conn);
    }
    $arr = ["result" => $type, "Message" => $msg];
    echo json_encode($arr);
  }
  function client_list()
  {
       header("Content-Type: application/json; charset=UTF-8");
       $json = array();
       $conn = database_open();
       $sql="select * from wind_mill.client order by client_name asc";
       $stmt = $conn->prepare($sql); 
       $stmt->execute();
       $row =$stmt->rowCount();
       if($row)
       {
           $sno=0;
           while($row = $stmt->fetch(PDO::FETCH_BOTH))
           {
               $json[$sno] = array(
               'id' => $row['client_id'],
               'person' => $row['contact_person'],
               'designation' => $row['person_designation'],
               'name' => $row['client_name'],
               'line1' => $row['address1'],
               'line2' => $row['address2'],
               'line3' => $row['address3'],
               'line4' => $row['address4'],
               'mobile' => $row['mobile_no'],
               'mail' => $row['mail_id'],
               'edc' => $row['EDC_name'],
               'htsc' => $row['HTSC_no'],
               'gst' => $row['GSTIN_no'],
               'pan' => $row['PAN_no'],
               'cost' => $row['finalized_unit_cost']);
               $sno++;
           }
       }
       database_close($conn);
       echo json_encode($json);
  }
    function Register_Invoice()
    {
        $msg="";$type="";
        if(isset($_POST['iid'])){  $id = $_POST['iid']; }else{$msg="Invoice Id was not posted";}
        if(isset($_POST['uname'])){  $uid = $_POST['uname']; }else{$msg="Unit Id was not posted";}
        if(isset($_POST['clname'])){  $cid = $_POST['clname']; }else{$msg="Client Id was not posted";}
        if(isset($_POST['imonth'])){  $month = $_POST['imonth']; }else{$msg="Invoice Month & Year was not posted";}
        //if(isset($_POST['iperiod'])){  $period = $_POST['iperiod']; }else{$msg="Invoice was not posted";}
       
        $ihtsc=$_POST['ihtsc'];
        $idate=$_POST['idate'];
        $pay_date=$_POST['ipay'];
        $allocated=$_POST['iallocated'];
        $loss=$_POST['iloss'];
        $net_gen=$_POST['net_gen'];
        $consumed=$_POST['consumed'];
        $banked=$_POST['ibanked'];
        $taken=$_POST['itaken'];
        $total_unit=$_POST['total_unit'];
        $rate=$_POST['irate'];
        $total_cost=$_POST['total_cost'];
        $rent=$_POST['imrent'];
        $penalty=$_POST['ipenalty'];
        $oper=$_POST['iop'];
        $om=$_POST['iom'];
        $trans=$_POST['itr'];
        $wheel=$_POST['iwhl'];
        $bank_charge=$_POST['ibc'];
        $self_tax=$_POST['isgt'];
        $neg=$_POST['inc'];
        $reimburse=$_POST['reimburse'];
        $payable=$_POST['payable'];
        $oname=$_POST['ocname'];
        $ocharge=$_POST['oc'];
        
        if($msg=="")
        {
            session_start();
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $update_time=get_datetime();
            $conn = database_open();
                try
                {
                    if($id!="")
                    {
                        $sql="select sum(paid_amount) from wind_mill.payment_history where invoice_id=:id";
                        $stmt1 = $conn->prepare($sql); 
                        $stmt1->bindParam(':id',$id);
                        $stmt1->execute();
                        $paid_amount = $stmt1->fetchColumn(0);
                        if($paid_amount==""){$paid_amount="0";}
                        $pending_amount=$payable-$paid_amount;

                        $sql="update wind_mill.invoice_list set htsc_no=:ihtsc,invoice_date=:idate,unit_id=:uid,client_id=:cid,pay_date=:pdate,month_year=:myear,bill_period=:period,unit_cost=:rate,alloted_units=:allocated,line_loss=:lossed,net_generation=:net_gen,units_consumed=:consume,units_banked=:banked,taken_bank=:taken,total_unit=:total_unit,consumption_cost=:total_cost,meter_rent=:rent,penalty=:penalty,operating_charge=:op,om_charge=:om,transmission_charge=:trans,wheeling_charge=:wheel,banking_charge=:bc,self_tax=:st,negative_charge=:nc,other_name=:oname,other_charge=:oc,less_reimburse=:lr,total_amount=:payable,amount_pending=:pending where invoice_id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id',$id);
                        $stmt->bindParam(':idate',$idate);
                        $stmt->bindParam(':ihtsc',$ihtsc);
                        $stmt->bindParam(':uid',$uid);
                        $stmt->bindParam(':cid',$cid);
                        $stmt->bindParam(':pdate',$pay_date);
                        $stmt->bindParam(':myear',$month);
                        $stmt->bindParam(':period',$period);
                        $stmt->bindParam(':rate',$rate);
                        $stmt->bindParam(':allocated',$allocated);
                        $stmt->bindParam(':lossed',$loss);
                        $stmt->bindParam(':net_gen',$net_gen);
                        $stmt->bindParam(':consume',$consumed);
                        $stmt->bindParam(':banked',$banked);
                        $stmt->bindParam(':taken',$taken);
                        $stmt->bindParam(':total_unit',$total_unit);
                        $stmt->bindParam(':total_cost',$total_cost);
                        $stmt->bindParam(':rent',$rent);
                        $stmt->bindParam(':penalty',$penalty);
                        $stmt->bindParam(':op',$oper);
                        $stmt->bindParam(':om',$om);
                        $stmt->bindParam(':trans',$trans);
                        $stmt->bindParam(':wheel',$wheel);
                        $stmt->bindParam(':bc',$bank_charge);
                        $stmt->bindParam(':st',$self_tax);
                        $stmt->bindParam(':nc',$neg);
                        $stmt->bindParam(':oname',$oname);
                        $stmt->bindParam(':oc',$ocharge);
                        $stmt->bindParam(':lr',$reimburse);
                        $stmt->bindParam(':payable',$payable);
                        $stmt->bindParam(':pending',$pending_amount);
                        if($stmt->execute()==TRUE)
                        {
                            $msg="The Invoice Information Updated Successfully";
                            $type="Success";
                        }
                    }
                    else
                    {
                        $sql="select client_id from wind_mill.invoice_list where client_id=:cid and htsc_no=:period and month_year=:myear";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':cid', $cid);
                        $stmt->bindParam(':period', $ihtsc);
                        $stmt->bindParam(':myear',$month);
                        $stmt->execute();
                        $row =$stmt->rowCount();
                        //if($row==0)
                        {
                            $sql="select count(*) from wind_mill.invoice_list";
                            $stmt = $conn->prepare($sql); 
                            $stmt->execute();
                            $result = $stmt->fetchColumn(0);
                            $row=$result;
                            $add_value="1";
                            $total=(integer)$row+(integer)$add_value;
                            $invoice_id="IN".SixDigitIdGenerator("$total");

                            $curent_date= get_date();
                            $fyear=AYEARINKRCEPLBYDATE($curent_date);

                            $sql="select count(*) from wind_mill.invoice_list where financial_year=:fyear and unit_id=:uid";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':fyear',$fyear);
                            $stmt->bindParam(':uid',$uid);
                            $stmt->execute();
                            $result = $stmt->fetchColumn(0);
                            $row=$result;
                            $add_value="1";
                            $total=(integer)$row+(integer)$add_value;
                            $invoice_no=$total."/".$fyear;

                            $status="yes";$dummy="";

                            $sql="insert into wind_mill.invoice_list values(:fyear,:id,:uid,:cid,:ihtsc,:ino,:idate,:pdate,:myear,:period,:rate,:allocated,:lossed,:net_gen,:consume,:banked,:taken,:total_unit,:total_cost,:rent,:penalty,:op,:om,:trans,:wheel,:bc,:st,:nc,:oname,:oc,:lr,:payable,:by,:session,:time,:pending,:uby,:usession,:utime,:stat)";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':fyear',$fyear);
                            $stmt->bindParam(':id',$invoice_id);
                            $stmt->bindParam(':uid',$uid);
                            $stmt->bindParam(':cid',$cid);
                            $stmt->bindParam(':ihtsc',$ihtsc);
                            $stmt->bindParam(':ino',$invoice_no);
                            $stmt->bindParam(':idate',$idate);
                            $stmt->bindParam(':pdate',$pay_date);
                            $stmt->bindParam(':myear',$month);
                            $stmt->bindParam(':period',$period);
                            $stmt->bindParam(':rate',$rate);
                            $stmt->bindParam(':allocated',$allocated);
                            $stmt->bindParam(':lossed',$loss);
                            $stmt->bindParam(':net_gen',$net_gen);
                            $stmt->bindParam(':consume',$consumed);
                            $stmt->bindParam(':banked',$banked);
                            $stmt->bindParam(':taken',$taken);
                            $stmt->bindParam(':total_unit',$total_unit);
                            $stmt->bindParam(':total_cost',$total_cost);
                            $stmt->bindParam(':rent',$rent);
                            $stmt->bindParam(':penalty',$penalty);
                            $stmt->bindParam(':op',$oper);
                            $stmt->bindParam(':om',$om);
                            $stmt->bindParam(':trans',$trans);
                            $stmt->bindParam(':wheel',$wheel);
                            $stmt->bindParam(':bc',$bank_charge);
                            $stmt->bindParam(':st',$self_tax);
                            $stmt->bindParam(':nc',$neg);
                            $stmt->bindParam(':oname',$oname);
                            $stmt->bindParam(':oc',$ocharge);
                            $stmt->bindParam(':lr',$reimburse);
                            $stmt->bindParam(':payable',$payable);
                            $stmt->bindParam(':by',$update_by);
                            $stmt->bindParam(':session',$update_session);
                            $stmt->bindParam(':time',$update_time);
                            $stmt->bindParam(':pending',$payable);
                            $stmt->bindParam(':uby',$dummy);
                            $stmt->bindParam(':utime',$dummy);
                            $stmt->bindParam(':usession',$dummy);
                            $stmt->bindParam(':stat',$status);
                            if($stmt->execute()==TRUE)
                            {
                                $msg="The Invoice Information Submitted Successfully";
                                $type="Success";
                            }
                        }
                        //else{$msg="The Invoice Information was already found";}
                    }
                }catch(Exception $e){$msg=$e->getMessage(); }
                database_close($conn);
        }
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
    }
    function invoice_list()
    {
        $unit_id=$_POST['unit_id'];
        $month_year=$_POST['month'];
        
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT wind_mill.unit.unit_id,wind_mill.unit.unit_name,wind_mill.client.client_id,wind_mill.client.client_name,wind_mill.client.address1,wind_mill.client.address2,wind_mill.client.address3,wind_mill.client.address4,wind_mill.client.EDC_name,wind_mill.client.contact_person,wind_mill.client.person_designation,wind_mill.invoice_list.financial_year,wind_mill.invoice_list.invoice_id,wind_mill.invoice_list.invoice_no,wind_mill.invoice_list.invoice_date,wind_mill.invoice_list.htsc_no,wind_mill.invoice_list.pay_date,wind_mill.invoice_list.month_year,wind_mill.invoice_list.bill_period,wind_mill.invoice_list.unit_cost,wind_mill.invoice_list.alloted_units,wind_mill.invoice_list.line_loss,wind_mill.invoice_list.net_generation,wind_mill.invoice_list.units_consumed,wind_mill.invoice_list.units_banked,wind_mill.invoice_list.taken_bank,wind_mill.invoice_list.total_unit,wind_mill.invoice_list.consumption_cost,wind_mill.invoice_list.meter_rent,wind_mill.invoice_list.penalty,wind_mill.invoice_list.operating_charge,wind_mill.invoice_list.om_charge,wind_mill.invoice_list.transmission_charge,wind_mill.invoice_list.wheeling_charge,wind_mill.invoice_list.banking_charge,wind_mill.invoice_list.self_tax,wind_mill.invoice_list.negative_charge,wind_mill.invoice_list.other_name,wind_mill.invoice_list.other_charge,wind_mill.invoice_list.less_reimburse,wind_mill.invoice_list.total_amount,wind_mill.invoice_list.amount_pending FROM wind_mill.invoice_list JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.invoice_list.unit_id JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.unit_id=:uid and wind_mill.invoice_list.month_year=:myear and wind_mill.invoice_list.status='yes'  order by wind_mill.invoice_list.invoice_id asc";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':uid',$unit_id);
        $stmt->bindParam(':myear',$month_year);
        $stmt->execute();
       // $stmt->debugDumpParams();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select sum(paid_amount) from wind_mill.payment_history where invoice_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$row['invoice_id']);
                $stmt1->execute();
                $paid_amount = $stmt1->fetchColumn(0);
                if($paid_amount==""){$paid_amount="0";}

                $pending_amount=$row['total_amount']-$paid_amount;
              

                $json[$sno] = array(
                'unit_id' => $row['unit_id'],
                'unit_name' => $row['unit_name'],
                'client_id' => $row['client_id'],
                'client_name' => $row['client_name'],
                'address1' => $row['address1'],
                'address2' => $row['address2'],
                'address3' => $row['address3'],
                'address4' => $row['address4'],
                'edc_name' => $row['EDC_name'],
                'htsc_no' => $row['HTSC_no'],
                'person' => $row['contact_person'],
                'designation' => $row['person_designation'],
                'fyear' => $row['financial_year'],
                'invoice_id' => $row['invoice_id'],
                'invoice_no' => $row['invoice_no'],
                'invoice_date' => $row['invoice_date'],
                'pay_date' => $row['pay_date'],
                'month_year' => $row['month_year'],
                'bill_period' => $row['bill_period'],
                'unit_cost' => $row['unit_cost'],
                'alloted_units' => $row['alloted_units'],
                'line_loss' => $row['line_loss'],
                'net_generation' => $row['net_generation'],
                'units_consumed' => $row['units_consumed'],
                'units_banked' => $row['units_banked'],
                'taken_bank' => $row['taken_bank'],
                'total_unit' => $row['total_unit'],
                'total_cost' => $row['consumption_cost'],
                'meter_rent' => $row['meter_rent'],
                'penalty' => $row['penalty'],
                'operating_charge' => $row['operating_charge'],
                'om_charge' => $row['om_charge'],
                'transmission_charge' => $row['transmission_charge'],
                'wheeling_charge' => $row['wheeling_charge'],
                'banking_charge' => $row['banking_charge'],
                'self_tax' => $row['self_tax'],
                'negative_charge' => $row['negative_charge'],
                'other_name' => $row['other_name'],
                'other_charge' => $row['other_charge'],
                'less_reimburse' => $row['less_reimburse'],
                'total_amount' => $row['total_amount'],
                'amount_paid' => $paid_amount,
                'amount_pending' => $pending_amount);
                $sno++;
            }
        }

        $sql="select sum(amount_pending) from wind_mill.invoice_list where amount_pending>0 and month_year=:myear and unit_id=:uid";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':myear',$month_year);
        $stmt->bindParam(':uid',$unit_id);
        $stmt->execute();
        $pending_amount = $stmt->fetchColumn(0);
        database_close($conn);
        $arr = ["data" => $json, "pending_amount" => $pending_amount];
        echo json_encode($arr);
    }
    function Pending_summary()
    {
        $date=get_date();
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT wind_mill.unit.unit_id,wind_mill.unit.unit_name,wind_mill.client.client_id,wind_mill.client.client_name,wind_mill.client.address1,wind_mill.client.address2,wind_mill.client.address3,wind_mill.client.address4,wind_mill.client.EDC_name,wind_mill.client.HTSC_no,wind_mill.client.contact_person,wind_mill.client.person_designation,wind_mill.invoice_list.financial_year,wind_mill.invoice_list.invoice_id,wind_mill.invoice_list.invoice_no,wind_mill.invoice_list.invoice_date,wind_mill.invoice_list.pay_date,wind_mill.invoice_list.month_year,wind_mill.invoice_list.bill_period,wind_mill.invoice_list.unit_cost,wind_mill.invoice_list.alloted_units,wind_mill.invoice_list.line_loss,wind_mill.invoice_list.net_generation,wind_mill.invoice_list.units_consumed,wind_mill.invoice_list.units_banked,wind_mill.invoice_list.taken_bank,wind_mill.invoice_list.total_unit,wind_mill.invoice_list.consumption_cost,wind_mill.invoice_list.meter_rent,wind_mill.invoice_list.penalty,wind_mill.invoice_list.operating_charge,wind_mill.invoice_list.om_charge,wind_mill.invoice_list.transmission_charge,wind_mill.invoice_list.wheeling_charge,wind_mill.invoice_list.banking_charge,wind_mill.invoice_list.self_tax,wind_mill.invoice_list.negative_charge,wind_mill.invoice_list.other_name,wind_mill.invoice_list.other_charge,wind_mill.invoice_list.less_reimburse,wind_mill.invoice_list.total_amount,wind_mill.invoice_list.amount_pending FROM wind_mill.invoice_list JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.invoice_list.unit_id JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.pay_date<=:date and wind_mill.invoice_list.amount_pending>0 and wind_mill.invoice_list.status='yes' order by wind_mill.invoice_list.invoice_id asc";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':date',$date);
        $stmt->execute();
       // $stmt->debugDumpParams();
        $row =$stmt->rowCount();
        if($row)
        {
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="select sum(paid_amount) from wind_mill.payment_history where invoice_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$row['invoice_id']);
                $stmt1->execute();
                $paid_amount = $stmt1->fetchColumn(0);
                if($paid_amount==""){$paid_amount="0";}
                $pending_amount=$row['total_amount']-$paid_amount;
                if($pending_amount>0)
                {
                    $bdate=$row['month_year'].'-01';
                    $myear= date('M, Y', strtotime($bdate .' -0 day'));
                    $paid_amount=$row['total_amount']- $row['amount_pending'];
                    $json[$sno] = array(
                    'unit_id' => $row['unit_id'],
                    'unit_name' => $row['unit_name'],
                    'client_id' => $row['client_id'],
                    'client_name' => $row['client_name'],
                    'address1' => $row['address1'],
                    'address2' => $row['address2'],
                    'address3' => $row['address3'],
                    'address4' => $row['address4'],
                    'edc_name' => $row['EDC_name'],
                    'htsc_no' => $row['HTSC_no'],
                    'person' => $row['contact_person'],
                    'designation' => $row['person_designation'],
                    'fyear' => $row['financial_year'],
                    'invoice_id' => $row['invoice_id'],
                    'invoice_no' => $row['invoice_no'],
                    'invoice_date' => $row['invoice_date'],
                    'pay_date' => $row['pay_date'],
                    'month_year' => $row['month_year'],
                    'myear'=>$myear,
                    'bill_period' => $row['bill_period'],
                    'unit_cost' => $row['unit_cost'],
                    'alloted_units' => $row['alloted_units'],
                    'line_loss' => $row['line_loss'],
                    'net_generation' => $row['net_generation'],
                    'units_consumed' => $row['units_consumed'],
                    'units_banked' => $row['units_banked'],
                    'taken_bank' => $row['taken_bank'],
                    'total_unit' => $row['total_unit'],
                    'total_cost' => $row['consumption_cost'],
                    'meter_rent' => $row['meter_rent'],
                    'penalty' => $row['penalty'],
                    'operating_charge' => $row['operating_charge'],
                    'om_charge' => $row['om_charge'],
                    'transmission_charge' => $row['transmission_charge'],
                    'wheeling_charge' => $row['wheeling_charge'],
                    'banking_charge' => $row['banking_charge'],
                    'self_tax' => $row['self_tax'],
                    'negative_charge' => $row['negative_charge'],
                    'other_name' => $row['other_name'],
                    'other_charge' => $row['other_charge'],
                    'less_reimburse' => $row['less_reimburse'],
                    'total_amount' => $row['total_amount'],
                    'amount_paid' => $paid_amount,
                    'amount_pending' => $pending_amount);
                    $sno++;
                }
            }
        }
        echo json_encode($json);
    }
    function report_invoice()
    {
        $invoice_id=$_POST['invoice_id'];
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");
        $conn = database_open();
        $sql="SELECT wind_mill.unit.unit_id,wind_mill.unit.unit_name,wind_mill.unit.PAN,wind_mill.unit.GST,wind_mill.unit.CIN,wind_mill.unit.bank_account_name,wind_mill.unit.Bank_name,wind_mill.unit.branch_name,wind_mill.unit.branch_location,wind_mill.unit.MICR_code,wind_mill.unit.IFSC_code,wind_mill.unit.Account_nature,wind_mill.unit.Account_no,wind_mill.client.client_id,wind_mill.client.client_name,wind_mill.client.address1,wind_mill.client.address2,wind_mill.client.address3,wind_mill.client.address4,wind_mill.client.EDC_name,wind_mill.invoice_list.htsc_no,wind_mill.client.contact_person,wind_mill.client.person_designation,wind_mill.invoice_list.financial_year,wind_mill.invoice_list.invoice_id,wind_mill.invoice_list.invoice_no,wind_mill.invoice_list.invoice_date,wind_mill.invoice_list.pay_date,wind_mill.invoice_list.month_year,wind_mill.invoice_list.bill_period,wind_mill.invoice_list.unit_cost,wind_mill.invoice_list.alloted_units,wind_mill.invoice_list.line_loss,wind_mill.invoice_list.net_generation,wind_mill.invoice_list.units_consumed,wind_mill.invoice_list.units_banked,wind_mill.invoice_list.taken_bank,wind_mill.invoice_list.total_unit,wind_mill.invoice_list.consumption_cost,wind_mill.invoice_list.meter_rent,wind_mill.invoice_list.penalty,wind_mill.invoice_list.operating_charge,wind_mill.invoice_list.om_charge,wind_mill.invoice_list.transmission_charge,wind_mill.invoice_list.wheeling_charge,wind_mill.invoice_list.banking_charge,wind_mill.invoice_list.self_tax,wind_mill.invoice_list.negative_charge,wind_mill.invoice_list.other_name,wind_mill.invoice_list.other_charge,wind_mill.invoice_list.less_reimburse,wind_mill.invoice_list.total_amount,wind_mill.invoice_list.amount_pending FROM wind_mill.invoice_list JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.invoice_list.unit_id JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.invoice_id=:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$invoice_id);
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row>0)
        {
            $result = $stmt -> fetch();
            $company_id=$result['unit_id'];$title="";
            $date=$result['month_year'].'-01';
            $myear = date('M, Y', strtotime($date .' -0 day'));

            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(15, 28, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->AddPage('P', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

           
            $pdf->SetAutoPageBreak(TRUE, $margin=1);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('dejavusans', '', 9, '', true);
            $pdf->AddPage();
            //$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

            $row_height=20;  $row_height1=18;
            $w1=10;$w2=49;$gap=1;$w3=14;$w4=26;
            $t1=6;$t2=60;$t3=10;$t4=12;$t5=12;
            $l1=60;$l2=40;

            $ids=explode('-',$result['invoice_date']);
            $pds=explode('-',$result['pay_date']);

            if($result['unit_id']=="D000002")
            {
                $tbl_rows='<tr>
                            <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black;height:200px;" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">1.</td>
                            <td colspan="1" style="border-right: 1px solid black;height:200px;" align="left" vertical-align="middle" width="'.$t2.'%">Charged for Wheeled Enery - '.$myear.'</td>
                            <td colspan="1" style="border-right: 1px solid black;height:200px;" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['total_unit'].'</td>
                            <td colspan="1" style="border-right: 1px solid black;height:200px;" align="right" vertical-align="middle" width="'.$t4.'%">'.$result['unit_cost'].'</td>
                            <td colspan="1" style="border-right: 1px solid black;height:200px;" align="right" vertical-align="middle" width="'.$t5.'%">'.$result['consumption_cost'].'</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" vertical-align="middle" width="'.($t1+$t2).'%" height="'.$row_height1.'"><b>Total Amount</b></td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$t3.'%"><b>'.$result['total_unit'].'</b></td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$t4.'%" ><b>'.$result['unit_cost'].'</b></td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$t5.'%"><b>'.$result['consumption_cost'].'</b></td>
                        </tr>';
            }
            else
            {
            $tbl_rows='<tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">A</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Wind Power Units Alloted for '.$myear.' </td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['alloted_units'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">B</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Line Loss </td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['line_loss'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%" ></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">C</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%" >Net Generation '.$myear.' </td>
                        <td colspan="1" style="border-right: 1px solid black;border-top: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%" >'.$result['net_generation'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">D</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Wind Power Units Consumed '.$myear.' </td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['units_consumed'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">E</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%" >Wind Power Units Banked '.$myear.' </td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%" >'.$result['units_banked'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%" ></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">F</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%" >Taken from banked units</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%" >'.$result['taken_bank'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%" ></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%" ></td>
                    </tr>
                    <tr>
                        <td colspan="1" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'"><b>G</b></td>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$t2.'%"><b>Charges for Consumption of Wind Power units '.$myear.'</b></td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$t3.'%"><b>'.$result['total_unit'].'</b></td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$t4.'%"><b>'.$result['unit_cost'].'</b></td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$t5.'%"><b>'.$result['consumption_cost'].'</b></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'"><b></b></td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%"><b><u>Reimbursement</u></b></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">H</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Meter Rent</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['meter_rent'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">I</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">RKVAH Penalty</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['penalty'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">J</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">System Operating Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['operating_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">K</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Substation O&M Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['om_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">L</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Transmission Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['transmission_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">M</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Wheeling Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['wheeling_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">N</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Banking Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['banking_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">O</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Self Generation Tax</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['self_tax'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">P</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">Negative Charges</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['negative_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'">Q</td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%">'.$result['other_name'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t3.'%">'.$result['other_charge'].'</td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-right: 1px solid black;border-left: 1px solid black" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height1.'"><b>R</b></td>
                        <td colspan="1" style="border-right: 1px solid black" align="left" vertical-align="middle" width="'.$t2.'%"><b>Less Reimbursements</b></td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$t3.'%"><b>'.$result['less_reimburse'].'</b></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t4.'%"></td>
                        <td colspan="1" style="border-right: 1px solid black" align="right" vertical-align="middle" width="'.$t5.'%"><b>'.$result['less_reimburse'].'</b></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right" vertical-align="middle" width="'.($t1+$t2+$t3+$t4).'%" height="'.$row_height.'"><b>Total Amount Payable</b></td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$t5.'%"><b>'.$result['total_amount'].'</b></td>
                    </tr>'; 
            }
            $tfooter="";
            $tbl='<table cellpadding="1" width="100%">
            <thead>
                <tr>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>PAN No.</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w2.'%" height="'.$row_height.'">'.$result['PAN'].'</th>

                    <th colspan="1" align="left" vertical-align="middle" width="'.$w3.'%" height="'.$row_height.'"><b>GSTIN No.</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w4.'%" height="'.$row_height.'">'.$result['GST'].'</th>
                </tr>
                <tr>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b></b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b></b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w2.'%" height="'.$row_height.'"></th>

                    <th colspan="1" align="left" vertical-align="middle" width="'.($w3).'%" height="'.$row_height.'"><b>CIN No.</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w4.'%" height="'.$row_height.'">'.$result['CIN'].'</th>
                </tr>
                <tr>
                    <th colspan="6" align="center" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%" height="'.$row_height.'"><h3><b><u>INVOICE</u></b></h3></th>
                </tr>
                <tr>
                    <th colspan="6" align="left" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%" height="'.$row_height.'"><b>To</b></th>
                </tr>
                <tr>
                    <th colspan="3" rowspan="3" align="left" vertical-align="middle" width="'.($w1+$w2+$gap).'%" height="'.$row_height.'">'.$result['contact_person'].' '.$result['person_designation'].'<br>'.$result['client_name'].'<br>'.$result['address1'].'<br>'.$result['address2'].'<br>'.$result['address3'].'<br>'.$result['address4'].'</th>
                    
                    <th colspan="1" align="left" vertical-align="middle" width="'.($w3).'%" height="'.$row_height.'"><b>Invoice No.</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w4.'%" height="'.$row_height.'">'.$result['invoice_no'].'</th>
                </tr>
                <tr>
                    <th colspan="1" align="left" vertical-align="middle" width="'.($w3).'%" height="'.$row_height.'"><b>Invoice Date</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w4.'%" height="'.$row_height.'">'.$ids[2].'-'.$ids[1].'-'.$ids[0].'</th>
                </tr>
                <tr>
                    <th colspan="1" align="left" vertical-align="middle" width="'.($w3).'%" height="'.$row_height.'"><b>Pay by Date</b></th>
                    <th colspan="1" align="center" vertical-align="middle" width="'.$gap.'%" height="'.$row_height.'"><b>:</b></th>
                    <th colspan="1" align="left" vertical-align="middle" width="'.$w4.'%" height="'.$row_height.'">'.$pds[2].'-'.$pds[1].'-'.$pds[0].'</th>
                </tr>
                <tr>
                    <th colspan="6" align="center" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%" height="30px"><h3><b><u>(HTSC No.'.$result['HTSC_no'].'/'.$result['EDC_name'].' - EDC)</u></b></h3></th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="6" align="center" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%">
                    <table  width="100%" border="1px" cellpadding="1" cellspacing="1">
                            <tr>
                                <td colspan="1" align="center" vertical-align="middle" width="'.$t1.'%" height="'.$row_height.'"><b>S.No.</b></td>
                                <td colspan="1" align="center" vertical-align="middle" width="'.$t2.'%" ><b>Description</b></td>
                                <td colspan="1" align="center" vertical-align="middle" width="'.$t3.'%" ><b>Qty in Units</b></td>
                                <td colspan="1" align="center" vertical-align="middle" width="'.$t4.'%" ><b>Rate Rs/Unit</b></td>
                                <td colspan="1" align="center" vertical-align="middle" width="'.$t5.'%" ><b>Amount in Rupees</b></td>
                            </tr>'. $tbl_rows.'
                    </table>
                </td>
            </tr>
            </tbody>
            <tr>
                <td colspan="6" align="left" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%">E. & O.E.</td>
            </tr>
            <tr>
                <td colspan="6" align="right" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%">(Rupees '.numberTowords($result['total_amount']).' only)</td>
            </tr>
            <tr>
                <td colspan="6" align="left" height="5px;" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"></td>
            </tr>
            <tr>
                <td colspan="6" align="left" height="60px;" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"><b>For '.$result['unit_name'].'</b></td>
            </tr>
            <tr>
                <td colspan="6" align="left" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"><b>Authorized Signatory</b></td>
            </tr>
            <tr>
                <td colspan="6" align="left" height="10px;" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"></td>
            </tr>
            <tr>
                <td colspan="6" align="left" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%">
                    <table  width="100%" border="1px" cellpadding="1" cellspacing="1">
                    <tr>
                        <td colspan="2" align="center" vertical-align="middle" width="'.($l1+$l2).'%"><b>Bank Details for RTGS Transfer</b></td>
                    </tr> 
                    <tr>
                        <td colspan="2" align="left" vertical-align="middle" width="'.($l1+$l2).'%"><b>Account Name : </b>'.$result['bank_account_name'].'</td>
                    </tr> 
                    <tr>
                        <td colspan="2" align="left" vertical-align="middle" width="'.($l1+$l2).'%"><b>Branch Name : </b>'.$result['Bank_name'].','.$result['branch_name'].','.$result['branch_location'].'</td>
                    </tr> 
                    <tr>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$l1.'%"><b>Account No : </b>'.$result['Account_no'].'</td>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$l2.'%"><b>Nature of Account : </b>'.$result['Account_nature'].'</td>
                    </tr> 
                    <tr>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$l1.'%"><b>MICR Code : </b>'.$result['MICR_code'].'</td>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$l2.'%"><b>IFSC Code : </b>'.$result['IFSC_code'].'</td>
                    </tr> 
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="left" height="2px;" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"></td>
            </tr>
            <tr>
                <td colspan="6" align="left" vertical-align="middle" width="'.($w1+$w2+$w3+$gap+$w4+$gap).'%"><b>Note : </b>If Payment is not made within the due date, interest @ 16% will be charged on the amount due computed from the date on which the bill was payable till the date of the payment.</td>
            </tr>
            </table>';
            $pdf->writeHTML($tbl, true, 0, true, 0);
            //$pdf->writeHTMLCell(0, 0, 'L', '', $tbl, 0, 1, 0, true, '', true);
            $doc_location=document_namecreate("ininvoice.pdf");
            $pdf->Output($doc_location, 'F');
            $_SESSION["download_docname"]="Invoice.pdf";
            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 

        }else{echo "No Data";}
        database_close($conn);
    }
    function invoice_consolidated()
    {
        $myear=$_POST['start'];
        $date=$myear.'-01';
        $month_year = date('M, Y', strtotime($date .' -0 day'));

        $myear1=$_POST['end'];
        $date1=$myear1.'-01';
        $month_year1 = date('M, Y', strtotime($date1 .' -0 day'));
       
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");

        $conn = database_open();
        $sql="SELECT distinct wind_mill.unit.unit_id,wind_mill.unit.unit_name FROM wind_mill.invoice_list JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.invoice_list.unit_id where wind_mill.invoice_list.month_year>=:syear and wind_mill.invoice_list.month_year<=:eyear and wind_mill.invoice_list.status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':syear',$myear);
        $stmt->bindParam(':eyear',$myear1);
        $stmt->execute();
        $rowc =$stmt->rowCount();
        if($rowc>0)
        {
            $company_id="xxxx";$title="";
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(10, 14, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->AddPage('L', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

           
            $pdf->SetAutoPageBreak(TRUE, $margin=1);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('dejavusans', '', 6.5, '', true);
          
           // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
            $w1=18;$w2=6;$w3=5;$w4=5;$w5=5;$w6=3;$w7=7;$w8=4;$w9=5;$w10=4;$w11=5;$w12=5;$w13=5;$w14=6;$w15=5;$w16=5;$w17=7;
            $row_height=25;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="SELECT distinct wind_mill.client.client_id,wind_mill.client.client_name,wind_mill.client.address1,wind_mill.client.address2,wind_mill.client.address3,wind_mill.client.address4,wind_mill.client.EDC_name,wind_mill.client.HTSC_no,wind_mill.client.contact_person,wind_mill.client.person_designation,wind_mill.client.finalized_unit_cost FROM wind_mill.invoice_list JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.unit_id=:unit_id and wind_mill.invoice_list.month_year>=:syear and wind_mill.invoice_list.month_year<=:eyear and wind_mill.invoice_list.status='yes'";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':syear',$myear);
                $stmt1->bindParam(':eyear',$myear1);
                $stmt1->bindParam(':unit_id',$row['unit_id']);
                $stmt1->execute();
                $cnt =$stmt1->rowCount();
                if($cnt>0)
                {
                    $pdf->AddPage();
                    $col1=$w1+$w2+$w3+$w4+$w5;
                    $col2=$w6+$w7+$w8+$w9+$w10+$w11;
                    $col3=$w12+$w13+$w14+$w15+$w16+$w17;
                    $col4=$w1+$w2+$w3+$w4+$w5+$w6+$w7+$w8;
                    $rows="";
                    $tunits=0;$tloss=0;$tbanked=0;$tbillded=0;$trate=0;$tamount=0;$trent=0;$trkvah=0;$tsoc=0;$tsom=0;$ttc=0;$twc=0;$tbc=0;$tsgt=0;$ttotal=0;$tbalance=0;
                   
                    while($data1 = $stmt1->fetch(PDO::FETCH_BOTH))
                    {
                        $sql="SELECT sum(alloted_units) as alloted_units,sum(line_loss) as line_loss,sum(net_generation) as net_generation,sum(units_consumed) as units_consumed,sum(units_banked) as units_banked,sum(taken_bank) as taken_bank,sum(total_unit) as total_unit,
                        sum(consumption_cost) as consumption_cost,sum(meter_rent) as meter_rent,sum(penalty) as penalty,sum(operating_charge) as operating_charge,sum(om_charge) as om_charge,sum(transmission_charge) as transmission_charge,
                        sum(wheeling_charge) as wheeling_charge,sum(banking_charge) as banking_charge,sum(self_tax) as self_tax,sum(negative_charge) as negative_charge,sum(other_charge) as other_charge,sum(less_reimburse) as less_reimburse,
                        sum(total_amount) as total_amount,sum(amount_pending) as amount_pending  FROM wind_mill.invoice_list where wind_mill.invoice_list.client_id=:cid and wind_mill.invoice_list.month_year>=:syear and wind_mill.invoice_list.month_year<=:eyear and wind_mill.invoice_list.status='yes'";
                        $stmt2 = $conn->prepare($sql); 
                        $stmt2->bindParam(':syear',$myear);
                        $stmt2->bindParam(':eyear',$myear1);
                        $stmt2->bindParam(':cid',$data1['client_id']);
                        $stmt2->execute();
                        $data = $stmt2 -> fetch();

                        $others=$data['banking_charge']+$data['negative_charge']+$data['other_charge'];
                        $rows.=
                        '<tr>
                            <td colspan="1" align="left" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'">'.$data1['client_name'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w2.'%">'.$data['alloted_units'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w3.'%">'.$data['line_loss'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w4.'%">'.$data['units_banked'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w5.'%">'.$data['total_unit'].'</td>
                            <td colspan="1" align="center" vertical-align="middle" width="'.$w6.'%">'.number_format($data1['finalized_unit_cost'],2).'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w7.'%">'.$data['consumption_cost'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w8.'%">'.$data['meter_rent'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w9.'%">'.$data['penalty'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w10.'%">'.$data['operating_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w11.'%">'.$data['om_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w12.'%">'.$data['transmission_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w13.'%">'.$data['wheeling_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w14.'%">'.$others.'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w15.'%">'.$data['self_tax'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w16.'%">'.$data['less_reimburse'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w17.'%">'.$data['total_amount'].'</td>
                        </tr>';

                        $tunits+=$data['alloted_units']; $tloss+=$data['line_loss']; $tbanked+=$data['units_banked']; $tbillded+=$data['total_unit'];
                        $tamount+=$data['consumption_cost']; $trent+=$data['meter_rent'];$trkvah+=$data['penalty']; $tsoc+=$data['operating_charge'];

                        $tsom+=$data['om_charge'];$ttc+=$data['transmission_charge'];$twc+=$data['wheeling_charge']; $tbc+=$others;
                        $tsgt+=$data['self_tax']; $ttotal+=$data['less_reimburse']; $tbalance+=$data['total_amount'];
                    }
                    $trate=$tamount/$tbillded;
                    $arkvah=$trkvah/$tbillded;$asoc=$tsoc/$tbillded;$asom=$tsom/$tbillded;$atc=$ttc/$tbillded;$awc=$twc/$tbillded;$abc=$tbc/$tbillded;$asgst=$tsgt/$tbillded;$atotal=$ttotal/$tbillded;$abalance=$tbalance/$tbillded;
                    $line_height=20;

                    $tbl='<table cellpadding="1" border="1" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="17" style="border-bottom: 1px solid black;" align="center" vertical-align="middle" width="100%" height="'.$row_height.'"><h3>Consolidated Invoice Information</h3></th>
                                </tr>
                                <tr>
                                    <th colspan="5" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col1.'%" height="'.$row_height.'"><b>Unit Name : </b>'.$row['unit_name'].'</th>
                                    <th colspan="6" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col2.'%"><b>Month & Year : </b>'.$month_year.' to '.$month_year1.'</th>
                                    <th colspan="6" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col3.'%"><b>Printed On : </b>'.get_datetime().'</th>
                                </tr>
                                <tr>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>Client Name</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w2.'%"><b>Generation Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w3.'%"><b>Line Loss</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w4.'%"><b>Banked Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w5.'%"><b>Billed Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w6.'%"><b>Rate</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w7.'%"><b>Amount</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w8.'%"><b>Meter Rent</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w9.'%"><b>RKVAH</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w10.'%"><b>SOC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w11.'%"><b>S O&M</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w12.'%"><b>TC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w13.'%"><b>WC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w14.'%"><b>BC/NC/OC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w15.'%"><b>SGT</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w16.'%"><b>Total</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w17.'%"><b>Balance</b></th>
                                </tr>
                            </thead>
                            <tbody>'.$rows.'</tbody>
                            <tfooter>
                                <tr>
                                    <td colspan="1" align="center" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>Total</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w2.'%"><b>'.$tunits.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w3.'%"><b>'.$tloss.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w4.'%"><b>'.$tbanked.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w5.'%"><b>'.$tbillded.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w6.'%"><b>'.number_format($trate,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w7.'%"><b>'.$tamount.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w8.'%"><b>'.$trent.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w9.'%"><b>'.$trkvah.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w10.'%"><b>'.$tsoc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w11.'%"><b>'.$tsom.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w12.'%"><b>'.$ttc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w13.'%"><b>'.$twc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w14.'%"><b>'.$tbc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w15.'%"><b>'.$tsgt.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w16.'%"><b>'.$ttotal.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.$tbalance.'</b></td>
                                </tr>
                                <tr>
                                    <td colspan="1" align="center" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$col4.'%" height="'.$row_height.'"><b>Cost Per Unit</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w9.'%"><b>'.number_format($arkvah,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w10.'%"><b>'.number_format($asoc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w11.'%"><b>'.number_format($asom,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w12.'%"><b>'.number_format($atc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w13.'%"><b>'.number_format($awc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w14.'%"><b>'.number_format($abc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w15.'%"><b>'.number_format($asgst,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w16.'%"><b>'.number_format($atotal,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.number_format($abalance,2).'</b></td>
                                </tr>
                            </tfooter>
                            </table>';
                    $pdf->writeHTML($tbl, true, 0, true, 0);
                }
            }
            $doc_location=document_namecreate("ininvoice.pdf");
            $pdf->Output($doc_location, 'F');
            $_SESSION["download_docname"]="Consolidated Invoice.pdf";
            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
        }else{echo "No Data";}
        database_close($conn);
    }
    function Update_Payment()
    {
        $msg="";$type="";
        $invoice_id=$_POST['piid'];
        $client_id=$_POST['pcid'];
        $unit_id=$_POST['puid'];
        $type=$_POST['sal'];
        $date=$_POST['pmonth'];
        $amount=$_POST['pamount'];
        $remarks=$_POST['premarks'];
     
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        $update_time=get_datetime();
        $conn = database_open();
        try
        {

            $sql="insert into wind_mill.payment_history values(:iid,:cid,:uid,:ptype,:pamount,:pdate,:remarks,:by,:session,:time)";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':iid',$invoice_id);
            $stmt->bindParam(':cid',$client_id);
            $stmt->bindParam(':uid',$unit_id);
            $stmt->bindParam(':ptype',$type);
            $stmt->bindParam(':pamount',$amount);
            $stmt->bindParam(':pdate',$date);
            $stmt->bindParam(':remarks',$remarks);
            $stmt->bindParam(':by',$update_by);
            $stmt->bindParam(':time',$update_time);
            $stmt->bindParam(':session',$update_session);
            if($stmt->execute()==TRUE)
            {
                $sql="select amount_pending from wind_mill.invoice_list where invoice_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$invoice_id);
                $stmt1->execute();
                $pending_amount = $stmt1->fetchColumn(0);
                $revise=$pending_amount-$amount;

                $sql="update wind_mill.invoice_list set amount_pending=:pending,updated_by=:by,updated_session=:session,updated_time=:time where invoice_id=:iid";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':iid',$invoice_id);
                $stmt->bindParam(':pending',$revise);
                $stmt->bindParam(':by',$update_by);
                $stmt->bindParam(':time',$update_time);
                $stmt->bindParam(':session',$update_session);
                $stmt->execute();

                $msg="The Payment Information Submitted Successfully";
                $type="Success";
            }

        }catch(Exception $e){$msg=$e->getMessage(); }

        database_close($conn);
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
    }
    function payment_list()
    {
         $id=$_POST['invoice_id'];
         header("Content-Type: application/json; charset=UTF-8");
         $json = array();
         $conn = database_open();
         $sql="select * from wind_mill.payment_history where invoice_id=:id";
         $stmt = $conn->prepare($sql); 
         $stmt->bindParam(':id',$id);
         $stmt->execute();
         $row =$stmt->rowCount();
         if($row)
         {
             $sno=0;
             while($row = $stmt->fetch(PDO::FETCH_BOTH))
             {
                 $json[$sno] = array(
                 'client_id' => $row['client_id'],
                 'unit_id' => $row['unit_id'],
                 'payment_type' => $row['payment_type'],
                 'paid_amount' => $row['paid_amount'],
                 'paid_date' => $row['paid_date'],
                 'remarks' => $row['remarks']);
                 $sno++;
             }
         }
         database_close($conn);
         echo json_encode($json);
    }
    function invoice_consolidated_client()
    {
        $smyear=$_POST['start'];
        $emyear=$_POST['end'];
        $uid=$_POST['unit_name'];
        
        $date=$smyear.'-01';  $date1=$emyear.'-01';
        $smonth_year = date('M, Y', strtotime($date .' -0 day'));
        $emonth_year = date('M, Y', strtotime($date1 .' -0 day'));
       
        session_start();
        $_SESSION["download_docname"]="";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");

        $conn = database_open();
        $sql="SELECT distinct wind_mill.client.client_id,wind_mill.client.client_name FROM wind_mill.invoice_list JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.month_year>=:syear and wind_mill.invoice_list.month_year<=:eyear and wind_mill.invoice_list.unit_id=:uid and wind_mill.invoice_list.status='yes'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':syear',$smyear);
        $stmt->bindParam(':eyear',$emyear);
        $stmt->bindParam(':uid',$uid);
        $stmt->execute();
        //$stmt->debugDumpParams();
        $rowc =$stmt->rowCount();
        if($rowc>0)
        {
            $company_id="xxxx";$title="";
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(10, 14, 10, true);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->AddPage('L', 'A4');
            $lastPage = $pdf->getPage();
            $pdf->deletePage($lastPage);

            $header_data=document_header($company_id,$title);
            $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
            $pdf->SetCreator(PDF_CREATOR);

           
            $pdf->SetAutoPageBreak(TRUE, $margin=1);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('dejavusans', '', 7, '', true);
          
            //$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
            $w1=7;$w2=7;$w3=4;$w4=5;$w5=5;$w6=3;$w7=7;$w8=4;$w9=5;$w10=4;$w11=5;$w12=5;$w13=5;$w14=6;$w15=5;$w16=5;$w17=6;
            $row_height=25;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $sql="SELECT wind_mill.unit.unit_id,wind_mill.unit.unit_name,wind_mill.unit.PAN,wind_mill.unit.GST,wind_mill.unit.CIN,wind_mill.unit.bank_account_name,wind_mill.unit.Bank_name,wind_mill.unit.branch_name,wind_mill.unit.branch_location,wind_mill.unit.MICR_code,wind_mill.unit.IFSC_code,wind_mill.unit.Account_nature,wind_mill.unit.Account_no,wind_mill.client.client_id,wind_mill.client.client_name,wind_mill.client.address1,wind_mill.client.address2,wind_mill.client.address3,wind_mill.client.address4,wind_mill.client.EDC_name,wind_mill.client.HTSC_no,wind_mill.client.contact_person,wind_mill.client.person_designation,wind_mill.invoice_list.financial_year,wind_mill.invoice_list.invoice_id,wind_mill.invoice_list.invoice_no,wind_mill.invoice_list.invoice_date,wind_mill.invoice_list.pay_date,wind_mill.invoice_list.month_year,wind_mill.invoice_list.bill_period,wind_mill.invoice_list.unit_cost,wind_mill.invoice_list.alloted_units,wind_mill.invoice_list.line_loss,wind_mill.invoice_list.net_generation,wind_mill.invoice_list.units_consumed,wind_mill.invoice_list.units_banked,wind_mill.invoice_list.taken_bank,wind_mill.invoice_list.total_unit,wind_mill.invoice_list.consumption_cost,wind_mill.invoice_list.meter_rent,wind_mill.invoice_list.penalty,wind_mill.invoice_list.operating_charge,wind_mill.invoice_list.om_charge,wind_mill.invoice_list.transmission_charge,wind_mill.invoice_list.wheeling_charge,wind_mill.invoice_list.banking_charge,wind_mill.invoice_list.self_tax,wind_mill.invoice_list.negative_charge,wind_mill.invoice_list.other_name,wind_mill.invoice_list.other_charge,wind_mill.invoice_list.less_reimburse,wind_mill.invoice_list.total_amount,wind_mill.invoice_list.amount_pending FROM wind_mill.invoice_list JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.invoice_list.unit_id JOIN wind_mill.client ON wind_mill.client.client_id=wind_mill.invoice_list.client_id where wind_mill.invoice_list.client_id=:client_id and wind_mill.invoice_list.month_year>=:syear and wind_mill.invoice_list.month_year<=:eyear and wind_mill.invoice_list.unit_id=:uid and wind_mill.invoice_list.status='yes' order by wind_mill.invoice_list.month_year asc";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':syear',$smyear);
                $stmt1->bindParam(':eyear',$emyear);
                $stmt1->bindParam(':client_id',$row['client_id']);
                $stmt1->bindParam(':uid',$uid);
                $stmt1->execute();
                $cnt =$stmt1->rowCount();
                if($cnt>0)
                {
                    $pdf->AddPage();
                    $col1=$w1+$w2+$w3+$w4+$w5;
                    $col2=$w6+$w7+$w8+$w9+$w10;
                    $col3=$w11+$w12+$w13+$w14+$w15+$w16+$w17;
                    $col4=$w1+$w2+$w3+$w4+$w5+$w6+$w7+$w8;
                    $rows="";
                    $tunits=0;$tloss=0;$tbanked=0;$tbillded=0;$trate=0;$tamount=0;$trent=0;$trkvah=0;$tsoc=0;$tsom=0;$ttc=0;$twc=0;$tbc=0;$tsgt=0;$ttotal=0;$tbalance=0;
                    $total_pending=0;$total_receive=0;

                    while($data = $stmt1->fetch(PDO::FETCH_BOTH))
                    {
                        $sql="select sum(paid_amount) from wind_mill.payment_history where invoice_id=:id";
                        $stmtp = $conn->prepare($sql); 
                        $stmtp->bindParam(':id',$data['invoice_id']);
                        $stmtp->execute();
                        $paid_amount = $stmtp->fetchColumn(0);
                        if($paid_amount==""){$paid_amount="0";}
                        $pending_amount=$data['total_amount']-$paid_amount;

                        $total_pending+=$pending_amount;$total_receive+=$paid_amount;
                       
                        $idate=$data['month_year'].'-01';
                        $imy = date('M, Y', strtotime($idate .' -0 day'));

                        $others=$data['banking_charge']+$data['negative_charge']+$data['other_charge'];
                        $rows.=
                        '<tr>
                            <td colspan="1" align="left" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'">'.$imy.'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w2.'%">'.$data['alloted_units'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w3.'%">'.$data['line_loss'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w4.'%">'.$data['units_banked'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w5.'%">'.$data['total_unit'].'</td>
                            <td colspan="1" align="center" vertical-align="middle" width="'.$w6.'%">'.number_format($data['unit_cost'],2).'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w7.'%">'.$data['consumption_cost'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w8.'%">'.$data['meter_rent'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w9.'%">'.$data['penalty'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w10.'%">'.$data['operating_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w11.'%">'.$data['om_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w12.'%">'.$data['transmission_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w13.'%">'.$data['wheeling_charge'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w14.'%">'.$others.'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w15.'%">'.$data['self_tax'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w16.'%">'.$data['less_reimburse'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w17.'%">'.$data['total_amount'].'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w17.'%">'.$paid_amount.'</td>
                            <td colspan="1" align="right" vertical-align="middle" width="'.$w17.'%">'.$pending_amount.'</td>
                        </tr>';

                        $tunits+=$data['alloted_units']; $tloss+=$data['line_loss']; $tbanked+=$data['units_banked']; $tbillded+=$data['total_unit'];
                        $tamount+=$data['consumption_cost']; $trent+=$data['meter_rent'];$trkvah+=$data['penalty']; $tsoc+=$data['operating_charge'];

                        $tsom+=$data['om_charge'];$ttc+=$data['transmission_charge'];$twc+=$data['wheeling_charge']; $tbc+=$others;
                        $tsgt+=$data['self_tax']; $ttotal+=$data['less_reimburse']; $tbalance+=$data['total_amount'];
                    }
                    $trate=$tamount/$tbillded;
                    $arkvah=$trkvah/$tbillded;$asoc=$tsoc/$tbillded;$asom=$tsom/$tbillded;$atc=$ttc/$tbillded;$awc=$twc/$tbillded;$abc=$tbc/$tbillded;$asgst=$tsgt/$tbillded;$atotal=$ttotal/$tbillded;$abalance=$tbalance/$tbillded;
                    $line_height=20;

                    $tbl='<table cellpadding="1" border="1" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="19" style="border-bottom: 1px solid black;" align="center" vertical-align="middle" width="100%" height="'.$row_height.'"><h3>Consolidated Invoice Information</h3></th>
                                </tr>
                                <tr>
                                    <th colspan="5" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col1.'%" height="'.$row_height.'"><b>Client Name : </b>'.$row['client_name'].'</th>
                                    <th colspan="6" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col2.'%"><b>Duration : </b>'.$smonth_year.' to '.$emonth_year.'</th>
                                    <th colspan="8" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col3.'%"><b>Printed On : </b>'.get_datetime().'</th>
                                </tr>
                                <tr>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>Month Year</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w2.'%"><b>Generation Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w3.'%"><b>Line Loss</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w4.'%"><b>Banked Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w5.'%"><b>Billed Units</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w6.'%"><b>Rate</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w7.'%"><b>Amount</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w8.'%"><b>Meter Rent</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w9.'%"><b>RKVAH</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w10.'%"><b>SOC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w11.'%"><b>S O&M</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w12.'%"><b>TC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w13.'%"><b>WC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w14.'%"><b>BC/NC/OC</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w15.'%"><b>SGT</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w16.'%"><b>Total</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w17.'%"><b>Invoice Amount</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w17.'%"><b>Received Amount</b></th>
                                    <th colspan="1" align="center" vertical-align="middle" width="'.$w17.'%"><b>Pending Amount</b></th>
                                </tr>
                            </thead>
                            <tbody>'.$rows.'</tbody>
                            <tfooter>
                                <tr>
                                    <td colspan="1" align="center" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>Total</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w2.'%"><b>'.$tunits.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w3.'%"><b>'.$tloss.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w4.'%"><b>'.$tbanked.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w5.'%"><b>'.$tbillded.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w6.'%"><b>'.number_format($trate,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w7.'%"><b>'.$tamount.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w8.'%"><b>'.$trent.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w9.'%"><b>'.$trkvah.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w10.'%"><b>'.$tsoc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w11.'%"><b>'.$tsom.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w12.'%"><b>'.$ttc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w13.'%"><b>'.$twc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w14.'%"><b>'.$tbc.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w15.'%"><b>'.$tsgt.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w16.'%"><b>'.$ttotal.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.$tbalance.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.$total_receive.'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.$total_pending.'</b></td>
                                </tr>
                                <tr>
                                    <td colspan="1" align="center" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$col4.'%" height="'.$row_height.'"><b>Cost Per Unit</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w9.'%"><b>'.number_format($arkvah,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w10.'%"><b>'.number_format($asoc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w11.'%"><b>'.number_format($asom,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w12.'%"><b>'.number_format($atc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w13.'%"><b>'.number_format($awc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w14.'%"><b>'.number_format($abc,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w15.'%"><b>'.number_format($asgst,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w16.'%"><b>'.number_format($atotal,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b>'.number_format($abalance,2).'</b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b></b></td>
                                    <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w17.'%"><b></b></td>
                                </tr>
                            </tfooter>
                            </table>';
                    $pdf->writeHTML($tbl, true, 0, true, 0);
                }
            }
            $doc_location=document_namecreate("ininvoice.pdf");
            $pdf->Output($doc_location, 'F');
            $_SESSION["download_docname"]="Consolidated Invoice.pdf";
            $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
        }else{echo "No Data".$uid;}
        database_close($conn);
    }
  ?>