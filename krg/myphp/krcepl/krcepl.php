<?php
 require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
 require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/validation.php");
 $callparameter="";
 if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
 if($callparameter=="")
 {
   $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
   echo json_encode($arr);
 }
 else
 {
       switch($callparameter)
       {
           case "contactform":Contact_info();
                   break;
           default:
                   $arr = ["result" => "danger", "Message" => "Invalid Access"];
                   echo json_encode($arr);  
                   break;
       }
   }
   function Contact_info()
   {
        $type="Error";$msg="";
        $conn = database_open();
        if (strpos($conn,"Failed") === 0) {$msg=$conn;}
        else
        {
            $varry=array(
                array('name'=>"Name",'disp'=>'Contact Person Name','param'=>"1","max"=>200),
                array('name'=>"contact",'disp'=>'Contact No.','param'=>"1","max"=>200),
                array('name'=>"Email",'disp'=>'Mail Id','param'=>"1","max"=>200),
                array('name'=>"Subject",'disp'=>'Subject Name','param'=>"1","max"=>500),
                array('name'=>"Message",'disp'=>'Message','param'=>"1","max"=>3000)
            );
            $valid=new Validation();
            $msg = $valid->ValidationCheck($_POST,$varry);
            if($msg=="")
            {
                $name = $_POST['Name'];
                $contact = $_POST['contact'];
                $Email = $_POST['Email'];
                $Subject = $_POST['Subject'];
                $Message = $_POST['Message'];

                $update_time=get_datetime();
                $ip="";
                $sql="insert into wind_mill.contact_form values(:name,:no,:mail,:subject,:message,:ip,:time)";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':name',$name);
                $stmt->bindParam(':no',$contact);
                $stmt->bindParam(':mail',$Email);
                $stmt->bindParam(':subject',$Subject);
                $stmt->bindParam(':message',$Message);
                $stmt->bindParam(':ip',$ip);
                $stmt->bindParam(':time',$update_time);
                if ($stmt->execute() == TRUE) 
                {
                    $type="Success";$msg="The Information Submitted Successfully";
                     $msg_subject=$Subject;
                    $msg_content="Dear Wind Mill Administration,<br>Warm Greetings from KRG Portal.A New Enquiry is made in KRG Portal.The details are as below <br><strong>Name :</strong> $name<br><strong>Contact No. :</strong> $contact<br><strong>E-mail Id :</strong> $Email<br><strong>Subject :</strong> $Subject<br><strong>Message Details :</strong> $Message<br><br>Please Visit the information @ http://210.212.249.135 <br><br>With Regards<br>KRG Portal,<br>KR Group of Companies,<br>Karur.";
                    $mail_msg=sendmail($msg_subject,$msg_content,"kramakrishnancleanenergy@gmail.com,krvgreen@gmail.com");
                    $msg_content="Dear $name,<br>Warm Greetings from KRG Portal.You had made a New Enquiry about KRCEPL.The details are as below <br><strong>Name :</strong> $name<br><strong>Contact No. :</strong> $contact<br><strong>E-mail Id :</strong> $Email<br><strong>Subject :</strong> $Subject<br><strong>Message Details :</strong> $Message<br><br>The reponsible person will contact you at the earliest <br><br>With Regards<br>KRG Portal,<br>KR Group of Companies,<br>Karur.";
                    $mail_msg=sendmail($msg_subject,$msg_content,$Email);
                } 
            }
            database_close($conn);
        }
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);

   }
?>