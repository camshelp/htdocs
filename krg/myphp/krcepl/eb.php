<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/pdfheader.php"); 
  require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
  include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "ebmonth_summary":unitwise_ebsummary();
                    break;
            case "eb_register":Register_EBDetails();
                    break;
            case "eb_expense_list":List_Machines();
                    break;
            case "expense_summary":Summary_Expense();
                    break;
        }
  }
  function unitwise_ebsummary()
  {
       $mname=$_POST['month'];
       //echo '123'.$mname;
       $date= explode('-',get_date());
       if($mname==""){$month_name=$date[0].'-'.$date[1];}else{ $month_name=$mname;}
       $json = array();
       $conn = database_open();
       $sql="SELECT unit_id,unit_name from wind_mill.unit where status='yes'";
       $stmt = $conn->prepare($sql); 
       $stmt->execute();
       $row =$stmt->rowCount();
       if($row>0)
       {
           $total_import=0;$total_export=0;$net_generation=0;$total_charge=0;
           $sno=0;
           while($row = $stmt->fetch(PDO::FETCH_BOTH))
           {
                $sql="SELECT sum(total_export),sum(total_import),sum(transmission),sum(net_generation),sum(10_generation),sum(power_factor),sum(RKVAH_penalty),sum(meter_charge),sum(om_charge),sum(schedule_charge),sum(operating_charge),sum(total_charges) from wind_mill.eb_expense where unit_id=:uid and month_year=:myear";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':uid', $row['unit_id']);
                $stmt1->bindParam(':myear',$month_name);
                $stmt1->execute();
                $result = $stmt1 -> fetch();

                $total_import+=$result[1]; $total_export+=$result[0];$net_generation+=$result[2]; $total_charge+=$result[11];

               $json[$sno] = array(
               'unit_id' => $row['unit_id'],
               'total_charges'=>$result[11],
               'unit_name' => $row['unit_name'],
               'export'=>$result[0],
               'import'=>$result[1],
               'transmission'=>$result[2],
               'net_generation'=>$result[3],
               '10_net_generation'=>$result[4],
               'power_factor'=>$result[5],
               'RKVAH_penalty'=>$result[6],
               'meter_charge'=>$result[7],
               'om_charge'=>$result[8],
               'schedule_charge'=>$result[9],
               'system_charge'=>$result[10]);
               $sno++;
           }
       }
       database_close($conn);
       $arr = ["unit_data" => $json, "month_name" => $month_name,'total_import'=>$total_import,'total_export'=>$total_export,'net_generation'=>$net_generation,'total_charge'=>$total_charge];
       echo json_encode($arr);
  }
  function Register_EBDetails()
  {
    $msg="";$type="";
    $unit_id=$_POST['euid'];
    $type=$_POST['etype'];
    $machine_id=$_POST['emname'];
    $myear=$_POST['myear'];
    $export=$_POST['eexport'];
    $import=$_POST['eimport'];
    $trans=$_POST['etrans'];
    $generation=$_POST['egen'];
    $ten_gen=$_POST['e10gen'];
    $power=$_POST['epower'];
    $penalty=$_POST['epenalty'];
    $reading=$_POST['ereading'];
    $om=$_POST['eomcharge'];
    $schedule=$_POST['eschedule'];
    $operation=$_POST['eoper'];

    $total_charge=$penalty+$reading+$om+$schedule+$operation+$trans;

    session_start();
    $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
    $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
    $update_time=get_datetime();

    $conn = database_open();
    try
    {

      if($type=="Update")
      {
          $sql="update wind_mill.eb_expense set total_export=:export,total_import=:import,transmission=:trans,net_generation=:gen,10_generation=:ten_gen,power_factor=:power,RKVAH_penalty=:penalty,meter_charge=:meter,om_charge=:om,schedule_charge=:schedule,operating_charge=:operating,total_charges=:total,updated_by=:by,updated_session=:session,updated_time=:time where unit_id=:uid and machine_id=:mid and month_year=:myear";
          $stmt = $conn->prepare($sql); 
          $stmt->bindParam(':uid',$unit_id);
          $stmt->bindParam(':mid',$machine_id);
          $stmt->bindParam(':myear',$myear);
          $stmt->bindParam(':export',$export);
          $stmt->bindParam(':import',$import);
          $stmt->bindParam(':trans',$trans);
          $stmt->bindParam(':gen',$generation);
          $stmt->bindParam(':ten_gen',$ten_gen);
          $stmt->bindParam(':power',$power);
          $stmt->bindParam(':penalty',$penalty);
          $stmt->bindParam(':meter',$reading);
          $stmt->bindParam(':om',$om);
          $stmt->bindParam(':schedule',$schedule);
          $stmt->bindParam(':operating',$operation);
          $stmt->bindParam(':total',$total_charge);
          $stmt->bindParam(':by',$update_by);
          $stmt->bindParam(':session',$update_session);
          $stmt->bindParam(':time',$update_time);
          if($stmt->execute()==TRUE)
          {
              $msg="The EB Information Updated Successfully";
              $type="Success";
          }
      }
      else
      {
        $sql="select unit_id from wind_mill.eb_expense where unit_id=:uid and machine_id=:mid and month_year=:myear";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':uid', $unit_id);
        $stmt->bindParam(':mid', $machine_id);
        $stmt->bindParam(':myear', $myear);
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row==0)
        {
            $sql="insert into wind_mill.eb_expense values(:uid,:mid,:myear,:export,:import,:trans,:gen,:ten_gen,:power,:penalty,:meter,:om,:schedule,:operating,:total,:by,:session,:time)";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':uid',$unit_id);
            $stmt->bindParam(':mid',$machine_id);
            $stmt->bindParam(':myear',$myear);
            $stmt->bindParam(':export',$export);
            $stmt->bindParam(':import',$import);
            $stmt->bindParam(':trans',$trans);
            $stmt->bindParam(':gen',$generation);
            $stmt->bindParam(':ten_gen',$ten_gen);
            $stmt->bindParam(':power',$power);
            $stmt->bindParam(':penalty',$penalty);
            $stmt->bindParam(':meter',$reading);
            $stmt->bindParam(':om',$om);
            $stmt->bindParam(':schedule',$schedule);
            $stmt->bindParam(':operating',$operation);
            $stmt->bindParam(':total',$total_charge);
            $stmt->bindParam(':by',$update_by);
            $stmt->bindParam(':session',$update_session);
            $stmt->bindParam(':time',$update_time);
            if($stmt->execute()==TRUE)
            {
                $msg="The EB Information Submitted Successfully";
                $type="Success";
            }
        }
        else
        {
            $msg="The EB Information of the Machine was already found";
        }
      }
    }catch(Exception $e){$msg=$e->getMessage(); }
    database_close($conn);
    $arr = ["result" => $type, "Message" => $msg];
    echo json_encode($arr);
  }
  function List_Machines()
  {
    $unit_id=$_POST['euid'];
    $myear=$_POST['myear'];
    $conn = database_open();
    $sno=0;
    $sql="SELECT wind_mill.division_list.division_id,wind_mill.division_list.division_name,wind_mill.machine.machine_id,wind_mill.machine.machine_name,wind_mill.machine.htsc,wind_mill.machine.function_date,wind_mill.machine.capacity,wind_mill.machine.insurance_date,wind_mill.machine.insurance_amount,wind_mill.machine.om_date,wind_mill.machine.om_amount,wind_mill.machine.location,wind_mill.location.location_name,wind_mill.eb_expense.total_export,wind_mill.eb_expense.total_import,wind_mill.eb_expense.transmission,wind_mill.eb_expense.net_generation,wind_mill.eb_expense.10_generation,wind_mill.eb_expense.power_factor,wind_mill.eb_expense.RKVAH_penalty,wind_mill.eb_expense.meter_charge,wind_mill.eb_expense.om_charge,wind_mill.eb_expense.schedule_charge,wind_mill.eb_expense.operating_charge,wind_mill.eb_expense.total_charges FROM wind_mill.machine JOIN wind_mill.location ON wind_mill.machine.location=wind_mill.location.location_id left join wind_mill.division_list on wind_mill.machine.division_id=wind_mill.division_list.division_id join wind_mill.eb_expense on wind_mill.eb_expense.machine_id=wind_mill.machine.machine_id and wind_mill.eb_expense.month_year=:myear where wind_mill.machine.status='yes' and wind_mill.machine.unit=:uid";
    $stmt1 = $conn->prepare($sql); 
    $stmt1->bindParam(':uid',$unit_id);
    $stmt1->bindParam(':myear',$myear);
    $stmt1->execute();
    //$stmt1->debugDumpParams();
    $json = array();
    while($row = $stmt1->fetch(PDO::FETCH_BOTH))
    {
        $json[$sno] = array(
            'machine_id' => $row['machine_id'],
            'machine_name' => $row['machine_name'],
            'total_export' => $row['total_export'],
            'total_import' => $row['total_import'],
            'transmission' => $row['transmission'],
            'net_generation' => $row['net_generation'],
            'ten_generation' => $row['10_generation'],
            'power_factor' => $row['power_factor'],
            'RKVAH_penalty' => $row['RKVAH_penalty'],
            'meter_charge' => $row['meter_charge'],
            'operating_charge' => $row['operating_charge'],
            'schedule_charge' => $row['schedule_charge'],
            'om_charge' => $row['om_charge'],
            'total_charges' => $row['total_charges']);
            $sno++;
    }
    database_close($conn);
    echo json_encode($json);
   
  }
  function Summary_Expense()
  {
    $myear=$_POST['start'];
    $date=$myear.'-01';
    $month_year = date('M, Y', strtotime($date .' -0 day'));

    $myear1=$_POST['end'];
    $date1=$myear1.'-01';
    $month_year1 = date('M, Y', strtotime($date1 .' -0 day'));
   
    session_start();
    $_SESSION["download_docname"]="";
    $_SESSION["download_path"]=encrypt_decrypt("encrypt","11");

    $conn = database_open();
    $sql="SELECT distinct wind_mill.unit.unit_id,wind_mill.unit.unit_name FROM wind_mill.eb_expense JOIN wind_mill.unit ON wind_mill.unit.unit_id=wind_mill.eb_expense.unit_id where wind_mill.eb_expense.month_year>=:syear and wind_mill.eb_expense.month_year<=:eyear";
    $stmt = $conn->prepare($sql); 
    $stmt->bindParam(':syear',$myear);
    $stmt->bindParam(':eyear',$myear1);
    $stmt->execute();
   // $stmt->debugDumpParams();
    $rowc =$stmt->rowCount();
    if($rowc>0)
    {
        $company_id="xxxx";$title="";
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(10, 14, 10, true);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->AddPage('P', 'A4');
        $lastPage = $pdf->getPage();
        $pdf->deletePage($lastPage);

        $header_data=document_header($company_id,$title);
        $pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$header_data, $tc=array(0,0,0), $lc=array(0,0,0));
        $pdf->SetCreator(PDF_CREATOR);

       
        $pdf->SetAutoPageBreak(TRUE, $margin=1);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 8, '', true);
      
       // $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
        $w1=5;$w2=13;$w3=8;$w4=8;$w5=12;$w6=8;$w7=7;$w8=7;$w9=7;$w10=8;$w11=9;$w12=8;
        $row_height=25;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $sql="SELECT distinct wind_mill.machine.machine_id,wind_mill.machine.machine_name FROM wind_mill.eb_expense JOIN wind_mill.machine ON wind_mill.machine.machine_id=wind_mill.eb_expense.machine_id where wind_mill.eb_expense.unit_id=:unit_id and wind_mill.eb_expense.month_year>=:syear and wind_mill.eb_expense.month_year<=:eyear";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':syear',$myear);
            $stmt1->bindParam(':eyear',$myear1);
            $stmt1->bindParam(':unit_id',$row['unit_id']);
            $stmt1->execute();
            $cnt =$stmt1->rowCount();
            if($cnt>0)
            {
                $pdf->AddPage();
                $col1=$w1+$w2+$w3+$w4;
                $col2=$w5+$w6+$w7+$w8;
                $col3=$w11+$w12+$w9+$w10;
                $rows="";
                $texport=0;$timport=0;$ttrans=0;$tgen=0;$tpenalty=0;$tmeter=0;$tom=0;$tschedule=0;$toper=0;$ttotal=0;
                $sno=1;
                while($data1 = $stmt1->fetch(PDO::FETCH_BOTH))
                {
                    $sql="SELECT sum(total_export) as total_export,sum(total_import) as total_import,sum(transmission) as transmission,sum(net_generation) as net_generation,sum(10_generation) as 10_generation,sum(power_factor) as power_factor,sum(RKVAH_penalty) as RKVAH_penalty,
                    sum(meter_charge) as meter_charge,sum(om_charge) as om_charge,sum(schedule_charge) as schedule_charge,sum(operating_charge) as operating_charge,sum(total_charges) as total_charges FROM wind_mill.eb_expense where wind_mill.eb_expense.machine_id=:mid and wind_mill.eb_expense.month_year>=:syear and wind_mill.eb_expense.month_year<=:eyear";
                    $stmt2 = $conn->prepare($sql); 
                    $stmt2->bindParam(':syear',$myear);
                    $stmt2->bindParam(':eyear',$myear1);
                    $stmt2->bindParam(':mid',$data1['machine_id']);
                    $stmt2->execute();
                    $data = $stmt2 -> fetch();

                    $others=$data['banking_charge']+$data['negative_charge']+$data['other_charge'];
                    $rows.=
                    '<tr>
                        <td colspan="1" align="center" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'">'.$sno.'</td>
                        <td colspan="1" align="left" vertical-align="middle" width="'.$w2.'%" height="'.$row_height.'">'.$data1['machine_name'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w3.'%">'.$data['total_export'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w4.'%">'.$data['total_import'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w5.'%">'.$data['transmission'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w6.'%">'.$data['net_generation'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w7.'%">'.$data['RKVAH_penalty'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w8.'%">'.$data['meter_charge'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w9.'%">'.$data['om_charge'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w10.'%">'.$data['schedule_charge'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w11.'%">'.$data['operating_charge'].'</td>
                        <td colspan="1" align="right" vertical-align="middle" width="'.$w12.'%">'.$data['total_charges'].'</td>
                    </tr>';
                    $sno++;
                    
                    $texport+=$data['total_export'];$timport+=$data['total_import'];$ttrans+=$data['transmission'];
                    $tgen+=$data['net_generation'];$tpenalty+=$data['RKVAH_penalty'];$tmeter+=$data['meter_charge'];
                    $tom+=$data['om_charge'];$tschedule+=$data['schedule_charge'];$toper+=$data['operating_charge'];$ttotal+=$data['total_charges'];
                }
                $line_height=15;

                $tbl='<table cellpadding="1" border="1" width="100%">
                        <thead>
                            <tr>
                                <th colspan="12" style="border-bottom: 1px solid black;" align="center" vertical-align="middle" width="100%" height="'.$row_height.'"><h3>Consolidated EB Expense Information</h3></th>
                            </tr>
                            <tr>
                                <th colspan="4" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col1.'%" height="'.$row_height.'"><b>Unit Name : </b>'.$row['unit_name'].'</th>
                                <th colspan="4" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col2.'%"><b>Month & Year : </b>'.$month_year.' to '.$month_year1.'</th>
                                <th colspan="4" style="border-bottom: 1px solid black;" align="left" vertical-align="middle" width="'.$col3.'%"><b>Printed On : </b>'.get_datetime().'</th>
                            </tr>
                            <tr>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w1.'%" height="'.$row_height.'"><b>S.No.</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w2.'%"><b>Machine Name</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w3.'%"><b>Total Export</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w4.'%"><b>Total Import</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w5.'%"><b>Transmission</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w6.'%"><b>Net Generation</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w7.'%"><b>RKVAH</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w8.'%"><b>Meter Charge</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w9.'%"><b>OM Charge</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w10.'%"><b>Schedule Charge</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w11.'%"><b>Operating Charge</b></th>
                                <th colspan="1" align="center" vertical-align="middle" width="'.$w12.'%"><b>Total Charges</b></th>
                            </tr>
                        </thead>
                        <tbody>'.$rows.'</tbody>
                        <tfooter>
                            <tr>
                                <td colspan="2" align="center" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.($w1+$w2).'%" height="'.$row_height.'"><b>Total</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w3.'%"><b>'.$texport.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w4.'%"><b>'.$timport.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w5.'%"><b>'.$ttrans.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w6.'%"><b>'.$tgen.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w7.'%"><b>'.$tpenalty.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w8.'%"><b>'.$tmeter.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w9.'%"><b>'.$tom.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w10.'%"><b>'.$tschedule.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w11.'%"><b>'.$toper.'</b></td>
                                <td colspan="1" align="right" style="line-height:'.$line_height.'px;" vertical-align="middle" width="'.$w12.'%"><b>'.$ttotal.'</b></td>
                            </tr>
                        </tfooter>
                        </table>';
                $pdf->writeHTML($tbl, true, 0, true, 0);
            }
        }
        $doc_location=document_namecreate("ininvoice.pdf");
        $pdf->Output($doc_location, 'F');
        $_SESSION["download_docname"]="Consolidated EB Expense.pdf";
        $_SESSION["download_path"]=encrypt_decrypt("encrypt",$doc_location); 
    }else{echo "No Data";}
    database_close($conn);
  }