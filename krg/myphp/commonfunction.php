<?php 
function database_open() 
{
            //$servername = "localhost";
            $servername = "210.212.249.135";
            $username = "app_user";
            $password = "mkceinKRG@12";
            $conn=null;
            try 
            {
               $conn = new PDO("mysql:host=$servername", $username, $password);
               $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
               return $conn;
             }
             catch(PDOException $e)
             {
               return "Failed" . $e->getMessage();
             }
}
function database_close(&$conn)
{
   $conn=null;
}
function SixDigitIdGenerator(int $row_count)
{
    $id_gen="";
    if($row_count<=9){$id_gen="00000$row_count";}
    else if($row_count<=99){$id_gen="0000$row_count";}
    else if($row_count<=999){$id_gen="000$row_count";}
    else if($row_count<=9999){$id_gen="00$row_count";}
    else if($row_count<=99999){$id_gen="0$row_count";}
    else{$id_gen="$row_count";}
    return $id_gen;
}
function path_location()
{
    //return @"\\\\172.2.2.50\\krg_documents\\";
    //return @"D:\\krg_documents\\";
    return @"\\\\10.0.13.150\\krg_documents\\";
    //return @"\\\\210.212.249.135\\krg_documents\\";
} 
function report_location()
{
    //return @"\\\\172.2.2.50\\krg_documents\\";
    return @"D:\\krg_documents\\";
    //return @"\\\\10.0.13.150\\krg_documents\\";
    //return @"\\\\210.212.249.135\\krg_documents\\";
} 
function get_datetime()
        {
            $pid="";
            $conn = database_open();
            $sth = $conn->prepare("select DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s %p') AS DATETIME;");
            $sth->execute();
            $result = $sth->fetchColumn(0);
            $pid="".$result;
            database_close($conn);
            return $pid;
        }
        function get_date()
        {
            $pid="";
            $conn = database_open();
            $sth = $conn->prepare("select DATE_FORMAT(NOW(),'%Y-%m-%d') AS DATETIME;");
            $sth->execute();
            $result = $sth->fetchColumn(0);
            $pid="".$result;
            database_close($conn);
            return $pid;
        }
        function get_day()
        {
            $pid="";
            $conn = database_open();
            $sth = $conn->prepare("select DAYNAME(DATE_FORMAT(NOW(),'%Y-%m-%d')) AS DAYNAME;");
            $sth->execute();
            $result = $sth->fetchColumn(0);
            $pid="".$result;
            database_close($conn);
            return $pid;
        }
        function encrypt_decrypt($action, $string) {
            $output = false;
            $encrypt_method = "AES-256-CBC";
            $secret_key = 'RSKP1702*';
            $secret_iv = 'This is my secret iv';
            $key = hash('sha256', $secret_key);
            $iv = substr(hash('sha256', $secret_iv), 0, 16);
            if ( $action == 'encrypt' ) {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            } else if( $action == 'decrypt' ) {
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }
            return $output;
        }
        function random_no($num_size)
        {
            $random_no="";
            while($num_size>=0)
            {
                $random_no=$random_no.mt_rand(0,9);
                $num_size=$num_size-1;
            }
            return $random_no;
        }
        function isMobile() {
            return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
        }
        function get_client_ip() {
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
            return $ipaddress;
        }
        function get_mac()
        {
            ob_start();
            system('ipconfig /all'); 
            $mycom=ob_get_contents();
            ob_clean(); 
            $findme = "Physical";
            $pos = strpos($mycom, $findme);
            $macp=substr($mycom,($pos+36),17);
            return $macp;
        }
        function get_sysmtem_info()
        {
            ob_start();
            system('systeminfo'); 
            $mycom=ob_get_contents();
            ob_clean(); 
            $findme = "System Model";
            $pos = strpos($mycom, $findme);
            $macp=substr($mycom,($pos+36),17);
            return $pos;
        }
        function get_logged_user()
        {
            ob_start();
            system('whoami'); 
            $mycom=ob_get_contents();
            ob_clean(); 
           
            return $mycom;
        }
        class OS_BR{

            private $agent = "";
            private $info = array();
            function __construct(){
                $this->agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
                $this->getBrowser();
                $this->getOS();
            }
            function getBrowser(){
                $browser = array("Navigator"            => "/Navigator(.*)/i",
                                 "Firefox"              => "/Firefox(.*)/i",
                                 "Internet Explorer"    => "/MSIE(.*)/i",
                                 "Google Chrome"        => "/chrome(.*)/i",
                                 "MAXTHON"              => "/MAXTHON(.*)/i",
                                 "Opera"                => "/Opera(.*)/i",
                                 );
                foreach($browser as $key => $value){
                    if(preg_match($value, $this->agent)){
                        $this->info = array_merge($this->info,array("Browser" => $key));
                        $this->info = array_merge($this->info,array(
                          "Version" => $this->getVersion($key, $value, $this->agent)));
                        break;
                    }else{
                        $this->info = array_merge($this->info,array("Browser" => "UnKnown"));
                        $this->info = array_merge($this->info,array("Version" => "UnKnown"));
                    }
                }
                return $this->info['Browser'];
            }
            function getOS(){
                $OS = array("Windows"   =>   "/Windows/i",
                            "Linux"     =>   "/Linux/i",
                            "Unix"      =>   "/Unix/i",
                            "Mac"       =>   "/Mac/i"
                            );
        
                foreach($OS as $key => $value){
                    if(preg_match($value, $this->agent)){
                        $this->info = array_merge($this->info,array("Operating System" => $key));
                        break;
                    }
                }
                return $this->info['Operating System'];
            }
            function getVersion($browser, $search, $string){
                $browser = $this->info['Browser'];
                $version = "";
                $browser = strtolower($browser);
                preg_match_all($search,$string,$match);
                switch($browser){
                    case "firefox": $version = str_replace("/","",$match[1][0]);
                    break;
        
                    case "internet explorer": $version = substr($match[1][0],0,4);
                    break;
        
                    case "opera": $version = str_replace("/","",substr($match[1][0],0,5));
                    break;
        
                    case "navigator": $version = substr($match[1][0],1,7);
                    break;
        
                    case "maxthon": $version = str_replace(")","",$match[1][0]);
                    break;
        
                    case "google chrome": $version = substr($match[1][0],1,10);
                }
                return $version;
            }
            function showInfo($switch){
                $switch = strtolower($switch);
                switch($switch){
                    case "browser": return $this->info['Browser'];
                    break;
        
                    case "os": return $this->info['Operating System'];
                    break;
        
                    case "version": return $this->info['Version'];
                    break;
        
                    case "all" : return array($this->info["Version"], 
                      $this->info['Operating System'], $this->info['Browser']);
                    break;
        
                    default: return "Unkonw";
                    break;
        
                }
            }
        }
        function sendmail($subject,$message,$toid)
        {
            $image_loc=path_location()."\\general\\institution\\";

            $sendermail="camshelp@mkce.ac.in";$sender_name="KRG Portal";
            $senderpwd="Success@123";
            $msg="";
            require_once($_SERVER['DOCUMENT_ROOT'] ."/PCTEM/PHPMail/PHPMailer.php");
            require_once($_SERVER['DOCUMENT_ROOT'] ."/PCTEM/PHPMail/SMTP.php");
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Username =$sendermail;
            $mail->From=$sendermail;
            $mail->FromName=$sender_name;
            $mail->Password = $senderpwd;
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPAuth=true;
            $mail->addAddress($toid);
            $mail->Subject = $subject;
            $mail->msgHTML($message);
            //$mail->addStringAttachment(file_get_contents($image_loc."INST000001l.jpeg"),'image.jpeg');
            //$mail->addStringAttachment(file_get_contents($image_loc."sample.pdf"),'sample.pdf');
           // $mail->SMTPDebug = 2;
            if (!$mail->send()) 
            { 
                 $msg = "Mailer Error: " . $mail->ErrorInfo; 
            }
            else 
            {
                 $msg='<p id="para">Message sent!</p>';
            }
           return $msg;
        }
        function task_approval($task_id,$update_by,$update_session)
        {
            $type="Error";$msg="";
                try
                {
                    if($msg=="")
                    {
                        $update_time=get_datetime();
                        $conn = database_open();
                        $sql="select approval_status from general.task_list where task_id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id', $task_id);
                        $stmt->execute();
                        $row =$stmt->rowCount();
                        if ($row==1) 
                        {
                            $row = $stmt -> fetch();
                            if($row["approval_status"]!="None"){$msg="The Task is Already Approved";}
                            else
                            {
                                $approve_status="Approved";
                                
                        
                                if($update_by==""){$update_by=$_POST['approve_by'];}
                                if($update_session==""){$update_session=$_POST['session'];}
                    
                                $sql="update general.task_list set approval_status=:astatus,approved_by=:by,approved_session=:session,approved_time=:time where task_id=:id";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':astatus', $approve_status);
                                $stmt->bindParam(':by', $update_by);
                                $stmt->bindParam(':session', $update_session);
                                $stmt->bindParam(':time',$update_time);
                                $stmt->bindParam(':id', $task_id);
                                if ($stmt->execute() == TRUE) 
                                {
                                    $desc="Task Approved Successfully";$type="Task Approval";
                                    task_timeline_update($conn,"","Task",$task_id,$type,$desc,$user,$update_session,$update_time);
                                     $type="Success";$msg="The Information Submitted Successfully";
                                }
                                else {$msg=mysqli_error($conn);}
                            }
                        }else{$msg="Task Id Not Matching";}
                        database_close($conn);
                    }
                }catch(Exception $e){$msg=$e->getMessage();}
                $arr = ["result" => $type, "Message" => $msg];
               return json_encode($arr);
        }
        function task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time)
        {
            $sql = "select count(*) from general.task_timeline";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $row = $stmt->fetchColumn(0);

            $sql="insert into general.task_timeline values(:tid,:id,:parent_id,:task_type,:type,:description,:by,:session,:time)";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':tid', $row);
            $stmt->bindParam(':id', $task_id);
            $stmt->bindParam(':parent_id', $parent_id);
            $stmt->bindParam(':task_type', $task_type);
            $stmt->bindParam(':type', $type);
            $stmt->bindParam(':description', $desc);
            $stmt->bindParam(':by', $update_by);
            $stmt->bindParam(':session', $update_session);
            $stmt->bindParam(':time',$update_time);
            $stmt->execute();
            return $row;
        }
        function numberTowords($number)
        { 
            $no = round($number);
            $decimal = round($number - ($no = floor($number)), 2) * 100;    
            $digits_length = strlen($no);    
            $i = 0;
            $str = array();
            $words = array(
                0 => '',
                1 => 'One',
                2 => 'Two',
                3 => 'Three',
                4 => 'Four',
                5 => 'Five',
                6 => 'Six',
                7 => 'Seven',
                8 => 'Eight',
                9 => 'Nine',
                10 => 'Ten',
                11 => 'Eleven',
                12 => 'Twelve',
                13 => 'Thirteen',
                14 => 'Fourteen',
                15 => 'Fifteen',
                16 => 'Sixteen',
                17 => 'Seventeen',
                18 => 'Eighteen',
                19 => 'Nineteen',
                20 => 'Twenty',
                30 => 'Thirty',
                40 => 'Forty',
                50 => 'Fifty',
                60 => 'Sixty',
                70 => 'Seventy',
                80 => 'Eighty',
                90 => 'Ninety');
            $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
            while ($i < $digits_length) {
                $divider = ($i == 2) ? 10 : 100;
                $number = floor($no % $divider);
                $no = floor($no / $divider);
                $i += $divider == 10 ? 1 : 2;
                if ($number) {
                    $plural = (($counter = count($str)) && $number > 9) ? 's' : null;            
                    $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
                } else {
                    $str [] = null;
                }  
            }
            
            $Rupees = implode(' ', array_reverse($str));
            $paise = ($decimal) ? "And Paise " . ($words[$decimal - $decimal%10]) ." " .($words[$decimal%10])  : '';
            return ($Rupees ? $Rupees : '') . $paise;
            //return ($Rupees ? 'Rupees ' . $Rupees : '') . $paise . " Only";
    }
    function document_header($company_id,$title)
    {
        $conn = database_open();
        $sql="select * from general.institution_list where group_id=:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$company_id);
        $stmt->execute();
        $row =$stmt->rowCount();
        $company_name="";$line1="";$line2="";$line3="";$line4="";$report_header=""; $rbase64=""; $lbase64="";$error="";
        if ($row>0) 
        {
            $row = $stmt -> fetch();
            $company_name=$row["group_name"];
            $line1=$row["address_line1"];
            $line2=$row["address_line2"];
            $line3=$row["address_line3"];
            $line4=$row["address_line4"];

            $image_loc=report_location()."\\general\\institution\\";
            $rbase64 = $image_loc.$row['group_id']."r.". $row['rformat']; 
            $lbase64 = $image_loc.$row['group_id']."l.". $row['lformat']; 
            
        }
        database_close($conn);
        if($company_name!="")
        {
             return '<table width="100%"><tr><td width="20%" rowspan="3"><img src="'.$rbase64.'" width="65" heigth="35"></td><td width="60%" align="left"><h2>'.$company_name.'</h2></td><td width="20%" rowspan="3"><img src="'.$lbase64.'" width="60" heigth="60"></td></tr><tr><td>'.$line1.'</td></tr><tr><td>'.$line2.'</td></tr><tr><td></td><td align="center"><h3>'.$title.'</h3></td><td></td></tr></table>';
        }
        else
        {
            if($company_id=="D000002")
            {
                return '<table width="100%">
                  <tr><td width="70%"><h2><b>Dr.K.Ramakrishnan</b></h2></td><td width="30%">No.127-128 Main Road</td></tr>
                  <tr><td><b>Mobile No. : </b>+91-98924 16377</td><td>Thalavapalayam</td></tr>
                  <tr><td><b>Mail Id : </b><u>chairmanmkce@gmail.com</u></td><td>Karur-639 113.</td></tr>
                  <tr><td><b>Fax No. : </b>04324 - 272457</td><td>Tamil Nadu.</td></tr>
                </table>';
            }
            else
            {
                return '<table width="100%"><tr><td></td><td align="center"><h3>'.$title.'</h3></td><td></td></tr></table>';
            }
        }
    }
    function document_namecreate($report_name)
    {
        session_start();
        return report_location().'reports\\'.$_SESSION["session_id"].str_replace(":","",get_datetime()).$report_name;
    }
    function ReadFromExcelFunctionCommon($id,$sheet_name,$work,$path)
    {
        include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
        $fileNameCmps   = explode(".", $sheet_name);
        $fileExtension  = strtolower(end($fileNameCmps));
        $inputFileName =$path.$id.'.'.$fileExtension;
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
        $sheet=$objPHPExcel->getSheet($work);
        $data = array(1,$objPHPExcel->getSheet($work)->toArray(null,true,true,true));
        //$maxrow = $sheet->getHighestRow();
        //$highestColumn = $sheet->getHighestColumn();
        return $data;
    }
    function AYEARINKRCEPLBYDATE($date)
    {
        $splt=explode("-",$date);
        if($splt[1]<4)
        {
            return ($splt[0]-1)."-".$splt[0];
        }
        else
        {
            return $splt[0]."-".($splt[0]+1);
        }
    }
?>