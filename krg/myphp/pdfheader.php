<?php
 require_once($_SERVER['DOCUMENT_ROOT'] .'/TCPDF-master/examples/tcpdf_include.php');
class MYPDF extends TCPDF {

    public function Header() 
    {
        $headerData = $this->getHeaderData();
        $this->SetFont('helvetica', 'B', 10);
        $this->writeHTML($headerData['string']);
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    public function Footer() 
    {
        $this->SetY(-15);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
?>