<?php
require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
error_reporting(E_ALL);

$callparameter="";
if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
if($callparameter=="")
{
  $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
  echo json_encode($arr);
}
else
{
      switch($callparameter)
      {
            case "smspie":piechart_sms();
            break;
        case "smssend":sms_send_excel();
            break;
        case "list_excel":sms_excel_list();
            break;
        case "sheets":sheetnameInExcel();
            break;
        case "sheetsdata":ReadFromExcelFunction();
            break;
        case "sendsms":MessBillSMS();
            break;
        case "balsms":SMSSummary();
            break;
        case "historysms":SMSHistory();
            break;
        case "mstatusupdate":updatesms_history();
            break;
        case "smsstudents":students_list_sms();
            break;     
      }
  }
        function Group_NameToId($product_name)
        {
            $pid="";
            $conn = database_open();
            $sql = "SELECT group_id FROM general.institution_list WHERE group_name=:id";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $product_name);
            $stmt->execute();
            $result = $stmt->fetchColumn(0);
            $pid=$result;
            database_close($conn);
            return $pid;
        }
        function deptIdToName($dept_id)
        {
            $pid="";
            $conn = database_open();
            $sql = "SELECT course_name FROM general.department_list WHERE dept_id=:id";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $dept_id);
            $stmt->execute();
            $result = $stmt->fetchColumn(0);
            $pid=$result;
            database_close($conn);
            return $pid;
        }
       
       
        function piechart_sms()
    {
        $param=$_POST['param'];
        header("Content-Type: application/json; charset=UTF-8");
        $rows=array();
        $conn = database_open();
       
            $sql="select DISTINCT delivery_status,delivery_reason from general.sms_log where upload_id=:id";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $param);
            $stmt->execute();
            $row =$stmt->rowCount();
           if($row)
            {
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $name=$row['delivery_status']."(".$row['delivery_reason'].")";
                    $dept_name=$row['delivery_status'];  $reason=$row['delivery_reason'];
                    $sql="select count(*) from general.sms_log where delivery_status=:dept and delivery_reason=:reason";
                    $stmt1 = $conn->prepare($sql); 
                    $stmt1->bindParam(':dept', $dept_name);
                    $stmt1->bindParam(':reason',$reason);
                    $stmt1->execute();
                    $count=$stmt1->fetchColumn(0);
                    $rows += [ $name => $count ];
                    $sno++;
                }
            } 
            database_close($conn);
            //echo "1234";
           echo json_encode($rows);
        }
        function SMSHistory()
        {
            $param=$_POST['param'];
            header("Content-Type: application/json; charset=UTF-8");
            $json=array();
            $conn = database_open();
        
            $sql="select * from general.sms_history where upload_id=:id order by updated_time asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $param);
            $stmt->execute();
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $dif= $row['starting_sms']-$row['ending_sms'];
                $json[$sno] = array(
                    'sms_id' => $row['starting_sms_id']." to ".$row['ending_sms_id'],
                    'sms_bal' => $row['starting_sms']." to ".$row['ending_sms'],
                    'diff' => $dif,'success'=>"0",'fail'=>"0",'time'=>$row['updated_time'] );
                $sno++;
            }
            database_close($conn);
            echo json_encode($json);
        }
        function sms_send_excel()
        {
            session_start();
            $type="Error";$msg="";
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
            else
            {
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                try
                {
                    if(isset($_POST['type'])){  $type = $_POST['type']; }else{$msg="Category was not posted";}
                    if(isset($_POST['desc'])){ $desc = $_POST['desc']; }else{$msg="Description was not Posted was not posted";}
                    if($msg=="")
                    {
                        $fileTmpPath    = $_FILES['uploadedFile']['tmp_name'];
                        $fileName       = $_FILES['uploadedFile']['name'];
                        $fileSize       = $_FILES['uploadedFile']['size'];
                        $fileType       = $_FILES['uploadedFile']['type'];
                        $fileNameCmps   = explode(".", $fileName);
                        $fileExtension  = strtolower(end($fileNameCmps));
                        $stat="yes";$para="";

                        $sql="select group_id,dept_id from general.staff_basic_info where staff_id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id',$update_by);
                        $stmt->execute();
                        $result = $stmt -> fetch();
                        $group_id = $result["group_id"];
                        $dept_id = $result["dept_id"];
 
                        $sql = "select count(*) from general.upload_history";
                        $stmt = $conn->prepare($sql); 
                        $stmt->execute();
                        $row = $stmt->fetchColumn(0);
                        $add_value="1";
                        $total=(integer)$row+(integer)$add_value;
                        $upd_id="UPLD".SixDigitIdGenerator("$total");

                        $sql="insert into general.upload_history values(:gid,:did,:id,:para,:cate,:desc,:name,:stat,:by,:session,:time)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':gid',$group_id);
                        $stmt->bindParam(':did',$dept_id);
                        $stmt->bindParam(':id',$upd_id);
                        $stmt->bindParam(':para',$para);
                        $stmt->bindParam(':cate',$type);
                        $stmt->bindParam(':desc',$desc);
                        $stmt->bindParam(':name',$fileName);
                        $stmt->bindParam(':stat',$stat);
                        $stmt->bindParam(':by', $update_by);
                        $stmt->bindParam(':session', $update_session);
                        $stmt->bindParam(':time', $update_time);
                        if ($stmt->execute() == TRUE) 
                        {
                            $type="Success";$msg="The Information Submitted Successfully";
                            move_uploaded_file($fileTmpPath, path_location()."general\\upload\\".$upd_id.".".$fileExtension);
                        }
                        else {$msg=mysqli_error($conn);}
                    }
                }catch(PDOException $e){$msg=$e->getMessage();}
                database_close($conn);
            }
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);  
        }
        function sms_excel_list()
        {
            session_start();
            $acc_user=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT upload_id,category,description,sheet_name,updated_time from general.upload_history where active_status='yes'";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $json[$sno] = array(
                        'id' => $row['upload_id'],
                        'type' => $row['category'],
                        'description' => $row['description'],
                        'sheet_name' => $row['sheet_name'] ,
                        'updated_time' => $row['updated_time'],
                        'no_student' => "0",
                        'sent_student' => "0",
                        'no_sent_student' => "0");
                        $sno++;
                }
            database_close($conn);
           echo json_encode($json);
        }
        function sheetnameInExcel()
        {
            include_once($_SERVER['DOCUMENT_ROOT'] .'/PCTEM/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
            $param=$_POST['param'];
            $sheet_name=$_POST['sname'];
            $json = array();
             try
            {
            $fileNameCmps   = explode(".", $sheet_name);
            $fileExtension  = strtolower(end($fileNameCmps));
            $inputFileName = path_location()."general\\upload\\".$param.'.'.$fileExtension;
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            $sheetcount = $objPHPExcel->getSheetCount();
            $worksheet = $objPHPExcel->getSheetNames();
            for($s=0;$s<$sheetcount;$s++) 
            {
                $json[$s]=array("name"=>$worksheet[$s],"value"=>$s);
            } 
            echo json_encode($json);
           }catch(Exception $e){ $msg=$e->getMessage();echo $msg."123";} 
        
           
            
        }
        function updatesms_history()
        {
           /*  $conn = database_open();
            $sql="SELECT message_id,receiver_info,mobile_no from general.sms_log where delivery_status='sent' AND delivery_reason=''";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $sno=1;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
               $rinfo=$row["receiver_info"];$rmob=$row["mobile_no"];
                $sql="SELECT Status,Delivery_Status,unit,Message_ID,Delivery_Time from test.sheet1 where Mobile_No like CONCAT('%', :mobile, '%') and Message like CONCAT('%', :msg, '%')";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':mobile',$rmob );
                $stmt1->bindParam(':msg',$rinfo); 
                $stmt1->execute(); 
                $row1 =$stmt1->rowCount();
                echo $sno." = ".$row1." Msg Id ".$row["message_id"]."<br>";
                if($row1>0)
                {
                      $result = $stmt1 -> fetch();

                      $deli_id=$result["Message_ID"]; $deli_time=$result["Delivery_Time"];$unit=$result["unit"];
                      $reason="Unknown Status";
                      if($result["Delivery_Status"]=="DELIVRD"){$reason="success";}

                      $sql="update general.sms_log set delivery_status=:dstatus,delivery_reason=:reason,delivery_id=:delid,delivery_time=:deltime,msg_unit=:unit where message_id=:mid";
                      $stmt2 = $conn->prepare($sql); 
                      $stmt2->bindParam(':mid',$row["message_id"]);
                      $stmt2->bindParam(':dstatus',$result["Status"]);
                      $stmt2->bindParam(':reason',$reason);
                      $stmt2->bindParam(':delid',$deli_id);
                      $stmt2->bindParam(':deltime',$deli_time);
                      $stmt2->bindParam(':unit',$unit);
                      $stmt2->execute();
                }
                $sno++;
            }
                  try
                    {
                     $sql="SELECT * from general.sms_history";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $start_sms=0;$end_sms=0;
                    while($row = $stmt->fetch(PDO::FETCH_BOTH))
                    {
                        $start=$row["starting_sms_id"];$end=$row["ending_sms_id"];
                        $sql="SELECT sum(msg_unit) from general.sms_log where message_id>=:start and message_id<=:end";
                        $stmt1 = $conn->prepare($sql); 
                        $stmt1->bindParam(':start',$start);
                        $stmt1->bindParam(':end',$end); 
                        $stmt1->execute(); 
                        $start_sms += $stmt1->fetchColumn(0);

                        $sql="update general.sms_history set starting_sms=:start_sms,ending_sms=:end_sms where starting_sms_id=:start and ending_sms_id=:end";
                        $stmt2 = $conn->prepare($sql); 
                        $stmt2->bindParam(':start',$start);
                        $stmt2->bindParam(':end',$end); 
                        $stmt2->bindParam(':start_sms',$start_sms);
                        $stmt2->bindParam(':end_sms',$end_sms); 
                        $stmt2->execute();  
                        $end_sms=$start_sms;
                    } 
                }catch(PDOException $e) { $msg=$e->getMessage(); echo $msg;} 
            database_close($conn);*/
        }
        function ReadFromExcelFunction()
        {
            $id=$_POST['id'];
            $sheet_name=$_POST['sname'];
            $path=path_location()."general\\upload\\";
            $work=$_POST['work'];
            $data=ReadFromExcelFunctionCommon($id,$sheet_name,$work,$path);
            $exceldata=$data[1];
            $row=count($exceldata);
            $json = array();
            $conn = database_open();
            header("Content-Type: application/json; charset=UTF-8");
            $sno=0;
             for($i=2;$i<=$row;$i++)
             {
                $rowdata=$exceldata[$i];
                 $name=$rowdata["B"]."-".$rowdata["C"];
                 $status="--";$fstatus="";
                 $sql="select delivery_status,delivery_reason from general.sms_log where receiver_info=:recei and upload_id=:id order by updated_time desc limit 1";
                 $stmt1 = $conn->prepare($sql); 
                 $stmt1->bindParam(':recei', $name);
                 $stmt1->bindParam(':id', $id);
                 $stmt1->execute();
                 $result = $stmt1 -> fetch();
                 if($result["delivery_status"]!="")
                 {
                    $fstatus=$result["delivery_status"]."(".$result["delivery_reason"].")";
                 }
               
                if($fstatus!=""){$status=$fstatus;}
                $json[$sno] = array(
                    'name' => $name,
                    'current' => $rowdata["D"],
                    'misc' => $rowdata["E"],
                    'misc_desc' => $rowdata["H"],
                    'pending' => $rowdata["F"],
                    'total' => $rowdata["G"],
                    'mobile' => $rowdata["I"],
                    'status' =>$status
                );
                $sno++;
             } 
             database_close($conn);
            echo json_encode($json);
        }
        function MessBillSMS()
        {
            $type="Error";$msg="";
            if(isset($_POST['data'])){  $someJSON = $_POST['data']; }else{$msg="Student Data was not posted";}
            if(isset($_POST['id'])){  $id = $_POST['id']; }else{$msg="Upload Id was not posted";}
            if(isset($_POST['category'])){  $category = $_POST['category']; }else{$msg="Category was not posted";}
            if(isset($_POST['desc'])){  $desc = $_POST['desc']; }else{$msg="Description was not posted";}
            if(isset($_POST['file'])){  $file = $_POST['file']; }else{$msg="File Name was not posted";}
            if(isset($_POST['sheet'])){  $sheet = $_POST['sheet']; }else{$msg="Sheet Name was not posted";}
            if($msg==0)
            {
                session_start();
                $start_sms=SMSbalance();
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $conn = database_open();
                try
                {
                    $sql="select group_id,dept_id from general.staff_basic_info where staff_id=:id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':id',$update_by);
                    $stmt->execute();
                    $result = $stmt -> fetch();
                    $group_id = $result["group_id"];
                    $dept_id = $result["dept_id"];

                    $start_id="";$start_time=get_datetime();
                    $start_sms=SMSbalance();
                    $end_sms="";$end_id="";$end_time="";

                    $someArray = json_decode($someJSON, true);
                    $item_count=count($someArray);
                    $conn->beginTransaction();
                    //$item_count=2000;
                    for ($x = 0; $x<$item_count; $x++) 
                    {
                        $receive_name=$someArray[$x]["name"];
                        $current=$someArray[$x]["current"];if($current==""){$current="0";}
                        $misc=$someArray[$x]["Misc"];if($misc==""){$misc="0";}
                        $misc_desc="";
                        if($someArray[$x]["MiscDesc"]!="")
                        {
                            $misc_desc="(".rtrim($someArray[$x]["MiscDesc"],",").")";
                        }
                        $pending=$someArray[$x]["Pending"];if($pending==""){$pending="0";}
                        $total=$someArray[$x]["Total"];if($total==""){$total="0";}
                        $mobile=$someArray[$x]["Mobile"];

                        $message = "Dear Parent,".$someArray[$x]["name"]."-Mess Bill-$desc-Rs.". $current .", Misc Fee-Rs.".$misc."$misc_desc,Pending-Rs.".$pending.",Total-Rs.".$total;
                        if(strtoupper($desc)=="JAN 2019")
                        {
                            $message.=".College,Hostel and Mess Fee dues to be paid on or before 11thFeb";
                        }
                        $value= SMSAPI($conn,$group_id,$dept_id,$id,$category,$desc,$file,$sheet,$receive_name,$message,$mobile,$update_by,$update_session);
                        if($value=="EX"){$msg=$value;$x=$item_count;}
                        if($x==0){$start_id=$value;}
                        $end_id=$value;
                    }
                        $end_time=get_datetime();
                        $end_sms=SMSbalance();
                
                        $sql="insert into general.sms_history values(:gid,:did,:uid,:cate,:desc,:file,:sheet,:sbal,:sid,:stime,:ebal,:eid,:etime,:by,:session,:time)";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':gid',$group_id);
                        $stmt->bindParam(':did',$dept_id);
                        $stmt->bindParam(':uid',$id);
                        $stmt->bindParam(':cate',$category);
                        $stmt->bindParam(':desc',$desc);
                        $stmt->bindParam(':file',$file);
                        $stmt->bindParam(':sheet',$sheet);
                        $stmt->bindParam(':sbal',$start_sms);
                        $stmt->bindParam(':sid', $start_id);
                        $stmt->bindParam(':stime',$start_time);
                        $stmt->bindParam(':ebal',$end_sms);
                        $stmt->bindParam(':eid', $end_id);
                        $stmt->bindParam(':etime',$end_time);
                        $stmt->bindParam(':by', $update_by);
                        $stmt->bindParam(':session', $update_session);
                        $stmt->bindParam(':time', $end_time);
                        if ($stmt->execute() == TRUE) {} 
                        
                        $conn->commit();
                        $msg="The SMS Has been Sent";
                        $type="Success";

                }catch(Exception $e) { $msg=$e->getMessage();}
               database_close($conn);
              
            }
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);  
        }
        function SMSAPI($conn,$group_id,$dept_id,$id,$category,$desc,$file,$sheet,$receiver,$msg,$mobile,$update_by,$update_session)
        {
            //$mobile="9047213866aa";
               $xml_data ='<?xml version="1.0"?><parent><child>
                    <user>mkcekr</user>
                    <key>32bc9f8f8aXX</key>
                    <mobile>'.$mobile.'</mobile>
                    <message>'.$msg.'</message>
                    <accusage>1</accusage>
                    <senderid>MKCEKR</senderid>
                    </child></parent>';
                    $msg_id="";
                       try
                        {
                            $output="";
                                $URL = "vanakkamtamilan.com/submitsms.jsp?"; 
                                $ch = curl_init($URL);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $output = str_replace("\n","",curl_exec($ch));
                                curl_close($ch);  
        
                                $count= strlen($msg);
                                $max_count=160; $com=$count/$max_count;
                                $rem=$count%$max_count;if($rem>0){$rem=1;}     
                                $unit=floor($com)+$rem;
        
                               
                                $deli_status="fail";$deli_reason="";$deli_id="";$deli_time="";$deli_mob="";
                                $response= explode(",",$output);
                                if(isset($response[0])){$deli_status=$response[0];}
                                if(isset($response[1])){$deli_reason=$response[1];}
                                if(isset($response[2])){$deli_id=$response[2];}
                                if(isset($response[3])){$deli_time=$response[3];}
                                if(isset($response[4])){$deli_mob=$response[4];} 

                                $unit=0;
                                $update_time=get_datetime();
                                $sql = "select count(*) from general.sms_log";
                                $stmt = $conn->prepare($sql); 
                                $stmt->execute();
                                $row = $stmt->fetchColumn(0);
                                $add_value="1";
                                $total=(integer)$row+(integer)$add_value;
                         
                                                                
                                if($deli_status==""|| $deli_reason==null){ $deli_status="fail";}
                                $sql="insert into general.sms_log values(:gid,:dept_id,:id,:rece,:msg,:unit,:mob,:uid,:cate,:desc,:file,:sheet,:dstatus,:dreason,:did,:dtime,:dmobile,:by,:session,:time)";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':gid',$group_id);
                                $stmt->bindParam(':dept_id',$dept_id);
                                $stmt->bindParam(':id',$total);
                                $stmt->bindParam(':rece',$receiver);
                                $stmt->bindParam(':msg',$msg);
                                $stmt->bindParam(':unit',$unit);
                                $stmt->bindParam(':mob',$mobile);
                                $stmt->bindParam(':uid',$id);
                                $stmt->bindParam(':cate',$category);
                                $stmt->bindParam(':desc',$desc);
                                $stmt->bindParam(':file',$file);
                                $stmt->bindParam(':sheet',$sheet);
                                $stmt->bindParam(':dstatus',$deli_status);
                                $stmt->bindParam(':dreason',$deli_reason);
                                $stmt->bindParam(':did',$deli_id);
                                $stmt->bindParam(':dtime',$deli_time);
                                $stmt->bindParam(':dmobile',$deli_mob);
                                $stmt->bindParam(':by', $update_by);
                                $stmt->bindParam(':session', $update_session);
                                $stmt->bindParam(':time', $update_time);
                                if ($stmt->execute() == TRUE) 
                                {
                                    $msg_id= $total;
                                }  
                        }
                        catch(Exception $e)
                        {
                            //$msg_id=$e->getMessage();
                            $msg_id="EX";
                        }
                        return $msg_id;
        }
        function SMSSummary()
        {
            $sent=0;$failed=0;
            $id= $_POST['id'];
            $conn = database_open();
                if($id=="all")
                {
                    $sql="select count(*) from general.sms_log where delivery_status='sent'";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $sent = $stmt->fetchColumn(0);
                        
                    $sql="select count(*) from general.sms_log where delivery_status<>'sent'";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $failed = $stmt->fetchColumn(0);
                        
                }
                else
                {
                    $sql="select count(*) from general.sms_log where delivery_status='sent' and upload_id=:id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':id',$id);
                    $stmt->execute();
                    $sent = $stmt->fetchColumn(0);

                    $sql="select count(*) from general.sms_log where delivery_status<>'sent' and upload_id=:id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':id',$id);
                    $stmt->execute();
                    $failed = $stmt->fetchColumn(0);
                }
                database_close($conn);

            $arr = ["sms_sent"=>$sent,"sms_failed"=>$failed, "sms_balance" => SMSbalance()];
            echo json_encode($arr);  
        }
        function SMSbalance()
        {
            try
            {
                    $URL = "vanakkamtamilan.com/getbalance.jsp?user=mkcekr&key=32bc9f8f8aXX&accusage=1"; 
                    $ch = curl_init($URL);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
                    //curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);
                    curl_close($ch); 
                    $retmsg=str_replace("\n","", $output);
                    $retmsg=str_replace("\r","", $retmsg);
                    $retmsg=str_replace(" ","", $retmsg);
                    return $retmsg;
            }catch(Exception $e){return "0";}
        }
        function students_list_sms()
        {
            $id=$_POST['id'];
            $type = explode('(',$_POST['type']);
            $reason="";
            if(isset($type[1])){$reason=str_replace(")","",$type[1]);}
            $json = array();
            $conn = database_open();
            header("Content-Type: application/json; charset=UTF-8");
            $sql="select receiver_info,mobile_no,delivery_id,delivery_time,delivery_status,delivery_reason from general.sms_log where upload_id=:id and delivery_status=:status and delivery_reason=:reason";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':status', $type[0]);
            $stmt->bindParam(':reason', $reason);
            $stmt->execute();
            $sno=0;
            while($rowdata = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $json[$sno] = array(
                    'name' => $rowdata["receiver_info"],
                    'mobile' => $rowdata["mobile_no"],
                    'smsid' => $rowdata["delivery_id"],
                    'reason' => $rowdata["delivery_status"]."(".$rowdata["delivery_reason"].")",
                    'dtime' => $rowdata["delivery_time"]);
              $sno++;
            }
            database_close($conn);
            echo json_encode($json);  
        }
        
?>