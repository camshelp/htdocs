<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "login_check":login();
                    break;
            case "login_data":index_info();
                    break;
            case "logoff":sign_out();
                    break;
            case "unapprove_task":unapproved_task_list();
                    break;
            case "live_task":live_task_list();
                    break;
            case "page_check":page_previllage_check();
                    break;
            case "password_change":update_pwd();
                    break;
            case "password_forget":forget_pwd();
                    break;
            case "clist_check":check_clist();
                    break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function check_clist()
    {
        $check=0;
        $mail=$_POST['mail'];
        $group_ids=array("INST000004","INST000007");
        $result1="";$type="No";
        $conn = database_open();
        $sql="select group_id from general.staff_basic_info where official_mail=:mail";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':mail',$mail);
        $stmt->execute();
        $row =$stmt->rowCount();
        if($row>0)
        {
            $result = $stmt -> fetch();
            $group_id=$result['group_id'];
            $count=count($group_ids);
            for($x=0;$x<$count;$x++)
            {
                if($group_ids[$x]==$group_id){$check=1;$x=$count;}
            }
            if($check==1)
            {
                $sql="select group_id,group_name from general.institution_list where (group_id='INST000004' or group_id='INST000007')";
                $stmt = $conn->prepare($sql); 
                $stmt->execute();
                $result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $type="Yes";
            }else{$type="No";} 
        }
        else{$type="No";}
        database_close($conn);

        $arr = ["type" =>$type,"result"=>$result1];   
        echo json_encode($arr);
        
    }
    function page_previllage_check()
    {
         $urlvalue="";
        if(isset($_POST['enc_value']))
        {
            session_start();
            $user_id=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $posted=$_POST['enc_value'];
            $enry=encrypt_decrypt("decrypt",$posted);
            $posted_value= explode('_',$enry);
            $conn = database_open();

            $sql="SELECT DISTINCT general.faculty_roles.faculty_id,general.page_list.group_id,general.faculty_roles.role_id FROM general.faculty_roles RIGHT JOIN general.role_list ON general.faculty_roles.role_id=general.role_list.role_id RIGHT JOIN general.role_responsibility ON general.faculty_roles.role_id=general.role_responsibility.role_id RIGHT JOIN general.page_list ON general.page_list.page_id=general.role_responsibility.page_id WHERE general.faculty_roles.faculty_id=:fid AND general.faculty_roles.role_id=:rid AND general.page_list.page_url=:url_name";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':fid',$user_id);
            $stmt->bindParam(':rid', $posted_value[0]);
            $stmt->bindParam(':url_name', $posted_value[1]);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row>0)
            {
                $result = $stmt -> fetch();
                $urlvalue=$posted_value[1];  
                $_SESSION["role_id"]= $encrpt_pwd=encrypt_decrypt("encrypt",$posted_value[0]);
                $_SESSION["page_name"]= $encrpt_pwd=encrypt_decrypt("encrypt",$posted_value[1]);
                //$_SESSION["company_id"]= encrypt_decrypt("encrypt",$result["group_id"]);
            }
            else{$urlvalue="UnAuthorized";}
        }
        else{$urlvalue="Data Not Found";}
        
        $arr = ["name" =>$urlvalue];   
        echo json_encode($arr);
    }
    function sign_out()
    {
        session_start();
        $session_id=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        $log_status="no"; $update_time=get_datetime();
        $conn = database_open();

        $sql="update general.login_history set login_status=:lstatus,logoff_time=:ltime where session_id=:lsession";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':lstatus', $log_status);
        $stmt->bindParam(':ltime', $update_time);
        $stmt->bindParam(':lsession', $session_id);
        if ($stmt->execute() == TRUE) 
        {
            //echo "logoff success";
        }
        database_close($conn);    
        session_destroy();
        $arr = ["result" => "Redirect", "Message" => "/krg/login.php"];
        echo json_encode($arr);
    }
    function update_pwd()
    {
        $msg="";$type="danger";
        if(isset($_POST['cpass'])){  $cpass = $_POST['cpass']; }else{$msg="Current Password was not posted";}
        if(isset($_POST['npass'])){  $npass = $_POST['npass']; }else{$msg="New Password was not posted";}
        if(isset($_POST['ncpass'])){  $ncpass = $_POST['ncpass']; }else{$msg="Confirm Password was not posted";}
        if($msg=="")
        {
           session_start();
           $user_id=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
           if($user_id!="")
           {
                $count= strlen($npass);
                if($count>=6)
                {
                $encrpt_pwd=encrypt_decrypt("encrypt",$cpass);
                $new1=encrypt_decrypt("encrypt",$npass);
                $new2=encrypt_decrypt("encrypt",$ncpass);
                $conn = database_open();
                if (strpos($conn,"Failed") === 0) {$msg=$conn;}
                else
                {
                    $sql="select staff_id,group_id,dept_id,password from general.staff_basic_info where staff_id=:id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':id',$user_id);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                            $result = $stmt -> fetch();
                            if($result["password"] == $encrpt_pwd)
                            {
                                if($new1==$new2)
                                {
                                    $sql="update general.staff_basic_info set password=:pwd where staff_id=:id";
                                    $stmt = $conn->prepare($sql); 
                                    $stmt->bindParam(':id', $user_id);
                                    $stmt->bindParam(':pwd', $new1);
                                     $stmt->execute();
                                    sign_out();
                                    $type="Redirect"; $msg="/krg/login.php";

                                }else{$msg="New Password and Confirm Password are mismatch";}
                            }else{$msg="Invalid Current Password";}
                    }else{$msg="User Id Not Found in database";}
                    database_close($conn); 
                }
                }else{$msg="The New Password Length Must be Greater than or equal to 6";}
            }else{$msg="User Id Not Found";}
        }
        if($type!="Redirect")
        {
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
    }
    function forget_pwd()
    {
        $msg="";$type="danger";
        if(isset($_POST['mail'])){  $mail = $_POST['mail']; }else{$msg="Mail Id was not posted";}
        if($msg=="")
        {
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
            else
            {
                   $sql="select staff_id,saluation,first_name,last_name,group_id,dept_id,password from general.staff_basic_info where official_mail=:id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':id',$mail);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                         $result = $stmt -> fetch();
                         $pasword=encrypt_decrypt("decrypt",$result["password"]);
                         $staff_name=$result["saluation"].$result["last_name"]." ".$result["first_name"];

                         $msg_subject="KRG Portal Login Information - Reg.";
                         $msg_content="Dear $staff_name,<br>Warm Greetings from KRG Portal.You are registered as User of KRG Portal.Please Use the below information to login.<br>Your User Name : <strong>$mail</strong><br>Password : <strong>$pasword</strong><br>URL : http://210.212.249.135 <br> Kindly Note down this Credentials to use KRG Portal.<br><br>With Regards<br>KRG Portal,<br>KR Group of Companies,<br>Karur.";
                         $msg=sendmail($msg_subject,$msg_content,$mail);
                         $type="success";$msg="The Password Sent to ".$mail;
                
                    }else{$msg="The Maild Id was not found in datatable";}
                    database_close($conn); 
            }
        }
        //if($type!="Redirect")
        {
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
    }
    function login()
        {
            $company_id="";
            if(isset($_POST['gname'])){$company_id=$_POST["gname"];}
            $type="Error";$msg="";$login_code="Invalid";$login_status="No";
            $conn = database_open();
            if (strpos($conn,"Failed") === 0) {$msg=$conn;}
             else
             {
                $update_by="";$faculty_code="";$staff_id="";$group_id="";$dept_id="";
                $update_session="";
              try
              {
                      if(isset($_POST['email'])){  $mail = $_POST['email']; }else{$msg="e-mail Id was not posted";}
                      if(isset($_POST['password'])){  $pwd = $_POST['password']; }else{$msg="Password was not posted";}
                      $staff_id=$mail;
                      if($msg=="")
                      {
                                  $encrpt_pwd=encrypt_decrypt("encrypt",$pwd);
                                  $sql="select staff_id,group_id,dept_id,password from general.staff_basic_info where official_mail=:mailid";
                                  $stmt = $conn->prepare($sql); 
                                  $stmt->bindParam(':mailid', $mail);
                                  $stmt->execute();
                                  $row =$stmt->rowCount();
                                  if($row>0)
                                  {
                                         $result = $stmt -> fetch();
                                          if($result["password"] == $encrpt_pwd)
                                          {
                                            $type="Success";$login_code="Success";$login_status="YES";
                                            $staff_id=$result["staff_id"];
                                            if($company_id==""){ $group_id=$result["group_id"];}else{$group_id=$company_id;}
                                            $dept_id=$result["dept_id"];
                                          }else{$msg="Invalid e-mail or Password";$login_code="Invalid Cred1";}
                                  }
                                  else{$msg="Invalid e-mail ";$login_code="Invalid Mail";}
                      } 
                      $sql = "select count(*) from general.login_history";
                      $stmt = $conn->prepare($sql); 
                      $stmt->execute();
                      $result = $stmt->fetchColumn(0);
                      $row=$result;
                      $add_value="1";
                      $total=(integer)$row+(integer)$add_value;
                      $session_id="SESS".SixDigitIdGenerator("$total");

                      $user_type="Staff";$device_type="";$logoff_time="";
                      $system_name= gethostname(); $system_ip= get_client_ip(); $mac_addr= get_mac();
                      $obj = new OS_BR();
                      $browser_name=$obj->showInfo('browser')." ".$obj->showInfo('version');
                      $os_type=$obj->showInfo('os');
                      $is_mobile= isMobile();
                      $logged_user=get_logged_user(); 
                      $update_time=get_datetime();
                      
                      $sql="insert into general.login_history values(:sid,:gid,:did,:lid,:utype,:mpc,:manu,:brow,:os,:device,:user,:mac,:ip,:logtime,:logcode,:log_status,:logoff)";
                      $stmt = $conn->prepare($sql); 
                      $stmt->bindParam(':sid', $session_id);
                      $stmt->bindParam(':gid', $group_id);
                      $stmt->bindParam(':did', $dept_id);
                      $stmt->bindParam(':lid', $staff_id);
                      $stmt->bindParam(':utype',$user_type);
                      $stmt->bindParam(':mpc', $is_mobile);
                      $stmt->bindParam(':manu', $device_type);
                      $stmt->bindParam(':brow', $browser_name);
                      $stmt->bindParam(':os',$os_type);
                      $stmt->bindParam(':device', $device_type);
                      $stmt->bindParam(':user',$logged_user);
                      $stmt->bindParam(':mac',$mac_addr);
                      $stmt->bindParam(':ip',$system_ip);
                      $stmt->bindParam(':logtime',$update_time);
                      $stmt->bindParam(':logcode',$login_code);
                      $stmt->bindParam(':log_status',$login_status);
                      $stmt->bindParam(':logoff',$logoff_time);
                     
                      if ($stmt->execute() == true) 
                      {
                              if($login_status=="YES")
                              {
                                  session_start();
                                  $_SESSION["user_id"]= $encrpt_pwd=encrypt_decrypt("encrypt",$staff_id);
                                  $_SESSION["company_id"]= $encrpt_pwd=encrypt_decrypt("encrypt",$group_id);
                                  $_SESSION["user_type"]= $encrpt_pwd=encrypt_decrypt("encrypt",$user_type);
                                  $_SESSION["session_id"]= $encrpt_pwd=encrypt_decrypt("encrypt",$session_id);
                                  $_SESSION['loggedin_time']=time();
                                  page_list();
                              }
                     } 
                     else{$msg=$conn->errorCode();} 
              } catch(PDOException $e){$msg=$e->getMessage(); }
          
                 database_close($conn);
             }
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);  
        }
        function index_info()
        {
            $src="";
            try
            {
                $image = $_SERVER['DOCUMENT_ROOT'] .'/PCTEM/scripts/images/user.jpg';
                $imageData = base64_encode(file_get_contents($image));
                $src = 'data:image/jpg'.';base64,'.$imageData;

            }catch(Exception $e){$src= $e->getMessage(); }
            $name="abv";$desig="";$dept="";$company_name="";
            session_start();
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            if(isset($_SESSION["user_id"]))
            {
                 $user_id=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                 $company_id=encrypt_decrypt("decrypt",$_SESSION["company_id"]);
                 $conn = database_open();
                 try
                 {

                    $sql="SELECT general.staff_basic_info.staff_id,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name,general.staff_basic_info.designation,general.department_list.course_name,general.staff_basic_info.dob,general.staff_basic_info.gender FROM general.staff_basic_info JOIN general.department_list ON general.staff_basic_info.dept_id=general.department_list.dept_id WHERE general.staff_basic_info.staff_id=:userid";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':userid', $user_id);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                           $row = $stmt -> fetch();
                           $name=$row['saluation']."".$row['last_name'].".".$row['first_name'];
                           $desig=$row['designation'];
                           $dept=$row['course_name'];
                          
                    }
                    $sql="select group_name from general.institution_list where group_id=:cid";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':cid', $company_id);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                           $row = $stmt -> fetch();
                           $company_name=$row['group_name'];
                    }
                 }catch(Exception $e){$msg=$e->getMessage(); }
                 database_close($conn);
            }
            $sno=0;
            $nav_link="<li><a href='#'><i class='fa fa-check-square-o nav_icon'></i>Forms<span class='fa arrow'></span></a><ul class='nav nav-second-level collapse'><li><a href='forms.html'>Basic Forms <span class='nav-badge-btm'>07</span></a></li><li><a href='validation.html'>Validation</a></li></ul></li>";
            $json = ['company_name'=>$company_name,'name' =>$name,'designation' => $desig,'dept'=>$dept,'image'=>$src,'nav_link'=>$nav_link];
            echo json_encode($json);
           
        }
        function page_list()
        {
            $myvalue="";
            //session_start();
            $faculty_id=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $conn = database_open();
            
            $sql="SELECT general.role_list.role_id,general.role_list.role_name,general.role_list.role_icon FROM general.role_list JOIN general.faculty_roles ON general.role_list.role_id=general.faculty_roles.role_id WHERE general.faculty_roles.faculty_id=:id";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':id', $faculty_id);
            $stmt1->execute();
            $row1 =$stmt1->rowCount(); 
            if($row1>0)
            {
                while($data = $stmt1->fetch(PDO::FETCH_BOTH))
                {
                    $role_name=$data["role_name"];
                    $role_icon=$data["role_icon"];
                    $myvalue.="<li><a href='#'>$role_icon$role_name<i class='material-icons arrow'>arrow_drop_down</i></a>";
                    try
                    {
                            $sql="SELECT general.page_list.page_id,general.page_list.page_name,general.page_list.page_icon,general.page_list.page_url FROM general.page_list JOIN general.role_responsibility ON general.page_list.page_id=general.role_responsibility.page_id where general.role_responsibility.role_id=:rid";
                            $stmt = $conn->prepare($sql);   
                            $stmt->bindParam(':rid',$data["role_id"]);  
                            $stmt->execute();
                            $myvalue.="<ul class='nav nav-second-level collapse'>"; 
                            while($row = $stmt->fetch(PDO::FETCH_BOTH))
                            {
                                $encry_value=encrypt_decrypt("encrypt",$data["role_id"]."_".$row['page_url']);
                                $page_name=$row['page_name'];
                                $page_icon=$row['page_icon'];
                                $current_item="<li><a id='$encry_value' just='$just' href='forms.html' class='btnCustomers'>$page_icon$page_name</a></li>";
								$encry_value_resposibilities=encrypt_decrypt("encrypt",$data["role_id"]."_"."/krg/general/roles.php");
								$_SESSION["rolesid"]=$encry_value_resposibilities;
                                $myvalue.=$current_item;
                            }
                            $myvalue.="</ul>";
                    }catch(Exception $e){$msg=$e->getMessage();}
                    $myvalue.="</li>"; 
                }
            }
            database_close($conn);
            $_SESSION["page_list"]= $myvalue;
        }
        function unapproved_task_list()
        {
            header("Content-Type: application/json; charset=UTF-8");
            $conn = database_open();
            $sql="SELECT task_id,work_name,description,deadline_date from general.task_list where approval_status='None'";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            database_close($conn);
            echo json_encode($result);
        }
        function live_task_list()
        {
            header("Content-Type: application/json; charset=UTF-8");
            $conn = database_open();
            $sql="SELECT task_id,work_name,current_description,deadline_date,finished_percentage from general.task_list where approval_status='Approved' and task_status<>'Completed'";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            database_close($conn);
            echo json_encode($result);
        }
?>


