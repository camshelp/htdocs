<?php
    require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
    $callparameter="";
    if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
    if($callparameter=="")
    {
      $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
      echo json_encode($arr);
    }
    else
    { 
       switch($callparameter)
       {
        case "allfaculty":faculty_list_all();
                         break;
        case "faculty_register":faculty_register();
                         break;
        case "faculty_list":faculty_list();
                         break;
        case "designation_list":designation_list();
                         break;
        case "gender_list":gender_list1(); 
                         break;
        case "saluation_list":saluation_list();
                         break;
        default:
                                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                                    echo json_encode($arr);  
                                    break;
       }
    }
    function faculty_list_all()
    {
        
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        if (strpos($conn,"Failed") === 0) {$msg=$conn;}
         else
         {
                $sql="SELECT general.staff_basic_info.staff_id,general.staff_basic_info.faculty_code,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name,general.staff_basic_info.designation,general.department_list.course_name,general.staff_basic_info.mobile_no FROM general.staff_basic_info JOIN general.department_list ON general.staff_basic_info.dept_id=general.department_list.dept_id WHERE general.staff_basic_info.active_status='yes'";
                $stmt = $conn->prepare($sql); 
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row)
                {
                    $sno=0;
                    while($row = $stmt->fetch(PDO::FETCH_BOTH))
                    {
                        $name=$row['saluation'].$row['last_name'].".".$row['first_name'];
                        $json[$sno] = array(
                            'faculty_id' => $row['staff_id'],
                            'faculty_code' => $row['faculty_code'],
                            'name' => $name,
                            'designation' =>  $row['designation'],
                            'department' =>  $row['course_name'],
                            'mobile' =>  $row['mobile_no']);
                            $sno++;
                    }
                }
             database_close($conn);
             echo json_encode($json);
         }
    }
         function faculty_register()
        {
            session_start();
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $update_time=get_datetime();

            $type="Error";$msg="";
             $faculty_code="";$active="yes";
            $conn = database_open();
            try
            {
                if(isset($_POST['Iid'])){  $iid = $_POST['Iid']; }else{$msg="Institution Name was not posted";}
                if(isset($_POST['did'])){  $did = $_POST['did']; }else{$msg="Department Name was not posted";}
                if(isset($_POST['sal'])){  $saluation = $_POST['sal']; }else{$msg="Saluation was not posted";}
                if(isset($_POST['FName'])){  $fname = $_POST['FName']; }else{$msg="Faculty First Name was not posted";}
                if(isset($_POST['LName'])){  $lname = $_POST['LName']; }else{$msg="Faculty Last Name was not posted";}
                if(isset($_POST['desig'])){  $design = $_POST['desig']; }else{$msg="Designation was not posted";}
                if(isset($_POST['doj'])){  $doj = $_POST['doj']; }else{$msg="Date of Joining was not posted";}
                if(isset($_POST['dob'])){  $dob = $_POST['dob']; }else{$msg="Date of Birth was not posted";}
                if(isset($_POST['gender'])){  $gender = $_POST['gender']; }else{$msg="Gender was not posted";}
                if(isset($_POST['aadhar'])){  $aadhar = $_POST['aadhar']; }else{$msg="Aadhar No. was not posted";}
                if(isset($_POST['mobile'])){  $mobile = $_POST['mobile']; }else{$msg="Mobile No. was not posted";}
                if(isset($_POST['mail'])){  $mail = $_POST['mail']; }else{$msg="Mail Id was not posted";}
				if(isset($_POST['faculty_roles'])){  $roles = $_POST['faculty_roles']; }else{$msg="Roles and Responsibilities was not posted";}
                if($msg=="")
                {
                        $sql="select staff_id from general.staff_basic_info where official_mail=:mail";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':mail', $mail);
                        $stmt->execute();
                        $row =$stmt->rowCount();
                        if($row==0)
                        {
                            $sql = "select count(*) from general.staff_basic_info";
                            $stmt = $conn->prepare($sql); 
                            $stmt->execute();
                            $result = $stmt->fetchColumn(0);
                            $row=$result;
                            $add_value="1";
                            $total=(integer)$row+(integer)$add_value;

                            $gen_id="FLTY".SixDigitIdGenerator("$total");
                            $pwd=random_no(6);
                            $encrypted=encrypt_decrypt("encrypt",$pwd);
                            $decrypted=encrypt_decrypt("decrypt",$encrypted);
                            $active_status="yes";
                            $sql="insert into general.staff_basic_info values(:iid,:did,:gen_id,:fcode,:saluation,:fname,:lname,:design,:dob,:doj,:gender,:aadhar,:mobile,:mail,:roles,:pwd,:by,:session,:time,:active)";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':iid', $iid);
                            $stmt->bindParam(':did', $did);
                            $stmt->bindParam(':gen_id', $gen_id);
                            $stmt->bindParam(':fcode', $faculty_code);
                            $stmt->bindParam(':saluation',$saluation);
                            $stmt->bindParam(':fname',$fname);
                            $stmt->bindParam(':lname', $lname);
                            $stmt->bindParam(':design',$design);
                            $stmt->bindParam(':dob', $dob);
                            $stmt->bindParam(':doj', $doj);
                            $stmt->bindParam(':gender',$gender);
                            $stmt->bindParam(':aadhar', $aadhar);
                            $stmt->bindParam(':mobile', $mobile);
                            $stmt->bindParam(':mail',$mail);
							$stmt->bindParam(':roles',$roles);
                            $stmt->bindParam(':pwd', $encrypted);
                            $stmt->bindParam(':by', $update_by);
                            $stmt->bindParam(':session', $update_session);
                            $stmt->bindParam(':time',$update_time);
                            $stmt->bindParam(':active',$active);
                            if ($stmt->execute() == TRUE) 
                            {
                                $type="Success";$msg="The Information Submitted Successfully";

                                update_general_roles($conn,$iid,$gen_id);
                                $msg_subject="KRG Portal Registration - Reg.";
                                $msg_content="Dear $saluation$fname $lname,<br>Warm Greetings from KRG Portal.You are registered as User of KRG Portal.Please Use the below information to login.<br>Your User Name : <strong>$mail</strong><br>Password : <strong>$decrypted</strong><br>URL : http://210.212.249.135 <br> Kindly Note down this Credentials to use KRG Portal.<br><br>With Regards<br>KRG Portal,<br>KR Group of Companies,<br>Karur.";
                                $mail_msg=sendmail($msg_subject,$msg_content,$mail);
                               
                            }
                        }else{$msg="The Official Mail Id already exists";}
                }
            }catch(Exception $e){$msg=$e->getMessage(); }
            database_close($conn);
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);
        }
        function update_general_roles($conn,$iid,$faculty_id)
        {
            $primary="yes";
            $sql="select role_id from general.role_list where group_id=:id and is_primary=:primary";
            $stmt1 = $conn->prepare($sql); 
            $stmt1->bindParam(':id', $iid);
            $stmt1->bindParam(':primary', $primary);
            $stmt1->execute();
            $roles="";
            while($data = $stmt1->fetch(PDO::FETCH_BOTH))
            {
            $roles.=$data['role_id'].",";
            }
            update_faculty_roles1($faculty_id,rtrim($roles,",")); 
        }
        function update_faculty_roles()
        {
            $type="Success";
            if(isset($_POST['facultyid'])){  $pid = $_POST['facultyid']; }else{ $msg="Faculty Id was not posted";}
            if(isset($_POST['resp']))  {  $resp = $_POST['resp']; }else {$msg="Responsibility Information was not posted";}
            $msg= update_faculty_roles1($pid,rtrim($resp,","));
            $arr = ["result" => $type, "Message" => $msg];
            echo json_encode($arr);  
        }
        function update_faculty_roles1($pid,$resp)
        {
                $msg="";
                $conn = database_open();
                if (strpos($conn,"Failed") === 0) {$msg=$conn;}
                else
                {
                    session_start();
                    $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                    $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                    $update_time=get_datetime();
                    $conn->beginTransaction();
                    try
                    {
                        $sql="delete from general.faculty_roles where faculty_id=:rid";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':rid',$pid);
                        $stmt->execute();

                        $previll = explode(',',$resp);
                        $max_count=count($previll);
                        for($x=0;$x<$max_count;$x++)
                        {
                            if($previll[$x]!="")
                            {
                                $sql="insert into general.faculty_roles values(:rid,:pid,:by,:session,:time)";
                                $stmt = $conn->prepare($sql); 
                                $stmt->bindParam(':rid',$pid);
                                $stmt->bindParam(':pid',$previll[$x]);
                                $stmt->bindParam(':by',$update_by);
                                $stmt->bindParam(':session',$update_session);
                                $stmt->bindParam(':time',$update_time);
                                if ($stmt->execute() == TRUE) 
                                {
                                
                                }
                            }
                        }
                        $conn->commit();
                      $msg="The Responsibility Information was updated Successfully";
                    }catch(Exception $e){$conn->rollBack(); $msg=$e->getMessage();}
                }
                database_close($conn);
                return $msg;
           
        }       
        function faculty_list()
        {
            $group_id=$_POST['gname'];
            $dept_id=$_POST['dname'];
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            
            $image = $_SERVER['DOCUMENT_ROOT'] .'/krg/scripts/images/user.jpg';
            $imageData = base64_encode(file_get_contents($image));
            $src = 'data:image/jpg'.';base64,'.$imageData;
            $conn = database_open();
            try
            {

            $sql="SELECT * from general.staff_basic_info where group_id=:gid and dept_id=:did";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':gid', $group_id);
            $stmt->bindParam(':did', $dept_id);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row)
            {
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $json[$sno] = array(
                     'faculty_id' => $row['staff_id'],
                     'saluation' => $row['saluation'],
                     'first_name' => $row['first_name'],
                     'last_name' => $row['last_name'],
                     'designation' => $row['designation'],
                     'dob' => $row['dob'],
                     'doj' => $row['doj'],
                     'image'=>$src);
                    $sno++;
                }
            }
        }catch(Exception $e){$msg=$e->getMessage(); }
        database_close($conn);
        echo json_encode($json);
        
    }
    function gender_list1()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $conn = database_open();
        $sql="SELECT gender_type from general.gender_list order by gender_type asc";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        database_close($conn);
        echo json_encode($result);
    }
    function designation_list()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $conn = database_open();
        $sql="SELECT designation from general.designation order by designation asc";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        database_close($conn);
        echo json_encode($result);
    }
    function saluation_list()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $conn = database_open();
        $sql="SELECT saluation from general.saluation_list";
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        database_close($conn);
        echo json_encode($result);
    }
  
  