<?php
    require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
    require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
    $callparameter="";
    if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
    if($callparameter=="")
    {
      $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php".$callparameter];
      echo json_encode($arr);
    }
    else
    { 
       switch($callparameter)
       {
         case "dept_list":dept_list();
                     break;
         default:
                                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                                    echo json_encode($arr);  
                                    break;
        
       }
    }
        function dept_list()
        {
            $group_id=$_POST['group_name'];
            try
            {
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT general.department_list.group_id,general.department_list.dept_id,general.department_list.graduation_type,general.department_list.degree_name,general.department_list.course_name,general.department_list.acronym,general.department_list.sem_type,general.department_list.min_duration,general.department_list.max_duration,general.department_list.no_semester,general.department_list.board_name,general.department_list.register_code,general.department_list.intake,general.department_list.starting_year,general.department_list.is_academic_type,general.department_list.display_order,general.department_list.faculty_code,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name FROM general.department_list LEFT JOIN general.staff_basic_info ON general.department_list.head_id=general.staff_basic_info.staff_id WHERE general.department_list.group_id=:group ORDER BY display_order asc";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':group', $group_id);
            $stmt->execute();
            $row =$stmt->rowCount();
            $sno=0;
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $name=$row["saluation"]."".$row["last_name"].".".$row["first_name"];
                $json[$sno] = array(
                    'group_id' => $row['group_id'],
                    'dept_id' => $row['dept_id'],
                    'graduation_type' => $row['graduation_type'],
                    'degree_name' => $row['degree_name'],
                    'course_name' => $row['course_name'],
                    'acronym' => $row['acronym'],
                    'sem_type' => $row['sem_type'],
                    'min_duration' => $row['min_duration'],
                    'max_duration' => $row['max_duration'],
                    'no_semester' => $row['no_semester'],
                    'board_name' => $row['board_name'],
                    'register_code' => $row['register_code'],
                    'intake' => $row['intake'],
                    'starting_year' => $row['starting_year'],
                    'is_academic_type' => $row['is_academic_type'],
                    'display_order' => $row['display_order'],
                    'faculty_code' => $row['faculty_code'],
                    'head_name' =>rtrim($name,"."));
                $sno++;
            }
            database_close($conn);
            echo json_encode($json);
            }catch(Exception $e){$msg=$e->getMessage(); }
        }
        