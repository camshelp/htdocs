<?php
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
  require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
  $callparameter="";
  if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
  if($callparameter=="")
  {
    $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php"];
    echo json_encode($arr);
  }
  else
  {
        switch($callparameter)
        {
            case "task_register":task_list_update();
                break;
            case "task_get":task_list();
                break;
            case "task_approve":approve_task();
                break;
            case "task_status":update_task_status();
                break;
            case "revise_task_status":revise_task();
                break;
            case "work_nature":work_nature();
                break;
            case "timelinetask":task_timeline();
                break;
            case "subwork_nature":sub_work_nature();
                break;
            case "subtask_register":subtask_list_update();
                break;
            case "subtask_get":subtask_list();
                break;
            case "getmyid":Get_myId();
                break;
            case "getmyresp":Get_ChangeResp();
                break;
            case "downloadprep":download_preparation();
                break;
            case "task_summary":task_summary();
                break;
            default:
                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                    echo json_encode($arr);  
                    break;
        }
    }
    function task_list_update()
    {
        session_start();
        $type="Error";$msg="";
        try
        {
            if(isset($_POST['task_fid'])){  $assigned_id = $_POST['task_fid']; }else{$msg="Responsible Person Not Assigned was not posted";}
            if(isset($_POST['WName'])){  $wname = $_POST['WName']; }else{$msg="Work Name was not posted";}
            if(isset($_POST['WDesc'])){ $wdesc = $_POST['WDesc']; }else{$msg="Work Description was not posted";}
            if(isset($_POST['wtype'])){ $wtype = $_POST['wtype']; }else{$msg="Work Nature was not posted";}
            if(isset($_POST['dead'])){  $dead = $_POST['dead']; }else{$msg="Work Deadline was not posted";}
          
            if($msg=="")
            {
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $role_id=encrypt_decrypt("decrypt",$_SESSION["role_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                $conn = database_open();
                
                $updated_name="";
                $sql="SELECT group_id,dept_id,CONCAT(saluation,last_name,'.',first_name) as name,designation,official_mail from general.staff_basic_info where staff_id=:by";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':by', $update_by);
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row>0)
                {
                    $row = $stmt -> fetch();
                    $updated_name=$row["name"];
                }
                //$stmt->debugDumpParams();

                $sql="SELECT group_id,dept_id,CONCAT(saluation,last_name,'.',first_name) as name,designation,official_mail from general.staff_basic_info where staff_id=:by";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':by', $assigned_id);
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row>0)
                {
                    $result = $stmt -> fetch();
                    $group_id=$result['group_id'];$dept_id=$result['dept_id'];
                    $fname=$result['name'];
                    $fdesig=$result['designation'];
                    $fofficial=$result['official_mail'];
                    $approve_status="None";$dummy="";$percentage=0;$aby="";$asession="";$atime="";
                    if($role_id=="ROLE000003"){$approve_status="Approved";$aby=$update_by;$asession=$update_session;$atime=$update_time;}
                    $sql = "select count(*) from general.task_list";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $row = $stmt->fetchColumn(0);
                    $add_value="1";
                    $total=(integer)$row+(integer)$add_value;
                    $task_id="TASK".SixDigitIdGenerator("$total");
                    $task_type="Task";$parent_id=""; $task_status="Not Started";
                    $sql="INSERT INTO general.task_list VALUES (:gid,:did,:created,:tid,:ttype,:tparent,:name,:nature,:desc,:dead,:by,:session,:time,:appr,:aby,:asession,:atime,:cdesc,:tstatus,:per,:uby,:usession,:utime)";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':gid', $group_id);
                    $stmt->bindParam(':did', $dept_id);
                    $stmt->bindParam(':created', $assigned_id);
                    $stmt->bindParam(':tid', $task_id);
                    $stmt->bindParam(':ttype', $task_type);
                    $stmt->bindParam(':tparent',$parent_id);
                    $stmt->bindParam(':name', $wname);
                    $stmt->bindParam(':nature', $wtype);
                    $stmt->bindParam(':desc', $wdesc);
                    $stmt->bindParam(':dead', $dead);
                    $stmt->bindParam(':by', $update_by);
                    $stmt->bindParam(':session', $update_session);
                    $stmt->bindParam(':time', $update_time);
                    $stmt->bindParam(':appr', $approve_status);
                    $stmt->bindParam(':aby', $aby);
                    $stmt->bindParam(':asession',$asession);
                    $stmt->bindParam(':atime',$atime);
                    $stmt->bindParam(':cdesc',$dummy);
                    $stmt->bindParam(':tstatus',$task_status);
                    $stmt->bindParam(':per',$percentage);
                    $stmt->bindParam(':uby',$dummy);
                    $stmt->bindParam(':usession',$dummy);
                    $stmt->bindParam(':utime',$dummy);
                    if ($stmt->execute() == TRUE) 
                    {
                        $desc="Task Created Successfully";$type="Task Creation";
                        task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time);
                        $type="Success";$msg="The Information Submitted Successfully";
                        $subject="Work Load Assignment - Reg.";
                        $message="Dear ".$fname.",<br>New Work is Assigned to You in KRG Portal.The details are following<br><strong>Work Name :</strong>$wname<br><strong>Work Description :</strong>$wdesc<br><strong>Work Nature :</strong>$wtype<br><strong>Expected Completion Date :</strong>$dead<br><strong>Assigned by :</strong>$updated_name<br><strong>Assigned Time :</strong>$update_time<br>";
                        sendmail($subject,$message,$fofficial);
                        if($role_id!="ROLE000003")
                        {
                            $sql="SELECT general.staff_basic_info.staff_id,CONCAT(general.staff_basic_info.saluation,general.staff_basic_info.last_name,'.',general.staff_basic_info.first_name) AS name,general.staff_basic_info.designation,general.staff_basic_info.official_mail FROM general.staff_basic_info JOIN general.department_list ON general.department_list.head_id=general.staff_basic_info.staff_id WHERE general.department_list.dept_id=:id";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':id', $dept_id);
                            $stmt->execute();
                            $row =$stmt->rowCount();
                            if($row>0)
                            {
                                $row = $stmt -> fetch();
                                $hname=$row["name"];
                                $subject="Work Load Assignment - Reg.";
                                $id_encrypted=encrypt_decrypt("encrypt",$task_id.",".$row["staff_id"]);
                                $act_link="<a href = 'http://210.212.249.135/krg/activate.php?callvalue=workapproval&id=$id_encrypted')>Click Here to Approve<a>";
                                $message="Dear ".$hname.",<br>New Work is Assigned to You in KRG Portal.The details are following<br><strong>Work Name :</strong>$wname<br><strong>Work Description :</strong>$wdesc<br><strong>Work Nature :</strong>$wtype<br><strong>Expected Completion Date :</strong>$dead<br><strong>Assigned by :</strong>$updated_name<br><strong>Assigned Time :</strong>$update_time<br><br>".$act_link;
                                sendmail($subject,$message,$row["official_mail"]);
                            } 
                        }
                    }
                    else {$msg=$e->getMessage();}
                }
                database_close($conn);
            }
        }catch(Exception $e){$msg=$e->getMessage();}
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
    }
    function Get_myId()
    {
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $arr = ["staff_id" => $update_by];
        echo json_encode($arr);
    }
    function Get_ChangeResp()
    {
        session_start();
        $status="no";
        $role=encrypt_decrypt("decrypt",$_SESSION["role_id"]);
        if($role=="ROLE000003"){$status="yes";}
        $arr = ["change" =>$status];
        echo json_encode($arr);
    }
    function subtask_list_update()
    {
        //echo var_dump($_POST);
        session_start();
        $type="Error";$msg="";
        try
        {
            if(isset($_POST['sub_fid'])){  $assigned_id = $_POST['sub_fid']; }else{$msg="Responsible Person Name was not posted";}
            if(isset($_POST['subname'])){ $sub_name = $_POST['subname']; }else{$msg="Sub-Task Name was not posted";}
            if(isset($_POST['subdesc'])){ $sub_desc = $_POST['subdesc']; }else{$msg="Sub Task Description was not posted";}
            if(isset($_POST['subtype'])){  $sub_type = $_POST['subtype']; }else{$msg="Sub-Task Type was not posted";}
            if(isset($_POST['sub_dead'])){  $sub_dead = $_POST['sub_dead']; }else{$msg="Sub-Task Deadline was not posted";}
            if(isset($_POST['parent_id'])){ $parent_id = $_POST['parent_id']; }else{$msg="Task Name was not posted";}
           
            if($msg=="")
            {
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                $conn = database_open();
                
                $updated_name="";
                $sql="SELECT group_id,dept_id,CONCAT(saluation,last_name,'.',first_name) as name,designation,official_mail from general.staff_basic_info where staff_id=:by";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':by', $update_by);
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row>0)
                {
                    $row = $stmt -> fetch();
                    $updated_name=$row["name"];
                }
                //$stmt->debugDumpParams();

                $sql="SELECT group_id,dept_id,CONCAT(saluation,last_name,'.',first_name) as name,designation,official_mail from general.staff_basic_info where staff_id=:by";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':by', $assigned_id);
                $stmt->execute();
                $row =$stmt->rowCount();
                if($row>0)
                {
                    $result = $stmt -> fetch();
                    $group_id=$result['group_id'];$dept_id=$result['dept_id'];
                    $fname=$result['name'];
                    $fdesig=$result['designation'];
                    $fofficial=$result['official_mail'];
                    $dummy="";$percentage=0;
                   
                    $sql = "select count(*) from general.task_list";
                    $stmt = $conn->prepare($sql); 
                    $stmt->execute();
                    $row = $stmt->fetchColumn(0);
                    $add_value="1";
                    $total=(integer)$row+(integer)$add_value;
                    $task_id="TASK".SixDigitIdGenerator("$total");
                    $task_type="Sub-Task";
                    $approve_status="Approved";
                    $task_status="Not Started";
                    $sql="INSERT INTO general.task_list VALUES (:gid,:did,:created,:tid,:ttype,:tparent,:name,:nature,:desc,:dead,:by,:session,:time,:appr,:aby,:asession,:atime,:cdesc,:tstatus,:per,:uby,:usession,:utime)";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':gid', $group_id);
                    $stmt->bindParam(':did', $dept_id);
                    $stmt->bindParam(':created', $assigned_id);
                    $stmt->bindParam(':tid', $task_id);
                    $stmt->bindParam(':ttype', $task_type);
                    $stmt->bindParam(':tparent',$parent_id);
                    $stmt->bindParam(':name', $sub_name);
                    $stmt->bindParam(':nature', $sub_type);
                    $stmt->bindParam(':desc',$sub_desc);
                    $stmt->bindParam(':dead', $sub_dead);
                    $stmt->bindParam(':by', $update_by);
                    $stmt->bindParam(':session', $update_session);
                    $stmt->bindParam(':time', $update_time);
                    $stmt->bindParam(':appr', $approve_status);
                    $stmt->bindParam(':aby', $update_by);
                    $stmt->bindParam(':asession', $update_session);
                    $stmt->bindParam(':atime',$update_time);
                    $stmt->bindParam(':cdesc',$dummy);
                    $stmt->bindParam(':tstatus',$task_status);
                    $stmt->bindParam(':per',$percentage);
                    $stmt->bindParam(':uby',$dummy);
                    $stmt->bindParam(':usession',$dummy);
                    $stmt->bindParam(':utime',$dummy);
                    if ($stmt->execute() == TRUE) 
                    {
                        $desc="Sub-Task Created Successfully"; $type="Task Creation";
                        task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time);
                        $desc="Sub-Task Auto Approved Successfully"; $type="Task Approval";
                        task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time);
                        $type="Success";$msg="The Information Submitted Successfully";

                        $subject="Work Load Assignment - Reg.";
                        $message="Dear ".$fname.",<br>New Work is Assigned to You in KRG Portal.The details are following<br><strong>Work Name :</strong>$sub_name<br><strong>Work Description :</strong>$sub_desc<br><strong>Work Nature :</strong>$sub_type<br><strong>Expected Completion Date :</strong>$sub_dead<br><strong>Assigned by :</strong>$updated_name<br><strong>Assigned Time :</strong>$update_time<br>";
                        sendmail($subject,$message,$fofficial);

                       /*  $sql="SELECT general.staff_basic_info.staff_id,CONCAT(general.staff_basic_info.saluation,general.staff_basic_info.last_name,'.',general.staff_basic_info.first_name) AS name,general.staff_basic_info.designation,general.staff_basic_info.official_mail FROM general.staff_basic_info JOIN general.department_list ON general.department_list.head_id=general.staff_basic_info.staff_id WHERE general.department_list.dept_id=:id";
                        $stmt = $conn->prepare($sql); 
                        $stmt->bindParam(':id', $dept_id);
                        $stmt->execute();
                        $row =$stmt->rowCount();
                        if($row>0)
                        {
                            $row = $stmt -> fetch();
                            $hname=$row["name"];
                            $subject="Work Load Assignment - Reg.";
                            $id_encrypted=encrypt_decrypt("encrypt",$task_id.",".$row["staff_id"]);
                            $act_link="<a href = 'http://210.212.249.135/krg/activate.php?callvalue=workapproval&id=$id_encrypted')>Click Here to Approve<a>";
                            $message="Dear ".$hname.",<br>New Work is Assigned to You in KRG Portal.The details are following<br><strong>Work Name :</strong>$wname<br><strong>Work Description :</strong>$wdesc<br><strong>Work Nature :</strong>$wtype<br><strong>Expected Completion Date :</strong>$dead<br><strong>Assigned by :</strong>$updated_name<br><strong>Assigned Time :</strong>$update_time<br><br>".$act_link;
                            sendmail($subject,$message,$row["official_mail"]);
                        }  */
                    }
                    else {$msg=$e->getMessage();}
                }else{$msg="Invalid Responsible Person";}
                database_close($conn);
            }
        }catch(Exception $e){$msg=$e->getMessage();}
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);
    }
    function task_summary()
    {
        session_start();
        $acc_user=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $aother=0;$pother=0;$cother=0; $aown=0;$pown=0;$cown=0;

        if($acc_user == "FLTY000018" or $acc_user == "FLTY000006")
		{
		$conn = database_open();
        $sql = "select count(*) from general.task_list";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $aother = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where task_status<>'completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $pother = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where task_status='completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $cother = $stmt->fetchColumn(0);
		}
		else {
		
		
		$conn = database_open();
        $sql = "select count(*) from general.task_list where created_by=:id and staff_id<>:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $aother = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where created_by=:id and staff_id<>:id and task_status<>'completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $pother = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where created_by=:id and staff_id<>:id and task_status='completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $cother = $stmt->fetchColumn(0);
		}
		
        $sql = "select count(*) from general.task_list where staff_id=:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $aown = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where staff_id=:id and task_status<>'completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $pown = $stmt->fetchColumn(0);

        $sql = "select count(*) from general.task_list where staff_id=:id and task_status='completed'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id',$acc_user);
        $stmt->execute();
        $cown = $stmt->fetchColumn(0);

        database_close($conn);



        $arr = ["aothers" => $aother, "pothers" => $pother,'cothers'=>$cother,'aown'=>$aown,'pown'=>$pown,'cown'=>$cown];
        echo json_encode($arr);
    }
    function task_list()
    {
        session_start();
        $acc_user=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();    
        $conn = database_open();
       
        $sql="select dept_id from general.department_list where head_id=:user";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':user', $acc_user);
        $stmt->execute();
        $rowcount =$stmt->rowCount();
        $sort_str="";
        if($rowcount>0)
        {
            $head_depts=" or (";
            while($row = $stmt->fetch(PDO::FETCH_BOTH))
            { 
                $head_depts.=" general.task_list.dept_id='".$row["dept_id"]."' or";
            }
            //$sort_str=$head_depts;
           $sort_str=rtrim($head_depts,"r");
           $sort_str=rtrim($sort_str,"o").")";
        }
      
        if($acc_user=="FLTY000013"){$sort_str="or general.task_list.dept_id='DEPT000002'";}

if($acc_user == "FLTY000018" or $acc_user == "FLTY000006" )
{
  $sql="SELECT general.task_list.task_id,general.task_list.updated_time,general.task_list.staff_id,general.task_list.work_name,general.task_list.description,general.task_list.deadline_date,general.task_list.current_description,general.task_list.approval_status,general.task_list.task_status,general.task_list.finished_percentage,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name,general.department_list.head_id FROM general.task_list LEFT JOIN general.staff_basic_info ON general.task_list.staff_id=general.staff_basic_info.staff_id LEFT JOIN general.department_list ON general.task_list.dept_id=general.department_list.dept_id order by general.task_list.updated_time desc";
        $stmt = $conn->prepare($sql);          
        $stmt->bindParam(':user', $acc_user);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $rdate=$row['deadline_date'];
                $sql="SELECT revised_date from general.task_revision_date where task_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$row['task_id']);
                $stmt1->execute();
                $row1 =$stmt1->rowCount();
                if($row1>0)
                {
                    $result=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                    $rdate=$result[1]["revised_date"]."(R".$row1.")";
                } 
                $astatus=$row['approval_status'];
                $estatus="";
                if($astatus=="Not Started")
                {
                    if($acc_user==$row['head_id'])
                    {
                        $astatus="Approve";
                    }
                    else{ $astatus="Waiting for Approval"; }
                    $tstatus="Waiting";
                }
                else
                {
                    $astatus=$row['current_description'];
                    if($acc_user!=$row['staff_id']){$estatus="Yes";}
                }
                $name=$row['saluation']."".$row['last_name'].".".$row['first_name'];
				
				
				date_default_timezone_set('Asia/Kolkata');
                $date=date("d-m-Y");     
                 $original=$row['task_status'];
				$udate=$row['updated_time'];
				$date=strtotime($date); 
				$r1date=strtotime($rdate);
				if(($original!='Completed') and ($date > $r1date)) { 
				$original='Overdue'."-".$original;
				} 
                 else if($original=='Completed') { 
                  $date2=date("Y-m-d");
				  $date2=strtotime($date2); 
				  
				   $udate2=(explode(" ",$udate));
				   $ansdate=(explode("-",$udate2[0]));
				   $ansoriginaldate=$ansdate[2]."-".$ansdate[1]."-".$ansdate[0];
				   $ansoriginaldate=strtotime($ansoriginaldate);
				   if($ansoriginaldate>$r1date) {   
				   $original='Overdue'."-".$original;
				   } 
				}

				 
                $json[$sno] = array(
                    'task_id' => $row['task_id'],
                    'work_name' => $row['work_name'],
                    'description' => $row['description'],
                    'responsibile' => $name,
					
                    'deadline_date' => $rdate,
                    'current_description' => $row['current_description'],
                    'finished_percentage' => $row['finished_percentage'],
                    'approval_status' => $row['approval_status'],
                    'astatus' => $astatus, 'estatus' => $estatus,
                    'task_status' =>$original);
                    $sno++;
            }
        database_close($conn);
       echo json_encode($json);

}

else{


        $sql="SELECT general.task_list.task_id,general.task_list.updated_time,general.task_list.staff_id,general.task_list.work_name,general.task_list.description,general.task_list.deadline_date,general.task_list.current_description,general.task_list.approval_status,general.task_list.task_status,general.task_list.finished_percentage,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name,general.department_list.head_id FROM general.task_list LEFT JOIN general.staff_basic_info ON general.task_list.staff_id=general.staff_basic_info.staff_id LEFT JOIN general.department_list ON general.task_list.dept_id=general.department_list.dept_id where general.task_list.created_by=:user or general.task_list.staff_id=:user ".$sort_str." order by general.task_list.updated_time desc";
        $stmt = $conn->prepare($sql);          
        $stmt->bindParam(':user', $acc_user);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $rdate=$row['deadline_date'];
                $sql="SELECT revised_date from general.task_revision_date where task_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$row['task_id']);
                $stmt1->execute();
                $row1 =$stmt1->rowCount();
                if($row1>0)
                {
                    $result=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                    $rdate=$result[1]["revised_date"]."(R".$row1.")";
                } 
                $astatus=$row['approval_status'];
                $estatus="";
                if($astatus=="Not Started")
                {
                    if($acc_user==$row['head_id'])
                    {
                        $astatus="Approve";
                    }
                    else{ $astatus="Waiting for Approval"; }
                    $tstatus="Waiting";
                }
                else
                {
                    $astatus=$row['current_description'];
                    if($acc_user!=$row['staff_id']){$estatus="Yes";}
                }
                $name=$row['saluation']."".$row['last_name'].".".$row['first_name'];
				date_default_timezone_set('Asia/Kolkata');
                $date=date("d-m-y");
                 $original=$row['task_status'];
				 $udate=$row['updated_time'];
				 if($original!='Completed' and $date>$rdate) { 
				 $original='Overdue'."-".$original;
				 }
                 else if($original=='Completed') { 
                  $date2=date("y-m-d");
				   $udate2=(explode(" ",$udate));
				   $ansdate=(explode("-",$udate2[0]));
				   $ansoriginaldate=$ansdate[2]."-".$ansdate[1]."-".$ansdate[0];
				   if($ansoriginaldate>$rdate) {   
				   $original='Overdue'."-".$original;
				   } 
				 } 				 
                $json[$sno] = array(
                    'task_id' => $row['task_id'],
                    'work_name' => $row['work_name'],
                    'description' => $row['description'],
                    'responsibile' => $name,
					
                    'deadline_date' => $rdate,
                    'current_description' => $row['current_description'],
                    'finished_percentage' => $row['finished_percentage'],
                    'approval_status' => $row['approval_status'],
                    'astatus' => $astatus, 'estatus' => $estatus,
                    'task_status' =>$original);
                    $sno++;
            }
        database_close($conn);
       echo json_encode($json);
    }
 }
    function subtask_list()
    {
        $parent_id=$_POST["parent_id"];
        session_start();
        $acc_user=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        header("Content-Type: application/json; charset=UTF-8");
        $json = array();
        $conn = database_open();
        $sql="SELECT general.task_list.task_id,general.task_list.work_name,general.task_list.description,general.task_list.deadline_date,general.task_list.current_description,general.task_list.approval_status,general.task_list.task_status,general.task_list.finished_percentage,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name FROM general.task_list JOIN general.staff_basic_info ON general.task_list.staff_id=general.staff_basic_info.staff_id and general.task_list.parent_task_id=:parent_id and general.task_list.type='Sub-Task'";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':parent_id',$parent_id);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
            {
                $rdate=$row['deadline_date'];
                $sql="SELECT revised_date from general.task_revision_date where task_id=:id";
                $stmt1 = $conn->prepare($sql); 
                $stmt1->bindParam(':id',$row['task_id']);
                $stmt1->execute();
                $row1 =$stmt1->rowCount();
                if($row1>0)
                {
                    $result=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                    $rdate=$result[1]["revised_date"]."(R".$row1.")";
                } 
                $astatus=$row['approval_status'];
                $estatus="";
                if($astatus=="None")
                {
                    if($acc_user==$row['head_id'])
                    {
                        $astatus="Approve";
                    }
                    else{ $astatus="Waiting for Approval";}
                }
                else
                {
                    $astatus=$row['current_description'];
                    if($acc_user!=$row['head_id']){$estatus="Yes";}
                  
                }
                $name=$row['saluation']."".$row['last_name'].".".$row['first_name'];
                $json[$sno] = array(
                    'task_id' => $row['task_id'],
                    'work_name' => $row['work_name'],
                    'description' => $row['description'],
                    'responsibile' => $name,
                    'deadline_date' => $rdate ,
                    'current_description' => $row['current_description'],
                    'approval_status' => $row['approval_status'],
                    'astatus' => $astatus, 'estatus' => $estatus,
                    'task_status' => $row['task_status'],
                    'finished_percentage' => $row['finished_percentage']);
                    $sno++;
            }
        database_close($conn);
       echo json_encode($json);
    }
    
function revise_task()
{
    session_start();
    $type="Error";$msg="";
    try
    {
        if(isset($_POST['reason'])){  $reason = $_POST['reason']; }else{$msg="Reason was not posted";}
        if(isset($_POST['rdead'])){ $dead = $_POST['rdead']; }else{$msg="Revised Deadline was not posted";}
        $task_id=$_POST['parameter'];
        if($msg=="")
        {
            $approve_status="Approved";
            $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
            $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
            $update_time=get_datetime();
             $conn = database_open();
            $sql="SELECT task_id from general.task_revision_date where task_id=:id and revised_date=:date";
            $stmt = $conn->prepare($sql); 
            $stmt->bindParam(':id', $task_id);
            $stmt->bindParam(':date', $dead);
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row==0)
            {
                $sql="insert into general.task_revision_date values(:id,:date,:reason,:by,:session,:time)";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':id', $task_id);
                $stmt->bindParam(':reason', $reason);
                $stmt->bindParam(':date', $dead);
                $stmt->bindParam(':by', $update_by);
                $stmt->bindParam(':session', $update_session);
                $stmt->bindParam(':time',$update_time);
                if ($stmt->execute() == TRUE) 
                {
                    $desc="Task Deadline is extended upto ".$dead." due to ".$reason;
                    $type="Deadline Revision";

                    $parent_id="";$task_type="";
                    $sql="select type,parent_task_id from general.task_list where task_id=:task_id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':task_id',$task_id);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                        $result = $stmt -> fetch();
                        $task_type=$result["type"];
                        $parent_id=$result["parent_task_id"];
                    }

                    task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time);
                   
                    $type="Success";$msg="The Information Submitted Successfully";
                }
                else {$msg=mysqli_error($conn);}
            }else{$msg="The Revised Date is already found";}
            database_close($conn);
        }
    }catch(PDOException $e){$msg=$e->getMessage();}
    $arr = ["result" => $type, "Message" => $msg];
    echo json_encode($arr);
}
    function approve_task()
    {
        $task_id=$_POST['parameter'];
        session_start();
        $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
        $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
        echo task_approval($task_id,$update_by,$update_session);
        
    }
    function update_task_status()
    {
        session_start();
        $type="Error";$msg="";
        try
        {
            if(isset($_POST['CDesc'])){  $description = $_POST['CDesc']; }else{$msg="Current Description was not posted";}
            if(isset($_POST['WPer'])){ $per = $_POST['WPer']; }else{$msg="Completed Percentage was not posted";}
            if(isset($_POST['stat'])){  $stat = $_POST['stat']; }else{$msg="Work Status was not posted";}

            $task_id=$_POST['parameter'];
            if($msg=="")
            {
                $update_by=encrypt_decrypt("decrypt",$_SESSION["user_id"]);
                $update_session=encrypt_decrypt("decrypt",$_SESSION["session_id"]);
                $update_time=get_datetime();
                $conn = database_open();

                $sql="update general.task_list set current_description=:cdesc,task_status=:stat,finished_percentage=:per,updated_by=:by,updated_session=:session,updated_time=:time where task_id=:id";
                $stmt = $conn->prepare($sql); 
                $stmt->bindParam(':cdesc', $description);
                $stmt->bindParam(':stat', $stat);
                $stmt->bindParam(':per', $per);
                $stmt->bindParam(':by', $update_by);
                $stmt->bindParam(':session', $update_session);
                $stmt->bindParam(':time',$update_time);
                $stmt->bindParam(':id', $task_id);
                if ($stmt->execute() == TRUE) 
                {
					
					if($stat=="Completed" and $per=="100")
                        {
							
							$sql1="SELECT general.task_list.task_id,general.task_list.staff_id,general.task_list.dept_id,general.task_list.work_name,CONCAT(general.staff_basic_info.saluation,general.staff_basic_info.last_name,'.',general.staff_basic_info.first_name) AS StaffNAME,general.department_list.head_id FROM general.staff_basic_info JOIN general.task_list ON general.task_list.staff_id=general.staff_basic_info.staff_id JOIN general.department_list ON general.task_list.dept_id=general.department_list.dept_id WHERE general.task_list.task_id=:tid ";
                            $stmt1 = $conn->prepare($sql1); 
                            $stmt1->bindParam(':tid', $task_id);
                            $stmt1->execute();
							$row1c =$stmt1->rowCount();
							$row1f = $stmt1 -> fetch();
							
                            $sql="SELECT CONCAT(general.staff_basic_info.saluation,general.staff_basic_info.last_name,'.',general.staff_basic_info.first_name) AS 
							HeadNAME,general.staff_basic_info.official_mail FROM general.staff_basic_info JOIN general.department_list ON general.staff_basic_info.staff_id=general.department_list.head_id WHERE general.department_list.head_id=:hid";
                            $stmt = $conn->prepare($sql); 
                            $stmt->bindParam(':hid', $row1f["head_id"]);
                            $stmt->execute();
                            $rowc =$stmt->rowCount();
                            if($rowc>0 and $row1c>0)
                            {
                                $rowf = $stmt -> fetch();
                                $hname=$rowf["HeadNAME"];
								$hemail=$rowf["official_mail"];
								$fname=$row1f["StaffNAME"];
								$workname=$row1f["work_name"];
                                $subject="Task Update - Reg.";
                                $message="Dear ".$hname.",<br>Work assigned to $fname has been completed<br>Name of work : $workname";
                                sendmail($subject,$message,$hemail);
                            }
else{
echo "<script>alert('Mail not sent');</script>"; 
}	
							
                        }
					
                    $desc=$stat." - ".$description."(".$per."% completed)";
                    $type="Task Progress";

                    $parent_id="";$task_type="";
                    $sql="select type,parent_task_id from general.task_list where task_id=:task_id";
                    $stmt = $conn->prepare($sql); 
                    $stmt->bindParam(':task_id',$task_id);
                    $stmt->execute();
                    $row =$stmt->rowCount();
                    if($row>0)
                    {
                        $result = $stmt -> fetch();
                        $task_type=$result["type"];
                        $parent_id=$result["parent_task_id"];
                    }

                    $upload_id=task_timeline_update($conn,$parent_id,$task_type,$task_id,$type,$desc,$update_by,$update_session,$update_time);
                    $fileName=""; $fileType="";
                    if(isset($_FILES['file_to_upload']['tmp_name']))
                    {
                        $fileTmpPath    = $_FILES['file_to_upload']['tmp_name'];
                        $fileName       = $_FILES['file_to_upload']['name'];
                        $fileSize       = $_FILES['file_to_upload']['size'];
                        $fileType       = $_FILES['file_to_upload']['type'];
                        $fileNameCmps   = explode(".", $fileName);
                        $fileExtension  = strtolower(end($fileNameCmps));
                      //  if($fileSize<=2048)
                        {
                           move_uploaded_file($fileTmpPath, path_location()."general\\task\\".$upload_id.".".$fileExtension);
                           $sql="insert into general.task_uploaded values(:id,:name,:type,:by,:session,:time)";
                           $stmt = $conn->prepare($sql); 
                           $stmt->bindParam(':id', $upload_id);
                           $stmt->bindParam(':name', $fileName);
                           $stmt->bindParam(':type',$fileExtension);
                           $stmt->bindParam(':by', $update_by);
                           $stmt->bindParam(':session', $update_session);
                           $stmt->bindParam(':time',$update_time);
                           $stmt->execute(); 
                           $msg="The Information Submitted Successfully";
                        } //else{$msg="The Document Size Must be less than 2 MB";}
                    }else{$msg="The Information Submitted Successfully";}
                    $type="Success";

                }
                else {$msg=mysqli_error($conn);}
                database_close($conn);
            }
        }catch(Exception $e){$msg=$e->getMessage();}
        $arr = ["result" => $type, "Message" => $msg];
        echo json_encode($arr);  
    }
    function work_nature()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $stype=$_POST['type'];
        $json = array();
        $conn = database_open();
        $sql="select work_name from general.work_nature where type=:type";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':type', $stype);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $json[$sno] = array('name' => $row['work_name']);
            $sno++;
        }
        database_close($conn);
        echo json_encode($json);
    }
    function sub_work_nature()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $stype=$_POST['type'];
        $json = array();
        $conn = database_open();
        $sql="select work_name from general.subtask_nature where type=:type";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':type', $stype);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $json[$sno] = array('name' => $row['work_name']);
            $sno++;
        }
        database_close($conn);
        echo json_encode($json);
    }
    function task_timeline()
    {
        //https://codepen.io/thecodix/pen/PPePjm
        //glyphicon-grain,glyphicon-print,glyphicon-record,glyphicon-flag,glyphicon-screenshot,glyphicon-level-up,
        //glyphicon-phone-alt,glyphicon-education,glyphicon-picture,glyphicon-briefcase,glyphicon-plane,glyphicon-flag
         header("Content-Type: application/json; charset=UTF-8");
        $task_id=$_POST['parameter'];
        $json = array();
        $conn = database_open();
        $sql="SELECT general.task_timeline.line_id,general.task_timeline.task_type,general.task_timeline.type,general.task_timeline.description,general.task_timeline.updated_time,general.task_uploaded.upload_id,general.task_uploaded.document_name,general.task_uploaded.document_type,CONCAT(general.staff_basic_info.saluation,general.staff_basic_info.last_name,'.',general.staff_basic_info.first_name) AS name,general.task_uploaded.document_name,general.task_uploaded.document_type FROM general.task_timeline LEFT JOIN general.staff_basic_info ON general.staff_basic_info.staff_id=general.task_timeline.updated_by LEFT JOIN general.task_uploaded ON general.task_uploaded.upload_id=general.task_timeline.line_id WHERE general.task_timeline.task_id=:id OR general.task_timeline.parent_id=:id";
        $stmt = $conn->prepare($sql); 
        $stmt->bindParam(':id', $task_id);
        $stmt->execute();
        $sno=0;
        while($row = $stmt->fetch(PDO::FETCH_BOTH))
        {
            $fdocu=$row["upload_id"].".".$row["document_type"]; 
            $gly="";
            $type=$row['type'];
            if($type=="Task Creation"){$gly="glyphicon-grain timeline-badge.work";$type="Initiate";}
            else if($type=="Task Approval"){$gly="glyphicon-level-up";$type="Approve";}
            else if($type=="Task Progress"){$gly="glyphicon-flag";$type="Progress";}
            else if($type=="Deadline Revision"){$gly="glyphicon-phone-alt";$type="Deadline";}
                
            $json[$sno] = array(
                'id' => $type,
                'type' => $type,
                'glyphicon'=> $gly,
                'title'=> $row['type']." - ".$row['updated_time'],
                'updated_by'=> $row['name'],
                'text'=> $row['description'], 'download'=> $row["document_name"],'document_name'=>$fdocu);
            $sno++;
        }
        database_close($conn);
        echo json_encode($json); 
    }
    function download_preparation()
    {
        session_start();
        $_SESSION["download_docname"]=$_POST['parameter1'];
        $_SESSION["download_path"]=encrypt_decrypt("encrypt",path_location()."general\\task\\".$_POST['parameter']);
    }  
    ?>