<?php
    require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/timeout.php");
    require_once($_SERVER['DOCUMENT_ROOT'] ."/krg/myphp/commonfunction.php");
    $callparameter="";
    if(isset($_POST['callvalue'])){  $callparameter = $_POST['callvalue']; }
    if($callparameter=="")
    {
      $arr = ["result" => "Redirect".$callparameter, "Message" => "/krg/login.php".$callparameter];
      echo json_encode($arr);
    }
    else
    { 
       switch($callparameter)
       {
        case "institution_list":institution_list();
                                    break;
        default:
                                    $arr = ["result" => "danger", "Message" => "Invalid Access"];
                                    echo json_encode($arr);  
                                    break;
       }
    }
       function institution_list()
        {
            header("Content-Type: application/json; charset=UTF-8");
            $json = array();
            $conn = database_open();
            $sql="SELECT general.institution_list.group_id,general.institution_list.group_name,general.institution_list.address_line1,general.institution_list.address_line2,general.institution_list.address_line3,general.institution_list.address_line4,general.institution_list.report_header,general.institution_list.rformat,general.institution_list.lformat,general.staff_basic_info.saluation,general.staff_basic_info.first_name,general.staff_basic_info.last_name FROM general.institution_list left JOIN general.staff_basic_info ON general.institution_list.head_id=general.staff_basic_info.staff_id order by general.institution_list.group_id asc";
            $stmt = $conn->prepare($sql); 
            $stmt->execute();
            $row =$stmt->rowCount();
            if($row)
            {
                $image_loc=path_location()."\\general\\institution\\";
                $sno=0;
                while($row = $stmt->fetch(PDO::FETCH_BOTH))
                {
                    $head_name=$row["saluation"].$row["last_name"].".".$row["first_name"];
                    $path = $image_loc.$row['group_id']."r.". $row['rformat']; $data = file_get_contents($path);
                    $rbase64 = 'data:image/' . $row['rformat'] . ';base64,' . base64_encode($data);

                    $path = $image_loc.$row['group_id']."l.". $row['lformat']; $data = file_get_contents($path);
                    $lbase64 = 'data:image/' . $row['lformat'] . ';base64,' . base64_encode($data);

                    $json[$sno] = array(
                     'group_id' => $row['group_id'],
                     'group_name' => $row['group_name'],
                     'head_name' =>rtrim($head_name,"."),
                     'address_line1' => $row['address_line1'],
                     'address_line2' => $row['address_line2'],
                     'address_line3' => $row['address_line3'],
                     'address_line4' => $row['address_line4'],
                     'report_header' => $row['report_header'],
                     'rsource'=>$rbase64,'lsource'=>$lbase64);
                    $sno++;
                }
            }
            database_close($conn);
            echo json_encode($json);
        }
        ?>
        
