angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $scope.showHints = true; 
    $scope.listVisible=true; $scope.registervisible=false;$scope.timeline_task=false;$scope.head_update=false;
    $('.form_date').datetimepicker({ Position: "relative", language:  'fr', weekStart: 1, todayBtn:  1,autoclose: 1,todayHighlight: 1,	startView: 2,minView: 2,forceParse: 0});
    
    $scope.set_color = function (payment) {
        if (payment == "Not Started") { return { background: '#e1e9f4' } }
        else if (payment == "Pending") { return { background: '#f9cdc2' } }
        else if (payment == "Ongoing") { return { background: '#f9f9d4' } }
        else if (payment == "Completed") { return { background: '#c3f4c4' } }
		else if (payment == "Overdue") { return { background: '#ff4d4d' } }
             else { return { background: '#ff6666' }} 		
		
      }
    $scope.activities = 
      [{id:0,   text: "Initiate",   class: "icon-address",        active: "active"},
       {id:1,   text: "Approve",  class: "icon-graduation-cap", active: "active"},
       {id:2,   text: "Progress",   class: "icon-briefcase",      active: "active"},
       {id:3,   text: "Deadline",  class: "icon-user",      active: "active"},

      ];

    $scope.toggleAllActivities = function () {
      $scope.activities.map(function (activity) {
        activity.active = "active";
      });
      $scope.refreshEvents();
    }
     
    $scope.selectActivity = function (selected) {
      $scope.activities.map (function (activity) {
         if(activity.id === selected.id) {
           activity.active = "active";
         } else { 
           activity.active = null;
         }
      });
      $scope.refreshEvents();
    }
  
    $scope.refreshEvents = function () {
      $scope.active_events = [];
      $scope.events.map(function (event) {
        $scope.activities.map(function (activity) {
          if(activity.text == event.type && activity.active == "active") {
            $scope.active_events.push(event);
          }
        });
      });
    }; 

    $scope.searchTerm;
    $scope.clearSearchTerm = function() {$scope.searchTerm = ''; };
    $element.find('input').on('keydown', function(ev) { ev.stopPropagation(); });

    $scope.IsApproved = function (value) {
        var index=-1;
        $scope.task.some(function (obj, i) { return obj.task_id === value ? index = i : false; });
        if(index!=-1)
        {
           if($scope.task[index].approval_status==="None" && $scope.task[index].astatus==="Approve"){return true;}else{return false;}
        }
    }
    $scope.IsEntered = function (value) {
        var index=-1;
        $scope.task.some(function (obj, i) { return obj.task_id === value ? index = i : false; });
        if(index!=-1)
        {
           if($scope.task[index].approval_status!="None"){return true;}else{return false;}
        }
    }
    $scope.work_status=[{name:"Not Started"},{name:"Ongoing"},{name:"Pending"},{name:"Completed"},{name:"Overdue"}];
    $scope.task_summary = function () 
    {
        var data = $.param({ callvalue: "task_summary"});
        $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
        {
            $scope.assigned_others=response.data.aothers;
            $scope.pending_others=response.data.pothers;
            $scope.completed_others=response.data.cothers;
            
            $scope.assigned_own=response.data.aown;
            $scope.pending_own=response.data.pown;
            $scope.completed_own=response.data.cown;
            
        });
    }
$scope.task_summary();
    $scope.load_task = function () 
    {
          var data = $.param({ callvalue: "task_get"});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
          {
               if($.fn.dataTable.isDataTable('#task_list')){ $('#task_list').DataTable().clear().destroy();  }
               $scope.task = response.data; 
                setTimeout(function () {
                    $(function () {
                        $('#task_list').DataTable({
                            dom: 'Blfrtip',
                            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                            responsive:{breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]}
                        });
                    });
                }, 0);
          }); 
    }
    $scope.load_subtask = function () 
    {
          var data = $.param({ callvalue: "subtask_get",parent_id:$scope.Tid});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
          {
            if($.fn.dataTable.isDataTable('#subtask_list')){ $('#subtask_list').DataTable().clear().destroy();  }
               $scope.subtask = response.data; 
               setTimeout(function () {
                   $(function () {
                       $('#subtask_list').DataTable({
                           dom: 'Bfrtip',
                           buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                           responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                               { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                               { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                               { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                               { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                               ]
                           }
                       });
                   });
               }, 0);
          });
    };
    $scope.load_faculty = function () 
    {
        var data = $.param({ callvalue: "allfaculty"});
        $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
        {
            var table_id='#staff_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.faculty = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0); 
        });
    };
    $scope.load_faculty();
    $scope.timeline_data = function (value) 
    {
          var data = $.param({ callvalue: "timelinetask",parameter:value});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
          {
               $scope.task_timeline = response.data; 
               $scope.events = response.data;
               $scope.refreshEvents();
          });
    };
    $scope.download_document= function (value1,value2) 
    {
          var data = $.param({ callvalue: "downloadprep",parameter:value2,parameter1:value1});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
          {
            window.open('/krg/download.php','_blank');
          });
    };
    $scope.work_nature = function () 
    {
          var data = $.param({ callvalue: "work_nature",type:"Work Load"});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) { $scope.wnature = response.data; });
    };
    $scope.subwork_nature = function () 
    {
          var data = $.param({ callvalue: "subwork_nature",type:"Work Load"});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) { $scope.subnature = response.data; });
    };
    $scope.load_task();
    $scope.registration_list=function(){$scope.listVisible=true; $scope.registervisible=false;};
    $scope.registration_list1=function(){$scope.listVisible=true; $scope.update_task=false;$scope.timeline_task=false;};
    $scope.task_register_clear=function()
    {
        $scope.WName='';$scope.WDesc='';$scope.dead='';$scope.wtype='';
    };
    $scope.task_update_clear=function()
    {
        $scope.CDesc='';$scope.WPer='';$scope.stat='';$scope.Tid='';
        $scope.reason=''; $scope.rdead=''; $("#file_to_upload").val(null);
    };
    $scope.Approve=function(value)
    {
        Pace.stop();
        Pace.bar.render();
        var form_data = new FormData();
        form_data.append('callvalue', "task_approve");
        form_data.append('parameter', value);
        $.ajax({
            type: "POST", url: "/krg/myphp/general/workload.php", datatype: 'json',data:form_data,
            contentType: false,
            cache: false,
            processData: false,

        }).then(function (response) {
           // alert(response);
            var myResult = JSON.parse(response);
            var msg_type = "danger";
            var message = myResult.Message;
            if (myResult.result == "Success") {  $scope.load_task(); msg_type = "success"; }
            message_display(msg_type, "Task Approval", message);Pace.stop();
        });
        
    }
    $scope.NewTask=function(value)
    {
        $scope.listVisible=false; $scope.registervisible=true;
        $scope.task_register_clear();
        $scope.work_nature();
        $scope.getmyid();
        
    }
    $scope.sub_clear=function(){$scope.subname='';$scope.subdesc='';$scope.subresp='';$scope.subtype='';$scope.sub_dead='';};
    $scope.EditTask=function(value)
    {
        $scope.listVisible=false; $scope.update_task=true;$scope.timeline_task=true; $scope.sub_clear();
        $scope.task_update_clear();$scope.subwork_nature();
        var index = -1;
        $scope.task.some(function (obj, i) 
        { return obj.task_id === value ? index = i : false; });
        if(index!=-1)
        {
            $scope.Tid=$scope.task[index].task_id;
            $scope.CDesc=$scope.task[index].current_description;
            $scope.WPer=Number($scope.task[index].finished_percentage);
            $scope.stat=$scope.task[index].task_status;
            $scope.disp_task_name=$scope.task[index].work_name;
            $scope.responsibile=$scope.task[index].responsibile;
        }
        $scope.timeline_data($scope.Tid);  $scope.load_subtask();
        
    }
    $scope.Fixhead=function(value)
    {
        var index = -1;
        $scope.faculty.some(function (obj, i){ return obj.faculty_id === value ? index = i : false; });
        if(index!=-1)
        {
            if($scope.select_type=="task"){$scope.taskresp=$scope.faculty[index].name;$scope.task_fid=$scope.faculty[index].faculty_id;}
            else if($scope.select_type=="subtask"){$scope.subresp=$scope.faculty[index].name;$scope.sub_fid=$scope.faculty[index].faculty_id;}
        }
        $scope.back_to_home();
    }
    $scope.head_view=function(value)
    {
        if(value=="task"){$scope.taskresp='';$scope.task_fid=''; $scope.registervisible=false;$scope.head_update=true;$scope.select_type="task";}
        else if(value=="subtask"){$scope.subresp='';$scope.sub_fid=''; $scope.timeline_task=false; $scope.update_task=false;$scope.head_update=true;$scope.select_type="subtask";}
    }
    $scope.back_to_home=function()
    {
        if($scope.select_type=="task"){ $scope.registervisible=true;$scope.head_update=false;}
        else if($scope.select_type=="subtask"){$scope.timeline_task=true; $scope.update_task=true;$scope.head_update=false;}
    }
    $scope.getmyid = function () 
    {
          var data = $.param({ callvalue: "getmyid"});
          $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
          {
              $scope.select_type="task";$scope.Fixhead(response.data.staff_id);
          });
    };
    var data = $.param({ callvalue: "getmyresp"});
    $http.post('/krg/myphp/general/workload.php',data,config).then(function (response) 
    {
        $scope.user_type=response.data.change;
    });
    $scope.timeline_back=function()
    {
        $scope.listVisible=true; $scope.timeline_task=false;
    }
    $scope.ViewTimeline=function(value)
    {
        var index = -1;
        $scope.task.some(function (obj, i) 
        { return obj.task_id === value ? index = i : false; });
        if(index!=-1)
        {
            $scope.Tid=$scope.task[index].task_id;
            $scope.disp_task_name=$scope.task[index].work_name;
            $scope.responsibile=$scope.task[index].responsibile;
        }
        $scope.listVisible=false; $scope.timeline_task=true;
        $scope.timeline_data(value);
    }
    $(document).ready(function (e) {
        $("#subtask").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue', "subtask_register");
            form_data.append('parent_id', $scope.Tid);
            form_data.append('sub_fid', $scope.sub_fid);
            $.ajax({
                type: "POST", url: "/krg/myphp/general/workload.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { $scope.load_subtask();$scope.task_summary(); $scope.sub_clear();$scope.timeline_data($scope.Tid);   msg_type = "success"; }
                message_display(msg_type, "Task Registration", message); Pace.stop();
            });
        });
    });
    $(document).ready(function (e) {
        $("#taskRegister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue', "task_register");
            $.ajax({
                type: "POST", url: "/krg/myphp/general/workload.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { $scope.task_register_clear();$scope.task_summary(); $scope.load_task(); $scope.listVisible=true; $scope.registervisible=false;   msg_type = "success"; }
                message_display(msg_type, "Task Registration", message); Pace.stop();
            });
        });
    });
    $(document).ready(function (e) {
        $("#taskupdate").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue', "task_status");
            form_data.append('parameter',$scope.Tid);
            $.ajax({
                type: "POST", url: "/krg/myphp/general/workload.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { $scope.load_task();$scope.task_summary(); $("#file_to_upload").val(null);  $scope.timeline_data($scope.Tid); msg_type = "success"; }
                message_display(msg_type, "Task Status Updation", message); Pace.stop();
            });
        });
    });
    $(document).ready(function (e) {
        $("#revisetask").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue', "revise_task_status");
            form_data.append('parameter',$scope.Tid);
            $.ajax({
                type: "POST", url: "/krg/myphp/general/workload.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
               // alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { $scope.load_task();  $scope.timeline_data($scope.Tid); msg_type = "success"; }
                message_display(msg_type, "Task Deadline Revision", message); Pace.stop();
            });
        });
    });

    
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}
$("#file_to_upload").change(function () {
    var fileExtension = ['pdf'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only formats are allowed : "+fileExtension.join(', '));this.value = '';
    }
    else
    {
        //if(this.files[0].size<=20)
        //{

        //}else{alert("The File Size Must be less than 5 MB"); this.value = '';}
    }
});

