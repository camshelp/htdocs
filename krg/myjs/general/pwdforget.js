angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
     $scope.showHints = true; 
      $scope.krg_home=function()
      {
        window.location="/krg/login.php";
      }
     $(document).ready(function (e) {
        $("#updatepwd").on('submit', function (e) {
            e.preventDefault();
           Pace.stop();
           Pace.bar.render();
            var form_data=new FormData(this);
            form_data.append('callvalue','password_forget');
            $.ajax({
                type: "POST", url: "/krg/myphp/general/loginindex.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                var myResult = JSON.parse(response);
                var message = myResult.Message; 
                if(myResult.result=="success"){$scope.mail='';}
                message_display(myResult.result,"KRG Portal - Home",message);
                Pace.stop();
            });
        });
    });

});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}