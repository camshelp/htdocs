
angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $scope.showHints = true; $scope.add_button=false;
    $scope.listVisible=true; $scope.RegisterVisible=false;$scope.dept_button=false;
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	
    $scope.editFaculty=function(value)
    {

		$scope.funcName = "load_faculty";
		$http.post(
                "/krg/myphp/general/editfaculty.php", {
                    'facultyid': value,
					'funcName': $scope.funcName
                }
				).then(function successCallBack(response){
										
					$scope.sal = response.data[0].saluation;
					$scope.searchText = response.data[0].first_name;
					$scope.LName = response.data[0].last_name;
					$scope.desig = response.data[0].designation;
					$scope.gender = response.data[0].gender;
					$scope.mobile = response.data[0].mobile_no;
					$scope.mail = response.data[0].official_mail;
					$scope.faculty_roles = response.data[0].roles;
					
					$scope.Iid = response.data[0].group_id;
					$scope.did = response.data[0].dept_id;
					$scope.Faculty_id = value;
					
					$scope.toload_IName = response.data[0].group_id;
					$scope.toload_DeptName = response.data[0].dept_id;
					
					$scope.load_deptcomp_data();
					
				}, function errorCallBack(response){
					alert("Something went wrong!");
			})		
			
    }
	
	$scope.load_deptcomp_data=function()
    {
        
		$scope.funcName = "load_deptcomp";
			$http.post(
                "/krg/myphp/general/editfaculty.php", {
                    'instid': $scope.toload_IName,
					'deptid': $scope.toload_DeptName,
					'funcName': $scope.funcName
                }
			).then(function successCallBack(response){
										
				$scope.IName = response.data[0].group_name;
				$scope.DeptName = response.data[1].course_name;
				
				$scope.listVisible=false; $scope.RegisterVisible=true;
				$scope.editbtn = true;
				$scope.registerbtn = false;
									
			}, function errorCallBack(response){
					alert("Something went wrong!");
		})	
		
    }
	
	$scope.faculty_update=function()
    {
        
		alert($scope.faculty_roles);
		$scope.funcName = "update_faculty";
		$http.post(
                "/krg/myphp/general/editfaculty.php", {
                    'facultyid': $scope.Faculty_id,
					'saluation': $scope.sal,
					'firstname': $scope.searchText,
					'lastname': $scope.LName,
					'designation': $scope.desig,
					'gender': $scope.gender,
					'mobileno': $scope.mobile,
					'email': $scope.mail,
					'roles': $scope.faculty_roles,
					'funcName': $scope.funcName
                }
				).then(function successCallBack(response){
					alert("Faculty updated successfully!");
					
				}, function errorCallBack(response){
					alert("Something went wrong!");
			})	
		
    }
   
    $scope.load_institutions = function () 
    {
        var data = $.param({ callvalue: "institution_list"});
        $http.post('/krg/myphp/general/company.php',data,config).then(function (response) 
        {
            $scope.institution = response.data;
            $scope.no_company= $scope.institution.length;
        });
    };
    $scope.load_dept=function(group_name)
    {
        var data = $.param({ callvalue: "dept_list",'group_name':group_name});$scope.dept_button=true;
        $http.post('/krg/myphp/general/department.php',data,config).then(function (response) 
        {
            $scope.dept = response.data;
            $scope.no_dept= $scope.dept.length;
        });
    }

    var data = $.param({ callvalue: "designation_list"});
    $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
    {
        $scope.designation = response.data;
    });
    var data = $.param({ callvalue: "gender_list"});
    $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
    {
        $scope.gender_type = response.data;
    });
    var data = $.param({ callvalue: "saluation_list"});
    $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
    {
        $scope.saluation = response.data;
    });

    $scope.load_faculty=function(value)
    {
		alert("staff register load faculty...");
        var data = $.param({ callvalue: "faculty_list",gname:$scope.gname,dname:value});
        $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
        {
            var table_id='#data_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.staff_data=response.data;
            $scope.no_staff= $scope.faculty.length;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'Bflrtip',
                        buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                              { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                              { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                              { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                              { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
        });
    }
<<<<<<< HEAD
	$scope.dept_load = function () 
=======
    $scope.registration_list=function(){ $scope.listVisible=true; $scope.RegisterVisible=false; $scope.faculty_roles = null; $scope.searchText = null;};
    $scope.load_institutions();
    $scope.searchTerm;
    $scope.clearSearchTerm = function() {$scope.searchTerm = ''; };
    $element.find('input').on('keydown', function(ev) { ev.stopPropagation(); });
    $scope.faculty_register_clear=function()
>>>>>>> 0b7f5c4b7a2ff83cbd3b8d664847ad86539f5245
    {
		alert("department load faculty...");
        var data = $.param({ callvalue: "allfaculty"});
        $http.post('/krg/myphp/general/staff.php',data,config).then(function (response) 
        {
            var table_id='#staff_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.faculty = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0); 
        });
    };
<<<<<<< HEAD
    $scope.dept_load();
	
	$scope.registration_list=function(){$scope.listVisible=true; $scope.RegisterVisible=false;};
	$scope.registration_list1=function(){$scope.listVisible=true; $scope.update_task=false;$scope.timeline_task=false;};
	
	$scope.EditFaculty=function(value)
=======
    $scope.option_click=function(group_name)
    {
        $scope.dept=null;$scope.faculty=null;  
        $scope.load_dept(group_name);$scope.add_button=false; 
    }
    $scope.option_click1=function(dept_id)
    {
        $scope.add_button=true;$scope.faculty=null; 
        $scope.load_faculty(dept_id);     
    }
    $scope.EditFaculty=function(value)
>>>>>>> 0b7f5c4b7a2ff83cbd3b8d664847ad86539f5245
    {
        $scope.listVisible=false; $scope.RegisterVisible=true;$scope.faculty_register_clear();
        var index = -1;
        $scope.institution.some(function (obj, i) { return obj.group_id === $scope.gname ? index = i : false; });
        if(index!="-1")
        {   $scope.Iid = $scope.institution[index].group_id;
            $scope.IName = $scope.institution[index].group_name;
        }
        index = -1;
        $scope.dept.some(function (obj, i) {return obj.dept_id === $scope.dname ? index = i : false;});
        if(index != -1)
        {  
             $scope.did = $scope.dept[index].dept_id;
             $scope.DeptName = $scope.dept[index].course_name;
        }
<<<<<<< HEAD
    }
	
	
	//new start
	$scope.EditTask=function(value)
    {
        $scope.listVisible=false; $scope.update_task=true;$scope.timeline_task=true; $scope.sub_clear();
        $scope.task_update_clear();$scope.subwork_nature();
        var index = -1;
        $scope.task.some(function (obj, i) 
        { return obj.task_id === value ? index = i : false; });
        if(index!=-1)
        {
            $scope.Tid=$scope.task[index].task_id;
            $scope.CDesc=$scope.task[index].current_description;
            $scope.WPer=Number($scope.task[index].finished_percentage);
            $scope.stat=$scope.task[index].task_status;
            $scope.disp_task_name=$scope.task[index].work_name;
            $scope.responsibile=$scope.task[index].responsibile;
        }
        $scope.timeline_data($scope.Tid);  $scope.load_subtask();
        
    }
	
	
    $scope.Department_register=function(){ $scope.listVisible=true; $scope.update_task=false;};
	
	$scope.EditDepartment=function(value)
    {
        $scope.listVisible=false; $scope.update_task=true;$scope.faculty_register_clear();
        var index = -1;
        $scope.institution.some(function (obj, i) { return obj.group_id === $scope.gname ? index = i : false; });
        if(index!="-1")
        {   $scope.Iid = $scope.institution[index].group_id;
            $scope.IName = $scope.institution[index].group_name;
        }
        index = -1;
        $scope.dept.some(function (obj, i) {return obj.dept_id === $scope.dname ? index = i : false;});
        if(index != -1)
        {  
             $scope.did = $scope.dept[index].dept_id;
             $scope.DeptName = $scope.dept[index].course_name;
        }
    }
	
	
	$scope.head_update=false;
	$scope.Fixhead=function(value)
    {
        var index = -1;
        $scope.faculty.some(function (obj, i){ return obj.faculty_id === value ? index = i : false; });
		
        if(index!=-1)
        {
            if($scope.select_type=="task"){$scope.taskresp=$scope.faculty[index].name;$scope.task_fid=$scope.faculty[index].faculty_id;}
            else if($scope.select_type=="subtask"){$scope.subresp=$scope.faculty[index].name;$scope.sub_fid=$scope.faculty[index].faculty_id;}
        }
        $scope.back_to_home();
    }    
    $scope.company_head_view=function()
    {
        $scope.taskresp='';$scope.task_fid=''; $scope.RegisterVisible=false;$scope.head_update=true;$scope.select_type="task";
    }
	
	$scope.dept_head_view=function()
    {
		$scope.subresp='';$scope.sub_fid=''; $scope.update_task=false;$scope.head_update=true;$scope.select_type="subtask";
    }
	
    $scope.back_to_home=function()
    {
        if($scope.select_type=="task"){ $scope.RegisterVisible=true;$scope.head_update=false;}
        else if($scope.select_type=="subtask"){$scope.timeline_task=true; $scope.update_task=true;$scope.head_update=false;}
    }	
	
	//new end
	
	
    $scope.load_institutions();
    $scope.searchTerm;
    $scope.clearSearchTerm = function() {$scope.searchTerm = ''; };
    $element.find('input').on('keydown', function(ev) { ev.stopPropagation(); });
    $scope.faculty_register_clear=function()
    {
        $scope.Iid='';$scope.did='';$scope.IName='';$scope.DeptName='';$scope.sal='';$scope.FName='';$scope.LName='';$scope.desig='';$scope.doj='';
        $scope.dob='';$scope.gender='';$scope.aadhar='';$scope.mobile='';$scope.mail='';
    };
    $scope.option_click=function(group_name)
    {
        $scope.dept=null; 
        $scope.load_dept(group_name);$scope.add_button=false; 
    }
    $scope.option_click1=function(dept_id)
    {
        $scope.add_button=true;$scope.faculty=null; 
        $scope.load_faculty(dept_id);     
    }
    
=======
		$scope.registerbtn = true;
		$scope.editbtn = false;
        
    }
	

>>>>>>> 0b7f5c4b7a2ff83cbd3b8d664847ad86539f5245
	
    $(document).ready(function (e) {
        $("#FacultyRegister").on('submit', function (e) {
            //alert("123");
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data=new FormData(this);
            form_data.append('callvalue','faculty_register');
            $.ajax({
                type: "POST", url: "/krg/myphp/general/staff.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
               // alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") 
                {
                     $scope.load_faculty($scope.dname);$scope.faculty_register_clear(); $scope.listVisible=true; $scope.RegisterVisible=false; 
                     msg_type = "success";
                 }
                message_display(msg_type, "Faculty Registration", message); Pace.stop();
            });
        });
        
    });
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
<<<<<<< HEAD
}
=======
}
>>>>>>> 0b7f5c4b7a2ff83cbd3b8d664847ad86539f5245
