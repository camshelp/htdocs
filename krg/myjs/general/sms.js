angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
$scope.register_list=true;$scope.upload_list=false;
$scope.clear_entry=function(){$scope.type='';$scope.desc=''; $("#file_to_upload").val(null);};
$scope.clear_sms_form=function(){$scope.uid='';$scope.cate='';$scope.sdesc='';$scope.sheet='';$scope.update='';$scope.sname='';};
$scope.searchTerm;
$scope.clearSearchTerm = function() {$scope.searchTerm = ''; };
$element.find('input').on('keydown', function(ev) { ev.stopPropagation(); });
$scope.load_values=function()
{
    var data = $.param({ callvalue: "list_excel"});
    $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
     {
       $scope.list = response.data; 
       $scope.no_files=$scope.list.length;
    }); 
};
$scope.load_sheets=function()
{
    var data = $.param({ callvalue: "sheets",param:$scope.uid,sname:$scope.sheet});
    $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
    {
        $scope.sheets = response.data;
    });
};
$scope.load_data=function(value){
    var data = $.param({ callvalue: "balsms",id:value});
    $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
     {
        $scope.utcount=response.data.sms_balance;
        if(value=="all")
        {
            $scope.no_sent=response.data.sms_sent;
            $scope.no_fail=response.data.sms_failed;
        }
        else
        {
            $scope.no_sentf=response.data.sms_sent;
            $scope.no_failf=response.data.sms_failed;
        }
    }); 
    if(value!="all")
    {
        var data = $.param({ callvalue: "historysms",param:$scope.uid});
        $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
        {
            $scope.sms_hist=response.data;
        }); 
        $scope.chart_load();
    }
};
$scope.load_data("all");
$scope.chart_load=function()
{
    var data1 = $.param({ callvalue: "smspie",param:$scope.uid});
    $http.post('/krg/myphp/general/common1.php',data1,config).then(function (response) 
    {
        $scope.chart_data = response.data;
        var chart = c3.generate({
            data: {
               json: $scope.chart_data,
                type : 'pie',
                onclick: function (d, i) { $scope.sms_student_list(d.id); },
            }, bindto: '#chart'
        });
    });
}
$scope.sms_student_list=function(value)
{
    var data = $.param({ callvalue: "smsstudents",id:$scope.uid,type:value});
    $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
     {
        var table_id='#student_sms_list';
        if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
         $scope.sms_list = response.data;$scope.stcount=$scope.sms_list.length;
        setTimeout(function () {
            $(function () {
                $(table_id).DataTable({
                    dom: 'Blfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                        { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                        { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                        { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                        { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                        ]
                    }
                });
            });
        }, 0);
    }); 
};
$scope.sheet_data=function(value){ 
    var data = $.param({ callvalue: "sheetsdata",id:$scope.uid,sname:$scope.sheet,work:value});
    $http.post('/krg/myphp/general/common1.php',data,config).then(function (response) 
    {
        Pace.stop();
        Pace.bar.render();
        var table_id='#stud_list';
        if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
         $scope.students = response.data;
        setTimeout(function () {
            $(function () {
                $(table_id).DataTable({
                    dom: 'Blfrtip',
                    columnDefs: [ { orderable: false, targets:   0,'checkboxes': { 'selectRow': true } } ],
                    select: {'style': 'multi',selector: 'td:first-child' },
                    order: [[ 1, 'asc' ]],
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                        { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                        { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                        { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                        { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                        ]
                    }
                });
            });
        },0);
        Pace.stop();
    });
    $scope.load_data($scope.uid);
};

$scope.view_info=function(value)
{
    $scope.clear_sms_form();
    $scope.register_list=false;$scope.upload_list=true;
    var index=-1;
    $scope.list.some(function (obj, i) {return obj.id === value ? index = i : false;});
    if(index!=-1)
    {
        $scope.uid = $scope.list[index].id;
        $scope.cate = $scope.list[index].type;
        $scope.sdesc = $scope.list[index].description;
        $scope.sheet = $scope.list[index].sheet_name;
        $scope.update = $scope.list[index].updated_time;
        $scope.load_sheets();
        $scope.load_data($scope.uid);
    }

};
$scope.option_click=function(value){
    $scope.sheet_data(value);
}
$scope.registration_list=function(){$scope.register_list=true;$scope.upload_list=false; $scope.clear_sms_form();};
$scope.load_values();
    $(document).ready(function (e) {
        $("#excelupload").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            if($("#file_to_upload").val() != "")
		{
            var file_data = $('#file_to_upload').get(0).files[0];
            var file_data1 = $('#type').val();
            var file_data2 = $('#desc').val();
		    var form_data = new FormData();
            form_data.append('uploadedFile', file_data);
            form_data.append('type', file_data1);
            form_data.append('desc', file_data2);
            form_data.append('callvalue', "smssend");
            $.ajax({
                type: "POST", url: "/krg/myphp/general/common1.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
               // alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") 
                {
                     msg_type = "success"; $scope.load_values(); $scope.clear_entry(); }
                message_display(msg_type, "SMS Gateway - Excel Upload", message); Pace.stop();
            });
        }else{message_display("danger", "SMS Gateway - Excel Upload", 'Please Choose Excel Sheet');}
        });
    });

    $(document).ready(function (e) {
        $("#smssendform").on('submit', function (e) {
            e.preventDefault();
         var ret_data=storeTblValues();
         if(ret_data != "")
		{
            Pace.stop();
            Pace.bar.render();
            var TableData = JSON.stringify(ret_data);
            var form_data = new FormData();
            form_data.append('id', $scope.uid);
            form_data.append('category', $scope.cate);
            //form_data.append('desc', "JAN 2019");
            form_data.append('desc', $scope.sdesc);
            form_data.append('file', $scope.sheet);
            form_data.append('sheet', $scope.sname);
            form_data.append('data', TableData);
            form_data.append('callvalue', "sendsms");
            $.ajax({
                type: "POST", url: "/krg/myphp/general/common1.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") 
                {
                     msg_type = "success";$scope.sheet_data($scope.sname); $scope.clear_entry(); 
                }
                     message_display(msg_type, "SMS Gateway - SMS Send", message); Pace.stop();
            });
           
        }else{message_display("danger", "SMS Gateway - SMS Send", 'No Student Choosed');}
        });
    });

});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}
function storeTblValues() {
    var TableData = new Array();
    var table = $('#stud_list').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();
    var row=0;
    $.each(rows_selected, function(index, rowId){
        var arr= table.rows(rowId-1).data();
        TableData[row]={ "name":arr[0][1],"current":arr[0][2],"Misc":arr[0][3],"MiscDesc":arr[0][4],"Pending":arr[0][5],"Total":arr[0][6],"Mobile":arr[0][7] };
        row++;
    });
   // TableData.shift();  
   if(row==0){return "";}else{ return TableData;}
}