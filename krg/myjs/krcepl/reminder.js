angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $scope.work_status=[{name:"3M"},{name:"6M"},{name:"9M"},{name:"1Y"}];
    $scope.infovisible=false;
    $scope.load_data=function(value)
    {
        var data = $.param({ callvalue: "reminder_summary",date:value});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.icount=response.data.icount;
            $scope.iamount=response.data.iamount;
            $scope.ocount=response.data.omcount;
            $scope.oamount=response.data.omamount;
           
        });
         var data = $.param({ callvalue: "due_reminder",date:value});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
          var table_id="#machine_list";
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
             $scope.machines_list = response.data;
            if($scope.machines_list.length>0){$scope.infovisible=true;}
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        columnDefs: [ { orderable: false, targets:   0,'checkboxes': { 'selectRow': true } }, { "visible": false, "targets": 2 }, { "visible": false, "targets": 4 }, { "visible": false, "targets": 6 }, { "visible": false, "targets": 8 }, { "visible": false, "targets": 9 } ],
                        select: {'style': 'multi',selector: 'td:first-child' },
                        order: [[ 1, 'asc' ]],
                        /* buttons: ['copy', 'excel', 'pdf'], */
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            },0); 
        }); 
    }
   
    $('#machine_list tbody').on( 'click', 'button', function () {
        var table = $('#machine_list').DataTable();
        var tr=$(this).closest("tr"); ;
        var index=table.row(tr).index();
        var duration= $(tr).find('td:eq(9)').find('input').val();
        if(duration!="")
        {
                var arr= table.rows(index).data();
                var form_data = new FormData();
                form_data.append('mid',arr[0][2]);
                form_data.append('uid', arr[0][4]);
                form_data.append('lid', arr[0][6]);
                form_data.append('type', arr[0][8]);
                form_data.append('due',  arr[0][9]);
                form_data.append('duration', duration);
                form_data.append('callvalue', "approve_reminder");
                $.ajax({
                    type: "POST", url: "/krg/myphp/krcepl/machine.php", datatype: 'json', data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
    
                }).then(function (response) {
                    alert(response);
                    var myResult = JSON.parse(response);
                    var msg_type = "danger";
                    var message = myResult.Message;
                    if (myResult.result == "Success") 
                    {
                         msg_type = "success"; $scope.infovisible=false; $scope.load_data($scope.dead);
                    }
                         message_display(msg_type, "Insurance and O & M Reminder", message); 
                });
        }else{message_display("danger", "Insurance and O & M Reminder", 'Please Choose Due Duration');}
       
    });

    var data = $.param({ callvalue: "current_month"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.dead=response.data.month;
        $scope.load_data($scope.dead); 
    });

   
    $('#dead').change( function()
    {
        $scope.infovisible=false;
        $scope.load_data($scope.dead);
    });
    $scope.update_info=function()
    {

    }
    $(document).ready(function (e) {
        $("#updateform").on('submit', function (e) {
            var ret_data=storeTblValues();
            if(ret_data != "")
            {
                Pace.stop();
                Pace.bar.render();
                var TableData = JSON.stringify(ret_data);
                var form_data = new FormData(this);
                form_data.append('month_year',$scope.dead);
                form_data.append('data', TableData);
                form_data.append('callvalue', "update_reminder");
                $.ajax({
                    type: "POST", url: "/krg/myphp/krcepl/machine.php", datatype: 'json', data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
    
                }).then(function (response) {
                   // alert(response);
                    var myResult = JSON.parse(response);
                    var msg_type = "danger";
                    var message = myResult.Message;
                    if (myResult.result == "Success") 
                    {
                         msg_type = "success"; $scope.infovisible=false; $scope.load_data($scope.dead);
                    }
                         message_display(msg_type, "Insurance and O & M Reminder", message);  Pace.stop();
                });
               
            }else{message_display("danger", "Insurance and O & M Reminder", 'No Records Choosen');}
        });
    });
    $(document).ready(function (e) {
        $("#export_pdf").on('click', function (e) {
            Pace.stop();
            Pace.bar.render();
            var ret_data=storeTblValuesExport();
            if(ret_data != "")
            {
                var TableData = JSON.stringify(ret_data);
                data = $.param({ callvalue: "export_pdf","month_year":"01-"+$scope.dead,'data':TableData});
                $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
                {
                    if(response.data=='\r\n\r\n')
                    {
                         window.open('/krg/download.php','_blank');
                    }
                    else if(response.data=="No Data"){message_display("danger","Insurance and O & M Reminder","No Data Found");}
                    else{message_display("danger","Insurance and O & M Reminder","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);}
                    Pace.stop();
                });
            }else{message_display("danger", "Insurance and O & M Reminder", 'No Records Found');} 
           
        });
    });
    $(document).ready(function (e) {
        $("#export_xl").on('click', function (e) {
            Pace.stop();
            Pace.bar.render();
            var ret_data=storeTblValuesExport();
            if(ret_data != "")
            {
                var TableData = JSON.stringify(ret_data);
                data = $.param({ callvalue: "export_xls","month_year":"01-"+$scope.dead,'data':TableData});
                $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
                {
                    if(response.data=='\r\n\r\n')
                    {
                         window.open('/krg/download.php','_blank');
                    }
                    else if(response.data=="No Data"){message_display("danger","Insurance and O & M Reminder","No Data Found");}
                    else{message_display("danger","Insurance and O & M Reminder","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);}
                    Pace.stop();
                });
            }else{message_display("danger", "Insurance and O & M Reminder", 'No Records Found');} 
           
        });
    });
});
function storeTblValues() {

    var TableData = new Array();
    var table = $('#machine_list').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();
    var row=0;
    $.each(rows_selected, function(index, rowId){
        var tr=table.row(rowId-1).node();
        var amount= $(tr).find('td:eq(7)').find('input').val();
        var arr= table.rows(rowId-1).data();
        TableData[row]={ "mid":arr[0][2],"uid":arr[0][4],"lid":arr[0][6],"type":arr[0][8],"due":arr[0][9],"oamount":arr[0][11],"amount":amount };
        row++;
    });
   // TableData.shift();  
   if(row==0){return "";}else{ return TableData;}
}
function storeTblValuesExport() {

    var TableData = new Array();
    var table = $('#machine_list').DataTable();
    var row=0;
    table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        var arr = this.data();
       TableData[row]={"machine_id":arr[2],"machine_name":arr[1],"unit_name":arr[3],"location_name":arr[5],"capacity":arr[7],"type":arr[8],"due_date":arr[9],"pamount":arr[11] };
        arr.counter++; 
        row++;
    });
   // TableData.shift();  
   if(row==0){return "";}else{ return TableData;}
}
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}