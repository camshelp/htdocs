angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    $scope.invoiceinfo=true;$scope.invoiceregister=false;$scope.clientregister=false;$scope.paymentinfo=false;
    $scope.clientadd=false;
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $scope.invoiceclear=function()
    {
        $scope.ocname='Others';
        $scope.uname='';$scope.clname='';$scope.iperiod='';$scope.ipay='';$scope.iallocated='0';$scope.iloss='0';$scope.inet='0';
        $scope.iconsume='0';$scope.ibanked='0';$scope.itaken='0';$scope.irate='0';$scope.total_unit='0';$scope.con_amount='0';

        $scope.imrent='0';$scope.ipenalty='0';$scope.iop='0';$scope.iom='0';$scope.itr='0';$scope.iwhl='0';$scope.ibc='0';$scope.isgt='0';$scope.inc='0';
        $scope.oc='0';

        $scope.lr_amount='0';$scope.pay_amount='0';
    }
    $scope.invoicelist=function()
    {
        var data = $.param({ callvalue: "list_invoice","unit_id":$scope.suname,'month':$scope.imonths});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
          var SData=JSON.stringify(response.data);
            var jsondata=JSON.parse(SData);  
           var table_id="#ilist";
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.invoice_list=jsondata.data;   $scope.total_pending=jsondata.pending_amount;
            $scope.total_invoice=$scope.invoice_list.length;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
        });
    }
    $scope.RefreshInvoice=function(unit_id)
    {
        setTimeout(function () {
            $(function () {
                $scope.invoicelist(); 
            });
        }, 1000);
       
    }
    $('#monthd').change( function()
    {
        setTimeout(function () {
            $(function () {
                $scope.invoicelist(); 
            });
        }, 1000);
    });
     $scope.newinvoice=function(id)
    {
        $scope.invoiceinfo=false;$scope.invoiceregister=true; $scope.invoiceclear();
        var index="-1";
        $scope.invoice_list.some(function (obj, i) { return obj.invoice_id === id ? index = i : false; });
        if(index!="-1")
        {
            $scope.iid=$scope.invoice_list[index].invoice_id;
            $scope.idate=$scope.invoice_list[index].invoice_date;
            $scope.uname=$scope.invoice_list[index].unit_id;
            $scope.clname=$scope.invoice_list[index].client_id; $scope.select_client($scope.clname);
            $scope.imonth=$scope.invoice_list[index].month_year;
            $scope.iperiod=$scope.invoice_list[index].bill_period;
            $scope.ipay=$scope.invoice_list[index].pay_date;
            $scope.iallocated=$scope.invoice_list[index].alloted_units;
            $scope.iloss=$scope.invoice_list[index].line_loss;
            $scope.inet=$scope.invoice_list[index].net_generation;
            $scope.iconsume=$scope.invoice_list[index].units_consumed;
            $scope.ibanked=$scope.invoice_list[index].units_banked;
            $scope.itaken=$scope.invoice_list[index].taken_bank;
            $scope.total_unit=$scope.invoice_list[index].total_unit;
            $scope.irate=$scope.invoice_list[index].unit_cost;
            $scope.total_cost=$scope.invoice_list[index].total_cost;

            $scope.imrent=$scope.invoice_list[index].meter_rent;
            $scope.ipenalty=$scope.invoice_list[index].penalty;
            $scope.iop=$scope.invoice_list[index].operating_charge;
            $scope.iom=$scope.invoice_list[index].om_charge;
            $scope.itr=$scope.invoice_list[index].transmission_charge;
            $scope.iwhl=$scope.invoice_list[index].wheeling_charge;
            $scope.ibc=$scope.invoice_list[index].banking_charge;
            $scope.isgt=$scope.invoice_list[index].self_tax;
            $scope.inc=$scope.invoice_list[index].negative_charge;
            $scope.ocname=$scope.invoice_list[index].other_name;
            $scope.oc=$scope.invoice_list[index].other_charge;
            $scope.lr_amount=$scope.invoice_list[index].less_reimburse;
            $scope.pay_amount=$scope.invoice_list[index].total_amount;
            $scope.RefreshValues();
            
        }
        
    }
    $scope.saluation=[{'saluation':'Online Payment'},{'saluation':'DD'},{'saluation':'Cheque'}];
    $scope.RefreshValues=function()
    {
        var allocated = Number($scope.iallocated || 0);
        var loss = Number($scope.iloss || 0);
        var banked = Number($scope.ibanked || 0);
        var taken = Number($scope.itaken || 0);
        var rate = Number($scope.irate || 0);

        var rent = Number($scope.imrent || 0);
        var penalt = Number($scope.ipenalty || 0);
        var op = Number($scope.iop || 0);
        var om = Number($scope.iom || 0);
        var trans = Number($scope.itr || 0);
        var wheel = Number($scope.iwhl|| 0);
        var ibc = Number($scope.ibc || 0);
        var isgt = Number($scope.isgt || 0);
        var inc = Number($scope.inc || 0);
        var other = Number($scope.oc || 0);

        var net =allocated-loss;var consumption=net-banked+taken;var cons_amount=Math.round(consumption*rate);
        var lr_amount=rent+penalt+op+om+trans+wheel+ibc+isgt+inc+other;
        var gross=cons_amount-lr_amount;

        $scope.inet=net;
        $scope.iconsume=consumption;
        $scope.total_unit=consumption;
        $scope.con_amount=cons_amount;

        $scope.lr_amount=lr_amount;

        $scope.pay_amount=gross;

    }
    $('#iallocated, #iloss, #ibanked, #itaken, #irate, #imrent, #ipenalty, #iop, #iom, #itr, #iwhl, #ibc, #isgt, #inc,#oc').blur( function() 
    {
        $scope.RefreshValues();
    }); 
    $('#iallocated, #iloss, #ibanked, #itaken, #irate, #imrent, #ipenalty, #iop, #iom, #itr, #iwhl, #ibc, #isgt, #inc,#oc').change( function()
    {
        $scope.RefreshValues();
    });
    $scope.newclient=function(id)
    {
        $scope.invoiceinfo=false;$scope.clientregister=true;
    }
    $scope.addclient=function(id)
    {
        $scope.clientregister=false;$scope.clientadd=true; $scope.client_clear();
        var index="-1";
        $scope.clientlist.some(function (obj, i) { return obj.id === id ? index = i : false; });
        if(index!="-1")
        {
            $scope.ccid=$scope.clientlist[index].id;
            $scope.cname=$scope.clientlist[index].name;
            $scope.ccost=$scope.clientlist[index].ccost;
            $scope.cline1=$scope.clientlist[index].line1;
            $scope.cline2=$scope.clientlist[index].line2;
            $scope.cline3=$scope.clientlist[index].line3;
            $scope.cline4=$scope.clientlist[index].line4;
            $scope.cmobile=$scope.clientlist[index].mobile;
            $scope.cmail=$scope.clientlist[index].mail;
            $scope.cedc=$scope.clientlist[index].edc;
            $scope.chtsc=$scope.clientlist[index].htsc;
             $scope.cgst=$scope.clientlist[index].gst;
             $scope.cpan=$scope.clientlist[index].pan;
             $scope.ccost=$scope.clientlist[index].cost;
             $scope.ccontact=$scope.clientlist[index].person;
             $scope.cdesig=$scope.clientlist[index].designation;
        }
    }
    $scope.select_client=function(id)
    {
        var index="-1";
        $scope.clientlist.some(function (obj, i) { return obj.id === id ? index = i : false; });
        if(index!="-1")
        {
            $scope.irate=$scope.clientlist[index].cost;
            $scope.RefreshValues();
            $scope.comm_person=$scope.clientlist[index].person+"-"+$scope.clientlist[index].designation;
            $scope.ihtsc=$scope.clientlist[index].htsc;
        }
    }
        var data = $.param({ callvalue: "unit_list"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.unit_list=response.data;
        });
    $scope.invoicereport=function(invoice_id)
    {
        var data = $.param({ callvalue: "invoice_report","invoice_id":invoice_id});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
             //console.log(response.data);
             if(response.data=="")
             {
                  window.open('/krg/download.php','_blank');
             }
             else if(response.data=="No Data"){message_display("danger","Invoice Report","No Data Found");}
             else{message_display("danger","Invoice Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
             Pace.stop();
        });
    }
    $scope.invoiceremove=function(invoice_id)
    {
        Pace.stop();
        Pace.bar.render();
        var form_data = new FormData();
        form_data.append('invoice_id',invoice_id);
        form_data.append('callvalue',"invoice_remove");
        $.ajax({
            type: "POST", url: "/krg/myphp/krcepl/invoice.php", datatype: 'json', data: form_data,
            contentType: false,
            cache: false,
            processData: false,

        }).then(function (response) {
            //alert(response);
            var myResult = JSON.parse(response);
            var msg_type = "danger";
            var message = myResult.Message;
            if (myResult.result == "Success") {  $scope.invoicelist(); msg_type = "success"; }
            message_display(msg_type, "Invoice Removal", message);  Pace.stop();
        });
    }
    $scope.register_back1=function()
    {
        $scope.invoiceinfo=true;$scope.invoiceregister=false;
    }
    $scope.client_back1=function()
    {
        $scope.invoiceinfo=true;$scope.clientregister=false;
    }
    $scope.client_back2=function()
    {
        $scope.clientregister=true;$scope.clientadd=false;
    }
    $scope.payment_back=function()
    {
        $scope.invoiceinfo=true;$scope.paymentinfo=false;
    }
    $scope.payment_clear=function()
    {
       $scope.sal='';$scope.pmonth='';$scope.pamount='0';$scope.premarks='';
    } 
    $scope.payment_load=function(id)
    {
        var data = $.param({ callvalue: "invoice_payments","invoice_id":id});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
            $scope.phistory=response.data;
        });
    }
    $scope.Update_Payment=function(invoice_id)
    {
        $scope.invoiceinfo=false;$scope.paymentinfo=true;$scope.payment_clear();
        var index="-1";
        $scope.invoice_list.some(function (obj, i) { return obj.invoice_id === invoice_id ? index = i : false; });
        if(index!="-1")
        {
            $scope.piid=$scope.invoice_list[index].invoice_id;
            $scope.puid=$scope.invoice_list[index].unit_id;
            $scope.pcid=$scope.invoice_list[index].client_id;
            $scope.pino=$scope.invoice_list[index].invoice_no;
            $scope.pidate=$scope.invoice_list[index].invoice_date;
            $scope.puname=$scope.invoice_list[index].unit_name;
            $scope.pcname=$scope.invoice_list[index].client_name;
            $scope.pmyear=$scope.invoice_list[index].month_year;
            $scope.pbper=$scope.invoice_list[index].bill_period;
            $scope.ppay=$scope.invoice_list[index].amount_pending;
            $scope.payment_load($scope.piid);
        }
    }
    $scope.client_load=function()
    {
        var data = $.param({ callvalue: "client_list"});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
            var table_id="#clist";
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.clientlist = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
            $scope.total_cli=$scope.clientlist.length;
        });
    }
    $scope.client_load();
    $scope.client_clear=function()
    {
        $scope.ccid='';$scope.cname='';$scope.ccost='';$scope.cline1='';$scope.cline2='';$scope.cline3='';$scope.cline4='';
        $scope.cmobile='';$scope.cmail='';$scope.cedc=''; $scope.chtsc=''; $scope.cgst='';$scope.cpan='';
    }
    $(document).ready(function (e) {
        $("#InvoiceRegister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('net_gen',$scope.inet);
            form_data.append('consumed',$scope.iconsume);
            form_data.append('total_unit',$scope.total_unit);
            form_data.append('total_cost',$scope.con_amount);
            form_data.append('reimburse',$scope.lr_amount);
            form_data.append('payable',$scope.pay_amount);
            form_data.append('callvalue',"invoice_register");
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/invoice.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") {  $scope.invoiceclear();$scope.invoicelist(); $scope.invoiceinfo=true;$scope.invoiceregister=false;  msg_type = "success"; }
                message_display(msg_type, "Invoice Registration", message);  Pace.stop();
            });
        });
        $("#cregister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue',"client_register");
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/invoice.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") {   $scope.client_clear(); $scope.client_load();$scope.clientregister=true;$scope.clientadd=false; msg_type = "success"; }
                message_display(msg_type, "Client Registration", message);  Pace.stop();
            });
        });

        $("#PRegister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue',"payment_update");
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/invoice.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") {   $scope.payment_clear(); $scope.payment_load($scope.piid); $scope.invoicelist(); msg_type = "success"; }
                message_display(msg_type, "Client Registration", message);  Pace.stop();
            });
        });
    });
    $scope.consolidated_invoice=function()
    {
        var data = $.param({ callvalue: "invoice_consolidated","start":$scope.imonth1,"end":$scope.imonthe});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
             //console.log(response.data);
             if(response.data=="")
             {
                  window.open('/krg/download.php','_blank');
             }
             else if(response.data=="No Data"){message_display("danger","Invoice Report","No Data Found");}
             else{message_display("danger","Invoice Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
             Pace.stop();
        });
    }
    $scope.consolidated_invoice1=function()
    {
        //alert($scope.suname+" 1111");
        var data = $.param({ callvalue: "clientinvoice_consolidated","start":$scope.imonth1,"end":$scope.imonthe,'unit_name':$scope.suname});
        $http.post('/krg/myphp/krcepl/invoice.php',data,config).then(function (response) 
        {
             //console.log(response.data);
             if(response.data=="")
             {
                  window.open('/krg/download.php','_blank');
             }
             else if(response.data=="No Data"){message_display("danger","Invoice Report","No Data Found");}
             else{message_display("danger","Invoice Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
             Pace.stop();
        });
    }
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}