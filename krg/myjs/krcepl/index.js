$(document).ready(function (e) {
    $("#Message").on('submit', function (e) {
        e.preventDefault();
        var form_data = new FormData(this);
        form_data.append('callvalue',"contactform");
        $.ajax({
            type: "POST", url: "/krg/myphp/krcepl/krcepl.php", datatype: 'json', data: form_data,
            contentType: false,
            cache: false,
            processData: false,

        }).then(function (response) {
            //alert(response);
            var myResult = JSON.parse(response);
            var msg_type = "danger";
            var message = myResult.Message;
            if (myResult.result == "Success") { ClearForm(); msg_type = "success"; }
            message_display(msg_type, "Cotact Form", message); 
        });
    });
});
function ClearForm() {
    document.getElementById("Message").reset();
}
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}