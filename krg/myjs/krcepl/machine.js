angular.module('myApp', ['ngFileUpload','ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile, Upload) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $scope.showHints = true;
    $scope.RegisterVisible=false;$scope.listVisible=true;
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $scope.load_data=function()
    {
        var data = $.param({ callvalue: "machine_summary"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.mc_count=response.data.total_machine;
            $scope.tot_capa=response.data.machine_capacity;
        });
        var data = $.param({ callvalue: "unit_list"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.unit_list=response.data;
        });
        var data = $.param({ callvalue: "location_list"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.location_list=response.data;
        });
        var data = $.param({ callvalue: "division_list"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            $scope.division_list=response.data;
        });
    }
    $scope.load_machines = function () 
    {
        var data = $.param({ callvalue: "machine_list"});
        $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
        {
            var table_id="#machine_list";
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.machines_list = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                     /*    buttons: ['copy', 'excel', 'pdf','print'], */
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
        });
    };
    $scope.load_machines();$scope.load_data();
    $scope.machine_clear=function(){
       $scope.mid='';$scope.uname='';$scope.dname='';$scope.lname='';$scope.mname='';$scope.htsc='';$scope.dof='';$scope.capa='';$scope.idd='';$scope.iamount='';$scope.omd='';$scope.oamount='';
    }
    $scope.registration_list=function(){ $scope.RegisterVisible=false;$scope.listVisible=true;};
    $scope.editUser =function(value)
    {
        $scope.RegisterVisible=true;$scope.listVisible=false;
        $scope.machine_clear();
        if(value=="new"){}
        else
        {
            var index = -1;
            $scope.machines_list.some(function (obj, i) {
                return obj.machine_id === value ? index = i : false;
            });
            if(index!="-1")
            {   $scope.mid = $scope.machines_list[index].machine_id;
                $scope.uname = $scope.machines_list[index].unit_id;
                $scope.lname = $scope.machines_list[index].location_id;
                $scope.dname = $scope.machines_list[index].division_id;
                $scope.mname=$scope.machines_list[index].machine_name;
                $scope.htsc = $scope.machines_list[index].htsc;
                $scope.dof = $scope.machines_list[index].function_date;
                $scope.capa = $scope.machines_list[index].capacity;
                $scope.idd = $scope.machines_list[index].insurance_date;
                $scope.iamount = $scope.machines_list[index].insurance_amount;
                $scope.omd = $scope.machines_list[index].om_date;
                $scope.oamount = $scope.machines_list[index].om_amount;
             
            }     
        }     
    }
  $(document).ready(function (e) {

        $("#MachineRegister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue',"machine_register");
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/machine.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                //alert(response);
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") {  $scope.load_machines(); $scope.RegisterVisible=false;$scope.listVisible=true;$scope.machine_clear(); msg_type = "success"; }
                message_display(msg_type, "Machine Registration", message);  Pace.stop();
            });
        });
    });
    $(document).ready(function (e) {
        $("#export_pdf").on('click', function (e) {
            var ret_data=storeTblValuesExport();
            if(ret_data != "")
            {
                var TableData = JSON.stringify(ret_data);
                data = $.param({ callvalue: "machine_pdf",'data':TableData});
                $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
                {
                    if(response.data=='\r\n\r\n')
                    {
                         window.open('/krg/download.php','_blank');
                    }
                    else if(response.data=="No Data"){message_display("danger","Insurance and O & M Reminder","No Data Found");}
                    else{message_display("danger","Insurance and O & M Reminder","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);}
                });
            }else{message_display("danger", "Insurance and O & M Reminder", 'No Records Found');} 
        });
    });
});
function storeTblValuesExport() {

    var TableData = new Array();
    var table = $('#machine_list').DataTable();
    var row=0;
    table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        var arr = this.data();
       TableData[row]={"machine_name":arr[0],"unit_name":arr[1],"location_name":arr[2],"capacity":arr[3],"insurance_date":arr[4],"insurance_amount":arr[5],"om_date":arr[7],"om_amount":arr[8] };
        arr.counter++; 
        row++;
    });
   // TableData.shift();  
   if(row==0){return "";}else{ return TableData;}
}
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}