angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    $scope.Add_Info=false;$scope.Disp_Info=true;$scope.View_Info=false;
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    //edit_info(info.unit_id,info.unit_name)
    $scope.unit_machines_details=function(unit_id)
    {
        var data = $.param({ callvalue: "eb_expense_list","euid":unit_id,"myear":$scope.imonth});
        $http.post('/krg/myphp/krcepl/eb.php',data,config).then(function (response) 
        {
            var table_id='#mdata';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  } 
            $scope.machinelist = response.data;
              setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'flrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                                { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                                { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                                { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                                { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);  
        });
    }
    $scope.view_info=function(unit_id,unit_name)
    {
        $scope.Disp_Info=false;$scope.View_Info=true;
        $scope.euname='';$scope.euid='';
        $scope.m_list=[];
        setTimeout(function () {
            $(function () {
                    var cat_count=$scope.mlist.length;
                    for(x=0;x<cat_count;x++)
                    {
                        var category_id=$scope.mlist[x].unit_id;
                        if(category_id==unit_id)
                        {
                            $scope.m_list.push($scope.mlist[x]);
                        }
                    }
                });
                
            }, 0);
        $scope.euname=unit_name;$scope.euid=unit_id;
        $scope.unit_machines_details(unit_id);
    }
    $scope.back_info1=function(unit_id)
    {
        $scope.Disp_Info=true;$scope.View_Info=false;
    }
    $scope.edit_info=function(unit_id,unit_name)
    {
        $scope.etype='Insert';
        $scope.Add_Info=true;$scope.View_Info=false;
    }
    $scope.update_info=function(machine_id)
    {
            $scope.clear_form();
            $scope.Add_Info=true;$scope.View_Info=false;
            $scope.etype='Update';   
            var index = -1;
            $scope.machinelist.some(function (obj, i) {
                return obj.machine_id === machine_id ? index = i : false;
            });
            if(index!="-1")
            {  
                 $scope.emname = $scope.machinelist[index].machine_id;
                 $scope.eexport = $scope.machinelist[index].total_export;
                 $scope.eimport = $scope.machinelist[index].total_import;
                 $scope.egen = $scope.machinelist[index].net_generation;
                 $scope.e10gen = $scope.machinelist[index].ten_generation;
                 $scope.epower = $scope.machinelist[index].power_factor;
                 $scope.epenalty = $scope.machinelist[index].RKVAH_penalty;
                 $scope.ereading = $scope.machinelist[index].meter_charge;
                 $scope.eoper = $scope.machinelist[index].operating_charge;
                 $scope.eschedule= $scope.machinelist[index].schedule_charge;
                 $scope.eomcharge= $scope.machinelist[index].om_charge;
                 $scope.etrans= $scope.machinelist[index].transmission;
            }
    }
    $scope.clear_form=function()
    {
        $scope.emname='';$scope.eexport='0';$scope.eimport='0';$scope.egen='0';$scope.e10gen='0';$scope.epower='0';
        $scope.epenalty='0'; $scope.ereading='0'; $scope.eomcharge='0'; $scope.eschedule='0'; $scope.eoper='0';$scope.etrans='0';
    }
    var data = $.param({ callvalue: "machine_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.mlist = response.data;
    });
    $scope.RefreshValues=function()
    {
       /*  var exportp = Number($scope.eexport || 0);
        var importp = Number($scope.eimpport || 0);
        var ten_per=(exportp+importp)/10;
        $scope.e10gen=ten_per; */

    }
   /*  $('#egen').blur( function() {$scope.RefreshValues();} );  */
    $scope.back_info=function(unit_id)
    {
        $scope.Add_Info=false;$scope.View_Info=true;
    }
    $scope.summary=function()
    {
    var data = $.param({ callvalue: "ebmonth_summary",'month':$scope.imonth});
    $http.post('/krg/myphp/krcepl/eb.php',data,config).then(function (response) 
    {
        //console.log(response.data);
        var SData=JSON.stringify(response.data);
        var jsondata=JSON.parse(SData);
        $scope.imonth=jsondata.month_name;
        $scope.total_import=jsondata.total_import;
        $scope.total_export=jsondata.total_export;
        $scope.total_generation=jsondata.net_generation;
        $scope.total_expense=jsondata.total_charge;

           var table_id='#clist';
                        if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  } 
                        $scope.unitlist=jsondata.unit_data;
                          setTimeout(function () {
                            $(function () {
                                $(table_id).DataTable({
                                    dom: 'flrtip',
                                    responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                                        ]
                                    }
                                });
                            });
                        }, 0); 

                        //console.log($scope.unitlist);
    });
}
$scope.summary();
    $('#myr').change( function() 
    {
        setTimeout(function () {
            $(function () {
                $scope.summary();
            });
        }, 100);  

         
    });

    $(document).ready(function (e) {
        $("#cregister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue',"eb_register");
            form_data.append('myear',$scope.imonth);
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/eb.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") {  $scope.clear_form();$scope.unit_machines_details($scope.euid); $scope.Add_Info=false;$scope.View_Info=true;  msg_type = "success"; }
                message_display(msg_type, "EB Details Update", message);  Pace.stop();
            });
        });
    });
    $scope.expense_summary=function()
    {
        var data = $.param({ callvalue: "expense_summary",'start':$scope.imonth1,'end':$scope.imonthe});
        $http.post('/krg/myphp/krcepl/eb.php',data,config).then(function (response) 
        {
                //console.log(response.data);
                if(response.data=="")
                {
                    window.open('/krg/download.php','_blank');
                }
                else if(response.data=="No Data"){message_display("danger","Eb Expense Report","No Data Found");}
                else{message_display("danger","Eb Expense Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
        });   
    }
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}