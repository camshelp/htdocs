angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile, $filter) {
	
	$scope.Updatepro2 = function(){ 
		    $scope.funcName = "machine_details_load";
		if ($scope.acyear == null) {
            alert("Enter Academic Year");
        } else if ($scope.date == null) {
            alert("Enter Date");
        }else if ($scope.uname == null) {
            alert("Enter Unit Name");
        }else if ($scope.lname == null) {
            alert("Enter Location Name");
        }else if ($scope.dname == null) {
            alert("Enter Division Name");
        } else {
			$scope.loaded_academic_year = $scope.acyear;
			$scope.loaded_date = $scope.date;
			$scope.loaded_unit_id = $scope.uname;
			$scope.loaded_location_id = $scope.lname;
			$scope.loaded_division_id = $scope.dname;
			$http.post(
                "/krg/myphp/krcepl/plf.php", {
                    'unitid': $scope.uname,
                    'funcName': $scope.funcName
                }
				).then(function successCallBack(response){
					
					$scope.machine_details=response.data;
					$scope.Updatepro=false;$scope.UpdateproExtended=true;
					
				}, function errorCallBack(response){
					alert("Something went wrong!");
			})
		}
    }
	
	$scope.myFuncPlf = function($data){
		    $scope.funcName = "plf_load";
			$scope.loaded_machine_name = $data.machine_name;
			$scope.loaded_production_value = $data.loaded_production;
			$http.post(
                "/krg/myphp/krcepl/plf.php", {
                    'machine_name': $scope.loaded_machine_name,
                    'funcName': $scope.funcName
                }
				).then(function successCallBack(response){
					
				$scope.part1 = response.data[0].capacity;
				$scope.part2 = $scope.part1*1000*24;
				$scope.part3 = $scope.loaded_production_value/$scope.part2;
				$scope.part4 = $scope.part3*100;
				$data.loaded_plf_value = $scope.part4;
					
				}, function errorCallBack(response){
					alert("Something went wrong!");
			})
			
    }
	
	$scope.production_submit = function($data){
		$scope.funcName = "insert_production_value";
		$scope.error_report = "";
		
		    for (var i = 0, length = $data.length; i < length; i++) { 
			$http.post(
                "/krg/myphp/krcepl/plf.php", {
					'funcName': $scope.funcName,
                    'academic_year': $scope.loaded_academic_year,
					'production_date': $filter('date')($scope.loaded_date, "yyyy-MM-dd"),
					'unit_id': $scope.loaded_unit_id,
					'location_id': $scope.loaded_location_id,
					'division_id': $scope.loaded_division_id,
					'machine_name': $data[i].machine_name,
					'machine_hour': $data[i].loaded_machine_hour,
					'machine_minute': $data[i].loaded_machine_minute,
					'machine_breakdown_hour': $data[i].loaded_machine_breakdown_hour,
					'machine_reason': $data[i].loaded_machine_reason,
					'machine_available': $data[i].loaded_machine_available,
					'grid_hour': $data[i].loaded_grid_hour,
					'grid_minute': $data[i].loaded_grid_minute,
					'grid_breakdown_hour': $data[i].loaded_grid_breakdown_hour,
					'reason': $data[i].loaded_reason,
					'grid_available': $data[i].loaded_grid_available,
					'production': $data[i].loaded_production,
					'plf_value': $filter('number')($data[i].loaded_plf_value, 2),
					'generation_hour': $data[i].loaded_generation_hour,
					'generation_minute': $data[i].loaded_generation_minute
                }).then(function successCallBack(response){
					
						if(response.data != ""){
						alert("Error at line : " + i);
						$scope.error_report = "just";
						}
						
						if(($scope.error_report == "") && (i == length)){
							alert("Table inserted without errors!");
						}
					
				}, function errorCallBack(response){
					alert("Something went wrong!");
				})
			}
		
		$scope.acyear = "2019-2020";
		$scope.date = null;
		$scope.uname = null;
		$scope.lname = null;
		$scope.dname = null;
		$scope.Updatepro=true;$scope.UpdateproExtended=false;
    }
	
	
	
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $scope.msummary=false;$scope.summaryVisible=false;
    var data = $.param({ callvalue: "unit_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.unit_list=response.data;
    });
	
	
	
	//new
    var data = $.param({ callvalue: "location_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.location_list=response.data;
    });
    var data = $.param({ callvalue: "division_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.division_list=response.data;
    });
    var data = $.param({ callvalue: "machine_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.machine_list=response.data;
    });
	//new
	
	
	
    var data = $.param({ callvalue: "machine_list"});
    $http.post('/krg/myphp/krcepl/machine.php',data,config).then(function (response) 
    {
        $scope.m_list = response.data;
    });

    

    $scope.type='Unit';
    $scope.listVisible=true;$scope.Upload=false;$scope.ProductionInfo=false;
    $scope.summary=function()
    {
        var data = $.param({ callvalue: "summary"});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
            $scope.sdate=response.data.sdate;
            $scope.edate=response.data.edate;
            $scope.fcount=response.data.no_file;
            $scope.tot_prod=response.data.production;
            $scope.grid_break=response.data.ghour+" Hours "+response.data.gmin+" Mins";
            $scope.machine_break=response.data.mhour+" Hours "+response.data.mmin+" Mins";
            $scope.date_selected(response.data.sdate,response.data.edate);
        });
        var data = $.param({ callvalue: "lastdaysummary"});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
            $scope.ldate=response.data.last_date;
            $scope.last_production=response.data.unit_data;

        });
    }

    $scope.view_detailed_summary=function(passed_id,name)
    {
        $scope.listVisible=false; $scope.summaryVisible=true;
        setTimeout(function () {
            $scope.disp_name=name;
            var data = $.param({ callvalue: "dateproductionview","sdate":$scope.sdate,"edate":$scope.edate,"id":passed_id});
            $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
             {
                         var SData=JSON.stringify(response.data);
                         var jsondata=JSON.parse(SData);
                         //console.log(SData);
                        var table_id='#vlist';
                         if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
                         $scope.vproduction_list=jsondata.data;
                         $scope.vftotal=jsondata.total;
                         setTimeout(function () {
                             $(function () {
                                 $(table_id).DataTable({
                                     dom: 'flrtip',
                                     responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                                             { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                                             { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                                             { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                                             { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                                         ]
                                     }
                                 });
                             });
                         }, 0); 
     
             });
        },0);
    }
    $scope.view_summary_back=function()
    {
        $scope.listVisible=true;$scope.summaryVisible=false;
    }
    $scope.machine_production_summary=function(machine_id)
    {
       
        var data = $.param({ callvalue: "machineproductionsummary",'machine_id':machine_id,'date':$scope.ldate});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
            $scope.m_list.some(function (obj, i) {
                return obj.machine_id === machine_id ? index = i : false;
            });
            if(index!="-1")
            {  
                 $scope.machine_name=$scope.m_list[index].machine_name;
            }
            var SData=JSON.stringify(response.data);
            var jsondata=JSON.parse(SData);

            $scope.msummary=true;
            $scope.mc_date=response.data.today_date;
            $scope.md_production=response.data.today_production;
            $scope.pdate=response.data.previous_date;
            $scope.mpproduction=response.data.previous_production;
            $scope.mproduction=response.data.monthly_production;
            $scope.yproduction=response.data.yearly_production;
          
            $timeout(function(){
            var chart = c3.generate({
                bindto: '#pmonth',
                data: {
                    columns:jsondata.mdata,
                    type: 'donut',labels: true
                },
                donut: {
                    title: "Production Month",
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }
                }
            });
        }, 500);
            
        $timeout(function(){
            var chart = c3.generate({
                bindto: '#pyear',
                data: {
                    columns:jsondata.yr_data,
                    type: 'bar',labels: true
                },
                bar: {
                    title: "Production Year"
                },
                axis: {
                    x: {
                        type: 'category',
                        categories:jsondata.months,
                        label: 'Months'
                    },
                    y: {
                        label: 'Production'
                    }
                }
            });
        }, 500);
        $timeout(function(){
            var chart = c3.generate({
                bindto: '#mcavl',
                data: {
                    columns:jsondata.mc_avl,
                    type: 'bar',labels: true
                },
                axis: {
                    x: {
                        type: 'category',
                        categories:jsondata.months,
                        label: 'Months'
                    },
                    y: {
                        label: 'Machine Availability'
                    }
                }
            });
        }, 500);
        $timeout(function(){
            var chart = c3.generate({
                bindto: '#gdavl',
                data: {
                    columns:jsondata.gd_avl,
                    type: 'bar',labels: true
                },
                axis: {
                    x: {
                        type: 'category',
                        categories:jsondata.months,
                        label: 'Months'
                    },
                    y: {
                        label: 'Grid Availability'
                    }
                }
            });
        }, 500);
        });
    }
    $scope.selectedcategory=[];
        var data = $.param({ callvalue: "fyear_data"});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
            $scope.fyear=response.data;
            setTimeout(function () {
                $(function () {
                        var cat_count=$scope.fyear.length;
                        for(x=0;x<cat_count;x++)
                        {
                            $scope.selectedcategory.push($scope.fyear[x].academic_year);
                        }
                    });
                }, 0);
        });
   $scope.FileUpload=function(value)
   {
      $scope.listVisible=false;$scope.Upload=true;$scope.clear_entry();
   }
   $scope.dailyupdate=function(value)
   {
      $scope.listVisible=false;$scope.Updatepro=true;
   }
   $scope.UploadSummary=function(value)
   {
      $scope.listVisible=true;$scope.Upload=false;
   }
   $scope.UploadSummary1=function()
   {
      $scope.listVisible=true;$scope.Updatepro=false;
   }
   $scope.UploadSummary2=function()
   {
      $scope.Updatepro=true;$scope.UpdateproExtended=false;
   }
    $('#sdate,#edate').change( function()
    { 
         $scope.date_selected($scope.sdate,$scope.edate);
   });
   $scope.date_selected=function(sdates,edates)
   {
                    var data = $.param({ callvalue: "dateproduction","sdate":sdates,"edate":edates,type:$scope.type});
                    $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
                    {
                        var SData=JSON.stringify(response.data);
                        var jsondata=JSON.parse(SData);
                        //console.log(jsondata.total);
                         var table_id='#list';
                        if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
                        $scope.production_list=jsondata.data;
                        $scope.ftotal=jsondata.total;
                        setTimeout(function () {
                            $(function () {
                                $(table_id).DataTable({
                                    dom: 'flrtip',
                                    responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                                        ]
                                    }
                                });
                            });
                        }, 0); 
                    }); 
   }
   $scope.summary();
   
   $scope.clear_entry=function(){$scope.etype=''; $("#file_to_upload").val(null);};
   $scope.load_excel=function()
   {
        var data = $.param({ callvalue: "unapprovedsheets"});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
            var table_id='#data_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.usheets = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            },0);
        });
   }
   $scope.load_excel();
   $(document).ready(function (e) {
    $("#UploadExcel").on('submit', function (e) {
        e.preventDefault();
        if($("#file_to_upload").val() != "")
        {
            Pace.stop();
            Pace.bar.render();
            var file_data = $('#file_to_upload').get(0).files[0];
            var file_data1 = $('#etype').val();
            var form_data = new FormData();
            form_data.append('uploadedFile', file_data);
            form_data.append('etype', file_data1);
            form_data.append('callvalue', "fileupload");
            $.ajax({
                type: "POST", url: "/krg/myphp/krcepl/daily.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
               // alert(response);
               var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") 
                {
                    msg_type = "success";$scope.clear_entry();$scope.load_excel(); }
                message_display(msg_type, "Excel Upload", message); Pace.stop();
            });
        }else{message_display("danger", "Excel Upload", 'Please Choose Excel Sheet');}
    });
});

$(document).ready(function (e) {
    $("#ApproveData").on('submit', function (e) {
        e.preventDefault();
        $scope.Approve_data();
    });
});

$(document).ready(function (e) {
    $("#yearsummary").on('click', function (e) {
      
        Pace.stop();
        Pace.bar.render();
        var data = $.param({ callvalue: "year_summary","unit":$scope.uname,"sdate":$scope.rsdate,"edate":$scope.redate,"fyears":$scope.selectedcategory});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
           //console.log(response.data);
            if(response.data=="")
            {
                 window.open('/krg/download.php','_blank');
            }
            else if(response.data=="No Data"){message_display("danger","Year Summary Report","No Data Found");}
            else{message_display("danger","Year Summary Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
            Pace.stop();
        });
    });
});
$(document).ready(function (e) {
    $("#dailysummary").on('click', function (e) {
      
        Pace.stop();
        Pace.bar.render();
        var data = $.param({ callvalue: "daily_summary","unit":$scope.uname,"sdate":$scope.rsdate,"edate":$scope.redate,"fyears":$scope.selectedcategory});
        $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
        {
           //console.log(response.data);
            if(response.data=="")
            {
                 window.open('/krg/download.php','_blank');
            }
            else if(response.data=="No Data"){message_display("danger","Year Summary Report","No Data Found");}
            else{message_display("danger","Year Summary Report","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);} 
            Pace.stop();
        });
    });
});
$scope.delete_info=function(upload_id)
{
    var TableData = JSON.stringify(storeTblValues());
    Pace.stop();
    Pace.bar.render();
    var form_data = new FormData();
    form_data.append('file_id',upload_id);
    form_data.append('callvalue', "deletedata");
    $.ajax({
        type: "POST", url: "/krg/myphp/krcepl/daily.php", datatype: 'json', data: form_data,
        contentType: false,
        cache: false,
        processData: false,
    }).then(function (response) {
       var myResult = JSON.parse(response);
        var msg_type = "danger";
        var message = myResult.Message;
        if (myResult.result == "Success") 
        {
            msg_type = "success";  $scope.load_excel(); 
           
         }
        message_display(msg_type, "Remove Excel File", message); Pace.stop();
    });
}
$scope.Approve_data=function()
{
    var TableData = JSON.stringify(storeTblValues());
    Pace.stop();
    Pace.bar.render();
    var form_data = new FormData();
    form_data.append('data', TableData);
    form_data.append('file_id', $scope.upload_id);
    form_data.append('callvalue', "approvedata");
    $.ajax({
        type: "POST", url: "/krg/myphp/krcepl/daily.php", datatype: 'json', data: form_data,
        contentType: false,
        cache: false,
        processData: false,
    }).then(function (response) {
       // alert(response);
       var myResult = JSON.parse(response);
        var msg_type = "danger";
        var message = myResult.Message;
        if (myResult.result == "Success") 
        {
            msg_type = "success"; $scope.upload_id='';$scope.sheet_type='';$scope.sheet_name=''; $scope.load_excel(); $scope.summary();
            $scope.ProductionInfo=false;$scope.Upload=true;
         }
        message_display(msg_type, "Approve Production Data", message); Pace.stop();
    });
}

$scope.expinfo=function(type,id,file_name)
   {
                    var data = $.param({ callvalue: "excelinfo","type":type,"id":id,"sheet_name":file_name});
                    $http.post('/krg/myphp/krcepl/daily.php',data,config).then(function (response) 
                    {
                      /*   var table_id='#pdata';
                        if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  } */
                        $scope.pinfo=response.data;
                        /*  setTimeout(function () {
                            $(function () {
                                $(table_id).DataTable({
                                    dom: 'flrtip',
                                    responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                                        ]
                                    }
                                });
                            });
                        }, 0);  */
                    }); 
   }

$scope.view_info=function(id)
{
    $scope.ProductionInfo=true;$scope.Upload=false;
    var index=-1;
    $scope.usheets.some(function (obj, i) {return obj.upload_id ===id ? index = i : false;});
    if(index!=-1)
    {
        $scope.upload_id=$scope.usheets[index].upload_id;
        $scope.sheet_type=$scope.usheets[index].format_type;
        $scope.sheet_name=$scope.usheets[index].file_name;
    }
   
     $scope.expinfo($scope.sheet_type,$scope.upload_id,$scope.sheet_name);
    /*  setTimeout(function () {
        $(function () {
            $scope.Approve_data();
        })
    },3000);  */
}
$scope.InfoUpload=function()
{
    $scope.upload_id='';$scope.sheet_type='';$scope.sheet_name='';$scope.pinfo='';
    $scope.ProductionInfo=false;$scope.Upload=true;
}
$scope.chart_load=function()
{
    setTimeout(function () {}, 0);
    var data1 = $.param({ callvalue: "lastweekgraph"});
    $http.post('/krg/myphp/krcepl/daily.php',data1,config).then(function (response) 
    {
        var SData=JSON.stringify(response.data);
        var jsondata=JSON.parse(SData);
       //console.log(jsondata.production);
        var chart = c3.generate({
            bindto: '#prod_chart',
            data: {
                columns: jsondata.production,
                type: 'bar',labels: true
            },
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories:jsondata.x_label,
                    label: 'Dates'
                },
                y: {
                    label: 'Production'
                }
            }
        });
        var chart = c3.generate({
            bindto: '#break_chart',
            data: {
                columns: jsondata.breakdown,
                type: 'bar',labels: true
            },
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories:jsondata.x_label,
                    label: 'Dates'
                },
                y: {
                    label: 'Available %'
                }
            }
        });
    });
}
$scope.chart_load();



});
function storeTblValues() {
    var TableData = new Array();
    $('#pdata tr').each(function (row, tr) {
        TableData[row] = {
            "date": $(tr).find('td:eq(2)').text(),
            "capacity": $(tr).find('td:eq(3)').text(),
            "mc_brk": $(tr).find('td:eq(4)').text(),
            "mc_brk_per": $(tr).find('td:eq(5)').text(),
            "mc_brk_reason": $(tr).find('td:eq(6)').text(),
            "mc_avl": $(tr).find('td:eq(7)').text(),
            "gd_brk": $(tr).find('td:eq(8)').text(),
            "gd_brk_per": $(tr).find('td:eq(9)').text(),
            "gd_brk_reason": $(tr).find('td:eq(10)').text(),
            "gd_avl": $(tr).find('td:eq(11)').text(),
            "production": $(tr).find('td:eq(12)').text(),
            "plf": $(tr).find('td:eq(13)').text(),
            "generation": $(tr).find('td:eq(14)').text(),
            "ids": $(tr).find('td:eq(1)').text()
        }
    });
    TableData.shift();  // first row will be empty - so remove
    return TableData;
}
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}




