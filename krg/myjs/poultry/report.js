angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });

  var data = $.param({ callvalue: "accounthead_list"});
  $http.post('/krg/myphp/poultry/master.php',data,config).then(function (response) 
  {
      $scope.hlist = response.data;
  });

  var data = $.param({ callvalue: "accountcategory_list"});
  $http.post('/krg/myphp/poultry/master.php',data,config).then(function (response) 
  {
      $scope.hcatlist = response.data;
  });
  $scope.selected=[];
  $scope.option_click=function(id)
  {
        $scope.hlist1=[];
        var count=$scope.hlist.length;
        setTimeout(function () {
            $(function () {
                    var cat_count=$scope.selectedcategory.length;
                    for(x=0;x<count;x++)
                    {
                        var category_id=$scope.hlist[x].category_id;
                        var matched=0;
                        for(y=0;y<cat_count;y++)
                        {   
                            if($scope.selectedcategory[y]==category_id)
                            {
                                matched=1;
                            }
                        }
                        if(matched==1)
                        {
                            $scope.hlist1.push({'head_id':$scope.hlist[x].head_id,'head_name':$scope.hlist[x].head_name,'category_id':$scope.hlist[x].category_id,'category_name':$scope.hlist[x].category_name});
                        }
                    }
                });
            }, 0);
  }
 

    $scope.daysheet=function()
    {
        data = $.param({ callvalue: "daysheet","date":$scope.sdate});
        Pace.stop();
        Pace.bar.render();
        $http.post('/krg/myphp/poultry/reports.php',data,config).then(function (response) 
        {
            if(response.data=="")
            {
               window.open('/krg/download.php','_blank');
            }
            else if(response.data=="No Data"){message_display("danger","Day Sheet","No Data Found");}
            else{message_display("danger","Day Sheet","Error in Creating Document. Please Contact KRG Portal Admin"+response.data);}
            Pace.stop();
        });
    }
    $scope.summary=function(value)
    {
        Pace.stop();
        Pace.bar.render();
        data = $.param({ callvalue: "summaryrv","type":value,"sdate":$scope.sdate,"edate":$scope.edate,"heads":$scope.selected});
        $http.post('/krg/myphp/poultry/reports.php',data,config).then(function (response) 
        {
            if(response.data=="")
            {
               window.open('/krg/download.php','_blank');
            }
            else if(response.data=="No Data"){message_display("danger","Income/Expense Summary","No Data Found");}
            else{message_display("danger","Income/Expense Summary","Error in Creating Document. Please Contact KRG Portal Admin");}
            Pace.stop();
        });
    }
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}