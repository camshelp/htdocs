angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $scope.ac_list=function()
    {
        var data = $.param({ callvalue: "accounthead_list"});
        $http.post('/krg/myphp/poultry/master.php',data,config).then(function (response) 
        {
            var table_id='#achead_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.head_list = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
        });
    }
    $scope.acategory_list=function()
    {
        var data = $.param({ callvalue: "accountcategory_list"});
        $http.post('/krg/myphp/poultry/master.php',data,config).then(function (response) 
        {
            var table_id='#acate_list';
            if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
            $scope.cate_list = response.data;
            setTimeout(function () {
                $(function () {
                    $(table_id).DataTable({
                        dom: 'lfrtip',
                        responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                            { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                            { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                            { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                            { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                            ]
                        }
                    });
                });
            }, 0);
        });
    }
    $scope.ac_list(); $scope.acategory_list();
    $(document).ready(function (e) {
        $("#acregister").on('submit', function (e) {
            e.preventDefault();
            if($scope.cname==undefined){ message_display("danger", "Account Category Registration", "Please Choose the Account Category"); }
            else
            {
                Pace.stop();
                Pace.bar.render();
                var form_data = new FormData(this);
                form_data.append('callvalue', "accounthead_register");
                $.ajax({
                    type: "POST", url: "/krg/myphp/poultry/master.php", datatype: 'json', data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,

                }).then(function (response) {
                    var myResult = JSON.parse(response);
                    var msg_type = "danger";
                    var message = myResult.Message;
                    if (myResult.result == "Success") {$scope.ac_list();  $scope.acname=''; msg_type = "success"; }
                    message_display(msg_type, "Account Head Registration", message);  Pace.stop();
                });
            }
        });
    });
    $(document).ready(function (e) {
        $("#acategory").on('submit', function (e) {
            e.preventDefault();
                Pace.stop();
                Pace.bar.render();
                var form_data = new FormData(this);
                form_data.append('callvalue', "accountcategory_register");
                $.ajax({
                    type: "POST", url: "/krg/myphp/poultry/master.php", datatype: 'json', data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,

                }).then(function (response) {
                    var myResult = JSON.parse(response);
                    var msg_type = "danger";
                    var message = myResult.Message;
                    if (myResult.result == "Success") { $scope.acategory_list();  $scope.accate=''; msg_type = "success"; }
                    message_display(msg_type, "Account Category Registration", message);  Pace.stop();
                });
        });
    });
});

function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}