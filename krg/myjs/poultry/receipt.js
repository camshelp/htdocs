angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache','ngSanitize']).controller('userCtrl', function ($scope,$element,$http, $timeout, $compile) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
   $scope.info_show=true;$scope.entry_show=false;
   $scope.summary_info=function()
   {
    var data = $.param({ callvalue: "receipt_summary"});
    $http.post('/krg/myphp/poultry/receipt.php',data,config).then(function (response) 
    {
        $scope.rdate=response.data.date;
        $scope.rday=response.data.day;

        $scope.total_receipt=response.data.no_receipt;
        $scope.total_voucher=response.data.no_voucher;

        $scope.open_bal=response.data.opening_bal;
        $scope.today_income=response.data.today_receipt;
        $scope.receipt_amount=response.data.total_receipt;

        $scope.voch_amount=response.data.today_voucher;
        $scope.close_balance=response.data.closing_balance;
    });
   }
   $scope.print_data=function(type,value)
   {

        var index = -1;
       
                if(type=="Voucher")
                {
                    $scope.vlist.some(function (obj, i) { return obj.id === value ? index = i : false; });
                    if(index!="-1")
                    {  
                        var ddd=$scope.vlist[index].date.split('-');
                        $scope.prno = $scope.vlist[index].id;
                        $scope.prdate = ddd[2]+"-"+ddd[1]+"-"+ddd[0];
                        $scope.prhead = $scope.vlist[index].head_name;
                        $scope.prname = $scope.vlist[index].name;
                        $scope.prpurp = $scope.vlist[index].purpose;
                        $scope.prword = $scope.vlist[index].amount_word+" Only";
                        $scope.pramount = $scope.vlist[index].amount;
                        
                    }
                }
                else if(type=="Receipt")
                {
                    $scope.rlist.some(function (obj, i) { return obj.id === value ? index = i : false; });
                    if(index!="-1")
                    {  
                        var ddd=$scope.rlist[index].date.split('-');
                        $scope.prno = $scope.rlist[index].id;
                        $scope.prdate = ddd[2]+"-"+ddd[1]+"-"+ddd[0];
                        $scope.prhead = $scope.rlist[index].head_name;
                        $scope.prname = $scope.rlist[index].name;
                        $scope.prpurp = $scope.rlist[index].purpose;
                        $scope.prword = $scope.rlist[index].amount_word+" Only";
                        $scope.pramount = $scope.rlist[index].amount;
                    
                    }
                }
                setTimeout(function () {
                    $(function () {  if(index!="-1"){  $scope.sent_to_print();}   
                }),1000});
         
   }
   $scope.sent_to_print=function()
   {
            var content = document.getElementById('printablediv').innerHTML;
            var mywindow = window.open('', '', 'height=600,width=800');
            mywindow.document.write('<html><head><title></title><style>@media print{a[href]:after{content:none}}</style>');
            mywindow.document.write('</head><body>');
            mywindow.document.write(content);
            mywindow.document.write('</body></html>');
            mywindow.document.URL = "";
            mywindow.location.href = "";
            mywindow.document.close();
            mywindow.focus();
          
            mywindow.print();
            mywindow.close();
            return true; 
   }
   $scope.load_receipt=function(value)
   {
       var data = $.param({ callvalue: "receipt_list",type:value,date:$scope.rdate});
       $http.post('/krg/myphp/poultry/receipt.php',data,config).then(function (response) 
       {
           var table_id='#'+value+"_list";
           if($.fn.dataTable.isDataTable(table_id)){ $(table_id).DataTable().clear().destroy();  }
           if(value=="Receipt"){$scope.rlist=response.data;}else{$scope.vlist=response.data;}
           setTimeout(function () {
               $(function () {
                   $(table_id).DataTable({
                       dom: 'flrtip',
                       responsive: {breakpoints: [{ name: 'bigdesktop', width: Infinity },
                             { name: 'meddesktop', width: 1480 },{ name: 'smalldesktop', width: 1280 },
                             { name: 'medium', width: 1188 }, { name: 'tabletl', width: 1024 },
                             { name: 'btwtabllandp', width: 848 },{ name: 'tabletp', width: 768 },
                             { name: 'mobilel', width: 480 },{ name: 'mobilep', width: 320 }
                           ]
                       }
                   });
               });
           }, 0);
       });
   }
   $scope.summary_info();$scope.load_receipt('Receipt');$scope.load_receipt('Voucher');
   $scope.form_clear=function()
   {
       $scope.rtype='';$scope.rno='';$scope.hname='';$scope.rname='';$scope.rpurp='';$scope.ramount='';$scope.rword='';
   }
    var data = $.param({ callvalue: "accounthead_list"});
    $http.post('/krg/myphp/poultry/master.php',data,config).then(function (response) 
    {
        $scope.head_list = response.data;
    });
    $('#ramount').change( function()
     {  var data = $.param({ callvalue: "numtoword","amount":$scope.ramount});
        $http.post('/krg/myphp/poultry/receipt.php',data,config).then(function (response) 
        {
            $scope.rword = response.data.word; 
        }); 
    });

    $scope.NewEntry=function(value)
    {
        $scope.bvisi=false;
        $scope.info_show=false;$scope.entry_show=true;$scope.form_clear();
        $scope.rtype=value;
        $scope.summary_info();
    }
    $scope.EditInfo=function(type,id)
    {
        $scope.bvisi=false;
        $scope.info_show=false;$scope.entry_show=true;$scope.form_clear();
        $scope.rtype=type;
        var index = -1;
        if(type=="Voucher")
        {
            $scope.vlist.some(function (obj, i) { return obj.id === id ? index = i : false; });
            if(index!="-1")
            {  
                $scope.rno = $scope.vlist[index].id;
                $scope.rdate = $scope.vlist[index].date;
                $scope.rday = $scope.vlist[index].day;
                $scope.hname = $scope.vlist[index].head_id;
                $scope.rname = $scope.vlist[index].name;
                $scope.rpurp = $scope.vlist[index].purpose;
                $scope.rword = $scope.vlist[index].amount_word;
                $scope.ramount = $scope.vlist[index].amount;
                
            }
        }
        else if(type=="Receipt")
        {
            $scope.rlist.some(function (obj, i) { return obj.id === id ? index = i : false; });
            if(index!=-1)
            {  
                $scope.rno = $scope.rlist[index].id;
                $scope.rdate = $scope.rlist[index].date;
                $scope.rday = $scope.rlist[index].day;
                $scope.hname = $scope.rlist[index].head_id;
                $scope.rname = $scope.rlist[index].name;
                $scope.rpurp = $scope.rlist[index].purpose;
                $scope.rword = $scope.rlist[index].amount_word;
                $scope.ramount = $scope.rlist[index].amount;
            }
        }


    }
    $scope.RemoveInfo=function(type,id)
    {
        if (confirm("Are you sure?")) 
        {
            var form_data = new FormData();
            form_data.append("rno",id);
            form_data.append('callvalue', "receipt_remove");
            $.ajax({
                type: "POST", url: "/krg/myphp/poultry/receipt.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { msg_type = "success"; $scope.summary_info();$scope.load_receipt('Receipt');$scope.load_receipt('Voucher'); }
                message_display(msg_type, "Receipt/Voucher Registration", message); 
            });
         }
    }
    $scope.registration_list=function()
    {
        $scope.info_show=true;$scope.entry_show=false;
    }

    $(document).ready(function (e) {
        $("#rvregister").on('submit', function (e) {
            e.preventDefault();
            Pace.stop();
            Pace.bar.render();
            var form_data = new FormData(this);
            form_data.append('callvalue', "receipt_register");
            $.ajax({
                type: "POST", url: "/krg/myphp/poultry/receipt.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false,

            }).then(function (response) {
                var myResult = JSON.parse(response);
                var msg_type = "danger";
                var message = myResult.Message;
                if (myResult.result == "Success") { msg_type = "success"; $scope.rno=myResult.id; $scope.bvisi=true; $scope.summary_info();$scope.load_receipt('Receipt');$scope.load_receipt('Voucher'); }
                message_display(msg_type, "Receipt/Voucher Registration", message);  Pace.stop();
            });
        });
    });
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}