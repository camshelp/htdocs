angular.module('myApp', []).controller('userCtrl', function ($scope,$http) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    var data = $.param({ callvalue: "login_data"});
    $scope.cname="";
    $http.post('/krg/myphp/general/loginindex.php',data,config).then(function (response) 
    {
         $scope.login_data = response.data; 
         $scope.cname=$scope.login_data.company_name;
    
    });

    $scope.loaddash=function()
    {
        setTimeout(function () {
            if($scope.cname=="K.Ramakrishnan Clean Energy Pvt. Ltd.")
            {
               $('#contentFrame').attr("src", "/krg/krcepl/winddash.php",function (response, status, xhr) {
                   $('#contentFrame').unmask("waiting ...");
               });
           }
        }, 1000);
    }
    $scope.loaddash();

     $('.btnCustomers').click(function () {

        $('#contentFrame').mask("waiting ...");
        data = $.param({ callvalue: "page_check","enc_value":this.id});
        $http.post('/krg/myphp/general/loginindex.php',data,config).then(function (response) 
        {
             $('#contentFrame').attr("src",response.data.name ,function (response, status, xhr) {
             $('#contentFrame').unmask("waiting ...");});
         });  
         return false;
           
    }); 

    data = $.param({ callvalue: "unapprove_task"});
    $http.post('/krg/myphp/general/loginindex.php',data,config).then(function (response) { $scope.utask=response.data; $scope.utcount=$scope.utask.length; });
   
    data = $.param({ callvalue: "live_task"});
    $http.post('/krg/myphp/general/loginindex.php',data,config).then(function (response) { $scope.ltask=response.data; $scope.ltcount=$scope.ltask.length; });
   
    $(document).ready(function (e) {
        $("#logoff").click(function (e) {
            form_data=new FormData();
            form_data.append('callvalue',"logoff");
            $.ajax({
                type: "POST", url: "/krg/myphp/general/loginindex.php", datatype: 'json', data: form_data,
                contentType: false,
                cache: false,
                processData: false
            }).then(function (response) {
                var myResult = JSON.parse(response);
                var message = myResult.Message;
                if (myResult.result == "Redirect") { window.location=myResult.Message; }
                else if (myResult.result == "danger") 
                {
                    message_display("danger","KRG Portal - Home",myResult.Message);
                }
            }); 
        });
    });
    $(document).ready(function (e) {
        $("#changepwd").click(function (e) {
            window.location="/krg/general/changepwd.php";
        });
    });
});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}