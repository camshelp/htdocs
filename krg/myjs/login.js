angular.module('myApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']).controller('userCtrl', function ($scope,$http) {
    var config = {headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}}
    $scope.glist=false;$scope.clist='';
    $('#email').change( function()
    {
        $scope.glist=false;
        var data = $.param({ callvalue: "clist_check",mail:$scope.email});
        $http.post('/krg/myphp/general/loginindex.php',data,config).then(function (response) 
        {
            if(response.data.type=="No"){}
            else
            {
                $scope.clist = response.data.result; 
               if($scope.clist.length>0){$scope.glist=true;}
            }
        });
    });

    $(document).ready(function (e) {
        $("#form1").on('submit', function (e) {
            e.preventDefault();
            var check=0;
            if($scope.gname== undefined && $scope.glist==true){message_display("danger", "KRG Portal - Login", "Please Choose Company Name");check=1;}
            if(check==0)
            {
                Pace.stop();
                Pace.bar.render();
                var form_data = new FormData(this);
                form_data.append('callvalue', "login_check");
            $.ajax({
                    type: "POST", url: "/krg/myphp/general/loginindex.php", datatype: 'json', data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false
                }).then(function (response) {
                    var myResult = JSON.parse(response);
                    var msg_type = "danger";
                    var message = myResult.Message;
                    if (myResult.result == "Redirect") { window.location=myResult.Message; }
                    else if (myResult.result == "Success") { msg_type = "success";window.location='/krg/index.php'; }
                    message_display(msg_type, "KRG Portal - Login", message); Pace.stop();
                }); 
            }
        });
    });

});
function message_display(msg_type,title,msg)
{
         $.notify({
        icon: 'glyphicon glyphicon-info-sign', title: '<strong>'+title+'</strong>', message: msg
        }, {
            type: msg_type, allow_dismiss: true, newest_on_top: false, showProgressbar: false, placement: { from: "top", align: "right" }, animate: { enter: 'animated bounceIn', exit: 'animated bounceOut' }
        });
}